package net.ihe.gazelle.simulator.hl7v3.sut.action;

import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfigurationQuery;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.jboss.seam.Component;
import org.jboss.seam.security.Identity;

import java.util.List;

/**
 * Created by aberge on 13/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public final class HL7v3ResponderSUTConfigurationDAO {
    private HL7v3ResponderSUTConfigurationDAO() {

    }

    /**
     * <p>getSUTForTransactionDomain.</p>
     *
     * @param transactionKeyword    a {@link java.lang.String} object.
     * @param affinityDomainKeyword a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public static List<HL7v3ResponderSUTConfiguration> getSUTForTransactionDomain(String transactionKeyword, String affinityDomainKeyword) {
        HL7v3ResponderSUTConfigurationQuery query = new HL7v3ResponderSUTConfigurationQuery();
        query.isAvailable().eq(true);
        query.listUsages().transaction().keyword().eq(transactionKeyword);
        if (affinityDomainKeyword != null) {
            query.listUsages().affinity().keyword().eq(affinityDomainKeyword);
        }
        query.name().order(true);
        if (!Identity.instance().isLoggedIn()) {
            query.isPublic().eq(true);
        } else if (!Identity.instance().hasRole("admin_role")) {

            GazelleIdentity gazelleIdentity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
            String institutionName = gazelleIdentity.getOrganisationKeyword();

            query.addRestriction(HQLRestrictions.or(query.ownerCompany().eqRestriction(institutionName), query.isPublic().eqRestriction(true)));
        }
        return query.getList();
    }

}
