package net.ihe.gazelle.simulator.pix.hl7;

import ca.uhn.hl7v2.AcknowledgmentCode;

/**
 * <p>QueryResponseStatus enum.</p>
 *
 * @author abe
 * @version 1.0: 15/04/19
 */
public enum QueryResponseStatus {
    OK(AcknowledgmentCode.AA, "OK", null),
    NF(AcknowledgmentCode.AA, "NF", null),
    AE_UNKNOWN_ID(AcknowledgmentCode.AE, "AE", "1"),
    AE_UNKNOWN_DOMAIN_TO_RETURN(AcknowledgmentCode.AE, "AE", null),
    AE_UNKNOWN_INCOMING_DOMAIN(AcknowledgmentCode.AE, "AE", "4");

    String qakCode;
    AcknowledgmentCode msaCode;
    String componentNumber;

    private QueryResponseStatus(AcknowledgmentCode msaCode, String qakCode, String componentNumber) {
        this.msaCode = msaCode;
        this.componentNumber = componentNumber;
        this.qakCode = qakCode;
    }

    public AcknowledgmentCode getMSACode() {
        return this.msaCode;
    }

    public String getQAKCode() {
        return this.qakCode;
    }

    public String getComponentNumber() {
        return this.componentNumber;
    }
}