package net.ihe.gazelle.simulator.pam.pes;

/**
 * <b>Class Description : </b>Selection<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 21/09/15
 *


 */
public enum Selection {
    ENCOUNTERS_PRE_ADMIT, //0
    ENCOUNTERS_OPEN, // 1
    PATIENTS, // 2
    MOVEMENTS, // 3
    ENCOUNTERS_OR_PATIENTS; //4

    Selection(){

    }

    public boolean displayEncounters(){
        return (this.equals(ENCOUNTERS_OPEN) || this.equals(ENCOUNTERS_OR_PATIENTS) || this.equals(ENCOUNTERS_PRE_ADMIT));
    }

    public boolean displayPatients(){
        return (this.equals(PATIENTS));
    }

    public boolean displayMovements(){
        return (this.equals(MOVEMENTS));
    }

    public boolean displayPatientButton(){
        return this.equals(ENCOUNTERS_OR_PATIENTS) || this.equals(PATIENTS);
    }
}
