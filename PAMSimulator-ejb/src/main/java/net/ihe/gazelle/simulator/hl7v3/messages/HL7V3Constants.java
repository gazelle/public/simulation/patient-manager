package net.ihe.gazelle.simulator.hl7v3.messages;

/**
 * Created by aberge on 08/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class HL7V3Constants {

    /** Constant <code>PRPA_TE_201305_UV_02="PRPA_TE201305UV02"</code> */
    public static final String PRPA_TE_201305_UV_02 = "PRPA_TE201305UV02";
    /** Constant <code>XCPD_INITGW_HOME_COMMUNITY_ID="hl7v3_xcpd_initgw_home_community_id"</code> */
    public static final String XCPD_INITGW_HOME_COMMUNITY_ID = "hl7v3_xcpd_initgw_home_community_id";
    /** Constant <code>XCPD_INIT_DEVICE_ID="hl7v3_xcpd_initgw_device_id"</code> */
    public static final String XCPD_INIT_DEVICE_ID = "hl7v3_xcpd_initgw_device_id";
    /** Constant <code>PRPA_IN_201305_UV_02="PRPA_IN201305UV02"</code> */
    public static final String PRPA_IN_201305_UV_02 = "PRPA_IN201305UV02";
    /** Constant <code>XCPD_INITGW_URL="hl7v3_xcpd_initgw_url"</code> */
    public static final String XCPD_INITGW_URL = "hl7v3_xcpd_initgw_url";
    /** Constant <code>PDQ_PDC_DEVICE_ID="hl7v3_pdq_pdc_device_id"</code> */
    public static final String PDQ_PDC_DEVICE_ID = "hl7v3_pdq_pdc_device_id";
    /** Constant <code>QUQI_IN_000003_UV_01="QUQI_IN000003UV01"</code> */
    public static final String QUQI_IN_000003_UV_01 = "QUQI_IN000003UV01";
    /** Constant <code>ACCEPT_ACK_CODE="AA"</code> */
    public static final String ACCEPT_ACK_CODE = "AA";
    /** Constant <code>PRPA_TE_000003_UV_01="PRPA_TE000003UV01"</code> */
    public static final String PRPA_TE_000003_UV_01 = "PRPA_TE000003UV01";
    /** Constant <code>NEW="new"</code> */
    public static final String NEW = "new";
    /** Constant <code>REAL_TIME="R"</code> */
    public static final String REAL_TIME = "R";
    /** Constant <code>DEFAULT_PROCESSING_MODE="T"</code> */
    public static final String DEFAULT_PROCESSING_MODE = "T";
    /** Constant <code>DEFAULT_ACK_CODE="AL"</code> */
    public static final String DEFAULT_ACK_CODE = "AL";
    /** Constant <code>DEFFERED="D"</code> */
    public static final String DEFFERED = "D";
    /** Constant <code>IMMEDIATE="I"</code> */
    public static final String IMMEDIATE = "I";
    /** Constant <code>LIVING_SUBJECT_ADMINISTRATIVE_GENDER="LivingSubject.administrativeGender"</code> */
    public static final String LIVING_SUBJECT_ADMINISTRATIVE_GENDER = "LivingSubject.administrativeGender";
    /** Constant <code>LIVING_SUBJECT_BIRTH_TIME="LivingSubject.birthTime"</code> */
    public static final String LIVING_SUBJECT_BIRTH_TIME = "LivingSubject.birthTime";
    /** Constant <code>CX_1_PATH="CX-1"</code> */
    public static final String CX_1_PATH = "CX-1";
    /** Constant <code>LIVING_SUBJECT_ID="LivingSubject.id"</code> */
    public static final String LIVING_SUBJECT_ID = "LivingSubject.id";
    /** Constant <code>LIVING_SUBJECT_NAME="LivingSubject.name"</code> */
    public static final String LIVING_SUBJECT_NAME = "LivingSubject.name";
    /** Constant <code>PERSON_MOTHERS_MAIDEN_NAME="Person.MothersMaidenName"</code> */
    public static final String PERSON_MOTHERS_MAIDEN_NAME = "Person.MothersMaidenName";
    /** Constant <code>LIVING_SUBJECT_BIRTH_PLACE="LivingSubject.birthPlace"</code> */
    public static final String LIVING_SUBJECT_BIRTH_PLACE = "LivingSubject.birthPlace";
    /** Constant <code>PATIENT_ADDR="Patient.addr"</code> */
    public static final String PATIENT_ADDR = "Patient.addr";
    /** Constant <code>PATIENT_TELECOM="Patient.telecom"</code> */
    public static final String PATIENT_TELECOM = "Patient.telecom";
    /** Constant <code>ASSIGNED_PROVIDER_ID="AssignedProvider.id"</code> */
    public static final String ASSIGNED_PROVIDER_ID = "AssignedProvider.id";
    /** Constant <code>OTHER_IDS_SCOPING_ORGANIZATION_ID="OtherIDs.scopingOrganization.id"</code> */
    public static final String OTHER_IDS_SCOPING_ORGANIZATION_ID = "OtherIDs.scopingOrganization.id";
    /** Constant <code>CX_4_2_PATH="CX-4-2"</code> */
    public static final String CX_4_2_PATH = "CX-4-2";
    /** Constant <code>INTERACTION_ID="2.16.840.1.113883.1.6"</code> */
    public static final String INTERACTION_ID = "2.16.840.1.113883.1.6";
    /** Constant <code>STATUS_CONTINUE="waitContinuedQueryResponse"</code> */
    public static final String STATUS_CONTINUE = "waitContinuedQueryResponse";
    /** Constant <code>STATUS_CANCEL="aborted"</code> */
    public static final String STATUS_CANCEL = "aborted";
    /** Constant <code>INIT_GW="INIT_GW"</code> */
    public static final String INIT_GW = "INIT_GW";
    /** Constant <code>PDC="PDC"</code> */
    public static final String PDC = "PDC";
    /** Constant <code>BR="BR"</code> */
    public static final String BR = "BR";
    /** Constant <code>PERSON_CLASS_CODE="PRS"</code> */
    public static final String PERSON_CLASS_CODE = "PRS";
}
