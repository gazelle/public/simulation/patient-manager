package net.ihe.gazelle.simulator.pam.action;

public final class SendindException extends RuntimeException {
    public SendindException(String comment) {
        super(comment);
    }

    public SendindException(String comment, Exception cause) {
        super(comment, cause);
    }
}
