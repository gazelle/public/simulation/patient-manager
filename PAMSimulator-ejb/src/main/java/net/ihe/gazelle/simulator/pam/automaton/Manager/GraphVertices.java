package net.ihe.gazelle.simulator.pam.automaton.Manager;

/**
 * Created by xfs on 09/11/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public enum GraphVertices {

    No_Encounter("NO_ENCOUNTER"),
    Outpatient("OUTPATIENT"),
    Inpatient("INPATIENT"),
    Preadmit("PREADMIT"),
    Preadmit_R("PREADMIT R"),
    Preadmit_I("PREADMIT I"),
    Preadmit_O("PREADMIT O"),
    Temporary_Leave("TEMPORARY_LEAVE"),
    Emergency("EMERGENCY");


    /**
     * <p>Constructor for GraphVertices.</p>
     *
     * @param name a {@link java.lang.String} object.
     */
    GraphVertices(String name) {
        this.name = name;
    }

    String name;

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        return this.name;
    }


}
