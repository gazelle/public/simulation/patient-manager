package net.ihe.gazelle.simulator.pam.automaton.model.graph;

import java.util.List;

/**
 * <p>GraphDescriptionDAO class.</p>
 *
 * @author aberge
 * @version 1.0: 04/10/17
 */

public class GraphDescriptionDAO {

    public static GraphDescription getGraphDescriptionById(Integer graphmlId){
        GraphDescriptionQuery query = new GraphDescriptionQuery();
        query.active().eq(true);
        query.id().eq(graphmlId);

        return query.getUniqueResult();
    }

    public static List<GraphDescription> getActiveGraphDescriptions(){
        GraphDescriptionQuery query = new GraphDescriptionQuery();
        query.active().eq(true);
        return query.getList();
    }
}
