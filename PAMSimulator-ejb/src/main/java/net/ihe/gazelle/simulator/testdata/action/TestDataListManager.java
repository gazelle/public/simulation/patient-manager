package net.ihe.gazelle.simulator.testdata.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.testdata.dao.TestDataDAO;
import net.ihe.gazelle.simulator.testdata.model.PatientIdentifierTestData;
import net.ihe.gazelle.simulator.testdata.model.PatientTestData;
import net.ihe.gazelle.simulator.testdata.model.TestData;
import net.ihe.gazelle.simulator.testdata.model.TestDataQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;

/**
 * Created by aberge on 13/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("testDataListManager")
@Scope(ScopeType.PAGE)
public class TestDataListManager implements Serializable {

    private Filter<TestData> filter;
    private TestData selectedTestData;

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<TestData> getFilter() {
        if (this.filter == null) {
            this.filter = new Filter<TestData>(getHQLCriteriaForFilter());
        }
        return filter;
    }

    private HQLCriterionsForFilter<TestData> getHQLCriteriaForFilter() {
        TestDataQuery query = new TestDataQuery();
        HQLCriterionsForFilter<TestData> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("transaction", query.applyToTransaction());
        criteria.addPath("modifier", query.lastModifier());
        criteria.addPath("shared", query.shared());
        return criteria;
    }

    /**
     * <p>getListFiltered.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<TestData> getListFiltered() {
        return new FilterDataModel<TestData>(getFilter()) {
            @Override
            protected Object getId(TestData testData) {
                return testData.getId();
            }
        };
    }

    /**
     * <p>clean.</p>
     */
    public void clean() {
        getFilter().clear();
    }

    /**
     * <p>getTestDataIdentifier.</p>
     *
     * @param inTestData a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     * @return a {@link java.lang.String} object.
     */
    public String getTestDataIdentifier(TestData inTestData) {
        if (inTestData instanceof PatientTestData) {
            PatientTestData patientData = (PatientTestData) inTestData;
            return "Patient #" + patientData.getPatient().getId();
        } else if (inTestData instanceof PatientIdentifierTestData) {
            PatientIdentifierTestData patientIdData = (PatientIdentifierTestData) inTestData;
            return "Patient identifier:" + patientIdData.getPatientIdentifier().getFullPatientId();
        } else {
            return "unset";
        }
    }

    /**
     * <p>getLinkToDetailedTestData.</p>
     *
     * @param inTestData a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     * @return a {@link java.lang.String} object.
     */
    public String getLinkToDetailedTestData(TestData inTestData) {
        if (inTestData instanceof PatientTestData) {
            PatientTestData patientData = (PatientTestData) inTestData;
            return "/patient.seam?id=" + patientData.getPatient().getId();
        } else {
            return null;
        }
    }

    /**
     * <p>deleteTestData.</p>
     */
    public void deleteTestData(){
        TestDataDAO.deleteEntity(selectedTestData);
        selectedTestData = null;




    }

    /**
     * <p>createNewTestDataForPDQv3.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String createNewTestDataForPDQv3() {
        return "/PatientManager/hl7v3/pdq/consumer.seam";
    }

    /**
     * <p>createNewTestDataForPDQ.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String createNewTestDataForPDQ() {
        return "/PatientManager/pdc/PDQPDC.seam";
    }

    /**
     * <p>createNewTestDataForXCPD.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String createNewTestDataForXCPD() {
        return "/PatientManager/hl7v3/xcpd/initGw.seam";
    }

    /**
     * <p>createNewTestDataForPIXCons.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String createNewTestDataForPIXCons() {
        return "/PatientManager/pixconsumer/pixQuery.seam";
    }

    /**
     * <p>createNewTestDataForPIXv3Cons.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String createNewTestDataForPIXv3Cons() {
        return "/PatientManager/pixconsumer/pixv3Query.seam";
    }

    /**
     * <p>createNewTestDataForPDQm.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String createNewTestDataForPDQm() {
        return "/PatientManager/hl7fhir/pdqm/consumer.seam";
    }

    /**
     * <p>createNewTestDataForPIXm.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String createNewTestDataForPIXm() {
        return "/PatientManager/hl7fhir/pixm/consumer.seam";
    }

    /**
     * <p>Getter for the field <code>selectedTestData</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     */
    public TestData getSelectedTestData() {
        return selectedTestData;
    }

    /**
     * <p>Setter for the field <code>selectedTestData</code>.</p>
     *
     * @param selectedTestData a {@link net.ihe.gazelle.simulator.testdata.model.TestData} object.
     */
    public void setSelectedTestData(TestData selectedTestData) {
        this.selectedTestData = selectedTestData;
    }
}
