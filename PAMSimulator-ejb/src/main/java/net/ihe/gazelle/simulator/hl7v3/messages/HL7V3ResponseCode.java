package net.ihe.gazelle.simulator.hl7v3.messages;


public enum HL7V3ResponseCode {
    OK("OK", "Application Accept"),
    NF("NF","Not Found"),
    AE("AE","Application Error"),
    QE("QE","");

    private final String value;
    private final String message;

    HL7V3ResponseCode(String value, String message) {
        this.value = value;
        this.message = message;
    }

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

}
