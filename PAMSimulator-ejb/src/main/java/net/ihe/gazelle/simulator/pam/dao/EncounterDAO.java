package net.ihe.gazelle.simulator.pam.dao;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.datamodel.EncounterDataModel;
import net.ihe.gazelle.simulator.pam.iti31.action.NumberGenerator;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.EncounterQuery;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientStatus;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * <p>EncounterDAO class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class EncounterDAO {


    /**
     * <p>getEncountersByAccountNumber.</p>
     *
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Encounter> getEncountersByAccountNumber(Patient patient) {
        EncounterQuery query = new EncounterQuery();
        query.patientAccountNumber().eq(patient.getAccountNumber());
        query.simulatedActor().eq(patient.getSimulatedActor());
        return query.getListNullIfEmpty();
    }

    /**
     * list the open, inpatient encounters for the given patient. if some exists, return true else return false
     *
     * @param selectedPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @return a boolean.
     */
    public static boolean isPatientAlreadyAdmitted(Patient selectedPatient, Actor simulatedActor) {
        EncounterDataModel patientEncounters = new EncounterDataModel(null, selectedPatient, simulatedActor, null,
                null, null, null, null, null, null, false, true);
        if (patientEncounters.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * list the open, inpatient encounters for the given patient. if some exists, return true else return false
     *
     * @param selectedPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return a boolean.
     */
    public static boolean isPatientAlreadyAdmitted(Patient selectedPatient, Actor simulatedActor,
                                                   EntityManager entityManager) {
        HQLQueryBuilder<Encounter> builder = new HQLQueryBuilder<Encounter>(entityManager, Encounter.class);
        builder.addEq("patient", selectedPatient);
        builder.addEq("simulatedActor", simulatedActor);
        builder.addEq("open", true);
        builder.addRestriction(HQLRestrictions.or(HQLRestrictions.neq("movements.triggerEvent", "A01"),
                HQLRestrictions.neq("movements.triggerEvent", "A07")));
        // taking into account the case when the A02 open a new encounter as
        // inpatient
        int count = builder.getCount();
        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * <p>isPatientPreAdmitted.</p>
     *
     * @param selectedPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @return a boolean.
     */
    public static boolean isPatientPreAdmitted(Patient selectedPatient, Actor simulatedActor) {
        HQLQueryBuilder<Encounter> builder = new HQLQueryBuilder<Encounter>(
                EntityManagerService.provideEntityManager(), Encounter.class);
        builder.addEq("patient", selectedPatient);
        builder.addEq("simulatedActor", simulatedActor);
        builder.addEq("open", true);
        builder.addEq("patientStatus", PatientStatus.STATUS_PRE_ADMITTED);
        int count = builder.getCount();
        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * <p>isVisitNumberAlreadyUsed.</p>
     *
     * @param visitNumber a {@link java.lang.String} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return a boolean.
     */
    public static boolean isVisitNumberAlreadyUsed(String visitNumber, Actor simulatedActor, EntityManager entityManager) {
        HQLQueryBuilder<Encounter> builder = new HQLQueryBuilder<Encounter>(entityManager, Encounter.class);
        builder.addEq("visitNumber", visitNumber);
        builder.addEq("simulatedActor", simulatedActor);
        int numberOfEncounters = builder.getCount();
        if (numberOfEncounters == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * <p>getOpenEncounterByActorByNumberByStatus.</p>
     *
     * @param visitNumber a {@link java.lang.String} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @param patientStatus  TODO
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public static Encounter getOpenEncounterByActorByNumberByStatus(String visitNumber, Actor simulatedActor,
                                                                    EntityManager entityManager, PatientStatus patientStatus) {
        EncounterQuery query = new EncounterQuery(entityManager);
        query.visitNumber().eq(visitNumber);
        query.simulatedActor().eq(simulatedActor);
        query.open().eq(true);
        if (patientStatus != null) {
            query.patientStatus().eq(patientStatus);
        }
        return query.getUniqueResult();
    }

    /**
     * <p>getClosedEncounterByActorByNumber.</p>
     *
     * @param visitNumber a {@link java.lang.String} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public static Encounter getClosedEncounterByActorByNumber(String visitNumber, Actor simulatedActor,
                                                              EntityManager entityManager) {
        HQLQueryBuilder<Encounter> builder = new HQLQueryBuilder<Encounter>(entityManager, Encounter.class);
        builder.addEq("visitNumber", visitNumber);
        builder.addEq("simulatedActor", simulatedActor);
        builder.addEq("open", false);
        List<Encounter> encounters = builder.getList();
        if ((encounters != null) && !encounters.isEmpty()) {
            return encounters.get(0);
        } else {
            return null;
        }
    }

    public static void randomlyFillEncounter(Encounter selectedEncounter) {
        selectedEncounter.setVisitNumber(NumberGenerator.generate(DesignatorType.VISIT_NUMBER));
        ValueSetProvider provider = ValueSetProvider.getInstance();
        String concept = provider.getRandomCodeFromValueSet("DOCTOR");
        selectedEncounter.setAttendingDoctorCode(concept);
        concept = provider.getRandomCodeFromValueSet("DOCTOR");
        selectedEncounter.setAdmittingDoctorCode(concept);
        concept = provider.getRandomCodeFromValueSet("DOCTOR");
        selectedEncounter.setReferringDoctorCode(concept);
        concept = provider.getRandomCodeFromValueSet("HL70007");
        if (concept != null) {
            selectedEncounter.setAdmissionType(concept);
        }
        concept = provider.getRandomCodeFromValueSet("HL70004");
        selectedEncounter.setPatientClassCode(concept);
    }
}
