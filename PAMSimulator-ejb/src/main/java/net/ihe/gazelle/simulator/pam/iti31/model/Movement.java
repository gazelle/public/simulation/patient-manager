package net.ihe.gazelle.simulator.pam.iti31.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.utils.SVSConsumer;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.international.LocaleSelector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>Movement class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "movement")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Name("movement")
@Table(name = "pam_movement", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pam_movement_sequence", sequenceName = "pam_movement_id_seq", allocationSize = 1)
public class Movement implements Serializable{

    private static Logger log = LoggerFactory.getLogger(Movement.class);
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 6151475459876629230L;

	@Id
	@GeneratedValue(generator = "pam_movement_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@XmlElement(name = "assignedPatientLocation")
	@Column(name = "assigned_patient_location")
	private String assignedPatientLocation;

	/**
	 * code from SVS to identify the bed (PL-3)
	 */
	@Transient
	private String bedCode;

    @Transient
    private String bedStatus;

	/**
	 * who created it, if logged at creation time
	 */
	@Column(name = "creator")
	private String creator;

	/**
	 * indicates whether the movement is the current one or a movement in the past
	 */
	@Column(name = "is_current_movement")
	private Boolean currentMovement;

	@ManyToOne(cascade = {CascadeType.MERGE})
	@JoinColumn(name = "encounter_id")
	private Encounter encounter;

	/**
	 * code from SVS to identify the facility (used in PL-4)
	 */
	@Transient
	private String facilityCode;

	@Column(name = "is_patient_deceased")
	private boolean isPatientDeceased;

	/**
	 * date of the creation of the entity in the database
	 */
	@Column(name = "last_changed")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastChanged;

	/**
	 * movement start date/time : ZBE-2
	 */
	@Column(name = "movement_date")
	private Date movementDate;

	/**
	 * ZBE-1: uniquely identify the movement
	 */
	@Column(name = "movement_unique_id")
	private String movementUniqueId;

	@Column(name = "patient_class_code")
	private String patientClassCode;

	/**
	 * indicates whether the movement is pending or is a movement in the past
	 */
	@Column(name = "is_pending_movement")
	private Boolean pendingMovement;

	@Column(name = "pending_patient_location")
	private String pendingPatientLocation;

	/**
	 * point of care (PL-1)
	 */
	@Transient
	private String pointOfCareCode;

	@Column(name = "prior_patient_location")
	private String priorPatientLocation;

	@Column(name = "prior_temporary_location")
	private String priorTemporaryLocation;

	/**
	 * code from SVS to identify the room (PL-2)
	 */
	@Transient
	private String roomCode;

	/**
	 * indicates whether the movement has been received by PEC or created by PES
	 */
	@ManyToOne
	@JoinColumn(name = "simulated_actor_id")
	private Actor simulatedActor;

	/**
	 * INSERT, CANCEL or UPDATE
	 */
	@Column(name = "action_type")
	private ActionType actionType;

	@Column(name = "temporary_patient_location")
	private String temporaryPatientLocation;

	@Column(name = "transfer_planned_date")
	@Temporal(TemporalType.DATE)
	private Date transferPlannedDate;

	/**
	 * ZBE-6: original trigger event
	 */
	@Column(name = "trigger_event")
	private String triggerEvent;

	/**
	 * ZBE-7 code from SVS to identify the ward
	 */
	@Column(name = "ward_code")
	private String wardCode;

	/**
	 * ZBE-8 code from SVS to identify the ward
	 */
    @Column(name = "nursing_care_ward")
    private String nursingCareWard;

	/**
	 * ZBE-9: nature of movement - code from SVS
	 * CP-ITI-749: field adopted by IHE Intl with name "scope of movement"
	 */
	@Column(name = "movement_nature")
    private String natureOfMovement;


	/**
	 * <p>Constructor for Movement.</p>
	 */
	public Movement() {

	}

	/**
	 * <p>Constructor for Movement.</p>
	 *
	 * @param inEncounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
	 */
	public Movement(Encounter inEncounter) {
		this.encounter = inEncounter;
	}

    /**
     * <p>Getter for the field <code>bedStatus</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getBedStatus() {
        return bedStatus;
    }

    /**
     * <p>Setter for the field <code>bedStatus</code>.</p>
     *
     * @param bedStatus a {@link java.lang.String} object.
     */
    public void setBedStatus(String bedStatus) {
        this.bedStatus = bedStatus;
    }

    /**
     * <p>Getter for the field <code>nursingCareWard</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNursingCareWard() {
        return nursingCareWard;
    }

    /**
     * <p>Setter for the field <code>nursingCareWard</code>.</p>
     *
     * @param nursingCareWard a {@link java.lang.String} object.
     */
    public void setNursingCareWard(String nursingCareWard) {
        this.nursingCareWard = nursingCareWard;
    }

    /**
     * <p>Getter for the field <code>natureOfMovement</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNatureOfMovement() {
        return natureOfMovement;
    }

    /**
     * <p>Setter for the field <code>natureOfMovement</code>.</p>
     *
     * @param natureOfMovement a {@link java.lang.String} object.
     */
    public void setNatureOfMovement(String natureOfMovement) {
        this.natureOfMovement = natureOfMovement;
    }

	/**
	 * <p>getMovementByUniqueIdByActor.</p>
	 *
	 * @param uniqueMovementId a {@link java.lang.String} object.
	 * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return Movement from DB
	 */
	public static Movement getMovementByUniqueIdByActor(String uniqueMovementId, Actor simulatedActor,
			EntityManager entityManager) {
		MovementQuery query = new MovementQuery(new HQLQueryBuilder<Movement>(entityManager, Movement.class));
		query.movementUniqueId().eq(uniqueMovementId);
		query.simulatedActor().eq(simulatedActor);
		List<Movement> abstractMovements = query.getListNullIfEmpty();
		if (abstractMovements != null) {
			return abstractMovements.get(0);
		} else {
			return null;
		}
	}

	/**
	 * <p>getMovementsFiltered.</p>
	 *
	 * @param encounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
	 * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @param status a {@link net.ihe.gazelle.simulator.pam.iti31.model.ActionType} object.
	 * @param movementUniqueId a {@link java.lang.String} object.
	 * @param triggerEvent a {@link java.lang.String} object.
	 * @param isCurrentMovement a {@link java.lang.Boolean} object.
	 * @param isPendingMovement a {@link java.lang.Boolean} object.
	 * @param creator a {@link java.lang.String} object.
	 * @param startDate a {@link java.util.Date} object.
	 * @param endDate a {@link java.util.Date} object.
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link java.util.List} object.
	 */
	public static List<Movement> getMovementsFiltered(Encounter encounter, Actor simulatedActor, ActionType status,
			String movementUniqueId, String triggerEvent, Boolean isCurrentMovement, Boolean isPendingMovement,
			String creator, Date startDate, Date endDate, EntityManager entityManager) {
		MovementQuery query = new MovementQuery(new HQLQueryBuilder<Movement>(entityManager, Movement.class));
		if (encounter != null) {
			query.encounter().eq(encounter);
		}
		if (simulatedActor != null) {
			query.simulatedActor().eq(simulatedActor);
		}
		if (status != null) {
			query.actionType().eq(status);
		}
		if ((movementUniqueId != null) && !movementUniqueId.isEmpty()) {
			query.movementUniqueId().like(movementUniqueId, HQLRestrictionLikeMatchMode.EXACT);
		}
		if (isCurrentMovement != null) {
			query.currentMovement().eq(isCurrentMovement);
		}
		if (isPendingMovement != null) {
			query.pendingMovement().eq(isPendingMovement);
		}
		if ((triggerEvent != null) && !triggerEvent.isEmpty()) {
			query.triggerEvent().eq(triggerEvent);
		}
		if ((creator != null) && !creator.isEmpty()) {
			query.creator().eq(creator);
		}
		if (endDate != null) {
			query.lastChanged().le(endDate);
		}
		if (startDate != null) {
			query.lastChanged().ge(startDate);
		}
		List<Movement> movements = query.getListNullIfEmpty();
		if (movements != null) {
			return movements;
		} else {
			return null;
		}
	}

	/**
	 * <p>buildLocation.</p>
	 *
	 * @return the location formatted as PL datatype
	 */
	public String buildLocation() {
		StringBuilder location = new StringBuilder();
		if (pointOfCareCode != null) {
			location.append(pointOfCareCode);
		}
		location.append("^");
		if (roomCode != null) {
			location.append(roomCode);
		}
		location.append("^");
		if (bedCode != null) {
			location.append(bedCode);
		}
		location.append("^");
		if (facilityCode != null) {
			location.append(facilityCode);
		}
        if (bedStatus != null){
            location.append("^");
            location.append(bedStatus);
        }
		return location.toString();
	}

	/**
	 * <p>Getter for the field <code>assignedPatientLocation</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getAssignedPatientLocation() {
		return assignedPatientLocation;
	}

	/**
	 * <p>Getter for the field <code>bedCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getBedCode() {
		return bedCode;
	}

	/**
	 * <p>getConceptList.</p>
	 *
	 * @param valueSet a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public List<SelectItem> getConceptList(String valueSet) {
		String valueSetId = ApplicationConfiguration.getValueOfVariable(valueSet);
		List<Concept> concepts = SVSConsumer.getConceptsListFromValueSet(valueSetId, LocaleSelector.instance()
				.getLanguage());
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(null, "Please select ..."));
		if (concepts != null) {
			for (Concept concept : concepts) {
				items.add(new SelectItem(concept.getCode(), concept.getDisplayName()));
			}
		}
		return items;
	}

	/**
	 * <p>Getter for the field <code>creator</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * <p>Getter for the field <code>currentMovement</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getCurrentMovement() {
		return currentMovement;
	}

	/**
	 * <p>Getter for the field <code>currentMovement</code>.</p>
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return the current movement for the encounter linked to the movement
	 */
	public Movement getCurrentMovement(EntityManager entityManager) {
		MovementQuery query = new MovementQuery(new HQLQueryBuilder<Movement>(entityManager, Movement.class));
		query.currentMovement().eq(true);
		query.encounter().eq(this.getEncounter());
		List<Movement> movements = query.getListNullIfEmpty();
		if (movements != null) {
			return movements.get(0);
		} else {
			return null;
		}
	}

	/**
	 * <p>Getter for the field <code>encounter</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
	 */
	public Encounter getEncounter() {
		return encounter;
	}

	/**
	 * <p>Getter for the field <code>facilityCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFacilityCode() {
		return facilityCode;
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Getter for the field <code>lastChanged</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getLastChanged() {
		return lastChanged;
	}

	/**
	 * <p>Getter for the field <code>movementDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getMovementDate() {
		return movementDate;
	}

	/**
	 * <p>Getter for the field <code>movementUniqueId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMovementUniqueId() {
		return movementUniqueId;
	}

	/**
	 * <p>Getter for the field <code>patientClassCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPatientClassCode() {
		return patientClassCode;
	}

	/**
	 * <p>Getter for the field <code>pendingMovement</code>.</p>
	 *
	 * @return a {@link java.lang.Boolean} object.
	 */
	public Boolean getPendingMovement() {
		return pendingMovement;
	}

	/**
	 * <p>Getter for the field <code>pendingMovement</code>.</p>
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @param pendingTrigger a {@link java.lang.String} object.
	 * @return the pending movement for the encounter linked to the movement
	 */
	public Movement getPendingMovement(EntityManager entityManager, String pendingTrigger) {
		MovementQuery query = new MovementQuery(new HQLQueryBuilder<Movement>(entityManager, Movement.class));
		query.pendingMovement().eq(true);
		query.triggerEvent().eq(pendingTrigger);
		query.encounter().eq(this.getEncounter());
		List<Movement> movements = query.getListNullIfEmpty();
		if (movements != null) {
			return movements.get(0);
		} else {
			return null;
		}
	}

	/**
	 * <p>Getter for the field <code>pendingPatientLocation</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPendingPatientLocation() {
		return pendingPatientLocation;
	}

	/**
	 * <p>Getter for the field <code>pointOfCareCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPointOfCareCode() {
		return pointOfCareCode;
	}

	/**
	 * <p>getPreviousValidMovement.</p>
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @param isCurrent a boolean.
	 * @return the previous movement
	 */
	public Movement getPreviousValidMovement(EntityManager entityManager, boolean isCurrent) {
		MovementQuery query = new MovementQuery(new HQLQueryBuilder<Movement>(entityManager, Movement.class));
		query.actionType().neq(ActionType.CANCEL);
		query.encounter().eq(this.getEncounter());
		query.pendingMovement().eq(false);
		query.currentMovement().eq(isCurrent);
		query.movementDate().order(false);
		List<Movement> movements = query.getListNullIfEmpty();
		if (movements != null) {
			return movements.get(0);
		} else {
			return null;
		}
	}

	/**
	 * <p>Getter for the field <code>priorPatientLocation</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPriorPatientLocation() {
		return priorPatientLocation;
	}

	/**
	 * <p>Getter for the field <code>priorTemporaryLocation</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPriorTemporaryLocation() {
		return priorTemporaryLocation;
	}

	/**
	 * <p>Getter for the field <code>roomCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getRoomCode() {
		return roomCode;
	}

	/**
	 * <p>Getter for the field <code>simulatedActor</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public Actor getSimulatedActor() {
		return simulatedActor;
	}

	/**
	 * <p>Getter for the field <code>actionType</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.ActionType} object.
	 */
	public ActionType getActionType() {
		return actionType;
	}

	/**
	 * <p>Getter for the field <code>temporaryPatientLocation</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTemporaryPatientLocation() {
		return temporaryPatientLocation;
	}

	/**
	 * <p>Getter for the field <code>transferPlannedDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getTransferPlannedDate() {
		return transferPlannedDate;
	}

	/**
	 * <p>Getter for the field <code>triggerEvent</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTriggerEvent() {
		return triggerEvent;
	}

	/**
	 * <p>Getter for the field <code>wardCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getWardCode() {
		return wardCode;
	}

	/**
	 * <p>isPatientDeceased.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isPatientDeceased() {
		return isPatientDeceased;
	}

	/**
	 * <p>save.</p>
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
	 */
	public Movement save(EntityManager entityManager) {
		log.info("currentMovement: " + this.currentMovement);
		Movement movement = entityManager.merge(this);
		entityManager.flush();
		return movement;
	}

	/**
	 * <p>Setter for the field <code>assignedPatientLocation</code>.</p>
	 *
	 * @param assignedPatientLocation a {@link java.lang.String} object.
	 */
	public void setAssignedPatientLocation(String assignedPatientLocation) {
		this.assignedPatientLocation = assignedPatientLocation;
	}

	/**
	 * <p>Setter for the field <code>bedCode</code>.</p>
	 *
	 * @param bed a {@link java.lang.String} object.
	 */
	public void setBedCode(String bed) {
		this.bedCode = bed;
	}

	/**
	 * <p>Setter for the field <code>creator</code>.</p>
	 *
	 * @param creator a {@link java.lang.String} object.
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * <p>Setter for the field <code>currentMovement</code>.</p>
	 *
	 * @param currentMovement a {@link java.lang.Boolean} object.
	 */
	public void setCurrentMovement(Boolean currentMovement) {
		this.currentMovement = currentMovement;
	}

	/**
	 * <p>Setter for the field <code>encounter</code>.</p>
	 *
	 * @param encounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
	 */
	public void setEncounter(Encounter encounter) {
		this.encounter = encounter;
	}

	/**
	 * <p>Setter for the field <code>facilityCode</code>.</p>
	 *
	 * @param facility a {@link java.lang.String} object.
	 */
	public void setFacilityCode(String facility) {
		this.facilityCode = facility;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Setter for the field <code>lastChanged</code>.</p>
	 *
	 * @param lastChanged a {@link java.util.Date} object.
	 */
	public void setLastChanged(Date lastChanged) {
		if (lastChanged != null) {
			this.lastChanged = (Date) lastChanged.clone();
		} else {
			this.lastChanged = null;
		}
	}

	/**
	 * <p>Setter for the field <code>movementDate</code>.</p>
	 *
	 * @param movementDate a {@link java.util.Date} object.
	 */
	public void setMovementDate(Date movementDate) {
		if (movementDate != null) {
			this.movementDate = (Date) movementDate.clone();
		} else {
			this.movementDate = null;
		}
	}

	/**
	 * <p>Setter for the field <code>movementUniqueId</code>.</p>
	 *
	 * @param movementUniqueId a {@link java.lang.String} object.
	 */
	public void setMovementUniqueId(String movementUniqueId) {
		this.movementUniqueId = movementUniqueId;
	}

	/**
	 * <p>Setter for the field <code>patientClassCode</code>.</p>
	 *
	 * @param patientClassCode a {@link java.lang.String} object.
	 */
	public void setPatientClassCode(String patientClassCode) {
		this.patientClassCode = patientClassCode;
	}

	/**
	 * <p>setPatientDeceased.</p>
	 *
	 * @param isPatientDeceased a boolean.
	 */
	public void setPatientDeceased(boolean isPatientDeceased) {
		this.isPatientDeceased = isPatientDeceased;
	}

	/**
	 * <p>Setter for the field <code>pendingMovement</code>.</p>
	 *
	 * @param pendingMovement a {@link java.lang.Boolean} object.
	 */
	public void setPendingMovement(Boolean pendingMovement) {
		this.pendingMovement = pendingMovement;
	}

	/**
	 * <p>Setter for the field <code>pendingPatientLocation</code>.</p>
	 *
	 * @param pendingPatientLocation a {@link java.lang.String} object.
	 */
	public void setPendingPatientLocation(String pendingPatientLocation) {
		this.pendingPatientLocation = pendingPatientLocation;
	}

	/**
	 * <p>Setter for the field <code>pointOfCareCode</code>.</p>
	 *
	 * @param pointOfCare a {@link java.lang.String} object.
	 */
	public void setPointOfCareCode(String pointOfCare) {
		this.pointOfCareCode = pointOfCare;
	}

	/**
	 * <p>Setter for the field <code>priorPatientLocation</code>.</p>
	 *
	 * @param priorPatientLocation a {@link java.lang.String} object.
	 */
	public void setPriorPatientLocation(String priorPatientLocation) {
		this.priorPatientLocation = priorPatientLocation;
	}

	/**
	 * <p>Setter for the field <code>priorTemporaryLocation</code>.</p>
	 *
	 * @param priorTemporaryLocation a {@link java.lang.String} object.
	 */
	public void setPriorTemporaryLocation(String priorTemporaryLocation) {
		this.priorTemporaryLocation = priorTemporaryLocation;
	}

	/**
	 * <p>Setter for the field <code>roomCode</code>.</p>
	 *
	 * @param room a {@link java.lang.String} object.
	 */
	public void setRoomCode(String room) {
		this.roomCode = room;
	}

	/**
	 * <p>Setter for the field <code>simulatedActor</code>.</p>
	 *
	 * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public void setSimulatedActor(Actor simulatedActor) {
		this.simulatedActor = simulatedActor;
	}

	/**
	 * <p>Setter for the field <code>actionType</code>.</p>
	 *
	 * @param status a {@link net.ihe.gazelle.simulator.pam.iti31.model.ActionType} object.
	 */
	public void setActionType(ActionType status) {
		this.actionType = status;
	}

	/**
	 * <p>Setter for the field <code>temporaryPatientLocation</code>.</p>
	 *
	 * @param temporaryPatientLocation a {@link java.lang.String} object.
	 */
	public void setTemporaryPatientLocation(String temporaryPatientLocation) {
		this.temporaryPatientLocation = temporaryPatientLocation;
	}

	/**
	 * <p>Setter for the field <code>transferPlannedDate</code>.</p>
	 *
	 * @param transferPlannedDate a {@link java.util.Date} object.
	 */
	public void setTransferPlannedDate(Date transferPlannedDate) {
		if (transferPlannedDate != null) {
			this.transferPlannedDate = (Date) transferPlannedDate.clone();
		} else {
			this.transferPlannedDate = null;
		}
	}

	/**
	 * <p>Setter for the field <code>triggerEvent</code>.</p>
	 *
	 * @param triggerEvent a {@link java.lang.String} object.
	 */
	public void setTriggerEvent(String triggerEvent) {
		this.triggerEvent = triggerEvent;
	}

	/**
	 * <p>Setter for the field <code>wardCode</code>.</p>
	 *
	 * @param ward a {@link java.lang.String} object.
	 */
	public void setWardCode(String ward) {
		this.wardCode = ward;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
	    return HibernateHelper.getLazyEquals(this, obj);
	}
}
