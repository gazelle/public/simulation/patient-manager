package net.ihe.gazelle.simulator.pam.action;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.patient.PatientImporter;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.iti31.action.NumberGenerator;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>PAMPatientImporter class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pamPatientImporter")
@Scope(ScopeType.PAGE)
public class PAMPatientImporter extends PatientImporter<Patient>{

	/**
	 *
	 */
	private static final long serialVersionUID = -3941108161987650306L;

	private Actor simulatedActor;
	private boolean assignIdentifier = true;
	private boolean computeDmetaphone = false;

	private static Logger log = LoggerFactory.getLogger(PAMPatientImporter.class);

	/** {@inheritDoc} */
	@Override
	public Class<Patient> getPatientClass() {
		return Patient.class;
	}

	/** {@inheritDoc} */
	@Override
	public void savePatient(Patient patient) {
		patient.setSimulatedActor(simulatedActor);
		patient.setCreator(Identity.instance().getCredentials().getUsername());
		patient.setStillActive(true);
		patient.setCreationDate(new Date());
		if (assignIdentifier){
			String id = NumberGenerator.generate(DesignatorType.PATIENT_ID);
			PatientIdentifier pid = new PatientIdentifier(id, "PI");
			pid = pid.save();
			patient.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
			patient.getPatientIdentifiers().add(pid);
		}
		List<PatientIdentifier> identifiers = PatientIdentifierDAO.createListFromNationalAndDDSIdentifiers(patient.getNationalPatientIdentifier(), patient.getDdsIdentifier());
		if (identifiers != null && !identifiers.isEmpty()){
			if (patient.getPatientIdentifiers() == null){
				patient.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
			}
			for (PatientIdentifier pid : identifiers){
				patient.getPatientIdentifiers().add(pid.save());
			}
		}
		if (patient.getMultipleBirthIndicatorAsString() != null){
			if (patient.getMultipleBirthIndicatorAsString().equals("Y")){
				patient.setMultipleBirthIndicator(true);
			} else {
				patient.setMultipleBirthIndicator(false);
			}
		}
		if (patient.getBirthOrderAsString() != null && StringUtils.isNumeric(patient.getBirthOrderAsString())){
			patient.setBirthOrder(Integer.decode(patient.getBirthOrderAsString()));
		}else{
			patient.setBirthOrder(null);
		}
		patient = patient.savePatient(EntityManagerService.provideEntityManager());
		if (computeDmetaphone){
			patient.computeDoubleMetaphones(EntityManagerService.provideEntityManager());
		}
	}

	/**
	 * <p>Getter for the field <code>simulatedActor</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public Actor getSimulatedActor() {
		return simulatedActor;
	}

	/**
	 * <p>Setter for the field <code>simulatedActor</code>.</p>
	 *
	 * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public void setSimulatedActor(Actor simulatedActor) {
		this.simulatedActor = simulatedActor;
	}

	/**
	 * <p>isAssignIdentifier.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isAssignIdentifier() {
		return assignIdentifier;
	}

	/**
	 * <p>Setter for the field <code>assignIdentifier</code>.</p>
	 *
	 * @param assignIdentifiers a boolean.
	 */
	public void setAssignIdentifier(boolean assignIdentifiers) {
		this.assignIdentifier = assignIdentifiers;
	}

	/**
	 * <p>isComputeDmetaphone.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isComputeDmetaphone() {
		return computeDmetaphone;
	}

	/**
	 * <p>Setter for the field <code>computeDmetaphone</code>.</p>
	 *
	 * @param computeDmetaphone a boolean.
	 */
	public void setComputeDmetaphone(boolean computeDmetaphone) {
		this.computeDmetaphone = computeDmetaphone;
	}

}
