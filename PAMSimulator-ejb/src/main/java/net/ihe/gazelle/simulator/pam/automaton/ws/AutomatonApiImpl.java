package net.ihe.gazelle.simulator.pam.automaton.ws;


import net.ihe.gazelle.HL7Common.dao.HL7V2ResponderSUTConfigurationDAO;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.application.PatientService;
import net.ihe.gazelle.simulator.pam.automaton.helper.AutomatonHelper;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescriptionDAO;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescriptionWrapper;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResultsDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientDAO;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.jboss.seam.annotations.In;
import org.jboss.seam.contexts.Lifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class AutomatonApiImpl implements AutomatonAPI {


    private static final Logger LOG = LoggerFactory.getLogger(AutomatonApiImpl.class);

    @In(create = true)
    private PatientService patientService;

    private Response launchAutomaton(boolean generatePatient, Integer graphmlID, boolean fullmovementcoverage,
                                     boolean validatetransactioninstance, boolean bp6Mode,
                                     boolean startwithanA28, String systemundertest, String patientsCountryCode, Integer patientId) {

        if (graphmlID == null) {
            return Response.status(400).header("Warning", "Warning: 199 PatientManager \"NAV: graphmlID is a required parameter and is missing in " +
                    "your request\"")
                    .build();
        } else if (systemundertest == null) {
            return Response.status(400).header("Warning", "Warning: 199 PatientManager \"NAV: systemundertest is a required parameter and is " +
                    "missing in your request\"").build();
        } else if (!generatePatient && patientId == null) {
            return Response.status(400).header("Warning", "Warning: 199 PatientManager \"NAV: patientID is a required parameter and is " +
                    "missing in your request\"").build();
        }

        Lifecycle.beginCall();
        GraphDescription graphToExecute = GraphDescriptionDAO.getGraphDescriptionById(graphmlID);
        if (graphToExecute == null) {
            Lifecycle.endCall();
            return Response.status(404).header("Warning", "Warning: 199 PatientManager \"NAV: the given graphmlID does not match an existing graph\"")
                    .build();
        }

        HL7V2ResponderSUTConfiguration sut = HL7V2ResponderSUTConfigurationDAO.getSUTByName(systemundertest);
        if (sut == null) {
            Lifecycle.endCall();
            return Response.status(400).header("Warning", "Warning: 199 PatientManager \"NAV: The systemundertest parameter does not match an " +
                    "existing HL7v2 responder configuration known by the tool\"").build();
        }

        Patient patient;
        AutomatonHelper helper = new AutomatonHelper(bp6Mode, patientService);
        if (generatePatient) {
            patient = helper.generatePatient(patientsCountryCode);
            if (patient == null) {
                Lifecycle.endCall();
                return Response.status(503).header("Warning", "Warning: 199 PatientManager \"NAV: the tool did not succeed in creating a patient\"")
                        .build();
            }else{
                List<PatientIdentifier> savedIdentifiers = new ArrayList<PatientIdentifier>();
                for (PatientIdentifier identifier: patient.getPatientIdentifiers()){
                    PatientIdentifier savedPid = identifier.save();
                    savedIdentifiers.add(savedPid);
                }
                patient.setPatientIdentifiers(savedIdentifiers);
                patient = patient.savePatient(EntityManagerService.provideEntityManager());
            }
        } else {
            patient = PatientDAO.getPatientById(patientId);
            if (patient == null) {
                return Response.status(400).header("Warning", "Warning: 199 PatientManager \"NAV: patientId does not match a patient in the tool's " +
                        "database\"").build();
            }
        }

        Encounter encounter = helper.createEncounter(patient);

        if (startwithanA28) {
            try {
                helper.sendA28(sut, encounter);
            } catch (Exception e) {
                return Response.status(503).header("Warning", "Warning: 199 PatientManager \"NAV: Tool as not been able to send the A28 message: "
                        + e.getMessage() + " \"").build();
            }
        }

        GraphExecutionResults results = GraphExecutionResultsDAO.initializeResults(fullmovementcoverage, validatetransactioninstance, bp6Mode,
                graphToExecute, sut);

        Integer executionId = helper.execute(results, encounter);
        Lifecycle.endCall();
        LOG.info("send back result");
        if (executionId != null) {
            return Response.ok(executionId).build();
        } else {
            return Response.status(503).header("Warning", "Warning: 199 PatientManager \"NAV: An error occurred when executing the automaton\"")
                    .build();
        }
    }

    @Override
    public Response listGraphs() {
        Lifecycle.beginCall();
        List<GraphDescription> graphs = GraphDescriptionDAO.getActiveGraphDescriptions();
        GraphDescriptionWrapper wrapper = new GraphDescriptionWrapper(graphs);
        Response.ResponseBuilder builder = Response.ok(wrapper);
        Lifecycle.endCall();
        return builder.build();
    }


    public Response runAutomaton(Integer graphmlID, boolean fullmovementcoverage,
                                 boolean validatetransactioninstance, boolean startwithanA28,
                                 String systemundertest, String patientsCountryCode, boolean BP6Mode) {

        return launchAutomaton(true, graphmlID, fullmovementcoverage, validatetransactioninstance, BP6Mode, startwithanA28, systemundertest,
                patientsCountryCode, null);
    }


    public Response runAutomatonWithExistingPatient(Integer graphmlID, boolean fullmovementcoverage,
                                                    boolean validatetransactioninstance, boolean startwithanA28, String systemundertest, Integer
                                                            patientID, boolean BP6Mode) {

        return launchAutomaton(false, graphmlID, fullmovementcoverage, validatetransactioninstance, BP6Mode, startwithanA28, systemundertest,
                null, patientID);
    }

    public Response statusExecution(Integer executionID) {


        if (executionID == null) {
            return Response.status(400).header("Warning", "Warning: 199 PatientManagery \"NAV: executionID is a required parameter but does not " +
                    "appear in your request\"")
                    .build();
        } else {
            Lifecycle.beginCall();
            GraphExecutionResults results = GraphExecutionResultsDAO.getGraphExecutionResultsById(executionID);
            Lifecycle.endCall();
            if (results == null) {
                return Response.status(404).header("Warning", "Warning: 199 PatientManagery \"NAV: The given executionID parameter does not match " +
                        "an entry in the tool\"").build();
            } else {
                return Response.ok(results.getExecutionStatus().getLabel()).build();
            }
        }
    }

}