package net.ihe.gazelle.simulator.hl7v3.sut.model;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
/**
 * <p>HL7v3ResponderSUTConfiguration class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("hl7v3ResponderSUTConfiguration")
@DiscriminatorValue("hl7v3_responder")
public class HL7v3ResponderSUTConfiguration extends SystemConfiguration {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1546363663867036043L;

	@Column(name = "device_oid")
	@NotNull
	private String deviceOID;
	
	@Column(name = "organization_oid")
	@NotNull
	private String organizationOID;

	/**
	 * <p>Constructor for HL7v3ResponderSUTConfiguration.</p>
	 */
	public HL7v3ResponderSUTConfiguration() {
		super();
	}

	/**
	 * <p>Constructor for HL7v3ResponderSUTConfiguration.</p>
	 *
	 * @param inConfig a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
	 */
	public HL7v3ResponderSUTConfiguration(HL7v3ResponderSUTConfiguration inConfig) {
		super(inConfig);
		this.deviceOID = inConfig.getDeviceOID();
		this.organizationOID = inConfig.getOrganizationOID();
	}

    /**
     * <p>Getter for the field <code>deviceOID</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDeviceOID() {
		return deviceOID;
	}

	/**
	 * <p>Setter for the field <code>deviceOID</code>.</p>
	 *
	 * @param deviceOID a {@link java.lang.String} object.
	 */
	public void setDeviceOID(String deviceOID) {
		this.deviceOID = deviceOID;
	}

	/**
	 * <p>Getter for the field <code>organizationOID</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getOrganizationOID() {
		return organizationOID;
	}

	/**
	 * <p>Setter for the field <code>organizationOID</code>.</p>
	 *
	 * @param organizationOID a {@link java.lang.String} object.
	 */
	public void setOrganizationOID(String organizationOID) {
		this.organizationOID = organizationOID;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null){
			return false;
		}
		if (this == o) {
			return true;
		}
		if (!(o instanceof HL7v3ResponderSUTConfiguration)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}

		HL7v3ResponderSUTConfiguration that = (HL7v3ResponderSUTConfiguration) o;

		if (deviceOID != null ? !deviceOID.equals(that.deviceOID) : that.deviceOID != null) {
			return false;
		}
		return organizationOID != null ? organizationOID.equals(that.organizationOID) : that.organizationOID == null;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (deviceOID != null ? deviceOID.hashCode() : 0);
		result = 31 * result + (organizationOID != null ? organizationOID.hashCode() : 0);
		return result;
	}
}
