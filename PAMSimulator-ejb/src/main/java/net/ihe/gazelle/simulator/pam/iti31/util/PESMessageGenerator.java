package net.ihe.gazelle.simulator.pam.iti31.util;

import net.ihe.gazelle.HL7Common.exception.HL7MessageBuilderException;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.Patient;

public interface PESMessageGenerator {

	void setAttributes(Encounter selectedEncounter, Movement selectedMovement, Patient selectedPatient,
                       String triggerEvent, boolean enableHistoric, Patient incorrePatient, HL7V2ResponderSUTConfiguration systemConfiguration,
                       String sendingApplication, String sendingFacility, String transactionKeyword);

	/**
	 * 
	 */
	String build() throws HL7MessageBuilderException;

	String getMessageType();

}