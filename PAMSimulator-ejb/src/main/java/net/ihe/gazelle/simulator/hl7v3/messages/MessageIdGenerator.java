package net.ihe.gazelle.simulator.hl7v3.messages;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
/**
 * <p>MessageIdGenerator class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("messageIdGenerator")
@Table(name="pam_message_id_generator", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="issuing_actor_id"))
@SequenceGenerator(name="pam_message_id_generator_sequence", sequenceName="pam_message_id_generator_id_seq", allocationSize=1)
public class MessageIdGenerator implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2082464001890243838L;
	private static final int INCREMENT = 1;

	@Id
	@GeneratedValue(generator="pam_message_id_generator_sequence", strategy=GenerationType.SEQUENCE)
	@Column(name="id", nullable=false, unique=true)
	@NotNull
	private Integer id;
	
	@Column(name="index")
	private Integer index;
	
	@ManyToOne(targetEntity = Actor.class)
	@JoinColumn(name="issuing_actor_id")
	private Actor issuingActor;
	
	@Column(name="oid")
	private String oid;
	
	/**
	 * <p>Constructor for MessageIdGenerator.</p>
	 */
	public MessageIdGenerator(){
		
	}
	
	/**
	 * <p>getGeneratorForActor.</p>
	 *
	 * @param actorKeyword a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.simulator.hl7v3.messages.MessageIdGenerator} object.
	 */
	public static MessageIdGenerator getGeneratorForActor(String actorKeyword){
		MessageIdGeneratorQuery query = new MessageIdGeneratorQuery();
		query.issuingActor().keyword().eq(actorKeyword);
		return query.getUniqueResult();
	}

	/**
	 * <p>getNextId.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getNextId(){
		String idString = index.toString();
		Integer nextIndex = index + INCREMENT;
		setIndex(nextIndex);
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		entityManager.merge(this);
		entityManager.flush();
		return idString;
	}
	
	/**
	 * <p>getOidForMessage.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getOidForMessage(){
		return this.oid + ".1";
	}
	
	/**
	 * <p>getOidForQuery.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getOidForQuery()
	{
		return this.oid + ".2";
	}
	
	/**
	 * <p>getIndexAsString.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIndexAsString(){
		return this.index.toString();
	}
	
	/**
	 * <p>Getter for the field <code>issuingActor</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public Actor getIssuingActor() {
		return issuingActor;
	}

	/**
	 * <p>Setter for the field <code>issuingActor</code>.</p>
	 *
	 * @param issuingActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 */
	public void setIssuingActor(Actor issuingActor) {
		this.issuingActor = issuingActor;
	}

	/**
	 * <p>Getter for the field <code>oid</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getOid() {
		return oid;
	}

	/**
	 * <p>Setter for the field <code>oid</code>.</p>
	 *
	 * @param oid a {@link java.lang.String} object.
	 */
	public void setOid(String oid) {
		this.oid = oid;
	}

	/**
	 * <p>Getter for the field <code>index</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * <p>Setter for the field <code>index</code>.</p>
	 *
	 * @param index a {@link java.lang.Integer} object.
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}
	
}
