package net.ihe.gazelle.simulator.hl7v3.initiator;

import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.MessageInstanceMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

/**
 * <b>Class Description : </b>HL7V3Utils<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 14/12/15
 *


 */
public class HL7V3Utils {

    protected static final String ERROR_MSG = "Unable to marshall message";
    private static Logger log = LoggerFactory.getLogger(HL7V3Utils.class);

    public static byte[] findCandidatesQueryToByteArray(PRPAIN201305UV02Type request) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            HL7V3Transformer.marshallMessage(PRPAIN201305UV02Type.class, baos, request);
            return baos.toByteArray();
        } catch (JAXBException e) {
            log.error(e.getMessage(), e);
            return ERROR_MSG.getBytes(StandardCharsets.UTF_8);
        }
    }

    public static byte[] findCandidatesQueryResponseToByteArray(PRPAIN201306UV02Type response) {
        if (response != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                HL7V3Transformer.marshallMessage(PRPAIN201306UV02Type.class, baos, response);
                return baos.toByteArray();
            } catch (JAXBException e) {
                log.error(e.getMessage(), e);
                return ERROR_MSG.getBytes(StandardCharsets.UTF_8);
            }
        } else {
            return null;
        }
    }

    public static byte[] acknowledgementToByteArray(MCCIIN000002UV01Type ack) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            HL7V3Transformer.marshallMessage(MCCIIN000002UV01Type.class, baos, ack);
            return baos.toByteArray();
        } catch (JAXBException e) {
            log.error(e.getMessage(), e);
            return ERROR_MSG.getBytes(StandardCharsets.UTF_8);
        }
    }

    public static byte[] getRevisePatientRecordToByteArray(PRPAIN201302UV02Type request) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            HL7V3Transformer.marshallMessage(PRPAIN201302UV02Type.class, baos, request);
            return baos.toByteArray();
        } catch (JAXBException e) {
            log.error(e.getMessage(), e);
            return ERROR_MSG.getBytes(StandardCharsets.UTF_8);
        }
    }

    public static void saveMessageId(II id, MessageInstance messageInstance){
        StringBuilder messageId = new StringBuilder(id.getRoot());
        messageId.append(':');
        messageId.append(id.getExtension());
        MessageInstanceMetadata metadata = new MessageInstanceMetadata(messageInstance, MessageInstanceMetadata.MESSAGE_ID_LABEL, messageId.toString());
        metadata.save();
    }

    public static HttpServletRequest getServletRequestFromContext(final WebServiceContext context){
        return (HttpServletRequest) context.getMessageContext()
                .get(MessageContext.SERVLET_REQUEST);
    }
}
