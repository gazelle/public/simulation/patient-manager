package net.ihe.gazelle.simulator.pix.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.simulator.pam.model.Patient;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Name("crossReference")
@Table(name = "pix_cross_reference", schema = "public")
@SequenceGenerator(name = "pix_cross_reference_sequence", sequenceName = "pix_cross_reference_id_seq", allocationSize = 1)
public class CrossReference implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1074748883268222875L;

	@Id
	@NotNull
	@GeneratedValue(generator = "pix_cross_reference_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	private Integer id;

	@OneToMany(mappedBy = "pixReference", fetch = FetchType.LAZY)
	@Fetch(FetchMode.SELECT)
	private List<Patient> patients;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_changed")
	private Date lastChanged;

	@Column(name = "last_modifier")
	private String lastModifier;

	public CrossReference(String username) {
		this.lastChanged = new Date();
		this.lastModifier = username;
	}

	public CrossReference() {
		this.lastChanged = new Date();
	}

	public List<Patient> getPatients() {
		return patients;
	}

	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}

	public Date getLastChanged() {
		return lastChanged;
	}

	public void setLastChanged(Date lastChanged) {
		if (lastChanged != null) {
			this.lastChanged = (Date) lastChanged.clone();
		} else {
			this.lastChanged = null;
		}
	}

	public String getLastModifier() {
		return lastModifier;
	}

	public void setLastModifier(String lastModifier) {
		this.lastModifier = lastModifier;
	}

	public Integer getId() {
		return id;
	}

	public CrossReference updateReference(EntityManager entityManager, String user) {
		this.lastChanged = new Date();
		this.lastModifier = user;
		return entityManager.merge(this);
	}

	@Override
	public int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return HibernateHelper.getLazyEquals(this, obj);
	}
}
