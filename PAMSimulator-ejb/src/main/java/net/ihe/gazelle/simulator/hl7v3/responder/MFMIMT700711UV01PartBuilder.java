package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.coctmt090003UV01.COCTMT090003UV01AssignedEntity;
import net.ihe.gazelle.hl7v3.datatypes.CS;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.INT;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01Custodian;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01QueryAck;
import net.ihe.gazelle.hl7v3.voc.ParticipationType;
import net.ihe.gazelle.hl7v3.voc.RoleClassAssignedEntity;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageBuilder;

/**
 * Created by aberge on 12/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class MFMIMT700711UV01PartBuilder extends HL7v3MessageBuilder {

	/**
	 * <p>populateCustodian.</p>
	 *
	 * @return a {@link net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01Custodian} object.
	 */
	public static MFMIMT700711UV01Custodian populateCustodian() {
		MFMIMT700711UV01Custodian custodian = new MFMIMT700711UV01Custodian();
		custodian.setTypeCode(ParticipationType.CST);
		COCTMT090003UV01AssignedEntity entity = new COCTMT090003UV01AssignedEntity();
		entity.setClassCode(RoleClassAssignedEntity.ASSIGNED);
		entity.addId(new II(PreferenceService.getString("hl7v3_organization_oid"), null));
		custodian.setAssignedEntity(entity);
		return custodian;
	}

	/**
	 * <p>populateQueryAck.</p>
	 *
	 * @param idRoot a {@link java.lang.String} object.
	 * @param idExtension a {@link java.lang.String} object.
	 * @param responseCode a {@link java.lang.String} object.
	 * @param statusCode a {@link java.lang.String} object.
	 * @param nbOfPatients a {@link java.lang.Integer} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01QueryAck} object.
	 */
	public static MFMIMT700711UV01QueryAck populateQueryAck(String idRoot, String idExtension, String responseCode,
			String statusCode, Integer nbOfPatients) {
		MFMIMT700711UV01QueryAck queryAck = new MFMIMT700711UV01QueryAck();
		if (responseCode != null) {
			queryAck.setQueryResponseCode(new CS(responseCode, null, null));
		}
		if (idRoot != null || idExtension != null) {
			queryAck.setQueryId(new II(idRoot, idExtension));
		}
		if (statusCode != null) {
			queryAck.setStatusCode(new CS(statusCode, null, null));
		}
		if (nbOfPatients != null) {
			queryAck.setResultCurrentQuantity(new INT(nbOfPatients));
		}
		return queryAck;
	}
}
