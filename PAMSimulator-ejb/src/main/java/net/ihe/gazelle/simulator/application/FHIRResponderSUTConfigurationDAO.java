package net.ihe.gazelle.simulator.application;

import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;

import java.util.List;

/**
 * Interface for {@link FHIRResponderSUTConfiguration} DAO.
 */
public interface FHIRResponderSUTConfigurationDAO {

    /**
     * Retrieve all SUT linked to the transaction with given keyword.
     *
     * @param transactionKeyword keyword of the transaction for which we want to get SUT.
     * @return the list of SUT linked to given transaction.
     */
    List<FHIRResponderSUTConfiguration> retrieveSUTByTransaction(String transactionKeyword);
}
