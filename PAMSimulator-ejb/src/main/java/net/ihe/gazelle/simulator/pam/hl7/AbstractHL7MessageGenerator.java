package net.ihe.gazelle.simulator.pam.hl7;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.AbstractMessage;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.v25.datatype.XAD;
import ca.uhn.hl7v2.model.v25.message.ADT_A01;
import ca.uhn.hl7v2.model.v25.message.ADT_A05;
import ca.uhn.hl7v2.model.v25.message.ADT_A24;
import ca.uhn.hl7v2.model.v25.message.ADT_A30;
import ca.uhn.hl7v2.model.v25.message.ADT_A37;
import ca.uhn.hl7v2.model.v25.message.ADT_A39;
import ca.uhn.hl7v2.model.v25.segment.EVN;
import ca.uhn.hl7v2.model.v25.segment.MRG;
import ca.uhn.hl7v2.model.v25.segment.MSH;
import ca.uhn.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.model.v25.segment.PV1;
import ca.uhn.hl7v2.parser.ParserConfiguration;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.cat.dao.SystemUnderTestPreferencesDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <b>Class Description : </b>PAMPDSMessageGenerator<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 02/11/15
 */
public abstract class AbstractHL7MessageGenerator extends HL7MessageGenerator {
    protected static class PIDSegmentCustomization {
        protected boolean fillRace;
        protected boolean fillReligion;
        protected boolean fillCountyParish;

        protected PIDSegmentCustomization() {
        }

        public boolean isFillRace() {
            return fillRace;
        }

        public boolean isFillReligion() {
            return fillReligion;
        }

        public boolean isFillCountyParish() {
            return fillCountyParish;
        }
    }

    protected static class IHEPID extends PIDSegmentCustomization {
        public IHEPID() {
            super();
            fillRace = true;
            fillReligion = true;
            fillCountyParish = false;
        }
    }

    protected static class BP6PID extends PIDSegmentCustomization {
        public BP6PID() {
            super();
            fillRace = false;
            fillReligion = false;
            fillCountyParish = true;
        }
    }

    protected static final String DATE_FORMATTER = "yyyyMMddHHmmss";
    protected PIDSegmentCustomization pidConfig;
    protected SimpleDateFormat dateFormat;
    protected List<HierarchicDesignator> restrictedDomain;


    /**
     * <p>Constructor for PAMPDSMessageGenerator.</p>
     *
     * @param sdf a {@link java.text.SimpleDateFormat} object.
     */
    protected AbstractHL7MessageGenerator(SimpleDateFormat sdf, PIDSegmentCustomization pidConfig) {
        this.dateFormat = sdf;
        this.pidConfig = pidConfig;
    }

    protected AbstractHL7MessageGenerator(PIDSegmentCustomization pidConfig) {
        this(new SimpleDateFormat(DATE_FORMATTER), pidConfig);
    }

    /**
     * <p>fillMSHSegment.</p>
     *
     * @param mshSegment           a {@link ca.uhn.hl7v2.model.v25.segment.MSH} object.
     * @param receivingApplication a {@link java.lang.String} object.
     * @param receivingFacility    a {@link java.lang.String} object.
     * @param sendingApplication   a {@link java.lang.String} object.
     * @param sendingFacility      a {@link java.lang.String} object.
     * @param messageCode          a {@link java.lang.String} object.
     * @param triggerEvent         a {@link java.lang.String} object.
     * @param msgStructure         a {@link java.lang.String} object.
     * @param characterSet         a {@link java.lang.String} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    protected void fillMSHSegment(MSH mshSegment, String receivingApplication, String receivingFacility,
                                  String sendingApplication, String sendingFacility, String messageCode, String triggerEvent,
                                  String msgStructure, String characterSet) throws DataTypeException {
        super.fillMSHSegment(mshSegment, receivingApplication, receivingFacility,
                sendingApplication, sendingFacility, messageCode, triggerEvent,
                msgStructure, dateFormat, characterSet);
    }


    /**
     * <p>fillPIDSegment.</p>
     *
     * @param pidSegment a {@link PID} object.
     * @param patient    a {@link Patient} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     * @throws ca.uhn.hl7v2.HL7Exception            if any.
     */
    public void fillPIDSegment(PID pidSegment, Patient patient,
                               boolean onlyFillPID3, boolean fillPID31) throws HL7Exception {
        List<PatientIdentifier> patientIdentifiers = PatientIdentifierDAO.getPatientIdentifiersForRestrictedDomains(patient, restrictedDomain);
        fillPIDSegment(pidSegment, patient, patientIdentifiers, onlyFillPID3, fillPID31);
    }

    public void fillPIDSegment(PID pidSegment, Patient patient, List<PatientIdentifier> patientIdentifiers,
                               boolean onlyFillPID3, boolean fillPID31) throws HL7Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        setPatientIdentifiersInPID(pidSegment, patientIdentifiers);
        if (!onlyFillPID3) {
            fillNames(pidSegment, patient);
            if (patient.getDateOfBirth() != null) {
                pidSegment.getDateTimeOfBirth().getTime().setValue(sdf.format(patient.getDateOfBirth()));
            }
            pidSegment.getAdministrativeSex().setValue(patient.getGenderCode());
            if (pidConfig.isFillRace()) {
                pidSegment.getRace(0).getIdentifier().setValue(patient.getRaceCode());
            }
            if (pidConfig.isFillReligion()) {
                pidSegment.getReligion().getIdentifier().setValue(patient.getReligionCode());
            }
            fillAddress(pidSegment, patient.getAddressList());
            int xtnIndex = 0;
            PatientPhoneNumber phoneNumber = patient.getPrincipalPhoneNumber();
            if (phoneNumber != null) {
                setPhoneNumber(pidSegment.getPhoneNumberHome(xtnIndex), phoneNumber.getValue());
                xtnIndex++;
            }
            if (patient.getEmail() != null && !patient.getEmail().isEmpty()) {
                setEmail(pidSegment.getPhoneNumberHome(xtnIndex), patient.getEmail());
            }
            if (fillPID31) { // this field does not exist for ADT message from HL7v2.3.1
                pidSegment.getIdentityUnknownIndicator().setValue("N");
            }
            if (patient.getAccountNumber() != null) {
                pidSegment.getPatientAccountNumber().parse(patient.getAccountNumber());
            }
            if (patient.getMaritalStatus() != null) {
                pidSegment.getMaritalStatus().getIdentifier().setValue(patient.getMaritalStatus());
            }
            if (patient.getIdentityReliabilityCode() != null) {
                pidSegment.getIdentityReliabilityCode(0).setValue(patient.getIdentityReliabilityCode());
            }
        } else {
            pidSegment.getPatientName(0).getFamilyName().getSurname().setValue(" ");
            pidSegment.getPatientName(0).getNameTypeCode().setValue("S");
        }
    }

    public void fillAddress(PID pidSegment, List<PatientAddress> patientAddresses) throws HL7Exception {
        for (PatientAddress address : patientAddresses) {
            if(!isAddressEmpty(address)) {
                setXAD((XAD) pidSegment.getField(11, pidSegment.getPatientAddress().length),
                        address.getAddressLine(),
                        address.getCity(),
                        address.getCountryCode(),
                        address.getZipCode(),
                        address.getState(),
                        address.getAddressType());
            }
        }
    }

    public void fillNames(PID pidSegment, Patient patient) throws HL7Exception {
        super.setPatientName(pidSegment.getPatientName(0), patient);
        super.setMotherMaidenName(pidSegment.getMotherSMaidenName(0), patient);
    }

    /**
     * <p>fillMRGSegment.</p>
     *
     * @param mrgSegment  a {@link ca.uhn.hl7v2.model.v25.segment.MRG} object.
     * @param identifiers a {@link java.util.List} object.
     * @param patient     a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    protected void fillMRGSegment(MRG mrgSegment, List<PatientIdentifier> identifiers, Patient patient)
            throws HL7Exception {
        if (identifiers != null) {
            int index = 0;
            for (PatientIdentifier pid : identifiers) {
                Map<String, String> splitPid = pid.splitPatientId();
                if (splitPid != null) {
                    mrgSegment.getPriorPatientIdentifierList(index).getIDNumber().setValue(splitPid.get("CX-1"));
                    mrgSegment.getPriorPatientIdentifierList(index).getAssigningAuthority().getNamespaceID()
                            .setValue(splitPid.get("CX-4-1"));
                    mrgSegment.getPriorPatientIdentifierList(index).getAssigningAuthority().getUniversalID()
                            .setValue(splitPid.get("CX-4-2"));
                    mrgSegment.getPriorPatientIdentifierList(index).getAssigningAuthority().getUniversalIDType()
                            .setValue(splitPid.get("CX-4-3"));
                    mrgSegment.getPriorPatientIdentifierList(index).getIdentifierTypeCode()
                            .setValue(pid.getIdentifierTypeCode());
                    index++;
                }
            }
        }
        mrgSegment.getPriorPatientName(0).getFamilyName().getSurname().setValue(patient.getLastName());
        mrgSegment.getPriorPatientName(0).getGivenName().setValue(patient.getFirstName());
        mrgSegment.getPriorPatientName(0).getNameTypeCode().setValue("L");
    }

    /**
     * <p>fillPV1Segment.</p>
     *
     * @param pv1Segment a {@link ca.uhn.hl7v2.model.v25.segment.PV1} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    protected void fillPV1Segment(PV1 pv1Segment) throws DataTypeException {
        SegmentBuilder.fillPV1Segment(pv1Segment, "N");
    }

    /**
     * <p>fillEVNSegment.</p>
     *
     * @param evnSegment a {@link ca.uhn.hl7v2.model.v25.segment.EVN} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    protected void fillEVNSegment(EVN evnSegment) throws DataTypeException {
        SegmentBuilder.fillEVNSegment(evnSegment, dateFormat);
    }


    /**
     * <p>initADTA30.</p>
     *
     * @return a {@link ca.uhn.hl7v2.model.v25.message.ADT_A30} object.
     */
    public ADT_A30 initADTA30() {
        return new ADT_A30();
    }

    /**
     * <p>initADTA01.</p>
     *
     * @return a {@link ca.uhn.hl7v2.model.v25.message.ADT_A01} object.
     */
    public ADT_A01 initADTA01() {
        return new ADT_A01();
    }

    /**
     * <p>initADTA39.</p>
     *
     * @return a {@link ca.uhn.hl7v2.model.v25.message.ADT_A39} object.
     */
    public ADT_A39 initADTA39() {
        return new ADT_A39();
    }

    /**
     * <p>initADTA24.</p>
     *
     * @return a {@link ca.uhn.hl7v2.model.v25.message.ADT_A24} object.
     */
    public ADT_A24 initADTA24() {
        return new ADT_A24();
    }

    /**
     * <p>initADTA37.</p>
     *
     * @return a {@link ca.uhn.hl7v2.model.v25.message.ADT_A37} object.
     */
    public ADT_A37 initADTA37() {
        return new ADT_A37();
    }

    /**
     * <p>createA28.</p>
     *
     * @param selectedSUT        a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     * @param sendingApplication a {@link java.lang.String} object.
     * @param sendingFacility    a {@link java.lang.String} object.
     * @param selectedPatient    a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @return a {@link ca.uhn.hl7v2.model.v25.message.ADT_A05} object.
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    protected ADT_A05 createA28(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, Patient
            selectedPatient) throws HL7Exception {
        return buildCreateOrUpdateMessage(selectedSUT, sendingApplication, sendingFacility, selectedPatient, false, "A28");
    }

    /**
     * @param selectedSUT
     * @param sendingApplication
     * @param sendingFacility
     * @param selectedPatient
     * @param onlyFillPID3       true for ITI-10, false otherwise
     * @return
     * @throws HL7Exception
     */
    protected ADT_A05 createA31(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, Patient
            selectedPatient, boolean onlyFillPID3) throws HL7Exception {
        return buildCreateOrUpdateMessage(selectedSUT, sendingApplication, sendingFacility, selectedPatient, onlyFillPID3, "A31");
    }

    protected ADT_A05 buildCreateOrUpdateMessage(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, Patient
            selectedPatient, boolean onlyFillPID3, String triggerEvent) throws HL7Exception {
        ADT_A05 msg = initADTA05();
        restrictedDomain = SystemUnderTestPreferencesDAO.getAssigningAuthoritiesForSystem(selectedSUT);
        fillMSHSegment(msg.getMSH(), selectedSUT.getApplication(), selectedSUT.getFacility(),
                sendingApplication, sendingFacility, "ADT", triggerEvent, "ADT_A05", selectedSUT.getCharset()
                        .getHl7Code());
        fillEVNSegment(msg.getEVN());
        fillPV1Segment(msg.getPV1());
        fillPIDSegment(msg.getPID(), selectedPatient,
                onlyFillPID3, true);
        return msg;
    }

    protected List<PatientIdentifier> createA30CorrectPatientIdentifierList(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, PatientIdentifier
            correctPatientIdentifier, PatientIdentifier incorrectPatientIdentifier, Patient selectedPatient, Patient secondaryPatient) throws
            HL7Exception {
        List<PatientIdentifier> correctPatientIdentifierList = new ArrayList<>();
        correctPatientIdentifierList.add(correctPatientIdentifier);
        return correctPatientIdentifierList;
    }

    protected List<PatientIdentifier> createA30IncorrectPatientIdentifierList(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, PatientIdentifier
            correctPatientIdentifier, PatientIdentifier incorrectPatientIdentifier, Patient selectedPatient, Patient secondaryPatient) throws
            HL7Exception {
        List<PatientIdentifier> incorrectPatientIdentifierList = new ArrayList<>();
        incorrectPatientIdentifierList.add(incorrectPatientIdentifier);
        return incorrectPatientIdentifierList;
    }

    protected ADT_A30 createA30(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, PatientIdentifier
            correctPatientIdentifier, PatientIdentifier incorrectPatientIdentifier, Patient selectedPatient, Patient secondaryPatient) throws
            HL7Exception {
        ADT_A30 message = initADTA30();

        restrictedDomain = SystemUnderTestPreferencesDAO.getAssigningAuthoritiesForSystem(selectedSUT);

        fillMSHSegment(message.getMSH(), selectedSUT.getApplication(),
                selectedSUT.getFacility(), sendingApplication, sendingFacility, "ADT", "A47", "ADT_A30",
                selectedSUT.getCharset().getHl7Code());
        fillEVNSegment(message.getEVN());
        List<PatientIdentifier> correctPatientIdentifierList = createA30CorrectPatientIdentifierList(selectedSUT, sendingApplication, sendingFacility, correctPatientIdentifier, incorrectPatientIdentifier, selectedPatient, secondaryPatient);
        List<PatientIdentifier> incorrectPatientIdentifierList = createA30IncorrectPatientIdentifierList(selectedSUT, sendingApplication, sendingFacility, correctPatientIdentifier, incorrectPatientIdentifier, selectedPatient, secondaryPatient);

        if (secondaryPatient != null) {
            fillPIDSegment(message.getPID(), selectedPatient, correctPatientIdentifierList,
                    false, true);
            fillMRGSegment(message.getMRG(), incorrectPatientIdentifierList,
                    secondaryPatient);
        } else {

            fillPIDSegment(message.getPID(), selectedPatient,
                    false, true);
            fillMRGSegment(message.getMRG(), correctPatientIdentifierList, selectedPatient);
        }
        return message;
    }

    protected ADT_A24 createA24(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, Patient
            selectedPatient, Patient secondaryPatient) throws HL7Exception {
        ADT_A24 message = initADTA24();
        fillMSHSegment(message.getMSH(), selectedSUT.getApplication(), selectedSUT.getFacility(),
                sendingApplication, sendingFacility, "ADT", "A24", "ADT_A24", selectedSUT.getCharset()
                        .getHl7Code());
        fillEVNSegment(message.getEVN());
        restrictedDomain = SystemUnderTestPreferencesDAO.getAssigningAuthoritiesForSystem(selectedSUT);
        fillPIDSegment(message.getPID(), selectedPatient,
                false, true);
        fillPIDSegment(message.getPID2(),
                secondaryPatient, false, true);
        return message;
    }

    protected ADT_A37 createA37(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, Patient
            selectedPatient, Patient secondaryPatient) throws HL7Exception {
        ADT_A37 message = initADTA37();
        fillMSHSegment(message.getMSH(), selectedSUT.getApplication(), selectedSUT.getFacility(),
                sendingApplication, sendingFacility, "ADT", "A37", "ADT_A37", selectedSUT.getCharset()
                        .getHl7Code());
        fillEVNSegment(message.getEVN());
        restrictedDomain = SystemUnderTestPreferencesDAO.getAssigningAuthoritiesForSystem(selectedSUT);
        fillPIDSegment(message.getPID(), selectedPatient,
                false, true);
        fillPIDSegment(message.getPID2(),
                secondaryPatient, false, true);
        return message;
    }

    protected ADT_A39 createA40(HL7V2ResponderSUTConfiguration selectedSUT, String sendingApplication, String sendingFacility, Patient
            selectedPatient, Patient secondaryPatient) throws HL7Exception {
        ADT_A39 msg = initADTA39();
        fillMSHSegment(msg.getMSH(), selectedSUT.getApplication(),
                selectedSUT.getFacility(), sendingApplication, sendingFacility, "ADT", "A40", "ADT_A39",
                selectedSUT.getCharset().getHl7Code());
        fillEVNSegment(msg.getEVN());
        restrictedDomain = SystemUnderTestPreferencesDAO.getAssigningAuthoritiesForSystem(selectedSUT);
        fillMRGSegment(msg.getPATIENT().getMRG(), secondaryPatient.getPatientIdentifiers(),
                secondaryPatient);

        fillPIDSegment(msg.getPATIENT().getPID(),
                selectedPatient, false, true);

        return msg;
    }

    protected String buildADTFrame(AbstractMessage message) throws HL7Exception {
        return buildADTFrame(message, true);

    }

    protected String buildADTFrame(AbstractMessage message, boolean validation) throws HL7Exception {
        return buildADTFrame(message, validation ? new PipeParser() : PipeParser.getInstanceWithNoValidation());
    }

    protected String buildADTFrame(AbstractMessage message, PipeParser parser) throws HL7Exception {
        return parser.encode(message);
    }

    public String buildADTA28Frame(HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility, Patient patient) throws HL7Exception {
        return buildADTFrame(createA28(sut, sendingApplication, sendingFacility, patient), false);
    }

    public String buildADTA31Frame(HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility, Patient patient, boolean onlyFillPID3) throws HL7Exception {
        // TODO Très bizarre, car le config n'est pas utilisé
        // in ITI-10, PID-5 is required but must content a single whitespace character (the one is not encoded by HAPI in its default
        // configuration)
        PipeParser parser = PipeParser.getInstanceWithNoValidation();
        ParserConfiguration config = new ParserConfiguration();
        config.addForcedEncode("PID-5");
        return buildADTFrame(createA31(sut, sendingApplication, sendingFacility, patient, onlyFillPID3), parser);
    }

    public String buildADTA47Frame(HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility, PatientIdentifier
            correctPatientIdentifier, PatientIdentifier incorrectPatientIdentifier, Patient selectedPatient, Patient secondaryPatient) throws HL7Exception {
        return buildADTFrame(createA30(sut, sendingApplication, sendingFacility, correctPatientIdentifier, incorrectPatientIdentifier, selectedPatient, secondaryPatient));
    }

    public String buildADTA24Frame(HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility, Patient selectedPatient, Patient secondaryPatient) throws HL7Exception {
        return buildADTFrame(createA24(sut, sendingApplication, sendingFacility, selectedPatient, secondaryPatient));
    }

    public String buildADTA37Frame(HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility, Patient selectedPatient, Patient secondaryPatient) throws HL7Exception {
        return buildADTFrame(createA37(sut, sendingApplication, sendingFacility, selectedPatient, secondaryPatient));
    }

    public String buildADTA40Frame(HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility, Patient selectedPatient, Patient secondaryPatient) throws HL7Exception {
        return buildADTFrame(createA40(sut, sendingApplication, sendingFacility, selectedPatient, secondaryPatient));
    }

    public void setRestrictedDomain(List<HierarchicDesignator> pl_restrictedDomain) {
        restrictedDomain = new ArrayList<>(pl_restrictedDomain);
    }

}
