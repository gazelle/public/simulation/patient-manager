package net.ihe.gazelle.simulator.pix.hl7;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.datatype.CX;
import ca.uhn.hl7v2.model.v25.message.RSP_K23;
import ca.uhn.hl7v2.parser.EncodingNotSupportedException;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>RSPMessageDecoder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class RSPMessageDecoder {

	private static Logger log = LoggerFactory.getLogger(RSPMessageDecoder.class);

	private RSPMessageDecoder(){
		
	}
	
	/**
	 * <p>extractIdentifiersFromRequest.</p>
	 *
	 * @param incomingMessage a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 * @throws ca.uhn.hl7v2.parser.EncodingNotSupportedException if any.
	 * @throws ca.uhn.hl7v2.HL7Exception if any.
	 */
	public static List<PatientIdentifier> extractIdentifiersFromRequest(String incomingMessage)
			throws EncodingNotSupportedException, HL7Exception {
		PipeParser parser = PipeParser.getInstanceWithNoValidation();
		Message message = parser.parse(incomingMessage);
		if (message instanceof RSP_K23) {
			RSP_K23 rspMessage = (RSP_K23) message;
			CX[] identifiers = rspMessage.getQUERY_RESPONSE().getPID().getPatientIdentifierList();
			if ((identifiers != null) && (identifiers.length > 0)) {
				List<PatientIdentifier> correspondingIdentifiers = new ArrayList<PatientIdentifier>();
				for (CX pid : identifiers) {
					PatientIdentifier currentIdentifier = new PatientIdentifier();
					try {
						currentIdentifier.setFullPatientId(pid.encode());
						correspondingIdentifiers.add(currentIdentifier);
					} catch (HL7Exception e) {
						log.error("Unparsable patient identifier: " + e.getMessage());
					}
				}
				return correspondingIdentifiers;
			}
		}
		return null;
	}
}
