package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Domain;

/**
 * Created by aberge on 13/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public final class HL7V3ResponderUtils {

    /** Constant <code>DEFAULT_DOMAIN_KEYWORD="ITI"</code> */
    public static final String DEFAULT_DOMAIN_KEYWORD = "ITI";

    private HL7V3ResponderUtils() {

    }

    /**
     * <p>getDefaultDomain.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public static Domain getDefaultDomain() {
        String domainKeyword = PreferenceService.getString("default_pdq_domain");
        if (domainKeyword == null) {
            domainKeyword = DEFAULT_DOMAIN_KEYWORD;
        }
        // If default_pdq_domain is filled with an unknown keyword, make sure a domain will be selected anyway. Default value is ITI for the basic
        // ITI domain from IHE
        Domain domain = Domain.getDomainByKeyword(domainKeyword);
        if (domain == null) {
            return Domain.getDomainByKeyword(DEFAULT_DOMAIN_KEYWORD);
        } else {
            return domain;
        }
    }
}
