package net.ihe.gazelle.simulator.application;

import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import net.ihe.gazelle.simulator.pam.hl7.BP6HL7Domain;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class BP6PatientBuilder extends AbstractPatientBuilder {
    @Override
    public Patient build(PatientBuilder.Context context) {
        Patient p = newPatient(context);
        p.setAlternateFirstName(p.getFirstName()); //AlternateFirstName is used to store the "used name"
        p.getAdditionalDemographics().setSocioProfessionalGroup(ValueSetProvider.getInstance().getRandomCodeFromValueSet
                (PatientManagerConstants.SOCIO_PRO_GROUP_TABLE));
        p.getAdditionalDemographics().setSocioProfessionalOccupation(ValueSetProvider.getInstance().getRandomCodeFromValueSet
                (PatientManagerConstants.SOCIO_PRO_OCCUP_TABLE));
        context.aPatient = p;
        this.setQualifiedIdentity(context.isQualifiedIdentity);
        if (isQualifiedIdentity()) {
            normalizeNames(p);
            generateQualifiedIdentityConstraints(context);
        }
        setIdentityReliabilityCode(context);
        try {
            generateINSIdentifier(context);
        } catch (IllegalArgumentException e) {
            throw new GenerationException("cannot-generate-ins-identifiers", e);
        }
        return p;
    }

    protected void normalizeNames(Patient p) {
        p.setLastName(normalizeName(p.getLastName()));
        p.setFirstName(normalizeName(p.getFirstName()));
        p.setSecondName(normalizeName(p.getSecondName()));
        p.setThirdName(normalizeName(p.getThirdName()));
        p.setAlternateFirstName(normalizeName(p.getAlternateFirstName()));
    }

    protected String normalizeName(String name) {
        return (name != null) ? StringUtils.stripAccents(name).toUpperCase() : null;
    }

    protected void generateINSIdentifier(PatientBuilder.Context context) {
        Patient inPatient = (Patient) context.aPatient;
        if (inPatient.getGenderCode() == null || inPatient.getGenderCode().isEmpty()) {
            String genderError = "Cannot generate an INS patient identifier if the patient doesn't have a gender";
            throw new IllegalArgumentException(genderError);
        }
        if (inPatient.getDateOfBirth() == null) {
            String dateOfBirthError = "Cannot generate an INS patient identifier if the patient doesn't have a date of birth";
            throw new IllegalArgumentException(dateOfBirthError);
        }

        StringBuilder ins = new StringBuilder();
        appendGenderToINS(ins, inPatient);
        appendDateOfBirthToINS(ins, inPatient);
        appendInseeCodeToINS(ins, inPatient);
        appendOrderNumberToINS(ins);
        appendKeyToIns(ins);
        if (ins.length() != 15) {
            String error = "Error while generating the identifier: the INS is not 15 character long";
            throw new RuntimeException(error);
        }
        ins.append("^^^");
        for (HierarchicDesignator hd : context.selectedAuthorities) {
            if (BP6HL7Domain.INS.getAuthorities().contains(hd.getUniversalID())) {
                StringBuilder identifier = new StringBuilder(ins);
                String hdString = hd.toString().replace('^', '&');
                identifier.append(hdString);
                PatientIdentifier pid = new PatientIdentifier(identifier.toString(), hd.getType());
                pid.setDomain(hd);
                inPatient.getPatientIdentifiers().add(pid);
            }
        }
    }

    private void appendGenderToINS(StringBuilder ins, Patient inPatient) {
        if (inPatient.getGenderCode().equals("M")) {
            ins.append("1");
        } else {
            ins.append("2");
        }
    }

    private void appendDateOfBirthToINS(StringBuilder ins, Patient inPatient) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyMM");
        Date date = inPatient.getDateOfBirth();
        if (date == null) {
            date = getRandomDate(85);
        }
        ins.append(sdf.format(date));
    }

    private void appendInseeCodeToINS(StringBuilder ins, Patient inPatient) {
        boolean bdlFound = false;
        for (PatientAddress patientAddress : inPatient.getAddressList()) {
            if (patientAddress.getAddressType().equals(AddressType.BDL)) {
                ins.append(patientAddress.getInseeCode());
                bdlFound = true;
                break;
            }
        }
        if (!bdlFound) {
            ins.append(getRandomInseeCode());
        }
    }

    private void appendOrderNumberToINS(StringBuilder ins) {
        Random random = new Random();
        for (int i = 0; i < 3; i++) {
            ins.append(random.nextInt(9 - 0));
        }
    }

    private void appendKeyToIns(StringBuilder ins) {
        String insForKey = ins.toString().replace("2A", "19");
        insForKey = insForKey.replace("2B", "18");
        long key = 97 - (Long.parseLong(insForKey) % 97);
        if (key < 10) {
            ins.append(0);
        }
        ins.append(key);
    }

    private String getRandomInseeCode() {
        Random random = new Random();
        int dept = random.nextInt(95 - 1) + 1;
        int city = random.nextInt(99);
        StringBuilder code = new StringBuilder();
        if (dept < 10) {
            code.append(0);
        }
        code.append(dept);
        code.append("0");
        if (city < 10) {
            code.append("0");
        }
        code.append(city);
        return code.toString();
    }

    /**
     * Returns a random date between now and some years ago in the past.
     *
     * @param range : number of years for the range to search for the date
     * @return
     */
    private static Date getRandomDate(Integer range) {
        Calendar now = Calendar.getInstance();
        Calendar lowerBoundary = Calendar.getInstance();
        lowerBoundary.roll(Calendar.YEAR, -range);

        long val2 = now.getTimeInMillis();
        long val1 = lowerBoundary.getTimeInMillis();

        Random r = new Random();
        long randomTS = (long) (r.nextDouble() * (val2 - val1)) + val1;
        Date d = new Date(randomTS);
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        return c.getTime();
    }

    protected void generateQualifiedIdentityConstraints(PatientBuilder.Context context) {
        if (context.aPatient == null || !(context.aPatient instanceof Patient)) {
            throw new GenerationException("invalid.patient");
        }
        if (context.selectedCountry == null) {
            throw new GenerationException("no.country.selected");
        }
        Patient selectedPatient = (Patient) context.aPatient;
        if (context.ddsStub == null) {
            throw new GenerationException("cannot-generate-addresses");
        } else {
            this.ddsStub = context.ddsStub;
        }
        PatientAddress birthPlace = generateAddress(context.selectedCountry.getIso(), context.isQualifiedIdentity);
        if (birthPlace == null) {
            throw new GenerationException("cannot-generate-birth-place");
        }
        birthPlace.setMainAddress(false);
        birthPlace.setPatient(selectedPatient);
        birthPlace.setAddressType(AddressType.BDL);
        if ("FRANCE".equals(context.selectedCountry.getName())) {
            birthPlace.setInseeCode(getTownInseeCode(birthPlace.getCity()));
        } else {
            birthPlace.setInseeCode(getCountryInseeCode(context.selectedCountry.getIso()));
        }
        selectedPatient.addPatientAddress(birthPlace);
        setGenderIns(selectedPatient);
    }

    protected void setGenderIns(Patient selectedPatient) {
        if (selectedPatient.getGenderCode() == null || (!selectedPatient.getGenderCode().equals("M") && !selectedPatient.getGenderCode().equals("F"))) {
            selectedPatient.setGenderCode("F");
        }
    }

    private void setIdentityReliabilityCode(PatientBuilder.Context context) {
        Patient selectedPatient = (Patient) context.aPatient;
        String identityReliabilityCode = isQualifiedIdentity() ? "VALI" : "PROV";
        selectedPatient.setIdentityReliabilityCode(identityReliabilityCode);
    }

    @Override
    public Patient buildIdentifiers(PatientBuilder.IdentifierContext context) {
        generatePatientIdentifiers((Patient) context.aPatient,
                context.selectedAuthorities,
                context.useIdsFromDDS,
                context.generateIdsForAuthorities,
                context.generateIdsForConnectathon);
        generateINSIdentifier(context);
        return (Patient) context.aPatient;
    }

    public Patient buildIdentifiers(Patient patient, List<HierarchicDesignator> authorities) {
        generateAdditionalIdentifiers(patient, authorities);
        Context context = new Context(null, patient, authorities, false,
                false, false, false,
                null, null);
        generateINSIdentifier(context);
        return patient;
    }

}
