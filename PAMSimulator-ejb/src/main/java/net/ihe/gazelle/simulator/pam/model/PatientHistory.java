package net.ihe.gazelle.simulator.pam.model;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.core.ResourceBundle;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Class description: PatientHistory
 * <p/>
 * This class is used to describe all the actions perform on a patient. For example, we store when the patient has been created, by who. We can also add information on the sut which sends or receives
 * the patient ...
 * <p/>
 * This class possesses the following attributes
 * <ul>
 * <li><b>id</b> unique ID in the database</li>
 * <li><b>currentPatient</b> The patient on which we are working</li>
 * <li><b>otherPatient</b> The patient to/with which the current patient has been merge, update, link, unlink ...</li>
 * <li><b>action</b> The action performs on the patient (see definition ActionPerformed class for further information)</li>
 * <li><b>application</b> Sending/Receiving application to/from which the patient has been sent/received</li>
 * <li><b>facility</b> Sending/Receiving facility to/from which the patient has been sent/received</li>
 * <li><b>messageType</b> Message type contains in the message sent/received</li>
 * <li><b>user</b> The user who performed the action (if logged in)</li>
 * <li><b>timestamp</b> When the action has been performed</li>
 * <ul>
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes-Bretagne Atlantique
 * @version 1.0 - June, 17th 2011
 */
@Entity
@Name("patientHistory")
@Table(name = "pam_patient_history", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pam_patient_history_sequence", sequenceName = "pam_patient_history_id_seq", allocationSize = 1)
public class PatientHistory implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * This structure gathers the different actions that can be performed on a patient
     *
     * @author aberge
     */
    public static class ActionPerformed {
        /**
         * UPDATE: currentPatient has been updated to otherPatient
         */
        public static final String UPDATE = "update";
        /**
         * MERGE: currentPatient has been merged with otherPatient
         */
        public static final String MERGE = "merge";
        /**
         * LINK: currentPatient has been linked to otherPatient
         */
        public static final String LINK = "link";
        /**
         * UNLINK: currentPatient has been unlinked from otherPatient
         */
        public static final String UNLINK = "unlink";
        /**
         * CHANGEID: IDs of currentPatient have been changed to IDs of otherPatient
         */
        public static final String CHANGEID = "changeId";
        /**
         * CREATED: patient has been created by a user (or by the simulator if it is received)
         */
        public static final String CREATED = "created";
        /**
         * SENT: patient has been sent to a SUT (application/facility/messageType)
         */
        public static final String SENT = "sent";
        /**
         * RECEIVED: patient has been received from a SUT (application/facility/messageType)
         */
        public static final String RECEIVED = "received";

        public static final String ACCOUNT_MOVE = "accountmovedto";
    }

    @Id
    @GeneratedValue(generator = "pam_patient_history_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id")
    private Integer id;

    @Column(name = "action")
    @NotNull
    private String action;

    @Column(name = "username")
    private String username;

    @Column(name = "facility")
    private String facility;

    @Column(name = "application")
    private String application;

    @Column(name = "message_type")
    private String messageType;

    @ManyToOne/*(cascade = CascadeType.MERGE)*/
    @JoinColumn(name = "other_patient_id")
    private Patient otherPatient;

    @ManyToOne/*(cascade = CascadeType.MERGE)*/
    @JoinColumn(name = "current_patient_id")
    private Patient currentPatient;

    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    /**
     * <p>Constructor for PatientHistory.</p>
     *
     * @param currentPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param otherPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param action a {@link java.lang.String} object.
     * @param messageType a {@link java.lang.String} object.
     * @param user a {@link java.lang.String} object.
     * @param facility a {@link java.lang.String} object.
     * @param application a {@link java.lang.String} object.
     */
    public PatientHistory(Patient currentPatient, Patient otherPatient, String action, String messageType, String user,
                          String facility, String application) {
        this.currentPatient = currentPatient;
        this.otherPatient = otherPatient;
        this.action = action;
        this.timestamp = new Date();
        this.messageType = messageType;
        this.facility = facility;
        this.application = application;
        this.username = user;
    }

    /**
     * <p>Constructor for PatientHistory.</p>
     */
    public PatientHistory() {

    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>timestamp</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * <p>Setter for the field <code>timestamp</code>.</p>
     *
     * @param timestamp a {@link java.util.Date} object.
     */
    public void setTimestamp(Date timestamp) {
        if (timestamp != null) {
            this.timestamp = (Date) timestamp.clone();
        } else {
            this.timestamp = null;
        }
    }

    /**
     * <p>Getter for the field <code>action</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAction() {
        return action;
    }

    /**
     * <p>Setter for the field <code>action</code>.</p>
     *
     * @param action a {@link java.lang.String} object.
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * <p>Getter for the field <code>username</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUsername() {
        return username;
    }

    /**
     * <p>Setter for the field <code>username</code>.</p>
     *
     * @param user a {@link java.lang.String} object.
     */
    public void setUsername(String user) {
        this.username = user;
    }

    /**
     * <p>Getter for the field <code>facility</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFacility() {
        return facility;
    }

    /**
     * <p>Setter for the field <code>facility</code>.</p>
     *
     * @param facility a {@link java.lang.String} object.
     */
    public void setFacility(String facility) {
        this.facility = facility;
    }

    /**
     * <p>Getter for the field <code>application</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getApplication() {
        return application;
    }

    /**
     * <p>Setter for the field <code>application</code>.</p>
     *
     * @param application a {@link java.lang.String} object.
     */
    public void setApplication(String application) {
        this.application = application;
    }

    /**
     * <p>Getter for the field <code>messageType</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * <p>Setter for the field <code>messageType</code>.</p>
     *
     * @param messageType a {@link java.lang.String} object.
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    /**
     * <p>Getter for the field <code>otherPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getOtherPatient() {
        return otherPatient;
    }

    /**
     * <p>Setter for the field <code>otherPatient</code>.</p>
     *
     * @param otherPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setOtherPatient(Patient otherPatient) {
        this.otherPatient = otherPatient;
    }

    /**
     * <p>Getter for the field <code>currentPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getCurrentPatient() {
        return currentPatient;
    }

    /**
     * <p>Setter for the field <code>currentPatient</code>.</p>
     *
     * @param currentPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setCurrentPatient(Patient currentPatient) {
        this.currentPatient = currentPatient;
    }

    /**
     * <p>getPatientsHistoryAsCurrentPatient.</p>
     *
     * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @return a {@link java.util.List} object.
     */
    public static List<PatientHistory> getPatientsHistoryAsCurrentPatient(Patient inPatient) {
        if (inPatient != null && inPatient.getId() != null) {
            PatientHistoryQuery query = new PatientHistoryQuery();
            query.currentPatient().eq(inPatient);
            List<PatientHistory> history = query.getList();
            if ((history != null) && !history.isEmpty()) {
                return history;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * <p>getPatientsHistoryAsOtherPatient.</p>
     *
     * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @return a {@link java.util.List} object.
     */
    public static List<PatientHistory> getPatientsHistoryAsOtherPatient(Patient inPatient) {
        if (inPatient != null) {
            PatientHistoryQuery query = new PatientHistoryQuery();
            query.otherPatient().eq(inPatient);
            List<PatientHistory> history = query.getList();
            if ((history != null) && !history.isEmpty()) {
                return history;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * <p>save.</p>
     *
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     */
    public void save(EntityManager entityManager) {
        entityManager.merge(this);
        entityManager.flush();
    }

    /**
     * <p>getActionDisplayName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getActionDisplayName() {
        if (this.getAction() != null) {
            return ResourceBundle.instance().getString(this.getAction());
        } else {
            return null;
        }
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PatientHistory)) return false;

        PatientHistory that = (PatientHistory) o;

        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        return !(timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null);

    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        return result;
    }
}
