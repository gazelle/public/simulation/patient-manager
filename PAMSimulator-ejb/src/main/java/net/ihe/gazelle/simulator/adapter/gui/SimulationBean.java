package net.ihe.gazelle.simulator.adapter.gui;

import net.ihe.gazelle.simulator.message.model.TransactionInstance;

import java.util.List;

/**
 * Interface necessary for a Bean to control the simulationTemplate.xhtml template.
 */
public interface SimulationBean {

    /**
     * Get the list of messages exhanges during the simulation to display in the simulation report.
     *
     * @return the list of exchanged messages.
     */
    List<TransactionInstance> getMessages();

    /**
     * Send the simulated message.
     */
    void send();

    /**
     * Reset the form for message configurations.
     */
    void resetForm();

    /**
     * Perform another test reset the page to the selection of the SUT.
     */
    void performAnotherTest();

    /**
     * Get the title of the Simulation page.
     *
     * @return the litteral value of the title.
     */
    String getTitle();

}
