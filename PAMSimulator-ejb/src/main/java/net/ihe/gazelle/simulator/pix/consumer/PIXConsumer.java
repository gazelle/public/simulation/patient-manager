package net.ihe.gazelle.simulator.pix.consumer;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.ADT_A05;
import net.ihe.gazelle.HL7Common.exception.HL7v2ParsingException;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.ACKBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import java.util.Arrays;

public class PIXConsumer extends IHEDefaultHandler {

    private static final String[] ACCEPTED_EVENTS = {"A31"};
    private static final String[] ACCEPTED_MESSAGE_TYPES = {"ADT"};

    public PIXConsumer() {
        super();
    }

    @Override
    public IHEDefaultHandler newInstance() {
        IHEDefaultHandler handler = new PIXConsumer();
        return handler;
    }

    @Override
    public Message processMessage(Message incomingMessage) throws HL7Exception {
        if (incomingMessage == null) {
            return null;
        }
        Message response;
        // check if we must accept the message
        ACKBuilder ackBuilder = new ACKBuilder(serverApplication, serverFacility, Arrays.asList(ACCEPTED_MESSAGE_TYPES), Arrays.asList
                (ACCEPTED_EVENTS));
        setSutActor(Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR", entityManager));
        simulatedTransaction = Transaction.GetTransactionByKeyword("ITI-10", entityManager);

        try {
            ackBuilder.isMessageAccepted(incomingMessage);
        } catch (HL7v2ParsingException e) {
            response = ackBuilder.buildNack(incomingMessage, e);
            setResponseMessageType(HL7MessageDecoder.getMessageType(response));
            return response;
        }

        messageControlId = terser.get("/.MSH-10");
        if (incomingMessage instanceof ADT_A05) {
            response = ackBuilder.buildAck(incomingMessage);
        } else {
            response = ackBuilder.buildNack(incomingMessage, new HL7v2ParsingException("Trigger event A31 shall be used with message structure " +
                    "ADT_A05"));
        }
        setResponseMessageType(HL7MessageDecoder.getMessageType(response));
        return response;
    }

}
