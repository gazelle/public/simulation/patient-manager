package net.ihe.gazelle.simulator.pam.automaton.Manager;


import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.HL7Common.action.HL7MessageBrowser;
import net.ihe.gazelle.HL7Common.exception.HL7MessageBuilderException;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults;
import net.ihe.gazelle.simulator.pam.automaton.report.TestStep;
import net.ihe.gazelle.simulator.pam.iti31.action.NumberGenerator;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.iti31.util.IHEPESEventHandler;
import net.ihe.gazelle.simulator.pam.iti31.util.IHEPESMessageGenerator;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.Patient;
import org.graphwalker.core.event.EventType;
import org.graphwalker.core.event.Observer;
import org.graphwalker.core.machine.Machine;
import org.graphwalker.core.model.Edge;
import org.graphwalker.core.model.Element;
import org.graphwalker.core.model.Vertex;
import org.jboss.seam.faces.FacesMessages;

import javax.persistence.EntityManager;
import java.util.Date;

/**
 * Created by xfs on 27/10/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class DataObserver implements Observer {

    private static final String PAMSIMULATOR = "PAMSimulator";
    private static final String IHE = "IHE";

    private GraphExecutionResults graphExecutionResults;
    private Encounter encounter;
    private final Actor simulatedActor;
    private final Actor receivingActor;
    private final Transaction simulatedTransaction;

    /**
     * <p>Constructor for DataObserver.</p>
     *
     * @param graphExecutionResults a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults} object.
     * @param encounter             a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public DataObserver(GraphExecutionResults graphExecutionResults, Encounter encounter) {
        this.graphExecutionResults = graphExecutionResults;
        this.encounter = encounter;
        simulatedActor = Actor.findActorWithKeyword("PES");
        receivingActor = Actor.findActorWithKeyword("PEC");
        simulatedTransaction = Transaction.GetTransactionByKeyword("ITI-31");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Machine machine, Element element, EventType eventType) {

        if (eventType.name().equals("BEFORE_ELEMENT")) {

            if (element instanceof Edge.RuntimeEdge) {
                Edge.RuntimeEdge edge = (Edge.RuntimeEdge) machine.getCurrentContext().getCurrentElement();
                if (!edge.getName().equals("ini")) {
                    TestStep currentTestStep = new TestStep();
                    currentTestStep.setExecutionResult(graphExecutionResults);
                    currentTestStep.setEvent(edge.getName());
                    addCurrentTestStepStatus(edge, currentTestStep);
                    addCurrentTestStepTransaction(edge, currentTestStep);
                    graphExecutionResults.getMessagesList().add(currentTestStep);
                }
            } else {
                Vertex.RuntimeVertex vertex = (Vertex.RuntimeVertex) machine.getCurrentContext().getCurrentElement();
                setPreadmitCode(vertex);

            }
        }

    }


    private void addCurrentTestStepTransaction(Edge.RuntimeEdge edge, TestStep currentTestStep) {
        currentTestStep.setRequestAndResponse(sendMessage(edge, currentTestStep));
    }


    private void addCurrentTestStepStatus(Edge.RuntimeEdge edge, TestStep currentTestStep) {
        Vertex.RuntimeVertex previousVertex = edge.getSourceVertex();
        Vertex.RuntimeVertex nextVertex = edge.getTargetVertex();
        currentTestStep.setInitialStatus(GraphVertices.valueOf(previousVertex.getName()));
        currentTestStep.setFinalStatus(GraphVertices.valueOf(nextVertex.getName()));
    }

    /**
     * <p>sendMessage.</p>
     *
     * @param edge            a {@link org.graphwalker.core.model.Edge.RuntimeEdge} object.
     * @param currentTestStep a {@link net.ihe.gazelle.simulator.pam.automaton.report.TestStep} object.
     *
     * @return a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public TransactionInstance sendMessage(Edge.RuntimeEdge edge, TestStep currentTestStep) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        boolean bp6mode = graphExecutionResults.getGraphExecution().isBp6mode();

        Patient patient = encounter.getPatient();
        String triggerEvent = edge.getName();
        String patientClassCode = getPatientClassCode(edge);
        Movement inMovement;
        if (encounter.getMovements() != null && !encounter.getMovements().isEmpty()) {
            inMovement = encounter.getMovements().get(0);
            inMovement.setSimulatedActor(simulatedActor);
            inMovement.setMovementUniqueId(NumberGenerator.generate(DesignatorType.MOVEMENT_ID));
            inMovement.setLastChanged(new Date());
            inMovement.setMovementDate(new Date());
            inMovement.setTriggerEvent(triggerEvent);
            inMovement.setCurrentMovement(true);
            inMovement.setPendingMovement(false);
        } else {
            inMovement = new Movement(encounter);
        }

        IHEPESEventHandler ihepesEventHandler = new IHEPESEventHandler();
        ihepesEventHandler.setAttributes(encounter, inMovement, triggerEvent, simulatedActor, entityManager);
        Movement movement = ihepesEventHandler.processEvent(patientClassCode);
        movement = movement.save(entityManager);

        HL7V2ResponderSUTConfiguration selectedSUT = graphExecutionResults.getGraphExecution().getSut();

        TransactionInstance instance = null;
        IHEPESMessageGenerator builder = new IHEPESMessageGenerator(bp6mode);
        builder.setAttributes(encounter, movement, patient, triggerEvent, true, patient, selectedSUT, PAMSIMULATOR, IHE, "ITI-31");
        try {
            String messageToSend = builder.build();
            messageToSend = convertMessageForPAMFR(bp6mode, builder, messageToSend);

            Initiator initiator = new Initiator(selectedSUT, IHE, PAMSIMULATOR, simulatedActor,
                    simulatedTransaction, messageToSend, builder.getMessageType(), getDomain(), receivingActor);
            instance = initiator.sendMessageAndGetTheHL7Message();
            currentTestStep.setRequestAndResponse(instance);
            currentTestStep.setAckAndErrorMessage();

        } catch (HL7Exception|HL7MessageBuilderException e) {
            FacesMessages.instance().add(e.getMessage());
        }

        return instance;
    }

    private String convertMessageForPAMFR(boolean bp6mode, IHEPESMessageGenerator builder, String messageToSend) throws HL7Exception {
        if (bp6mode) {
            messageToSend = builder.convertMessageToBP6(messageToSend);
        }
        return messageToSend;
    }

    private String getPatientClassCode(Edge.RuntimeEdge edge) {

        String edgeName = edge.getName();
        String targetVertexName = edge.getTargetVertex().getName();
        if (edgeName.equals("A07")) {
            if (targetVertexName.equals("v_Consultant_Urgences")) {
                return "E";
            } else if (targetVertexName.equals("v_Consultant_Externe")) {
                return "O";
            } else {
                return "O";
            }
        }
        if (edgeName.equals("A04")) {
            if (targetVertexName.equals("v_Consultant_Externe")) {
                return "O";
            }
        }
        if (edgeName.equals("A06")) {
            if (targetVertexName.equals("v_Hospitalise")) {
                return "I";
            }
        }
        return null;
    }

    private void validateInstance(TransactionInstance instance) {
        HL7MessageBrowser hl7MessageBrowser = new HL7MessageBrowser();
        hl7MessageBrowser.validate(instance);
    }


    /**
     * <p>getDomain.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDomain() {
        boolean bp6mode = graphExecutionResults.getGraphExecution().isBp6mode();
        Actor simulatedActor = Actor.findActorWithKeyword("PES");
        if (simulatedActor.getKeyword().equals("PES") && bp6mode) {
            return "ITI-FR";
        } else {
            return "ITI";
        }
    }

    /**
     * <p>setPreadmitCode.</p>
     *
     * @param vertex a {@link org.graphwalker.core.model.Vertex.RuntimeVertex} object.
     */
    public void setPreadmitCode(Vertex.RuntimeVertex vertex) {
        if (vertex.getName().equals("Preadmit_R")) {
            encounter.setPatientClassCode("R");
        } else if (vertex.getName().equals("Preadmit_I")) {
            encounter.setPatientClassCode("I");
        } else if (vertex.getName().equals("Preadmit_O")) {
            encounter.setPatientClassCode("O");
        }
    }
}
