package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.datatypes.CD;
import net.ihe.gazelle.hl7v3.datatypes.CE;
import net.ihe.gazelle.hl7v3.datatypes.CS;
import net.ihe.gazelle.hl7v3.datatypes.ED;
import net.ihe.gazelle.hl7v3.datatypes.INT;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01DetectedIssueEvent;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01DetectedIssueManagement;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01SourceOf;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Acknowledgement;
import net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01AcknowledgementDetail;
import net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Device;
import net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Receiver;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01Reason;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01CancelType;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01Type;
import net.ihe.gazelle.hl7v3.quqimt000001UV01.QUQIMT000001UV01QueryContinuation;
import net.ihe.gazelle.hl7v3.voc.AcknowledgementDetailType;
import net.ihe.gazelle.hl7v3.voc.ActClass;
import net.ihe.gazelle.hl7v3.voc.ActClassRoot;
import net.ihe.gazelle.hl7v3.voc.ActMood;
import net.ihe.gazelle.hl7v3.voc.ActRelationshipMitigates;
import net.ihe.gazelle.hl7v3.voc.XActMoodDefEvn;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3AcknowledgmentCode;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pdq.util.ContinuationPointerManager;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import javax.xml.ws.handler.MessageContext;
import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 * <p>PDQV3QueryHandler class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PDQV3QueryHandler extends QueryHandler {

    /** Constant <code>NULL_REQUEST_ERROR_MESSAGE="Simulator cannot handle null requests"</code> */
    public static final String NULL_REQUEST_ERROR_MESSAGE = "Simulator cannot handle null requests";
    /** Constant <code>FIND_CANDIDATE_RESPONSE_TYPE="PRPA_IN201306UV02"</code> */
    public static final String FIND_CANDIDATE_RESPONSE_TYPE = PatientManagerConstants.PRPA_IN_201306_UV_02;
    private static Logger log = LoggerFactory.getLogger(PDQV3QueryHandler.class);
    private static ContinuationPointerManager continuationPointerManager = null;

    static {
        continuationPointerManager = new ContinuationPointerManager();
    }

    /**
     * <p>Constructor for PDQV3QueryHandler.</p>
     *  @param inDomain a {@link Domain} object.
     * @param inSimulatedActor a {@link Actor} object.
     * @param inSutActor a {@link Actor} object.
     * @param inTransaction a {@link Transaction} object.
     * @param servletRequest a {@link String} object.
     * @param messageContext a {@link MessageContext} object.
     */
    public PDQV3QueryHandler(Domain inDomain, Actor inSimulatedActor, Actor inSutActor, Transaction inTransaction,
                             HttpServletRequest servletRequest, MessageContext messageContext) {
        super(inDomain, inSimulatedActor, inSutActor, inTransaction, servletRequest, messageContext);
        continuationPointerManager.clear();
    }

    /**
     * <p>Constructor for PDQV3QueryHandler.</p>
     *  @param inDomain a {@link Domain} object.
     * @param inSimulatedActor a {@link Actor} object.
     * @param inSutActor a {@link Actor} object.
     * @param inTransaction a {@link Transaction} object.
     * @param servletRequest a {@link String} object.
     */
    public PDQV3QueryHandler(Domain inDomain, Actor inSimulatedActor, Actor inSutActor, Transaction inTransaction,
                             HttpServletRequest servletRequest) {
        super(inDomain, inSimulatedActor, inSutActor, inTransaction, servletRequest);
        continuationPointerManager.clear();
    }

    /**
     * PDQv3/PDS : findCandidatesQuery
     *
     * @param inRequest a {@link net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     * @throws net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException if any.
     */
    public PRPAIN201306UV02Type handlePRPAIN201305UV02(final PRPAIN201305UV02Type inRequest) throws HL7V3ParserException {
        if (inRequest == null) {
            log.error(NULL_REQUEST_ERROR_MESSAGE);
            throw new HL7V3ParserException("/", NULL_REQUEST_ERROR_MESSAGE);
        } else {
            PRPAIN201306UV02Type response = null;
            // store request in TransactionInstance object
            ByteArrayOutputStream requestStream = new ByteArrayOutputStream();
            try {
                HL7V3Transformer.marshallMessage(PRPAIN201305UV02Type.class, requestStream, inRequest);
            } catch (JAXBException e) {
                throw new HL7V3ParserException("/", e.getMessage(), e);
            }
            instance.getRequest().setContent(requestStream.toByteArray());
            instance.getRequest().setType("PRPA_IN201305UV02");
            String sutDeviceOid = null;
            try {
                sutDeviceOid = inRequest.getSender().getDevice().getId().get(0).getRoot();
            } catch (NullPointerException|IndexOutOfBoundsException e) {
                log.warn("sender/device/id not provided in received message");
            }
            FindCandidateQueryParser parser = new FindCandidateQueryParser();
            IHEQueryParserCode returnedCode = parser.parsePRPAIN201305UV02(inRequest,
                    PatientManagerConstants.PDS, instance.getDomain().getKeyword());
            PDQV3PDSResponseBuilder responseBuilder = new PDQV3PDSResponseBuilder(instance.getDomain().getKeyword());
            switch (returnedCode) {
                case UNKNOWN_DOMAINS:
                    response = responseBuilder.buildFindCandidatesQueryResponse(inRequest.getId(), null, parser.getUnknownDomains(), null,
                            parser.getQueryByParameter(), sutDeviceOid);
                    break;
                case FORBIDDEN_FIELD:
                    response = responseBuilder
                            .buildFindCandidatesQueryResponse(inRequest.getId(), null, null, parser.getDomainsToReturn(),
                                    inRequest.getControlActProcess().getQueryByParameter(), sutDeviceOid
                            );
                    if (SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.SEHE)) {
                        MCCIMT000300UV01Acknowledgement ack = response.getAcknowledgement().get(0);
                        ack.setTypeCode(new CS(HL7V3AcknowledgmentCode.AE.getValue(), null, null));
                        MCCIMT000300UV01AcknowledgementDetail detail = new MCCIMT000300UV01AcknowledgementDetail();
                        detail.setCode(new CE("KSAContentValidation", null, null));
                        detail.setTypeCode(AcknowledgementDetailType.E);
                        detail.setText(new ED());
                        detail.getText().addMixed("KPDQ-021_PatientAddress_PatientAddress shall not be sent");
                        ack.addAcknowledgementDetail(detail);
                        MFMIMT700711UV01Reason reason = new MFMIMT700711UV01Reason();
                        MCAIMT900001UV01DetectedIssueEvent issue = new MCAIMT900001UV01DetectedIssueEvent();
                        issue.setClassCode(ActClass.ALRT);
                        issue.setMoodCode(ActMood.EVN);
                        issue.setCode(new CD("ActAdministrativeDetectedIssueCode", null, "2.16.840.1.113883.5.4"));
                        issue.addMitigatedBy(new MCAIMT900001UV01SourceOf());
                        issue.getMitigatedBy().get(0).setTypeCode(ActRelationshipMitigates.MITGT);
                        issue.getMitigatedBy().get(0)
                                .setDetectedIssueManagement(new MCAIMT900001UV01DetectedIssueManagement());
                        issue.getMitigatedBy().get(0).getDetectedIssueManagement().setClassCode(ActClassRoot.ACT);
                        issue.getMitigatedBy().get(0).getDetectedIssueManagement().setMoodCode(XActMoodDefEvn.EVN);
                        issue.getMitigatedBy().get(0).getDetectedIssueManagement()
                                .setCode(new CD("InternalError", null, "1.3.6.1.4.1.19376.1.2.27.3"));
                        reason.setDetectedIssueEvent(issue);
                        response.getControlActProcess().addReasonOf(reason);
                    }
                    break;
                case NO_PATIENT_FOUND:
                    response = responseBuilder
                            .buildFindCandidatesQueryResponse(inRequest.getId(), null, null, null, parser.getQueryByParameter(),
                                    sutDeviceOid);
                    break;
                case NO_QUERY_PARAMETER:
                    response = responseBuilder
                            .buildFindCandidatesQueryResponse(inRequest.getId(), null, null, null, null, sutDeviceOid
                            );
                    break;
                case RETURN_ALL_PATIENTS:
                    response = responseBuilder
                            .buildFindCandidatesQueryResponse(inRequest.getId(), parser.getFoundPatients(), null,
                                    parser.getDomainsToReturn(), parser.getQueryByParameter(), sutDeviceOid);
                    break;
                case RETURN_SUBSET_OF_PATIENTS:
                    List<Patient> foundPatients = parser.getFoundPatients();
                    Integer nbOfHintsToReturn = parser.getNbOfHintsToReturn();
                    List<Patient> patientsToReturn = foundPatients.subList(0, nbOfHintsToReturn);
                    response = responseBuilder
                            .buildFindCandidatesQueryResponse(inRequest.getId(), patientsToReturn, null,
                                    parser.getDomainsToReturn(), parser.getQueryByParameter(), sutDeviceOid);
                    continuationPointerManager.addPDQv3Pointer(parser.getPointer(), parser.getNbOfHintsToReturn(),
                            foundPatients.subList(nbOfHintsToReturn, foundPatients.size()), parser.getQueryByParameter(),
                            foundPatients.size(), parser.getDomainsToReturn());
                    response.getControlActProcess().getQueryAck()
                            .setResultTotalQuantity(new INT(foundPatients.size()));
                    response.getControlActProcess().getQueryAck().setResultRemainingQuantity(
                            new INT(continuationPointerManager.getRemainingQuantity(parser.getPointer())));
                    break;
            }
            instance.getResponse().setType(FIND_CANDIDATE_RESPONSE_TYPE);
            sendResponse(PRPAIN201306UV02Type.class, response);
            HL7V3Utils.saveMessageId(inRequest.getId(), instance.getRequest());
            if (response != null) {
                HL7V3Utils.saveMessageId(response.getId(), instance.getResponse());
            }
            return response;
        }

    }

    /**
     * <p>handleFindCandidatesContinue.</p>
     *
     * @param inRequest a {@link net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     * @throws net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException if any.
     */
    public PRPAIN201306UV02Type handleFindCandidatesContinue(final QUQIIN000003UV01Type inRequest) throws HL7V3ParserException {
        if (inRequest == null) {
            log.error(NULL_REQUEST_ERROR_MESSAGE);
            throw new HL7V3ParserException("/", NULL_REQUEST_ERROR_MESSAGE);
        } else {
            String sutDeviceOid = null;
            try {
                sutDeviceOid = inRequest.getSender().getDevice().getId().get(0).getRoot();
            } catch (NullPointerException e) {
                log.warn("sender/device/id not found in request");
            }
            PRPAIN201306UV02Type response = null;
            // store request in TransactionInstance object
            ByteArrayOutputStream requestStream = new ByteArrayOutputStream();
            try {
                HL7V3Transformer.marshallMessage(QUQIIN000003UV01Type.class, requestStream, inRequest);
            } catch (JAXBException e) {
                throw new HL7V3ParserException("/", e.getMessage(), e);
            }
            instance.getRequest().setContent(requestStream.toByteArray());
            instance.getRequest().setType(PatientManagerConstants.QUQI_IN_000003_UV_01);
            QUQIMT000001UV01QueryContinuation queryContinuation = inRequest.getControlActProcess()
                    .getQueryContinuation();
            String pointer = ContinuationPointer.buildContinuationPointer(queryContinuation.getQueryId());
            Integer nbOfHits = 0;
            if (queryContinuation.getContinuationQuantity() != null) {
                nbOfHits = queryContinuation.getContinuationQuantity().getValue();
            }
            PDQV3PDSResponseBuilder responseBuilder = new PDQV3PDSResponseBuilder(instance.getDomain().getKeyword());
            boolean responseSent = false;
            if (!pointer.isEmpty()) {
                Integer totalQuantity = continuationPointerManager.getTotalQuantity(pointer);
                PRPAMT201306UV02QueryByParameter queryByParameter = continuationPointerManager
                        .getQueryByParameter(pointer);
                List<Patient> patientsToReturn = continuationPointerManager.getPatientListForPointer(pointer, nbOfHits);
                if (patientsToReturn != null && !patientsToReturn.isEmpty()) {
                    log.debug("%s patients to return", patientsToReturn.size());
                    response = responseBuilder
                            .buildFindCandidatesQueryResponse(inRequest.getId(), patientsToReturn, null,
                                    continuationPointerManager.getDomainsToReturn(pointer), queryByParameter,
                                    sutDeviceOid);
                    response.getControlActProcess().getQueryAck().setResultTotalQuantity(new INT(totalQuantity));
                    response.getControlActProcess().getQueryAck().setResultRemainingQuantity(
                            new INT(continuationPointerManager.getRemainingQuantity(pointer)));
                    instance.getResponse().setType(PatientManagerConstants.PRPA_IN_201306_UV_02);
                    sendResponse(PRPAIN201306UV02Type.class, response);
                    responseSent = true;
                } else {
                    responseSent = false;
                }
            }
            if (!responseSent) {
                instance.getResponse().setType(PatientManagerConstants.PRPA_IN_201306_UV_02);
                response = responseBuilder
                        .buildFindCandidatesQueryResponse(inRequest.getId(), null, null, null, null, sutDeviceOid
                        );
                sendResponse(PRPAIN201306UV02Type.class, response);
            }
            HL7V3Utils.saveMessageId(inRequest.getId(), instance.getRequest());
            if (response != null) {
                HL7V3Utils.saveMessageId(response.getId(), instance.getResponse());
            }
            return response;
        }
    }


    /**
     * <p>handleFindCandidatesCancel.</p>
     *
     * @param inRequest a {@link net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01CancelType} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     * @throws net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException if any.
     */
    public MCCIIN000002UV01Type handleFindCandidatesCancel(final QUQIIN000003UV01CancelType inRequest)
            throws HL7V3ParserException {
        if (inRequest == null) {
            log.error(NULL_REQUEST_ERROR_MESSAGE);
            throw new HL7V3ParserException("/", NULL_REQUEST_ERROR_MESSAGE);
        } else {
            MCCIIN000002UV01Type response;
            // store request in TransactionInstance object
            ByteArrayOutputStream requestStream = new ByteArrayOutputStream();
            try {
                HL7V3Transformer.marshallMessage(QUQIIN000003UV01CancelType.class, requestStream, inRequest);
            } catch (JAXBException e) {
                throw new HL7V3ParserException("/", e.getMessage(), e);
            }
            instance.getRequest().setContent(requestStream.toByteArray());
            instance.getRequest().setType(PatientManagerConstants.QUQI_IN_000003_UV_01_CANCEL);
            QUQIMT000001UV01QueryContinuation queryContinuation = inRequest.getControlActProcess()
                    .getQueryContinuation();
            String pointer = ContinuationPointer.buildContinuationPointer(queryContinuation.getQueryId());
            if (!pointer.isEmpty() && !continuationPointerManager.cancelPDQv3Query(pointer)) {
                log.warn("%s does not exist", pointer);
            }
            String sutOid = null;
            try {
                sutOid = inRequest.getSender().getDevice().getId().get(0).getRoot();
            } catch (NullPointerException e) {
                log.warn("No Sender/device/id element in request");
            }
            MCCIMT000300UV01Device device = null;
            try {
                MCCIMT000300UV01Receiver receiver = inRequest.getReceiver().get(0);
                device = receiver.getDevice();
            } catch (Exception e) {
                log.warn("Receiver is either not present or malformed");
            }
            response = MCCIIN000002UV01Builder
                    .buildAcceptAcknowledgement(sutOid, inRequest.getId(), device, HL7V3AcknowledgmentCode.AA.getValue());
            instance.getResponse().setType(PatientManagerConstants.MCCI_IN_00002_UV_01);
            sendResponse(MCCIIN000002UV01Type.class, response);
            HL7V3Utils.saveMessageId(inRequest.getId(), instance.getRequest());
            if (response != null) {
                HL7V3Utils.saveMessageId(response.getId(), instance.getResponse());
            }
            return response;
        }
    }

}
