package net.ihe.gazelle.simulator.application;

public interface Factory<T,C> {
    T make(C context);
}
