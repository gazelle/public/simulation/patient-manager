package net.ihe.gazelle.simulator.fhir.util;

import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import ca.uhn.fhir.rest.server.exceptions.BaseServerResponseException;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.interceptor.InterceptorAdapter;
import ca.uhn.fhir.rest.server.servlet.ServletRequestDetails;
import net.ihe.gazelle.fhir.constants.FhirConstants;
import net.ihe.gazelle.fhir.constants.FhirParserProvider;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.simulator.message.model.MessageInstanceMetadata;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pdqm.pds.PDQmQueryHandler;
import net.ihe.gazelle.simulator.pixm.manager.PIXmQueryHandler;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>GazelleInterceptor class.</p>
 * This we are using hapi mechanisms to design the server side, the RestfulPatientResourceProvider class does not access the actual content
 * sent by the tool and thus cannot log the transaction instance details. The purpose of this interceptor is to "read" each outgoing message
 * and save it if it is related to Patient resource
 *
 * @author aberge
 * @version 1.0: 06/11/17
 */

public class GazelleServerInterceptor extends InterceptorAdapter {

    private static final String PIX_OPERATION = "$ihe-pix";
    private static final String PATIENT_RESOURCE_NAME = "Patient";
    private static final Logger LOG = LoggerFactory.getLogger(GazelleServerInterceptor.class);

    @Override
    public boolean outgoingResponse(RequestDetails theRequestDetails, IBaseResource theResponseObject, HttpServletRequest theServletRequest,
                                    HttpServletResponse theServletResponse) throws AuthenticationException {
        logTransaction(theRequestDetails, theResponseObject, false);
        return super.outgoingResponse(theRequestDetails, theResponseObject, theServletRequest, theServletResponse);
    }

    @Override
    public boolean handleException(RequestDetails theRequestDetails, BaseServerResponseException theException, HttpServletRequest
            theServletRequest, HttpServletResponse theServletResponse) throws ServletException, IOException {
        logTransaction(theRequestDetails, theException);
        return super.handleException(theRequestDetails, theException, theServletRequest, theServletResponse);
    }


    private void logTransaction(final RequestDetails theRequestDetails, final BaseServerResponseException theException) {
        logTransaction(theRequestDetails, theException.getOperationOutcome(), true);
    }

    private void logTransaction(final RequestDetails theRequestDetails, final IBaseResource responseResource, final boolean exceptionRaised) {
        // do not log the transaction if it is not related to Patient resource (eg. metadata, StructureDefinition...)
        if (PATIENT_RESOURCE_NAME.equals(theRequestDetails.getResourceName())) {
            ServletRequestDetails details = (ServletRequestDetails) theRequestDetails;
            HttpServletRequest request = details.getServletRequest();
            String acceptHeader = null;
            // first try to read format in request parameters
            String[] values = theRequestDetails.getParameters().get("_format");
            EncodingEnum encoding = EncodingEnum.XML;
            MessageInstanceMetadata requestMetadata = null;

            if (values != null && values.length > 0) {
                acceptHeader = values[0];
            }

            if (acceptHeader == null) {
                // if _format is not used in the requested URL, try to read HTTP header instead
                acceptHeader = request.getHeader("Accept");
                if (acceptHeader != null) {
                    requestMetadata = new MessageInstanceMetadata();
                    requestMetadata.setLabel("Accept header");
                    requestMetadata.setValue(acceptHeader);
                    String[] acceptParts = acceptHeader.split(";");
                    for (String part : acceptParts) {
                        encoding = EncodingEnum.forContentType(part.trim());
                        if (encoding != null) {
                            break;
                        }
                    }
                }
            }
            if (acceptHeader != null) {
                encoding = EncodingEnum.forContentType(acceptHeader);
            }

            String requestContent = theRequestDetails.getCompleteUrl();
            String responseContent;
            String errorMessageType = null;
            IParser parser = FhirParserProvider.getParserForEncoding(encoding);
            responseContent = parser.encodeResourceToString(responseResource);
            if (exceptionRaised) {
                errorMessageType = FhirConstants.OPERATION_OUTCOME;
            }
            RestOperationTypeEnum requestType = theRequestDetails.getRestOperationType();
            String operation = theRequestDetails.getOperation();
            String callerIP = request.getRemoteHost();
            try {
                saveTransactionInstance(encoding, requestContent, responseContent, callerIP, operation, requestType, errorMessageType,
                        requestMetadata);
            } catch (InternalErrorException e) {
                LOG.warn("The transaction instance has not been saved: " + e.getMessage());
            }
        } else {
            LOG.debug("Not a Patient resource: " + theRequestDetails.getResourceName());
        }

    }

    private void saveTransactionInstance(final EncodingEnum format, final String requestContent, final String responseContent, final String callerIP,
                                         final String operation, final RestOperationTypeEnum requestType, final String messageTypeOnError, final
                                         MessageInstanceMetadata requestMetadata)
            throws InternalErrorException {
        try {
            HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                @Override
                public Object performAction(EntityManager entityManager, Object... context) throws Exception {
                    TransactionInstance instance;
                    if (PIX_OPERATION.equals(operation)) {
                        instance = PIXmQueryHandler.populatePIXmTransaction(format, requestContent, responseContent, callerIP);
                    } else {
                        instance = PDQmQueryHandler.populatePDQmTransactionInstance(format, requestContent, responseContent, callerIP, requestType);
                    }
                    if (messageTypeOnError != null) {
                        instance.getResponse().setType(messageTypeOnError);
                    }
                    instance = instance.save(entityManager);
                    if (requestMetadata != null) {
                        requestMetadata.setMessageInstance(instance.getRequest());
                        entityManager.merge(requestMetadata);
                        entityManager.flush();
                    }
                    return null;
                }
            });
        } catch (HibernateFailure e) {
            throw new InternalErrorException("Error while processing the query", e);
        }
    }

}
