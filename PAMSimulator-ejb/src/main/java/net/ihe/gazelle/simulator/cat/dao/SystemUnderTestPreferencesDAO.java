package net.ihe.gazelle.simulator.cat.dao;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.cat.model.EConnectathonMessageType;
import net.ihe.gazelle.simulator.cat.model.SystemUnderTestPreferences;
import net.ihe.gazelle.simulator.cat.model.SystemUnderTestPreferencesQuery;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.sut.model.SystemConfigurationQuery;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.jboss.seam.Component;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>SystemUnderTestPreferencesDAO class.</p>
 *
 * @author aberge
 * @version 1.0: 29/11/17
 */

public class SystemUnderTestPreferencesDAO {

    public static List<SelectItem> getListPossibleMessageTypes(SystemConfiguration configuration, boolean addNullEntry) {
        List<Transaction> transactions = new ArrayList<Transaction>();
        for (Usage usage : configuration.getListUsages()) {
            Transaction transaction = usage.getTransaction();
            if (!transactions.contains(transaction)) {
                transactions.add(usage.getTransaction());
            }
        }
        List<SelectItem> items = EConnectathonMessageType.getMessageTypesForTransactions(transactions);
        if (addNullEntry) {
            items.add(new SelectItem(null, "Please Select..."));
        }
        return items;
    }

    public static SystemUnderTestPreferences getPreferencesForSUT(SystemConfiguration systemToAdd) {
        SystemUnderTestPreferencesQuery query = new SystemUnderTestPreferencesQuery();
        query.systemUnderTest().id().eq(systemToAdd.getId());
        return query.getUniqueResult();
    }

    public static SystemUnderTestPreferences createPreferencesForSUT(SystemConfiguration systemToAdd) {
        SystemUnderTestPreferences preferences = new SystemUnderTestPreferences(systemToAdd);
        preferences.setLastUpdate(new Date());
        GazelleIdentity gazelleIdentity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
        if (gazelleIdentity.isLoggedIn()) {
            preferences.setLastModifier(gazelleIdentity.getUsername());
        }
        return save(preferences);
    }

    public static SystemUnderTestPreferences save(SystemUnderTestPreferences preferences) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        GazelleIdentity gazelleIdentity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
        if (gazelleIdentity.isLoggedIn()) {
            preferences.setLastModifier(gazelleIdentity.getUsername());
        }
        preferences.setLastUpdate(new Date());
        SystemUnderTestPreferences savedInstance = entityManager.merge(preferences);
        entityManager.flush();
        return savedInstance;
    }

    public static List<HierarchicDesignator> getAssigningAuthoritiesForSystem(SystemConfiguration systemConfiguration) {
        SystemUnderTestPreferences preferences = getPreferencesForSUT(systemConfiguration);
        if (preferences == null) {
            return new ArrayList<HierarchicDesignator>();
        } else {
            return preferences.getAssigningAuthorities();
        }
    }

    public static SystemUnderTestPreferences getPreferencesById(Integer sutPrefId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        return entityManager.find(SystemUnderTestPreferences.class, sutPrefId);
    }

    public static List<SystemConfiguration> getSUTWithoutPreferences() {
        SystemUnderTestPreferencesQuery query = new SystemUnderTestPreferencesQuery();
        List<Integer> prefIds = query.id().getListDistinct();
        SystemConfigurationQuery sysQuery = new SystemConfigurationQuery();
        sysQuery.isAvailable().eq(true);
        sysQuery.id().nin(prefIds);
        sysQuery.name().order(true);
        GazelleIdentity gazelleIdentity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
        if (gazelleIdentity.isLoggedIn()) {
            if (!(gazelleIdentity.hasRole("admin_role") || gazelleIdentity.hasRole("monitor_role"))) {
                String username = gazelleIdentity.getUsername();
                String company = gazelleIdentity.getOrganisationKeyword();
                sysQuery.addRestriction(HQLRestrictions.or(
                        sysQuery.owner().eqRestriction(username),
                        sysQuery.ownerCompany().eqRestriction(company),
                        sysQuery.isPublic().eqRestriction(true)));
            }
        } else {
            sysQuery.isPublic().eq(true);
        }
        return sysQuery.getList();
    }

    public static void deletePreferencesForSUT(Integer sutId) {
        SystemUnderTestPreferencesQuery query = new SystemUnderTestPreferencesQuery();
        query.systemUnderTest().id().eq(sutId);
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        SystemUnderTestPreferences pref = query.getUniqueResult();
        if(pref != null) {
            entityManager.remove(query.getUniqueResult());
        }
        entityManager.flush();
    }

    public static void deleteSystemUnderTestPreferences(Integer preferenceId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        SystemUnderTestPreferences pref = entityManager.find(SystemUnderTestPreferences.class, preferenceId);
        entityManager.remove(pref);
        entityManager.flush();
    }
}
