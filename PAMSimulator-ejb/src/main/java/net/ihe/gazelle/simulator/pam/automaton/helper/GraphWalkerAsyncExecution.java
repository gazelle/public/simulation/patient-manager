package net.ihe.gazelle.simulator.pam.automaton.helper;

import net.ihe.gazelle.HL7Common.messages.HL7Validator;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;

/**
 * Created by xfs on 19/11/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("graphWalkerAsyncExecution")
@Stateless
public class GraphWalkerAsyncExecution implements GraphWalkerAsyncExecutionInterface {


    /** {@inheritDoc} */
    public void asyncRun(GraphExecutionResults graphExecutionResults, Encounter encounter) {
        GraphWalkerExecution.run(graphExecutionResults, encounter);
    }

    /** {@inheritDoc} */
    @Override
    public void asyncReplay(GraphExecutionResults graphExecutionResults) {
        GraphWalkerExecution.replay(graphExecutionResults);
    }

    /**
     * <p>validateInstance.</p>
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public void validateInstance(TransactionInstance instance) {
        HL7Validator.validateTransactionInstance(instance);
    }

}
