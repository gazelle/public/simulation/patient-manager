package net.ihe.gazelle.simulator.pam.action;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;

/**
 * <b>Class Description : </b>PatientDisplayManager<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 29/09/15
 */
@Name("patientDisplayManager")
@Scope(ScopeType.PAGE)
public class PatientDisplayManager implements Serializable, UserAttributeCommon {

    private Patient selectedPatient;
    private static Logger log = LoggerFactory.getLogger(PatientDisplayManager.class);

    @In(value="gumUserService")
    private UserService userService;

    /**
     * <p>getPatientById.</p>
     */
    @Create
    public void getPatientById() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if ((params != null) && params.containsKey("id")) {
            try {
                Integer patientId = Integer.decode(params.get("id"));
                EntityManager entityManager = EntityManagerService.provideEntityManager();
                selectedPatient = entityManager.find(Patient.class, patientId);
            } catch (NumberFormatException e) {
                selectedPatient = null;
                log.error(e.getMessage(), e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The given id is not a positive Integer");
            }
        }
    }

    /**
     * Called in patientColumns.xhtml
     */
    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    /**
     * <p>Getter for the field <code>selectedPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }
}
