package net.ihe.gazelle.simulator.pam.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.pam.filter.EncounterFilter;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientStatus;

import java.util.Date;

public class EncounterDataModel extends FilterDataModel<Encounter> {

    private String visitNumber;
    private Patient patient;
    private String patientClass;
    private String creator;
    private Actor simulatedActor;
    private Date startDate;
    private Date endDate;
    private String enclosedTriggerEvent;
    private String movementUniqueId;
    private Boolean isAbsent;
    private Boolean isOpen;

    /**
     * false: inpatient only true: outpatient only null: both inpatient and outpatient
     */
    private Boolean isOutpatient;

    public EncounterDataModel(String visitNumber, Patient patient, Actor simulatedActor, String patientClass,
                              String creator, Date startDate, Date endDate, String triggerEvent, String movementUniqueId,
                              Boolean isAbsent, Boolean isOutpatient, Boolean isOpen) {
        super(new EncounterFilter());
        this.visitNumber = visitNumber;
        this.patientClass = patientClass;
        this.patient = patient;
        this.creator = creator;
        this.simulatedActor = simulatedActor;
        setStartDate(startDate);
        setEndDate(endDate);
        this.enclosedTriggerEvent = triggerEvent;
        this.movementUniqueId = movementUniqueId;
        this.isAbsent = isAbsent;
        this.isOutpatient = isOutpatient;
        this.isOpen = isOpen;
    }

    public String getVisitNumber() {
        return visitNumber;
    }

    public void setVisitNumber(String visitNumber) {
        this.visitNumber = visitNumber;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getPatientClass() {
        return patientClass;
    }

    public void setPatientClass(String patientClass) {
        this.patientClass = patientClass;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreator() {
        return creator;
    }

    public void setSimulatedActor(Actor simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    public void setStartDate(Date startDate) {
        if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        } else {
            this.startDate = null;
        }
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date endDate) {
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        } else {
            this.endDate = null;
        }
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEnclosedTriggerEvent(String enclosedTriggerEvent) {
        this.enclosedTriggerEvent = enclosedTriggerEvent;
    }

    public String getEnclosedTriggerEvent() {
        return enclosedTriggerEvent;
    }

    public void setMovementUniqueId(String movementUniqueId) {
        this.movementUniqueId = movementUniqueId;
    }

    public String getMovementUniqueId() {
        return movementUniqueId;
    }

    /**
     * @param isAbsent the isAbsent to set
     */
    public void setIsAbsent(Boolean isAbsent) {
        this.isAbsent = isAbsent;
    }

    /**
     * @return the isAbsent
     */
    public Boolean getIsAbsent() {
        return isAbsent;
    }

    /**
     * @param isOutpatient the isOutpatient to set
     */
    public void setIsOutpatient(Boolean isOutpatient) {
        this.isOutpatient = isOutpatient;
    }

    /**
     * @return the isOutpatient
     */
    public Boolean getIsOutpatient() {
        return isOutpatient;
    }

    /**
     * @param isOpen the isOpen to set
     */
    public void setIsOpen(Boolean isOpen) {
        this.isOpen = isOpen;
    }

    /**
     * @return the isOpen
     */
    public Boolean getIsOpen() {
        return isOpen;
    }

    @Override
    public void appendFiltersFields(HQLQueryBuilder<Encounter> queryBuilder) {
        if ((visitNumber != null) && !visitNumber.isEmpty()) {
            queryBuilder.addRestriction(HQLRestrictions.like("visitNumber", visitNumber,
                    HQLRestrictionLikeMatchMode.ANYWHERE));
        }
        if (patient != null) {
            queryBuilder.addRestriction(HQLRestrictions.eq("patient", patient));
        }
        if ((patientClass != null) && !patientClass.isEmpty()) {
            queryBuilder.addRestriction(HQLRestrictions.like("patientClass", patientClass,
                    HQLRestrictionLikeMatchMode.EXACT));
        }
        if ((creator != null) && !creator.isEmpty()) {
            queryBuilder.addRestriction(HQLRestrictions.like("creator", creator, HQLRestrictionLikeMatchMode.EXACT));
        }
        if (simulatedActor != null) {
            queryBuilder.addRestriction(HQLRestrictions.eq("simulatedActor", simulatedActor));
        }
        if (endDate != null) {
            queryBuilder.addRestriction(HQLRestrictions.lt("creationDate", endDate));
        }
        if (startDate != null) {
            queryBuilder.addRestriction(HQLRestrictions.gt("creationDate", startDate));
        }
        if ((enclosedTriggerEvent != null) && !enclosedTriggerEvent.isEmpty()) {
            queryBuilder.addRestriction(HQLRestrictions.like("movements.triggerEvent", enclosedTriggerEvent,
                    HQLRestrictionLikeMatchMode.EXACT));
        }
        if ((movementUniqueId != null) && !movementUniqueId.isEmpty()) {
            queryBuilder.addRestriction(HQLRestrictions.like("movements.movementUniqueId", movementUniqueId,
                    HQLRestrictionLikeMatchMode.ANYWHERE));
        }
        if (isAbsent != null) {
            queryBuilder.addRestriction(HQLRestrictions.eq("leaveOfAbsence", isAbsent));
        }
        // we need an outpatient, that means the encounter with a A04 or A06 event
        if ((isOutpatient != null) && isOutpatient) {
            queryBuilder.addEq("patientStatus", PatientStatus.STATUS_REGISTERED);
        }
        // we need an inpatient, that means an encounter with A01, A02, A14, A07 or A05 event
        if ((isOutpatient != null) && !isOutpatient) {
            queryBuilder.addRestriction(HQLRestrictions.or(
                    HQLRestrictions.neq("patientStatus", PatientStatus.STATUS_ADMITTED),
                    HQLRestrictions.neq("patientStatus", PatientStatus.STATUS_PRE_ADMITTED)));
        }
        if (isOpen != null) {
            queryBuilder.addRestriction(HQLRestrictions.eq("open", isOpen));
        }
    }

    @Override
    protected Object getId(Encounter t) {
        // TODO Auto-generated method stub
        return t.getId();
    }
}
