package net.ihe.gazelle.simulator.pam.iti31.util;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.iti31.action.NumberGenerator;
import net.ihe.gazelle.simulator.pam.iti31.model.ActionType;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.PatientStatus;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

/**
 * <p>IHEPESEventHandler class.</p>
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes - Bretagne atlantique
 * @version 1.0 - September 6th, 2011
 */
public class IHEPESEventHandler implements PESEventHandler {

    private static Log log = Logging.getLog(IHEPESEventHandler.class);

    private Encounter encounter;
    private Movement movement;
    private Actor simulatedActor;
    private EntityManager entityManager;
    private TriggerEventEnum event;

    /**
     * <p>Constructor for IHEPESEventHandler.</p>
     */
    public IHEPESEventHandler() {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttributes(Encounter inEncounter, Movement inMovement, String triggerEvent, Actor simulatedActor,
                              EntityManager entityManager) {
        this.encounter = inEncounter;
        this.movement = inMovement;
        this.event = TriggerEventEnum.getEventForTrigger(triggerEvent);
        this.simulatedActor = simulatedActor;
        this.entityManager = entityManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Movement processInsert() {
        switch (event) {
            case A01:
                // process A01: admit
                handleA01Event();
                break;
            case A04:
                // process A04: register
                handleA04Event();
                break;
            case A03:
                // process A03 : discharge
                handleA03Event();
                break;
            case A05:
                // process A05: pre-admit
                handleA05Event();
                break;
            case A06:
                // process A06: change Outpatient to inpatient
                handleA06Event();
                break;
            case A07:
                // process A07: change inpatient to outpatient
                handleA07Event("O");
                break;
            case A02:
                // process A02: transfer
                handleA02Event();
                break;
            case A21:
                // process A21: leave of absence
                handleA21Event();
                break;
            case A22:
                // process A22: return from leave of absence
                handleA22Event();
                break;
            case A44:
                // process A44: move account information
                handleA44Event();
                break;
            case A54:
                // process A54: change attending doctor
                handleA54Event();
                break;
            default:
                // not yet handled events
                log.error("event " + event.getTriggerEvent() + " is not yet handled by the simulator");
        }
        movement.setEncounter(encounter);
        movement.setLastChanged(new Date());
        movement = movement.save(entityManager);
        return movement;
    }


    private void handleA54Event() {
        closePreviousMovement(true);
        generateMovementIdIfNecessary();
        movement.setTriggerEvent("A54");
        movement.setActionType(ActionType.INSERT);
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);
    }

    private void handleA44Event() {
        //there is nothing to do there, no "real" movement occurs for the patient
    }

    private void handleA22Event() {
        PatientStatus priorStatus = encounter.getPatientStatus();
        encounter.setPatientStatus(encounter.getPriorPatientStatus());
        encounter.setPriorPatientStatus(priorStatus);
        encounter.setLeaveOfAbsence(false);
        closePreviousMovement(true);
        generateMovementIdIfNecessary();
        movement.setTriggerEvent("A22");
        movement.setActionType(ActionType.INSERT);
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);
    }

    private void closePreviousMovement(boolean setAssignedLocation) {
        Movement lastMovement = movement.getPreviousValidMovement(entityManager, true);
        if (lastMovement != null) {
            log.info("last movement " + lastMovement.getMovementUniqueId());
            lastMovement.setLastChanged(new Date());
            lastMovement.setCurrentMovement(false);
            lastMovement.save(entityManager);
            if (setAssignedLocation) {
                movement.setAssignedPatientLocation(lastMovement.getAssignedPatientLocation());
            }
        } else {
            log.error("unable to retrieve the last movement");
        }
    }

    private void generateMovementIdIfNecessary() {
        if ((movement.getMovementUniqueId() == null) || movement.getMovementUniqueId().isEmpty()) {
            movement.setMovementUniqueId(NumberGenerator.generate(DesignatorType.MOVEMENT_ID));
        }
    }

    private void handleA21Event() {
        encounter.setPriorPatientStatus(encounter.getPatientStatus());
        encounter.setLeaveOfAbsence(true);
        encounter.setPatientStatus(PatientStatus.STATUS_TEMPORARILY_ABSENT);
        // close previous movement
        encounter = encounter.save(entityManager);

        closePreviousMovement(true);
        generateMovementIdIfNecessary();
        movement.setTriggerEvent("A21");
        movement.setActionType(ActionType.INSERT);
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);
    }

    /**
     * <p>processEvent.</p>
     *
     * @param patientClassCode a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
     */
    public Movement processEvent(String patientClassCode) {

        movement = movement.save(entityManager);
        if (!encounter.isOpen()) {
            encounter.setVisitNumber(NumberGenerator.generate(DesignatorType.VISIT_NUMBER));
            encounter.setOpen(true);
        }

        switch (event) {
            case A01:
                // process A01: admit
                handleA01Event();
                break;
            case A04:
                // process A04: register
                handleA04Event();
                break;
            case A03:
                // process A03 : discharge
                handleA03Event();
                break;
            case A05:
                // process A05: pre-admit
                handleA05Event();
                break;
            case A06:
                // process A06: change Outpatient to inpatient
                handleA06Event();
                break;
            case A07:
                // process A07: change inpatient to outpatient
                handleA07Event(patientClassCode);
                break;
            case A02:
                // process A02: transfer
                handleA02Event();
                break;
            case A21:
                // process A21: leave of absence
                handleA21Event();
                break;
            case A22:
                // process A22: return from leave of absence
                handleA22Event();
                break;
            case A44:
                // process A44: move account information
                handleA44Event();
                break;
            case A54:
                // process A54: change attending doctor
                handleA54Event();
                break;
            case A13:
                // process A13 :cancel discharge: the encounter is re-opened
                handleA13Event();
                encounter.save(entityManager);

                break;
            case A11:
                // process A11 :cancel admit or registration: the encounter is closed
                handleA11Event();
                encounter.save(entityManager);

                break;
            case A38:
                // process A38 ; cancel pre-admission
                handleA38Event();
                encounter.save(entityManager);

                break;
            case A12:
                // process A12 : cancel transfer
                handleA12Event();
                encounter.save(entityManager);

                break;
            case A52:
                // process A52 :cancel leave of absence
                handleA52Event();
                encounter.save(entityManager);
                break;
            case A53:
                // process A53 : cancel return from leave of absence
                handleA53Event();
                encounter.save(entityManager);
                break;
            case A55:
                // process A55 : cancel change of doctor
                handleA55Event();
                encounter.save(entityManager);
                break;
            default:
                // not yet handled events
                log.error("event " + event.getTriggerEvent() + " is not yet handled by the simulator");
        }

        return movement;

    }

    private void handleA11Event() {
        initCancelMovement();
        encounter.setPatientStatus(PatientStatus.STATUS_ENCOUNTER_CANCELLED);
        encounter.setOpen(false);
    }

    private void handleA13Event() {
        initCancelMovement();
        encounter.setOpen(true);
        encounter.setDischargeDate(null);
        encounter.setPatientStatus(encounter.getPriorPatientStatus());
    }

    private void handleA38Event() {
        initCancelMovement();
        encounter.setPatientStatus(PatientStatus.STATUS_ENCOUNTER_CANCELLED);
        encounter.setOpen(false);
    }

    private void handleA12Event() {
        initCancelMovement();
    }

    private void handleA52Event() {
        initCancelMovement();
        encounter.setPatientStatus(encounter.getPriorPatientStatus());
        encounter.setExpectedLOAReturn(null);
        encounter.setLeaveOfAbsenceDate(null);
        encounter.setLeaveOfAbsence(false);
    }

    private void handleA53Event() {
        initCancelMovement();
        encounter.setPatientStatus(PatientStatus.STATUS_TEMPORARILY_ABSENT);
        encounter.setLeaveOfAbsence(true);
        encounter.setLoaReturnDate(null);
    }

    private void handleA55Event() {
        initCancelMovement();
        encounter.setAttendingDoctorCode(encounter.getPriorAttendingDoctorCode());
        encounter.setPriorAttendingDoctorCode(null);
    }

    private void initCancelMovement() {
        Movement previousMovement = movement.getPreviousValidMovement(entityManager, false);
        if (previousMovement != null) {
            log.info("previous movement: " + previousMovement.getTriggerEvent());
            previousMovement.setCurrentMovement(true);
            previousMovement.setLastChanged(new Date());
            previousMovement.save(entityManager);
            movement.setAssignedPatientLocation(previousMovement.getAssignedPatientLocation());
        } else {
            log.info("No valid previous movement found !");
        }
        // change movement's status to cancelled
        movement.setActionType(ActionType.CANCEL);
        movement.setCurrentMovement(false);
        movement.setLastChanged(new Date());
        movement = movement.save(entityManager);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Movement processCancel() {
        Movement previousMovement = movement.getPreviousValidMovement(entityManager, false);
        if (previousMovement != null) {
            log.info("previous movement: " + previousMovement.getTriggerEvent());
            previousMovement.setCurrentMovement(true);
            previousMovement.setLastChanged(new Date());
            previousMovement.save(entityManager);
            movement.setAssignedPatientLocation(previousMovement.getAssignedPatientLocation());
        } else {
            log.info("No valid previous movement found !");
        }
        // change movement's status to cancelled
        movement.setActionType(ActionType.CANCEL);
        movement.setCurrentMovement(false);
        movement.setLastChanged(new Date());
        movement = movement.save(entityManager);
        switch (event) {
            case A13:
                // cancel discharge: the encounter is re-opened
                encounter.setOpen(true);
                encounter.setDischargeDate(null);
                encounter.setPatientStatus(encounter.getPriorPatientStatus());
                break;
            case A11:
                // cancel admit or registration: the encounter is closed except if the patient was pre-admitted
                if (encounter.getPriorPatientStatus() == null) {
                    encounter.setPatientStatus(PatientStatus.STATUS_ENCOUNTER_CANCELLED);
                    encounter.setOpen(false);
                } else {
                    encounter.setPatientStatus(encounter.getPriorPatientStatus());
                }
                break;
            case A38:
                // cancel pre-admission
                encounter.setPatientStatus(PatientStatus.STATUS_ENCOUNTER_CANCELLED);
                encounter.setOpen(false);
                break;
            case A12:
                // cancel transfer
                break;
            case A52:
                encounter.setPatientStatus(encounter.getPriorPatientStatus());
                encounter.setExpectedLOAReturn(null);
                encounter.setLeaveOfAbsenceDate(null);
                encounter.setLeaveOfAbsence(false);
                // cancel leave of absence
                break;
            case A53:
                encounter.setPatientStatus(PatientStatus.STATUS_TEMPORARILY_ABSENT);
                encounter.setLeaveOfAbsence(true);
                encounter.setLoaReturnDate(null);
                // cancel return from leave of absence
                break;
            case A55:
                encounter.setAttendingDoctorCode(encounter.getPriorAttendingDoctorCode());
                encounter.setPriorAttendingDoctorCode(null);
                // cancel change of doctor
                break;
            default:
                log.info("this trigger event is not handled");
        }
        encounter.save(entityManager);
        return movement;
    }

    /**
     * GETTERS AND SETTERS
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public Encounter getEncounter() {
        return encounter;
    }

    /**
     * <p>Setter for the field <code>encounter</code>.</p>
     *
     * @param encounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public void setEncounter(Encounter encounter) {
        this.encounter = encounter;
    }

    /**
     * <p>Getter for the field <code>movement</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
     */
    public Movement getMovement() {
        return movement;
    }

    /**
     * <p>Setter for the field <code>movement</code>.</p>
     *
     * @param movement a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
     */
    public void setMovement(Movement movement) {
        this.movement = movement;
    }

    /**
     * <p>Setter for the field <code>simulatedActor</code>.</p>
     *
     * @param simulatedActor the simulatedActor to set
     */
    public void setSimulatedActor(Actor simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return the simulatedActor
     */
    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Getter for the field <code>entityManager</code>.</p>
     *
     * @return a {@link javax.persistence.EntityManager} object.
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * <p>Setter for the field <code>entityManager</code>.</p>
     *
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    private void manageClosureOfPreadmission() {
        if (encounter.getPatientStatus() != null && encounter.getPatientStatus().equals(PatientStatus.STATUS_PRE_ADMITTED)) {
            closePreviousMovement(false);
            encounter.setPriorPatientStatus(PatientStatus.STATUS_PRE_ADMITTED);
            encounter.setPriorAttendingDoctorCode(encounter.getAttendingDoctorCode());
            encounter.setPriorPatientClassCode(encounter.getPatientClass());
        } else {
            if ((encounter.getVisitNumber() == null) || encounter.getVisitNumber().isEmpty()) {
                encounter.setVisitNumber(NumberGenerator.generate(DesignatorType.VISIT_NUMBER));
            }
            encounter.setCreationDate(new Date());
            encounter.setOpen(true);
            encounter.setLeaveOfAbsence(false);
        }
    }

    /**
     * admit patient
     */
    private void handleA01Event() {

       /* if ((encounter.getVisitNumber() == null) || encounter.getVisitNumber().isEmpty()) {
            encounter.setVisitNumber(NumberGenerator.generate(NumberGenerator.VISIT_NUMBER, entityManager));
        }*/

        encounter.setPatientStatus(PatientStatus.STATUS_ADMITTED);
        if (encounter.getPatientClassCode() == null) {
            encounter.setPatientClassCode("I");
        }
        manageClosureOfPreadmission();
        generateMovementIdIfNecessary();
        movement.setTriggerEvent("A01");
        movement.setActionType(ActionType.INSERT);
        movement.setAssignedPatientLocation(movement.buildLocation());
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);

    }

    /**
     * discharge patient
     */
    private void handleA03Event() {
        // check if the patient is an inpatient or an outpatient
        encounter.setPriorPatientStatus(encounter.getPatientStatus());
        encounter.setPatientStatus(PatientStatus.DISCHARGED);
        encounter.setOpen(false);
        encounter = encounter.save(entityManager);
        // retrieve the last movement to retrieve the patient's current location
        closePreviousMovement(true);
        // if a discharge is pending, set pending to false
        List<Movement> movements = Movement.getMovementsFiltered(encounter, simulatedActor, null, null, "A16", null,
                true, null, null, null, entityManager);
        if (movements != null) {
            for (Movement mov : movements) {
                mov.setPendingMovement(false);
                mov.save(entityManager);
            }
        } else {
            log.info("no pending discharge");
        }
        // set the current movement as current
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);
    }

    /**
     * register patient
     */
    private void handleA04Event() {
        if (encounter.getPatientClassCode() == null) {
            encounter.setPatientClassCode("O");
        }
        manageClosureOfPreadmission();
        encounter.setPatientStatus(PatientStatus.STATUS_REGISTERED);
        generateMovementIdIfNecessary();
        movement.setActionType(ActionType.INSERT);
        movement.setTriggerEvent("A04");
        movement.setAssignedPatientLocation(null);
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);

    }

    /**
     * transfer patient
     */
    private void handleA02Event() {
        if (encounter.getId() != null) {
            closePreviousMovement(false);
        }
        // in some cases, the transfer is received for an unknown patient or encounter
        // both should be created in this case
        else {
            encounter.setCreationDate(new Date());
            encounter.setPatientStatus(PatientStatus.STATUS_ADMITTED);
            encounter.setOpen(true);
            encounter.setLeaveOfAbsence(false);
            encounter.setPriorAttendingDoctorCode(null);
            encounter.setPriorPatientStatus(null);
        }
        if ((movement.getMovementUniqueId() == null) || movement.getMovementUniqueId().isEmpty()) {
            movement.setMovementUniqueId(NumberGenerator.generate(DesignatorType.MOVEMENT_ID));
        }
        movement.setAssignedPatientLocation(movement.buildLocation());
        movement.setTriggerEvent("A02");
        movement.setActionType(ActionType.INSERT);
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);
    }

    /**
     * change inpatient to outpatient
     */
    private void handleA07Event(String patientClassCode) {
        if (patientClassCode.equals("E")) {
            encounter.setPatientClassCode("E");
        } else {
            encounter.setPatientClassCode("O");
        }
        encounter.setPatientStatus(PatientStatus.STATUS_REGISTERED);
        // if it is a new encounter
        if (encounter.getId() == null) {
            if ((encounter.getVisitNumber() == null) || encounter.getVisitNumber().isEmpty()) {
                encounter.setVisitNumber(NumberGenerator.generate(DesignatorType.VISIT_NUMBER));
            }
            encounter.setPriorAttendingDoctorCode(null);
            encounter.setPriorPatientStatus(null);
            encounter.setOpen(true);
            encounter.setLeaveOfAbsence(false);
        }
        // close previous movement
        else {
            closePreviousMovement(false);
        }
        generateMovementIdIfNecessary();
        movement.setAssignedPatientLocation(movement.buildLocation());
        movement.setTriggerEvent("A07");
        movement.setActionType(ActionType.INSERT);
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);
    }

    /**
     * change outpatient to inpatient
     */
    private void handleA06Event() {

        encounter.setPatientClassCode("I");
        encounter.setPatientStatus(PatientStatus.STATUS_ADMITTED);
        // if it is a new encounter
        if (encounter.getId() == null) {
            if (encounter.getVisitNumber() == null || encounter.getVisitNumber().isEmpty()) {
                encounter.setVisitNumber(NumberGenerator.generate(DesignatorType.VISIT_NUMBER));
            }
            encounter.setPriorAttendingDoctorCode(null);
            encounter.setPriorPatientStatus(null);
            encounter.setOpen(true);
            encounter.setLeaveOfAbsence(false);
        }
        // close previous movement
        else {
            closePreviousMovement(false);
        }
        generateMovementIdIfNecessary();
        movement.setAssignedPatientLocation(movement.buildLocation());
        movement.setTriggerEvent("A06");
        movement.setActionType(ActionType.INSERT);
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);
    }

    /**
     * Pre-admit patient
     */
    private void handleA05Event() {
        if ((encounter.getVisitNumber() == null) || encounter.getVisitNumber().isEmpty()) {
            encounter.setVisitNumber(NumberGenerator.generate(DesignatorType.VISIT_NUMBER));
        }
        encounter.setPatientStatus(PatientStatus.STATUS_PRE_ADMITTED);
        encounter.setOpen(true);
        encounter.setLeaveOfAbsence(false);
        encounter.setPriorAttendingDoctorCode(null);
        encounter.setPriorPatientStatus(null);
        if ((movement.getMovementUniqueId() == null) || movement.getMovementUniqueId().isEmpty()) {
            movement.setMovementUniqueId(NumberGenerator.generate(DesignatorType.MOVEMENT_ID));
        }
        movement.setTriggerEvent("A05");
        movement.setActionType(ActionType.INSERT);
        movement.setAssignedPatientLocation(movement.buildLocation());
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);
        if (encounter.getPatientClassCode() == null) {
            encounter.setPatientClassCode("I");
        }
    }
}
