package net.ihe.gazelle.simulator.hl7v3.messages;

import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.PN;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01QueryAck;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02MFMIMT700711UV01Subject1;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02MFMIMT700711UV01Subject2;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201304UV02.PRPAMT201304UV02Patient;
import net.ihe.gazelle.hl7v3.prpamt201304UV02.PRPAMT201304UV02Person;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.jboss.seam.faces.FacesMessages;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>PRPAIN201310UV02Parser class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PRPAIN201310UV02Parser extends HL7v3MessageParser {

    private FacesMessages facesMessages;
    private boolean moreResultsAvailable;

    /**
     * <p>Constructor for PRPAIN201310UV02Parser.</p>
     *
     * @param inFacesMessages a {@link org.jboss.seam.faces.FacesMessages} object.
     */
    public PRPAIN201310UV02Parser(FacesMessages inFacesMessages) {
        this.facesMessages = inFacesMessages;
        this.moreResultsAvailable = false;
    }

    /**
     * <p>extractPatient.</p>
     *
     * @param response an array of byte.
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @throws javax.xml.bind.JAXBException if any.
     */
    public Patient extractPatient(byte[] response) throws JAXBException {
        Patient patient = new Patient();

        if (response != null) {
            String messageString = new String(response, StandardCharsets.UTF_8);
            messageString = messageString.replace("UTF8", "UTF-8");
            PRPAIN201310UV02Type hl7v3Response = HL7V3Transformer.unmarshallMessage(PRPAIN201310UV02Type.class,
                    new ByteArrayInputStream(messageString.getBytes(StandardCharsets.UTF_8)));
            messageId = hl7v3Response.getId();
            String responseCode = hl7v3Response.getControlActProcess().getQueryAck().getQueryResponseCode().getCode();

            if (responseCode.equals(HL7V3ResponseCode.OK.getValue())) {
                facesMessages.add("This response contains " + hl7v3Response.getControlActProcess().getSubject().size()
                        + " matches");
                for (PRPAIN201310UV02MFMIMT700711UV01Subject1 subject : hl7v3Response.getControlActProcess()
                        .getSubject()) {
                    PRPAIN201310UV02MFMIMT700711UV01Subject2 subject1 = subject.getRegistrationEvent().getSubject1();
                    PRPAMT201304UV02Patient hl7v3Patient = subject1.getPatient();
                    PRPAMT201304UV02Person patientPerson = hl7v3Patient.getPatientPerson();
                    List<II> ids = hl7v3Patient.getId();
                    if (!ids.isEmpty()) {
                        patient.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
                        for (II id : ids) {
                            patient.getPatientIdentifiers().add(
                                    new PatientIdentifier(id.getExtension() + "^^^" + id.getAssigningAuthorityName()
                                            + "&" + id.getRoot(), "PI"));
                        }
                    }

                    if (!patientPerson.getName().isEmpty()) {
                        List<PN> names = patientPerson.getName();
                        setPatientNames(names, patient);
                    }
                }
                MFMIMT700711UV01QueryAck queryAck = hl7v3Response.getControlActProcess().getQueryAck();
                if (queryAck.getResultRemainingQuantity() != null) {
                    Integer remainingQuantity = queryAck.getResultRemainingQuantity().getValue();
                    moreResultsAvailable = (remainingQuantity != null && remainingQuantity > FIRST_OCCURRENCE);
                    facesMessages.add(remainingQuantity + " additional patient(s) available on supplier side");
                }
            } else if (responseCode.equals(HL7V3ResponseCode.NF.getValue())) {
                facesMessages.add("No match found for this query");
            } else if (responseCode.equals(HL7V3ResponseCode.AE.getValue())) {
                facesMessages
                        .add("AE code received : your query is not correct, check that at least one parameter is set");
            } else {
                facesMessages.add("Unknown query response code received : " + responseCode);
            }

        } else {
            facesMessages.add("No response received from the supplier");
        }
        return patient;
    }

    /**
     * <p>isMoreResultsAvailable.</p>
     *
     * @return a boolean.
     */
    public boolean isMoreResultsAvailable() {
        return moreResultsAvailable;
    }

}
