package net.ihe.gazelle.simulator.pam.action;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.hl7.HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.IHEHL7Domain;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.Person;
import net.ihe.gazelle.simulator.pam.util.PatientIdentifierUtil;
import net.ihe.gazelle.simulator.pam.utils.Pair;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by aberge on 03/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("patientCreator")
@Scope(ScopeType.PAGE)
public class PatientCreator extends PatientSelectorManager {


    private static final Logger LOG = LoggerFactory.getLogger(PatientCreator.class);
    private static final String ACTOR_PARAM_KEY = "actor";
    private static final String PATIENT_ID_PARAM = "id";

    private Patient selectedPatient;
    private Actor ownerActor;
    // id management
    private String newPatientIdentifier;
    private List<Pair<Integer, String>> patientIdentifiers;
    private Integer idForUpdate;

    /**
     * <p>Getter for the field <code>idForUpdate</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getIdForUpdate() {
        return idForUpdate;
    }

    /**
     * <p>Setter for the field <code>idForUpdate</code>.</p>
     *
     * @param idForUpdate a {@link java.lang.Integer} object.
     */
    public void setIdForUpdate(Integer idForUpdate) {
        this.idForUpdate = idForUpdate;
    }


    /**
     * <p>onCreate.</p>
     */
    @Create
    public void onCreate() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey(ACTOR_PARAM_KEY)){
            ownerActor = Actor.findActorWithKeyword(urlParams.get(ACTOR_PARAM_KEY));
        }
        if (urlParams.containsKey(PATIENT_ID_PARAM)){
            String idString = urlParams.get(PATIENT_ID_PARAM);
            try{
                Integer id = Integer.parseInt(idString);
                selectedPatient = PatientDAO.getPatientById(id);
                if (selectedPatient == null){
                    FacesMessages.instance().add(idString + " does not match any patient in the database");
                } else if (!PatientManagerConstants.CONNECTATHON.equals(selectedPatient.getSimulatedActor().getKeyword())){
                    selectedPatient = null;
                    FacesMessages.instance().add("Only patients to be used for connectathon purpose can be edited");
                }else {
                    ownerActor = selectedPatient.getSimulatedActor();
                    generatePatientIdentifiersList();
                    displayDDSPanel = false;
                }
            }catch (NumberFormatException e){
                FacesMessages.instance().add(idString + " is not a valid patient identifier");
            }
        } else {
            super.initialize();
            displayDDSPanel = true;
        }
    }

    @Override
    protected void initializeListOfAvailableAuthorities(){
        if (ownerActor != null && PatientManagerConstants.CONNECTATHON.equals(ownerActor.getKeyword())) {
            setGenerateIdsForConnectathon(true);
            setGenerateIdsForAuthorities(false);
            setUseIdsFromDDS(false);
            setSelectedAuthorities(new ArrayList<HierarchicDesignator>());
        } else {
            setAvailableAuthorities(HierarchicDesignatorDAO.getToolsAssigningAuthoritiesNotForConnectathon(DesignatorType.PATIENT_ID));
        }
    }

    /**
     * <p>getListOfAvailableActors.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Actor> getListOfAvailableActors() {
        return Actor.listAllActors();
    }


    /**
     * <p>Getter for the field <code>selectedPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    /**
     * <p>Setter for the field <code>selectedPatient</code>.</p>
     *
     * @param selectedPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    /** {@inheritDoc} */
    @Override
    public void createNewRelationship() {
        setCurrentRelationship(new Person(selectedPatient));
    }

    /** {@inheritDoc} */
    @Override
    public void saveRelationship() {
        selectedPatient.addPersonalRelationship(getCurrentRelationship());
        setCurrentRelationship(null);
    }

    /**
     * <p>Getter for the field <code>ownerActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getOwnerActor() {
        return ownerActor;
    }

    /**
     * <p>Setter for the field <code>ownerActor</code>.</p>
     *
     * @param ownerActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public void setOwnerActor(Actor ownerActor) {
        this.ownerActor = ownerActor;
    }

    /** {@inheritDoc} */
    @Override
    public Actor getSendingActor() {
        return this.ownerActor;
    }

    /**
     * <p>generateAndSavePatient.</p>
     */
    public void generateAndSavePatient() {
        selectedPatient = generateNewPatient(this.ownerActor);
        if (selectedPatient != null) {
            displayDDSPanel = false;
            displayPatientsList = false;
            generatePatientIdentifiersList();
        }
    }

    /**
     * <p>savePatient.</p>
     */
    public void savePatient() {
        List<PatientIdentifier> pidList = PatientIdentifierDAO.savePatientIdentifierList(selectedPatient
                .getPatientIdentifiers());
        selectedPatient.setPatientIdentifiers(pidList);
        selectedPatient = selectedPatient.savePatient(EntityManagerService.provideEntityManager());
        FacesMessages.instance().add(StatusMessage.Severity.INFO,"Patient has been saved");
    }

    /**
     * <p>savePatientAndReset.</p>
     */
    public void savePatientAndReset() {
        savePatient();
        selectedPatient = null;
        displayDDSPanel = true;
    }

    /**
     * <p>Getter for the field <code>newPatientIdentifier</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNewPatientIdentifier() {
        return newPatientIdentifier;
    }

    /**
     * <p>Setter for the field <code>newPatientIdentifier</code>.</p>
     *
     * @param newPatientIdentifier a {@link java.lang.String} object.
     */
    public void setNewPatientIdentifier(String newPatientIdentifier) {
        this.newPatientIdentifier = newPatientIdentifier;
    }

    /**
     * <p>Setter for the field <code>patientIdentifiers</code>.</p>
     *
     * @param patientIdentifiers a {@link java.util.List} object.
     */
    public void setPatientIdentifiers(List<Pair<Integer, String>> patientIdentifiers) {
        this.patientIdentifiers = patientIdentifiers;
    }

    /**
     * <p>Getter for the field <code>patientIdentifiers</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Pair<Integer, String>> getPatientIdentifiers() {
        return patientIdentifiers;
    }

    private void generatePatientIdentifiersList() {
        patientIdentifiers = PatientIdentifierUtil.generatePatientIdentifierList(selectedPatient);
    }

    /**
     * <p>changeIdentifier.</p>
     *
     * @param pidIndex a {@link java.lang.Integer} object.
     * @param newIdString a {@link java.lang.String} object.
     */
    public void changeIdentifier(Integer pidIndex, String newIdString) {
        String[] pidComponents = newIdString.split("\\^");
        String fullId;
        String newIdentifierTypeCode;
        if (pidComponents.length > 4) {
            int cx5Index = newIdString.lastIndexOf('^');
            fullId = newIdString.substring(0, cx5Index);
            newIdentifierTypeCode = newIdString.substring(cx5Index + 1, newIdString.length());
        } else {
            fullId = newIdString;
            newIdentifierTypeCode = null;
        }
        PatientIdentifier newIdentifier = new PatientIdentifier(fullId, newIdentifierTypeCode);
        PatientIdentifier modifiedPid = selectedPatient.getPatientIdentifiers().get(pidIndex);
        if (!newIdentifier.equals(modifiedPid)) {
            removeIdentifier(modifiedPid);
            selectedPatient.getPatientIdentifiers().add(newIdentifier);
            generatePatientIdentifiersList();
        }
        idForUpdate = null;
    }

    /**
     * <p>addIdentifier.</p>
     */
    public void addIdentifier() {
        if ((newPatientIdentifier != null) && !newPatientIdentifier.isEmpty()) {
            String[] pidComponents = newPatientIdentifier.split("\\^");
            String newIdentifierTypeCode = null;
            if (pidComponents.length > 4) {
                int cx5Index = newPatientIdentifier.lastIndexOf('^');
                newIdentifierTypeCode = newPatientIdentifier.substring(cx5Index + 1, newPatientIdentifier.length());
            } else {
                newIdentifierTypeCode = null;
            }
            PatientIdentifier id = new PatientIdentifier(newPatientIdentifier, newIdentifierTypeCode);
            if (selectedPatient.getPatientIdentifiers() == null) {
                List<PatientIdentifier> identifiers = new ArrayList<PatientIdentifier>();
                identifiers.add(id);
                selectedPatient.setPatientIdentifiers(identifiers);
            } else {
                selectedPatient.getPatientIdentifiers().add(id);
            }
            newPatientIdentifier = null;
        }
        generatePatientIdentifiersList();
    }

    /**
     * <p>removeIdentifier.</p>
     *
     * @param pidIndex a {@link java.lang.Integer} object.
     */
    public void removeIdentifier(Integer pidIndex) {
        PatientIdentifier removedPid = selectedPatient.getPatientIdentifiers().get(pidIndex);
        removeIdentifier(removedPid);
        generatePatientIdentifiersList();
    }

    private void removeIdentifier(PatientIdentifier identifier) {
        if (selectedPatient.getPatientIdentifiers().contains(identifier)) {
            selectedPatient.getPatientIdentifiers().remove(identifier);
        } else {
            LOG.error("this identifier is not linked to the current patient");
        }
    }

    public  void createNewPhoneNumber(){
        PatientPhoneNumber patientPhoneNumber = new PatientPhoneNumber();
        patientPhoneNumber.setPatient(selectedPatient);
        setNewPhoneNumber(patientPhoneNumber);
    }

    public void addNewPhoneNumber() {
        if (this.getNewPhoneNumber().getValue() == null || this.getNewPhoneNumber().getValue().isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Phone number shall have a value");
        } else {
            if (selectedPatient != null) {
                selectedPatient.addPhoneNumber(getNewPhoneNumber());
            }
            setNewPhoneNumber(null);
        }
    }

    protected Class<? extends HL7Domain<?>> getHL7DomainClass() {
        return IHEHL7Domain.class;
    }


}
