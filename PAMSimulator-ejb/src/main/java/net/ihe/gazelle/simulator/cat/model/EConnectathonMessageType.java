package net.ihe.gazelle.simulator.cat.model;

import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>EConnectathonMessageType enum.</p>
 *
 * @author aberge
 * @version 1.0: 29/11/17
 */
public enum EConnectathonMessageType {

    A28_PAM_CREATE_PATIENT("ADT^A28^ADT_A05/v2.5 (Create patient)", "ITI-30"),
    A01_PAM_ADMIT_PATIENT("ADT^A01^ADT_A01/v2.5 (Admit inpatient)", "ITI-31"),
    A01_PIX_ADMIT_PATIENT("ADT^A01^ADT_A01/v2.3.1 (Admit inpatient)", "ITI-8"),
    PIXV3_ADD_RECORD("PRPA_IN201301UV02 (Add record)", "ITI-44"),
    A01_ADT_ADMIT_PATIENT("ADT^A01^ADT_A01/v2.3.1 (Admission of an in-patient)", "RAD-1");

    private String transactionKeyword;
    private String label;

    EConnectathonMessageType(String label, String transactionKeyword){
        this.label = label;
        this.transactionKeyword = transactionKeyword;
    }

    public String getTransactionKeyword() {
        return transactionKeyword;
    }

    public String getLabel() {
        return label;
    }

    public static List<SelectItem> getMessageTypesForTransactions(List<Transaction> transactions){
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (Transaction transaction: transactions){
            String keyword = transaction.getKeyword();
            for (EConnectathonMessageType messageType: values()){
                if (messageType.transactionKeyword.equals(keyword)){
                    items.add(new SelectItem(messageType, messageType.label));
                }
            }
        }
        return items;
    }

    public static List<String> getListOfTransactions() {
        List<String> transactionKeywords = new ArrayList<String>();
        for (EConnectathonMessageType entry: values()){
            String keyword = entry.getTransactionKeyword();
            if (!transactionKeywords.contains(keyword)){
                transactionKeywords.add(keyword);
            }
        }
        return transactionKeywords;
    }
}
