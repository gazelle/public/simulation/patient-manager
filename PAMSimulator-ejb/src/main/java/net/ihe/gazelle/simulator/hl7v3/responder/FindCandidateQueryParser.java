package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.datatypes.PN;
import net.ihe.gazelle.hl7v3.datatypes.TEL;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201306UV02.*;
import net.ihe.gazelle.hl7v3.voc.PersonalRelationshipRoleType;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3Constants;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <b>Class Description : </b>FindCandidateQueryParser<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 14/01/16
 */
public class FindCandidateQueryParser {

    private static final String WILD_CARD = "*";
    private static Logger log = LoggerFactory.getLogger(FindCandidateQueryParser.class);
    private List<Patient> foundPatients;
    private List<Integer> unknownDomains;
    private List<HierarchicDesignator> domainsToReturn;
    private PRPAMT201306UV02QueryByParameter queryByParameter;
    private Integer nbOfHintsToReturn;
    private String pointer;
    private boolean wildcardUsed = false;

    /**
     * PAM-509: We want the PDS actor to look into Connectathon demographics as well
     *
     * @param actorKeyword
     * @return
     */
    private List<String> getActorKeywords(String actorKeyword) {
        return Arrays.asList(actorKeyword, PatientManagerConstants.CONNECTATHON);
    }

    /**
     * <p>parsePRPAIN201305UV02.</p>
     *
     * @param inRequest     a {@link net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type} object.
     * @param actorKeyword  a {@link java.lang.String} object.
     * @param domainKeyword a {@link java.lang.String} object.
     * @return a {@link IHEQueryParserCode} object.
     */
    public IHEQueryParserCode parsePRPAIN201305UV02(PRPAIN201305UV02Type inRequest, String actorKeyword,
                                                    String domainKeyword) {
        PatientQuery query = new PatientQuery();
        query.simulatedActor().keyword().in(getActorKeywords(actorKeyword));
        query.stillActive().eq(true);

        // process request
        queryByParameter = inRequest.getControlActProcess()
                .getQueryByParameter();
        nbOfHintsToReturn = 0;
        if (queryByParameter.getInitialQuantity() != null
                && queryByParameter.getInitialQuantity().getValue() != null) {
            nbOfHintsToReturn = queryByParameter.getInitialQuantity().getValue();
        }
        PRPAMT201306UV02ParameterList parameterList = queryByParameter.getParameterList();
        domainsToReturn = null;

        // exclude from the search the patients for which one patient identifier is not linked to an assigning authority
        query.patientIdentifiers().domain().isNotNull();

        if (parameterList != null && parameterList.get_xmlNodePresentation().hasChildNodes()) {
            // scopingOrganization, processed first, may raise an error condition
            if (!parameterList.getOtherIDsScopingOrganization().isEmpty()) {
                unknownDomains = new ArrayList<Integer>();
                domainsToReturn = new ArrayList<HierarchicDesignator>();
                parseScopingOrganizations(parameterList);
                if (!unknownDomains.isEmpty()) {
                    return IHEQueryParserCode.UNKNOWN_DOMAINS;
                }
            }
            if (!parameterList.getLivingSubjectName().isEmpty()) {
                // FIXME : LivingSubjectName can have several instances
                List<PRPAMT201306UV02LivingSubjectName> subjectName = parameterList.getLivingSubjectName();
                if (!subjectName.isEmpty() && !subjectName.get(0).getValue().isEmpty()) {
                    // only one value is allowed
                    queryOnFamilyName(query, subjectName);

                    queryOnFirstName(query, subjectName);
                }
            }
            // MothersMaidenName cardinality is [0..1] (is [0..2] for CH-PDQ)
            List<PRPAMT201306UV02MothersMaidenName> mothersMaidenName = parameterList.getMothersMaidenName();
            if (!mothersMaidenName.isEmpty()) {
                queryOnMothersMaidenName(query, mothersMaidenName);
            }
            // LivingSubjectBirthTime cardinality is [0..1]
            List<PRPAMT201306UV02LivingSubjectBirthTime> birthTime = parameterList.getLivingSubjectBirthTime();
            if (!birthTime.isEmpty()) {
                queryOnBirthDate(query, birthTime);
            }
            // LivingSubjectAdministrativeGender cardinality is [0..1]
            List<PRPAMT201306UV02LivingSubjectAdministrativeGender> genderList = parameterList
                    .getLivingSubjectAdministrativeGender();
            if (!genderList.isEmpty()) {
                queryOnGender(query, genderList);
            }
            // LivingSubjectId
            List<PRPAMT201306UV02LivingSubjectId> subjectIds = parameterList.getLivingSubjectId();
            if (!subjectIds.isEmpty()) {
                queryOnSubjectIds(query, subjectIds);
            }
            // patientAddress cardinality is [0..1]
            List<PRPAMT201306UV02PatientAddress> addresses = parameterList.getPatientAddress();
            if (!addresses.isEmpty()) {
                log.info("Address provided");
                if (SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.SEHE)) {
                    return IHEQueryParserCode.FORBIDDEN_FIELD;
                } else {
                    queryOnAddress(query, addresses);
                }
            }
            // patient telecom
            if (!parameterList.getPatientTelecom().isEmpty()) {
                List<PRPAMT201306UV02PatientTelecom> phoneNumbers = parameterList.getPatientTelecom();
                queryOnPhoneNumbers(query, phoneNumbers);
            }
            foundPatients = query.getListNullIfEmpty();
            if (foundPatients != null) {
                if (nbOfHintsToReturn > 0 && foundPatients.size() > nbOfHintsToReturn) {
                    pointer = null;
                    if (queryByParameter.getQueryId() != null) {
                        pointer = ContinuationPointer.buildContinuationPointer(queryByParameter.getQueryId());
                    }
                    if (pointer == null || pointer.isEmpty()) {
                        log.error("No queryId provided in request, cannot build a consistent continuation pointer");
                        pointer = "temp";
                    }
                    return IHEQueryParserCode.RETURN_SUBSET_OF_PATIENTS;
                } else {
                    return IHEQueryParserCode.RETURN_ALL_PATIENTS;
                }
            } else {
                return IHEQueryParserCode.NO_PATIENT_FOUND;
            }
            // TODO SeHE : bloodGroup

        } else {
            return IHEQueryParserCode.NO_QUERY_PARAMETER;
        }
    }

    private void queryOnPhoneNumbers(PatientQuery query, List<PRPAMT201306UV02PatientTelecom> phoneNumbers) {
        List<String> numbers = new ArrayList<>();
        for (PRPAMT201306UV02PatientTelecom telecom : phoneNumbers) {
            for (TEL value : telecom.getValue()) {
                if (value.getValue() != null && !value.getValue().isEmpty()) {
                    String phoneNumberToMatch = value.getValue().replace("tel:", "");
                    numbers.add(phoneNumberToMatch);
                }
            }
        }
        if (!numbers.isEmpty()) {
            query.phoneNumbers().value().in(numbers);
        }
    }

    private void queryOnAddress(PatientQuery query, List<PRPAMT201306UV02PatientAddress> addresses) {
        try {
            String city = addresses.get(0).getValue().get(0).getCity().get(0).getListStringValues()
                    .get(0);
            query.addressList().city().like(formatValue(city), HQLRestrictionLikeMatchMode.ANYWHERE);
        } catch (IndexOutOfBoundsException e) {
            log.debug("No City provided");
        }
        try {
            String street = addresses.get(0).getValue().get(0).getStreetName().get(0)
                    .getListStringValues().get(0);
            query.addressList().street().like(formatValue(street), HQLRestrictionLikeMatchMode.ANYWHERE);
        } catch (IndexOutOfBoundsException e) {
            log.debug("No Street provided");
        }
        try {
            String country = addresses.get(0).getValue().get(0).getCountry().get(0)
                    .getListStringValues().get(0);
            query.countryCode().like(formatValue(country), HQLRestrictionLikeMatchMode.ANYWHERE);
        } catch (IndexOutOfBoundsException e) {
            log.debug("No country provided");
        }
        try {
            String zipcode = addresses.get(0).getValue().get(0).getPostalCode().get(0)
                    .getListStringValues().get(0);
            query.addressList().zipCode().like(formatValue(zipcode), HQLRestrictionLikeMatchMode.ANYWHERE);
        } catch (IndexOutOfBoundsException e) {
            log.debug("No postalCode provided");
        }
        try {
            String state = addresses.get(0).getValue().get(0).getState().get(0).getListStringValues()
                    .get(0);
            query.addressList().state().like(formatValue(state), HQLRestrictionLikeMatchMode.ANYWHERE);
            ;
        } catch (IndexOutOfBoundsException e) {
            log.debug("No State provided");
        }
    }

    /**
     * Complete the {@link PatientQuery} with the criteria on Subject IDs.
     *
     * @param query                                {@link PatientQuery} to complete.
     * @param subjectIds                           list of {@link PRPAMT201306UV02LivingSubjectId} found in the request.
     */
    void queryOnSubjectIds(PatientQuery query, List<PRPAMT201306UV02LivingSubjectId> subjectIds) {
        if (subjectIds != null && !subjectIds.isEmpty()){
            for (PRPAMT201306UV02LivingSubjectId subjectId : subjectIds) {
                query.patientIdentifiers().idNumber().eq(subjectId.getValue().get(0).getExtension());
                query.patientIdentifiers().domain().universalID()
                        .eq(subjectId.getValue().get(0).getRoot());
            }
        } else {
            log.debug("No LivingSubjectId provided");
        }
    }

    private void queryOnGender(PatientQuery query, List<PRPAMT201306UV02LivingSubjectAdministrativeGender> genderList) {
        try {
            query.genderCode().eq(genderList.get(0).getValue().get(0).getCode());
        } catch (IndexOutOfBoundsException e) {
            log.debug("No LivingSubjectAdministrativeGender");
        }
    }

    private void queryOnBirthDate(PatientQuery query, List<PRPAMT201306UV02LivingSubjectBirthTime> birthTime) {
        try {
            String birthTimeValue = birthTime.get(0).getValue().get(0).getValue();
            if (birthTimeValue != null && !birthTimeValue.isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                try {
                    Date date = sdf.parse(birthTimeValue);
                    Calendar begin = Calendar.getInstance();
                    begin.setTime(date);
                    begin.set(Calendar.HOUR_OF_DAY, 0);
                    begin.set(Calendar.MINUTE, 0);
                    begin.set(Calendar.SECOND, 0);
                    Calendar end = Calendar.getInstance();
                    end.setTime(date);
                    end.set(Calendar.HOUR_OF_DAY, 23);
                    end.set(Calendar.MINUTE, 59);
                    end.set(Calendar.SECOND, 59);
                    query.dateOfBirth().ge(begin.getTime());
                    query.dateOfBirth().le(end.getTime());
                } catch (Exception e) {
                    log.error(birthTimeValue + " has not the expected date format yyyyMMdd");
                }
            }
        } catch (IndexOutOfBoundsException e) {
            log.debug("No LivingSubjectBirthTime provided");
        }
    }

    private void queryOnMothersMaidenName(PatientQuery query, List<PRPAMT201306UV02MothersMaidenName> mothersMaidenName) {
        for (PRPAMT201306UV02MothersMaidenName personNameParam : mothersMaidenName) {
            if (personNameParam.getSemanticsText() != null
                    && !personNameParam.getSemanticsText().getListStringValues().isEmpty()) {
                String semanticText = personNameParam.getSemanticsText().getListStringValues().get(0);
                PN name = personNameParam.getValue().get(0);
                String family = name.getFamily().get(0).getListStringValues().get(0);
                try {
                    if (HL7V3Constants.PERSON_MOTHERS_MAIDEN_NAME.equals(semanticText)) {

                        query.addRestriction(HQLRestrictions.or(query.motherMaidenName().likeRestriction(formatValue(family)),
                                query.alternateMothersMaidenName().likeRestriction(formatValue(family))));
                    } else {
                        log.debug(semanticText + " is not handled by the simulator");
                    }
                } catch (IndexOutOfBoundsException e) {
                    log.debug("No MothersMaidenName provided");
                }
            } else {
                // semanticText is required but missing, go to the next entry
                continue;
            }
        }
    }

    private void queryOnRelative(PatientQuery query, String family, String given, PersonalRelationshipRoleType relationshipType) {
        if (family != null && !family.isEmpty()) {
            query.addRestriction(HQLRestrictions.and(
                    query.personalRelationships().relationshipCode().eqRestriction(relationshipType.value()),
                    query.personalRelationships().lastName().likeRestriction(family)));
        }
        if (given != null && !given.isEmpty()) {
            query.addRestriction(HQLRestrictions.and(
                    query.personalRelationships().relationshipCode().eqRestriction(relationshipType.value()),
                    query.personalRelationships().firstName().likeRestriction(given)));
        }
    }

    private void queryOnFirstName(PatientQuery query, List<PRPAMT201306UV02LivingSubjectName> subjectName) {
        try {
            List<String> firstName = subjectName.get(0).getValue().get(0).getGiven().get(0)
                    .getListStringValues();
            if (firstName != null && firstName.size() > 0) {
                query.addRestriction(HQLRestrictions.or(
                        query.firstName().likeRestriction(formatValue(firstName.get(0))),
                        query.alternateFirstName().likeRestriction(formatValue(firstName.get(0)))));
            }
            if (firstName != null && firstName.size() > 1) {
                query.addRestriction(HQLRestrictions.or(
                        query.secondName().likeRestriction(formatValue(firstName.get(1))),
                        query.alternateSecondName().likeRestriction(formatValue(firstName.get(1)))));
            }
            if (firstName != null && firstName.size() > 2) {
                query.addRestriction(HQLRestrictions.or(
                        query.thirdName().likeRestriction(formatValue(firstName.get(2))),
                        query.alternateThirdName().likeRestriction(formatValue(firstName.get(2)))));
            }
        } catch (IndexOutOfBoundsException e) {
            log.debug("No LivingSubjectName (first name) provided");
        }
    }

    private void queryOnFamilyName(PatientQuery query, List<PRPAMT201306UV02LivingSubjectName> subjectName) {
        try {
            List<String> familyName = subjectName.get(0).getValue().get(0).getFamily().get(0)
                    .getListStringValues();
            query.addRestriction(HQLRestrictions.or(
                    query.lastName().likeRestriction(formatValue(familyName.get(0))),
                    query.alternateLastName().likeRestriction(formatValue(familyName.get(0)))));
        } catch (IndexOutOfBoundsException e) {
            log.debug("No LivingSubjectName (family name) provided", e);
        }
    }

    private void parseScopingOrganizations(PRPAMT201306UV02ParameterList parameterList) {
        int index = 1;
        for (PRPAMT201306UV02OtherIDsScopingOrganization source : parameterList
                .getOtherIDsScopingOrganization()) {
            if (!source.getValue().isEmpty()) {
                List<HierarchicDesignator> domains = HierarchicDesignator
                        .getDomainFiltered(null, source.getValue().get(0).getRoot(), null,
                                EntityManagerService.provideEntityManager(),
                                DesignatorType.PATIENT_ID);
                for (HierarchicDesignator domain : domains) {
                    if (domain == null) {
                        unknownDomains.add(index);
                    } else {
                        domainsToReturn.add(domain);
                    }
                }
            }
            index++;
        }
    }


    /**
     * <p>formatValue.</p>
     *
     * @param param a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public String formatValue(final String param) {
        String value = null;
        if (param != null) {
            value = param.replace(WILD_CARD, "%");
            if (param.contains(WILD_CARD)) {
                wildcardUsed = true;
            }
        }
        return value;
    }

    /**
     * <p>isWildcardUsed.</p>
     *
     * @return a boolean.
     */
    public boolean isWildcardUsed() {
        return wildcardUsed;
    }

    /**
     * <p>Getter for the field <code>foundPatients</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Patient> getFoundPatients() {
        return foundPatients;
    }

    /**
     * <p>Getter for the field <code>unknownDomains</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Integer> getUnknownDomains() {
        return unknownDomains;
    }

    /**
     * <p>Getter for the field <code>domainsToReturn</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HierarchicDesignator> getDomainsToReturn() {
        return domainsToReturn;
    }

    /**
     * <p>Getter for the field <code>queryByParameter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter} object.
     */
    public PRPAMT201306UV02QueryByParameter getQueryByParameter() {
        return queryByParameter;
    }

    /**
     * <p>Getter for the field <code>nbOfHintsToReturn</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getNbOfHintsToReturn() {
        return nbOfHintsToReturn;
    }

    /**
     * <p>Getter for the field <code>pointer</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPointer() {
        return pointer;
    }
}
