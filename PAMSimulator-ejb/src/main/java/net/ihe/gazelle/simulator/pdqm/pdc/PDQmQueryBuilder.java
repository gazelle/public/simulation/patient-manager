package net.ihe.gazelle.simulator.pdqm.pdc;

import ca.uhn.fhir.rest.gclient.DateClientParam;
import ca.uhn.fhir.rest.gclient.IParam;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.StringClientParam;
import ca.uhn.fhir.rest.gclient.TokenClientParam;
import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;
import net.ihe.gazelle.fhir.constants.FhirConstants;
import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.fhir.util.IFhirQueryBuilder;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;
import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.instance.model.api.IBaseBundle;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>PDQmQueryBuilder class.</p>
 *
 * @author aberge
 * @version 1.0: 03/11/17
 */

public class PDQmQueryBuilder implements IFhirQueryBuilder{

    private static final String URN_PREFIX = "urn:oid:";
    private static final int ZERO = 0;
    public static final String STAR = "*";
    private Patient patientCriteria;
    private List<AssigningAuthority> domainsToReturn;
    private Integer limitValue;

    public PDQmQueryBuilder(final Patient patientCriteria, final List<AssigningAuthority>
            domainsReturned, final Integer limitValue, final FHIRResponderSUTConfiguration sut) {
        this.patientCriteria = patientCriteria;
        this.domainsToReturn = domainsReturned;
        this.limitValue = limitValue;
    }


    @Override
    public String getRequestType() {
        return FhirConstants.PDQM_REQUEST_TYPE;
    }


    public void populatePDQmQuery(IQuery<IBaseBundle> query) {

        String firstName = patientCriteria.getFirstName();
        if (firstName != null && !firstName.isEmpty()) {
            appendParameter(query, firstName, PatientFhirIHE.GIVEN);
        }

        String secondName = patientCriteria.getSecondName();
        if (secondName != null && !secondName.isEmpty()) {
            appendParameter(query, secondName, PatientFhirIHE.GIVEN);
        }

        String thirdName = patientCriteria.getThirdName();
        if (thirdName != null && !thirdName.isEmpty()) {
            appendParameter(query, thirdName, PatientFhirIHE.GIVEN);
        }


        String lastName = patientCriteria.getLastName();
        if (lastName != null && !lastName.isEmpty()) {
            appendParameter(query, lastName, PatientFhirIHE.FAMILY);
        }


        String genderCode = patientCriteria.getGenderCode();
        appendParameter(query, genderCode, PatientFhirIHE.GENDER);

        Date dateOfBirth1 = patientCriteria.getDateOfBirth();
        if (dateOfBirth1 != null) {
            Format formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateOfBirth = formatter.format(dateOfBirth1);
            appendParameter(query, dateOfBirth, PatientFhirIHE.BIRTHDATE);
        }

        if (patientCriteria.getAddressList() != null && !patientCriteria.getAddressList().isEmpty()) {
            // will allow only one patient address in the GUI
            PatientAddress addressCriteria = patientCriteria.getAddressList().get(ZERO);

            String city = addressCriteria.getCity();
            appendParameter(query, city, PatientFhirIHE.ADDRESS_CITY);

            String countryCode = addressCriteria.getCountryCode();
            appendParameter(query, countryCode, PatientFhirIHE.ADDRESS_COUNTRY);

            String zipCode = addressCriteria.getZipCode();
            appendParameter(query, zipCode, PatientFhirIHE.ADDRESS_POSTALCODE);

            String state = addressCriteria.getState();
            appendParameter(query, state, PatientFhirIHE.ADDRESS_STATE);

            String anypart = addressCriteria.getAddressLine();
            appendParameter(query, anypart, PatientFhirIHE.ADDRESS);
        }

        for (PatientPhoneNumber phoneNumber : patientCriteria.getPhoneNumbers()) {
            String value = phoneNumber.getValue();
            if (value != null && !value.isEmpty()) {
                query.where(PatientFhirIHE.TELECOM.exactly().code(value));
            }
        }

        if (patientCriteria.getPatientIdentifiers() != null && !patientCriteria.getPatientIdentifiers().isEmpty()) {
            // we allow only one patient identifier in the GUI
            PatientIdentifier pid = patientCriteria.getPatientIdentifiers().get(ZERO);
            HierarchicDesignator domain = pid.getDomain();
            if (domain.getUniversalID() != null && !domain.getUniversalID().isEmpty()) {
                String universalId = oidToUrn(domain.getUniversalID());
                query.where(PatientFhirIHE.IDENTIFIER.exactly().systemAndIdentifier(universalId, pid.getIdNumber()));
            } else {
                query.where(PatientFhirIHE.IDENTIFIER.exactly().identifier(pid.getIdNumber()));
            }
        }

        if (domainsToReturn != null && !domainsToReturn.isEmpty()) {
            StringBuilder value = new StringBuilder();
            boolean firstEntry = true;
            for (AssigningAuthority currentDomain : domainsToReturn) {
                if (firstEntry) {
                    firstEntry = false;
                } else {
                    value.append('|');
                    value.append(',');
                }
                String universalId = oidToUrn(currentDomain.getUniversalId());
                value.append(universalId);
            }
            query.where(PatientFhirIHE.IDENTIFIER.hasSystemWithAnyCode(value.toString()));
            // final pipe appended by hapi since we populate the system attribute
            value.append('|');
        }

        if (limitValue != null && limitValue > ZERO) {
            query.count(limitValue);
        }
    }

    private String oidToUrn(String namespace) {
        if (namespace == null) {
            return "";
        } else if (namespace.startsWith(URN_PREFIX)) {
            return namespace;
        } else {
            return URN_PREFIX.concat(namespace);
        }
    }

    private void appendParameter(IQuery<IBaseBundle> query, String criteriaValue, IParam fhirParam) {
        if (criteriaValue != null && !criteriaValue.isEmpty()) {
            boolean exact = false;
            if (fhirParam instanceof StringClientParam) {
                if (exact(criteriaValue)) {
                    query.where(((StringClientParam) fhirParam).matchesExactly().value(criteriaValue));
                } else {
                    criteriaValue = canonicalizeParameter(criteriaValue);
                    query.where(((StringClientParam) fhirParam).matches().value(criteriaValue));
                }
            } else if (fhirParam instanceof TokenClientParam) {
                query.where(((TokenClientParam) fhirParam).exactly().identifier(criteriaValue));
            } else if (fhirParam instanceof DateClientParam) {
                query.where(((DateClientParam) fhirParam).exactly().day(criteriaValue));
            }
        }
    }

    private boolean exact(String value) {
        return !value.endsWith(STAR);
    }

    private String canonicalizeParameter(String value) {
        return StringUtils.removeEnd(value, STAR);
    }

}
