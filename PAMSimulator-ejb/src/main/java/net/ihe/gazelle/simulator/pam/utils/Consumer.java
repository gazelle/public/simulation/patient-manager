package net.ihe.gazelle.simulator.pam.utils;

import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;

public abstract class Consumer extends IHEDefaultHandler {

    public String getReceivingApplication() {
        return receivingApplication;
    }

    public String getReceivingFacility() {
        return receivingFacility;
    }

    public abstract String getSutActorKeyword();

    public String getIncomingTriggerEvent() {
        return super.getIncomingTriggerEvent();
    }
}
