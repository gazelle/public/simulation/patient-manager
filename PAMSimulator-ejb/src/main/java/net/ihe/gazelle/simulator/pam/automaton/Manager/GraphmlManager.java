package net.ihe.gazelle.simulator.pam.automaton.Manager;

import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.util.io.graphml.GraphMLReader;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescriptionQuery;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionQuery;
import net.ihe.gazelle.simulator.pam.iti31.util.TriggerEventEnum;
import org.apache.commons.io.FilenameUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by xfs on 29/10/15.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Name("graphmlManager")
@Scope(ScopeType.PAGE)
public class GraphmlManager implements Serializable {

    static Logger log = LoggerFactory.getLogger(GraphmlManager.class);

    private GraphDescription selectedGraph;
    private Boolean editMode;
    private Boolean validFile = true;
    private List<String> notValidEdges = new ArrayList<String>();

    /**
     * <p>Getter for the field <code>validFile</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getValidFile() {
        return validFile;
    }

    /**
     * <p>Setter for the field <code>validFile</code>.</p>
     *
     * @param validFile a {@link java.lang.Boolean} object.
     */
    public void setValidFile(Boolean validFile) {
        this.validFile = validFile;
    }

    /**
     * <p>Getter for the field <code>notValidEdges</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getNotValidEdges() {
        return notValidEdges;
    }

    /**
     * <p>Setter for the field <code>notValidEdges</code>.</p>
     *
     * @param notValidEdges a {@link java.util.List} object.
     */
    public void setNotValidEdges(List<String> notValidEdges) {
        this.notValidEdges = notValidEdges;
    }

    /**
     * <p>Getter for the field <code>editMode</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getEditMode() {
        return editMode;
    }

    /**
     * <p>Setter for the field <code>editMode</code>.</p>
     *
     * @param editMode a {@link java.lang.Boolean} object.
     */
    public void setEditMode(Boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * <p>Getter for the field <code>selectedGraph</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription} object.
     */
    public GraphDescription getSelectedGraph() {
        return selectedGraph;
    }

    /**
     * <p>Setter for the field <code>selectedGraph</code>.</p>
     *
     * @param selectedGraph a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription} object.
     */
    public void setSelectedGraph(GraphDescription selectedGraph) {
        this.selectedGraph = selectedGraph;
    }

    /**
     * <p>isUploadedFileInvalid.</p>
     *
     * @return a boolean.
     */
    public boolean isUploadedFileInvalid() {
        return !validFile;
    }

    /**
     * <p>getAllAuthorizedEvents.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getAllAuthorizedEvents() {
        return TriggerEventEnum.getAllActiveEvents();
    }

    private List<String> getNotValidEdges(byte[] dataFile) {

        List<String> res = new ArrayList<String>();
        List<String> allAuthorizedEdges = getAllAuthorizedEvents();
        String xpath = "//*[local-name()='EdgeLabel']";

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new ByteArrayInputStream(dataFile));
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xPath.compile(xpath).evaluate(xmlDocument, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); i++) {
                String currentEdgeValue = nodeList.item(i).getFirstChild().getNodeValue();
                String edgeNameTrimmed = currentEdgeValue.substring(0, 3);
                if (!edgeNameTrimmed.equals("ini") && !allAuthorizedEdges.contains(edgeNameTrimmed)) {
                    res.add(currentEdgeValue);
                }
            }


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }


        return res;
    }

    /**
     * <p>uploadGraphmlEventListener.</p>
     *
     * @param event a {@link org.richfaces.event.FileUploadEvent} object.
     */
    public void uploadGraphmlEventListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        String uploadedFilename = item.getName();
        String fileBasename = FilenameUtils.removeExtension(uploadedFilename);
        String fileExtension = FilenameUtils.getExtension(uploadedFilename);

        String automatonFolder = PreferenceService.getString("graph_resources");
        boolean folderIsCreated = createAutomatonFolder(automatonFolder);
        if (folderIsCreated) {
            try {
                File graphmlFile = File.createTempFile(fileBasename, "." + fileExtension, new File(automatonFolder));
                byte[] dataFile = item.getData();
                if (dataFile != null) {

                    TinkerGraph graph = new TinkerGraph();
                    GraphMLReader reader = new GraphMLReader(graph);
                    reader.inputGraph(new ByteArrayInputStream(dataFile));

                    int vertices = ((ArrayList) graph.getVertices()).size();
                    int edges = ((ArrayList) graph.getEdges()).size();

                    notValidEdges = getNotValidEdges(dataFile);
                    if (notValidEdges.size() == 0) {
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(graphmlFile);
                            fos.write(dataFile);
                            selectedGraph.setGraphmlLink(graphmlFile.getAbsolutePath());
                            selectedGraph.setEdges(edges);
                            selectedGraph.setVertices(vertices);
                            validFile = true;
                        } catch (Exception e) {
                            log.error(e.getMessage(), e + "Impossible to write in this file");
                        } finally {
                            if (fos != null) {
                                fos.close();
                            }
                        }
                    } else {
                        selectedGraph.setGraphmlLink(null);
                        validFile = false;
                    }
                } else {
                    FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.WARN,
                            "The uploaded file is empty");
                }
            } catch (IOException io) {
                log.error(io.getMessage(), io + "The tool has not been able to upload the file");
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR,
                        "The tool has not been able to upload the file");
            }
        }

    }

    private boolean createAutomatonFolder(String automatonFolder) {
        if (automatonFolder == null) {
            return false;
        } else {
            File baseDir = new File(automatonFolder);
            if (!automatonFolder.isEmpty()) {
                if (!baseDir.exists()) {
                    boolean successfullDirectoryCreation = new File(automatonFolder).mkdirs();
                    if (successfullDirectoryCreation) {
                        return true;
                    } else {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Can't create the folders");
                        validFile = false;
                        return false;
                    }
                } else {
                    return true;
                }
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The graph_resources variable in the application configuration is not set");
                validFile = false;
                return false;
            }
        }
    }

    /**
     * <p>uploadImageEventListener.</p>
     *
     * @param event a {@link org.richfaces.event.FileUploadEvent} object.
     */
    public void uploadImageEventListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        String uploadedFilename = item.getName();
        String fileBasename = FilenameUtils.removeExtension(uploadedFilename);
        String fileExtension = FilenameUtils.getExtension(uploadedFilename);

        String automatonFolder = PreferenceService.getString("graph_resources");
        boolean folderIsCreated = createAutomatonFolder(automatonFolder);
        if (folderIsCreated) {
            try {
                File imageFile = File.createTempFile(fileBasename, "." + fileExtension, new File(automatonFolder));
                byte[] dataFile = item.getData();
                if (dataFile != null) {
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(imageFile);
                        fos.write(dataFile);
                        selectedGraph.setImageLink(imageFile.getAbsolutePath());
                    } catch (IOException e) {
                        log.error(e.getMessage(), e + "Impossible to write in this file");
                    } finally {
                        if (fos != null) {
                            fos.close();
                        }
                    }
                } else {
                    FacesMessages.instance().addFromResourceBundle("gazelle.hl7mpr.fileIsEmpty");
                }
            } catch (IOException io) {
                log.error(io.getMessage() + "The tool has not been able to upload the file");
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "The tool has not been able to upload the file");
            }
        }

    }

    /**
     * <p>init.</p>
     */
    public void init() {
        selectedGraph = new GraphDescription();
        notValidEdges = new ArrayList<String>();
    }

    /**
     * <p>displayGraph.</p>
     */
    public void displayGraph() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("editMode") && params.get("editMode").equals("true")) {
            editMode = true;
        }
        if (params.containsKey("id")) {
            Integer graphId = Integer.parseInt(params.get("id"));
            GraphDescriptionQuery gdq = new GraphDescriptionQuery();
            gdq.id().eq(graphId);
            selectedGraph = gdq.getUniqueResult();
        }
    }

    /**
     * <p>submit.</p>
     */
    public void submit() {

        if (selectedGraph.getImageLink() == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Preview image is missing");
        } else if (selectedGraph.getGraphmlLink() == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Graph upload is missing");
        } else {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            selectedGraph.setActive(true);
            em.merge(selectedGraph);
            em.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO,"The new graph has been properly created");
            editMode = false;
        }


    }

    /**
     * <p>deleteSelectedGraph.</p>
     */
    public void deleteSelectedGraph() {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        GraphDescription graphTobeDeleted = em.find(GraphDescription.class, selectedGraph.getId());
        em.remove(graphTobeDeleted);
        em.flush();
        FacesMessages.instance().add(StatusMessage.Severity.INFO,"The graph has been properly deleted");
    }

    /**
     * <p>displaySupportedEvents.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String displaySupportedEvents() {
        String res = "The following events are supported : ";
        for (String currentEvent : getAllAuthorizedEvents()) {
            res = res.concat(currentEvent + " ");
        }
        return res;
    }

    /**
     * <p>displayUnsupportedEvents.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String displayUnsupportedEvents() {
        String res = "The following events are unsupported : ";
        for (String currentEvent : getNotValidEdges()) {
            res = res.concat(currentEvent + " ");
        }
        return res;
    }

    /**
     * <p>toggleGraphStatus.</p>
     *
     * @param currentGraph a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription} object.
     */
    public void toggleGraphStatus(GraphDescription currentGraph) {
        currentGraph.setActive(!currentGraph.getActive());
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        em.merge(currentGraph);
    }

    /**
     * <p>canBeDeleted.</p>
     *
     * @param currentGraph a {@link net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription} object.
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean canBeDeleted(GraphDescription currentGraph) {

        GraphExecutionQuery gdq = new GraphExecutionQuery();
        gdq.graph().id().eq(currentGraph.getId());
        int count = gdq.getCount();
        return count == 0;
    }
}
