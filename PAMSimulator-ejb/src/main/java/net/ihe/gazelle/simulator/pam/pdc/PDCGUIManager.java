package net.ihe.gazelle.simulator.pam.pdc;

import net.ihe.gazelle.HL7Common.action.SimulatorResponderConfigurationDisplay;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("pdcGUIManagerBean")
@Scope(ScopeType.PAGE)
public class PDCGUIManager extends SimulatorResponderConfigurationDisplay {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private Actor actor;

	@Override
	@Create
	public void getSimulatorResponderConfiguration() {
        actor = Actor.findActorWithKeyword("PDC");
		getSimulatorResponderConfigurations(actor, null, null);
	}

	@Override
	public String getUrlForHL7Messages(SimulatorResponderConfiguration conf) {
		return "/messages/browser.seam?simulatedActor=" + conf.getSimulatedActor().getId()
                + "&transaction=" + conf.getTransaction().getId()
                + "&domain=" + conf.getDomain().getId();
	}

    public String linkToPatients(){
        return "/patient/allPatients.seam?actor=" + actor.getId();
    }

}
