package net.ihe.gazelle.simulator.pam.model;

/**
 * <b>Class Description : </b>PatientStatus<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 21/09/15
 *


 */
public enum PatientStatus {

    STATUS_ADMITTED("ADMITTED"), //0
    STATUS_REGISTERED("REGISTERED"), //1
    STATUS_DISCHARGE("DISCHARGED"), //2
    STATUS_VISIT_ENDED("VISIT ENDED"), //3
    STATUS_PRE_ADMITTED("PRE ADMITTED"), //4
    STATUS_PENDING_ADMIT("PENDING ADMIT"), //5
    STATUS_PENDING_TRANSFER("PENDING TRANSFER"), //6
    STATUS_PENDING_DISCHARGE("PENDING DISCHARGE"), //7
    STATUS_ENCOUNTER_CANCELLED("ENCOUNTER CANCELLED"), //8
    STATUS_TEMPORARILY_ABSENT("TEMPORARILY_ABSENT"), // 9
    DISCHARGED("DISCHARGED");

    PatientStatus(String name) {
        this.name = name;
    }

    String name;

    public String getName() {
        return this.name;
    }

}
