package net.ihe.gazelle.simulator.pam.iti31.model;


import org.jboss.seam.core.ResourceBundle;

/**
 * <b>Class Description : </b>ActionType<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 21/09/15
 *


 */
public enum ActionType {

    INSERT ("INSERT", "gzl-label-green", "gazelle.simulator.pam.Inserted"),
    CANCEL ("CANCEL", "gzl-label-red", "gazelle.simulator.pam.Cancelled"),
    UPDATE ("UPDATE", "gzl-label-blue", "gazelle.simulator.pam.Updated");

    ActionType (String name, String styleClass, String key){
        this.name = name;
        this.styleClass = styleClass;
        this.label = key;
    }

    String name;
    String styleClass;
    String label;

    public String getLabel(){
        return ResourceBundle.instance().getString(label);
    }

    public String getStyleClass() {
        return styleClass;
    }

    public String getName(){
        return name;
    }

    public boolean isInsert(){
        return INSERT.equals(this);
    }

    public boolean isCancel(){
        return CANCEL.equals(this);
    }

    public boolean isUpdate(){
        return UPDATE.equals(this);
    }
}
