package net.ihe.gazelle.simulator.pam.automaton.Manager;


import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.patient.AbstractPatient;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.application.PatientBuilder;
import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.action.PatientManager;
import net.ihe.gazelle.simulator.pam.action.PatientSelectorManager;
import net.ihe.gazelle.simulator.pam.automaton.helper.AutomatonHelper;
import net.ihe.gazelle.simulator.pam.automaton.helper.GraphWalkerAsyncExecutionInterface;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescription;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphDescriptionDAO;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecution;
import net.ihe.gazelle.simulator.pam.automaton.model.graph.GraphExecutionResults;
import net.ihe.gazelle.simulator.pam.automaton.report.TestStep;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.hl7.BP6HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.IHEHL7Domain;
import net.ihe.gazelle.simulator.pam.iti31.action.NumberGenerator;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.pes.FilterEncounterManager;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by xfs on 26/10/15.
 */

@Name("automatonManager")
@Scope(ScopeType.PAGE)
public class AutomatonManager extends PatientSelectorManager {

    private static final String FACILITY = "FACILITY";
    private static final String DOCTOR = "DOCTOR";
    private static Logger log = LoggerFactory.getLogger(AutomatonManager.class);

    private boolean displayTestResults;
    private GraphExecution selectedGraphExecution;
    private Actor simulatedActor;
    private boolean displayEditPatientPanel;
    private FilterEncounterManager encounterFilter;
    private String sendingFacility;
    private Actor receivingActor;
    private String sendingApplication;
    private List<HL7V2ResponderSUTConfiguration> availableSystems;
    private Movement selectedMovement;
    private GraphExecutionResults graphExecutionResults;
    private Encounter currentEncounter;
    private Movement historicMovement;
    EntityManager entityManager;
    private boolean startsWithA28;

    @In(create = true)
    public GraphWalkerAsyncExecutionInterface graphWalkerAsyncExecution;
    @In(create = true)
    public PatientManager patientManager;

    public boolean isDisplayTestResults() {
        return displayTestResults;
    }

    public void setDisplayTestResults(boolean displayTestResults) {
        this.displayTestResults = displayTestResults;
    }

    public String getSendingFacility() {
        return sendingFacility;
    }

    public void setSendingFacility(String sendingFacility) {
        this.sendingFacility = sendingFacility;
    }

    public Movement getHistoricMovement() {
        return historicMovement;
    }

    public void setHistoricMovement(Movement historicMovement) {
        this.historicMovement = historicMovement;
    }

    public Encounter getCurrentEncounter() {
        return currentEncounter;
    }

    public void setCurrentEncounter(Encounter currentEncounter) {
        this.currentEncounter = currentEncounter;
    }

    public Actor getReceivingActor() {
        return receivingActor;
    }

    public void setReceivingActor(Actor receivingActor) {
        this.receivingActor = receivingActor;
    }

    public boolean isStartsWithA28() {
        return startsWithA28;
    }

    public void setStartsWithA28(boolean startsWithA28) {
        this.startsWithA28 = startsWithA28;
    }

    public String getSendingApplication() {
        return sendingApplication;
    }

    public void setSendingApplication(String sendingApplication) {
        this.sendingApplication = sendingApplication;
    }

    public List<HL7V2ResponderSUTConfiguration> getAvailableSystems() {
        return availableSystems;
    }

    public void setAvailableSystems(List<HL7V2ResponderSUTConfiguration> availableSystems) {
        this.availableSystems = availableSystems;
    }

    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    public void setSimulatedActor(Actor simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    public Movement getSelectedMovement() {
        return selectedMovement;
    }

    public void setSelectedMovement(Movement selectedMovement) {
        this.selectedMovement = selectedMovement;
    }

    public GraphExecution getSelectedGraphExecution() {
        return selectedGraphExecution;
    }

    public void setSelectedGraphExecution(GraphExecution selectedGraphExecution) {
        this.selectedGraphExecution = selectedGraphExecution;
    }

    public GraphExecutionResults getGraphExecutionResults() {
        return graphExecutionResults;
    }

    public void setGraphExecutionResults(GraphExecutionResults graphExecutionResults) {
        this.graphExecutionResults = graphExecutionResults;
    }

    public List<GraphDescription> getGraphDescriptionList() {
        return GraphDescriptionDAO.getActiveGraphDescriptions();
    }

    public boolean isDisplayEditPatientPanel() {
        return displayEditPatientPanel;
    }

    public void setDisplayEditPatientPanel(boolean displayEditPatientPanel) {
        this.displayEditPatientPanel = displayEditPatientPanel;
    }

    public FilterEncounterManager getEncounterFilter() {
        return encounterFilter;
    }

    public void setEncounterFilter(FilterEncounterManager encounterFilter) {
        this.encounterFilter = encounterFilter;
    }

    public void init() {

        entityManager = EntityManagerService.provideEntityManager();
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String graphExecutionResultsParam = urlParams.get("graphExecutionResultsId");
        String graphDescriptionParam = urlParams.get("graphId");

        if (graphExecutionResultsParam == null) {
            initGraphWithTestsResults(graphDescriptionParam);
        } else {
            initGraphWithoutTestsResults(graphExecutionResultsParam);
        }

        simulatedActor = Actor.findActorWithKeyword("PES");
        receivingActor = Actor.findActorWithKeyword("PEC");
        sendingApplication = "PAMSimulator";
        sendingFacility = "IHE";
        availableSystems = HL7V2ResponderSUTConfiguration.getAllConfigurationsForSelection("ITI-31", HL7Protocol.MLLP);

        encounterFilter = new FilterEncounterManager(simulatedActor);
        historicMovement = new Movement();
        currentEncounter = new Encounter();
        displayDDSPanel = true;
        useIdsFromDDS = PreferenceService.getBoolean("use_ids_from_dds");
        super.initialize();
        super.switchToGenerationMode();
    }

    private void initGraphWithoutTestsResults(String graphExecutionResultsParam) {

        Integer graphExecutionResultsId = Integer.parseInt(graphExecutionResultsParam);
        graphExecutionResults = entityManager.find(GraphExecutionResults.class, graphExecutionResultsId);
        selectedGraphExecution = graphExecutionResults.getGraphExecution();
        displayTestResults = true;
    }

    private void initGraphWithTestsResults(String graphDescriptionParam) {
        selectedGraphExecution = new GraphExecution();
        graphExecutionResults = new GraphExecutionResults();
        displayTestResults = false;
        selectedGraphExecution.setBp6mode(isBP6ModeEnabled());

        if (graphDescriptionParam != null) {
            Integer graphId = Integer.parseInt(graphDescriptionParam);
            GraphDescription graph = entityManager.find(GraphDescription.class, graphId);
            selectedGraphExecution.setGraph(graph);
        }
        graphExecutionResults.setGraphExecution(selectedGraphExecution);
    }

    // Run the automaton
    public void run() {
        List<Movement> movementList = new ArrayList<>();
        movementList.add(historicMovement);
        currentEncounter.setMovements(movementList);

        File graphFile = new File(selectedGraphExecution.getGraph().getGraphmlLink());
        if (!graphFile.exists()) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot execute automaton, graph file is missing");
            log.error("Cannot read file at " + graphFile.getAbsolutePath());
        } else {
            if (startsWithA28) {
                try {
                    sendA28();
                } catch (Exception e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error occured when sending the A28 message: " + e.getMessage());
                    return;
                }
            }
            entityManager = EntityManagerService.provideEntityManager();
            graphExecutionResults.setTimestamp(new Date());
            graphExecutionResults.setMessagesList(new ArrayList<TestStep>());
            graphExecutionResults.setGraphExecution(selectedGraphExecution);
            graphExecutionResults.setExecutionStatus(GraphExecutionStatus.EXECUTING);
            graphExecutionResults = entityManager.merge(graphExecutionResults);
            entityManager.flush();

            graphWalkerAsyncExecution.asyncRun(graphExecutionResults, currentEncounter);
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Tests results are refreshed in real time. Permalink is available at the end " +
                    "of the process below.");

            displayTestResults = true;
        }
    }

    private void sendA28() throws Exception {
        AutomatonHelper helper = new AutomatonHelper(selectedGraphExecution.isBp6mode(), patientService);
        helper.sendA28(selectedGraphExecution.getSut(), currentEncounter);
    }

    public void replay() {

        entityManager = (EntityManager) Component.getInstance("entityManager");
        List<TestStep> oldTestSteps = graphExecutionResults.getMessagesList();
        graphExecutionResults = new GraphExecutionResults();
        graphExecutionResults = entityManager.merge(graphExecutionResults);
        entityManager.flush();
        List<TestStep> newTestSteps = updateGraphExecutionResultsId(oldTestSteps, graphExecutionResults);

        graphExecutionResults.setTimestamp(new Date());
        graphExecutionResults.setMessagesList(newTestSteps);
        graphExecutionResults.setGraphExecution(selectedGraphExecution);
        graphExecutionResults.setExecutionStatus(GraphExecutionStatus.EXECUTING);

        graphExecutionResults = entityManager.merge(graphExecutionResults);
        entityManager.flush();

        graphWalkerAsyncExecution.asyncReplay(graphExecutionResults);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Tests results are refreshed in real time. Permalink is available at the end of " +
                "the process below.");
        displayTestResults = true;


    }

    public void validate() {

        entityManager = (EntityManager) Component.getInstance("entityManager");
        graphExecutionResults.setTimestamp(new Date());
        graphExecutionResults.setExecutionStatus(GraphExecutionStatus.EXECUTING);
        graphExecutionResults = entityManager.merge(graphExecutionResults);
        entityManager.flush();

        graphWalkerAsyncExecution.asyncReplay(graphExecutionResults);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Tests results are refreshed in real time. Permalink is available at the end of " +
                "the process below.");
        displayTestResults = true;


    }

    public void displayPermanentLinkMessage() {
        if (graphExecutionResults.getExecutionStatus().equals(GraphExecutionStatus.VALIDATED)) {
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Process ended. Permalink to the tests results is available below.");
        }
    }

    @Override
    public Actor getSendingActor() {
        return simulatedActor;
    }

    @Override
    public void createNewRelationship() {
        // nothing to do, the patient is not editable here
    }

    @Override
    public void saveRelationship() {
        // nothing to do, the patient is not editable here
    }

    public void selectPatient(Patient inPatient) {

        displayPatientsList = false;
        displayDDSPanel = false;
        currentEncounter = new Encounter(inPatient);
    }

    private void saveGraphExecutionResults(GraphExecutionResults graphExecutionResults) {

        entityManager = (EntityManager) Component.getInstance("entityManager");
        setGraphExecutionResults(entityManager.merge(graphExecutionResults));
        entityManager.flush();
    }

    public void generateSelectedPatient() {
        // TODO Ne faut-il pas effectuer le même traitement que pour les autres PatientSelectorManager
        //  (à savoir utiliser la méthode generateNewPatient) pour bénéficier de la génération des builders associés
        //  aux différents profils
        AbstractPatient aPatient = generatePatient();
        if (aPatient != null) {
            Patient generatedPatient = newInstanceOfPatient(aPatient);
            if (isBP6ModeEnabled() && graphExecutionResults.getGraphExecution().isBp6mode()) {
                generatedPatient.getAdditionalDemographics().setSocioProfessionalGroup(ValueSetProvider.getInstance().getRandomCodeFromValueSet
                        ("HL79108"));
                generatedPatient.getAdditionalDemographics().setSocioProfessionalOccupation(ValueSetProvider.getInstance().getRandomCodeFromValueSet
                        ("HL79107"));
            }
            generatedPatient.setSimulatedActor(simulatedActor);
            generatedPatient.setStillActive(true);
            generatedPatient.setCreationDate(new Date());
            if (Identity.instance().getCredentials() != null) {
                String username = Identity.instance().getCredentials().getUsername();
                generatedPatient.setCreator(username);
            } else {
                generatedPatient.setCreator(null);
            }
            patientBuilderFactory.make(getHL7Domain()).buildIdentifiers(new PatientBuilder.IdentifierContext(generatedPatient, selectedAuthorities, useIdsFromDDS,generateIdsForAuthorities,generateIdsForConnectathon,isQualifiedIdentity()));
            // save identifiers before saving the patient
            List<PatientIdentifier> mergedIdentifiers = PatientIdentifierDAO.savePatientIdentifierList(generatedPatient.getPatientIdentifiers());
            generatedPatient.setPatientIdentifiers(mergedIdentifiers);
            generatedPatient = generatedPatient.savePatient(EntityManagerService.provideEntityManager());
            displayDDSPanel = false;
            currentEncounter = new Encounter(generatedPatient);
            currentEncounter.setSimulatedActor(simulatedActor);
        }
    }

    private Patient newInstanceOfPatient(AbstractPatient aPatient) {
        Patient patient = new Patient(aPatient);
        patient.setAccountNumber(NumberGenerator.generate(DesignatorType.ACCOUNT_NUMBER
        ));
        return patient;
    }

    public void resetPatientChoice() {
        currentEncounter = null;
        displayPatientsList = true;
        displayDDSPanel = true;
    }

    public void getNewVisitNumber() {
        if (currentEncounter != null) {
            currentEncounter.setVisitNumber(NumberGenerator.generate(DesignatorType.VISIT_NUMBER));
        }
    }

    public void randomlyFillEncounter() {

        if (currentEncounter == null) {
            currentEncounter = new Encounter(new Patient());
        }
        if ((currentEncounter.getVisitNumber() == null) || currentEncounter.getVisitNumber().isEmpty()) {
            currentEncounter.setVisitNumber(NumberGenerator.generate(DesignatorType.VISIT_NUMBER));
        }

        String concept;
        ValueSetProvider provider = ValueSetProvider.getInstance();

        if (historicMovement == null) {
            historicMovement = new Movement(currentEncounter);
        }

        concept = provider.getRandomCodeFromValueSet("BED");
        if (concept != null) {
            historicMovement.setBedCode(concept);
        }
        concept = provider.getRandomCodeFromValueSet(FACILITY);
        if (concept != null) {
            historicMovement.setFacilityCode(concept);
        }
        concept = provider.getRandomCodeFromValueSet("ROOM");
        if (concept != null) {
            historicMovement.setRoomCode(concept);
        }
        concept = provider.getRandomCodeFromValueSet("POINT_OF_CARE");
        if (concept != null) {
            historicMovement.setPointOfCareCode(concept);
        }
        concept = provider.getRandomCodeFromValueSet("HOSPITAL_SERVICE");
        if (concept != null) {
            currentEncounter.setHospitalServiceCode(concept);
        }

        concept = provider.getRandomCodeFromValueSet(DOCTOR);
        if (concept != null) {
            currentEncounter.setAttendingDoctorCode(concept);
        }

        concept = provider.getRandomCodeFromValueSet(DOCTOR);
        if (concept != null) {
            currentEncounter.setAdmittingDoctorCode(concept);
        }

        concept = provider.getRandomCodeFromValueSet(DOCTOR);
        if (concept != null) {
            currentEncounter.setReferringDoctorCode(concept);
            currentEncounter.setAdmitDate(new Date());
        }

        if (isBP6ModeEnabled() && selectedGraphExecution.isBp6mode()) {
            concept = provider.getRandomCodeFromValueSet("MEDICAL_WARD");
            if (concept != null) {
                historicMovement.setWardCode(concept);
            }

            concept = provider.getRandomCodeFromValueSet("NURSING_WARD");
            if (concept != null) {
                historicMovement.setNursingCareWard(concept);
            }

            concept = provider.getRandomCodeFromValueSet("HL79106");
            if (concept != null) {
                historicMovement.setNatureOfMovement(concept);
            }

            concept = provider.getRandomCodeFromValueSet("HL70007");
            if (concept != null) {
                currentEncounter.setAdmissionType(concept);
            }

            // DRG Movement

            concept = provider.getRandomCodeFromValueSet("HL73303");
            if (concept != null) {
                currentEncounter.getDrgMovement().setPmsiAdmissionMode(concept);
            }

            concept = provider.getRandomCodeFromValueSet("HL73304");
            if (concept != null) {
                currentEncounter.getDrgMovement().setPmsiDischargeMode(concept);
            }

            concept = provider.getRandomCodeFromValueSet("HL73305");
            if (concept != null) {
                currentEncounter.getDrgMovement().setPmsiEstablishmentOfOriginMode(concept);
            }

            concept = provider.getRandomCodeFromValueSet("HL73305");
            if (concept != null) {
                currentEncounter.getDrgMovement().setPmsiDestinationMode(concept);
            }

            // Additional Info

            concept = provider.getRandomCodeFromValueSet(FACILITY);
            if (concept != null) {
                currentEncounter.getEncounterAdditionalInfo().setEstablishmentOfOrigin(concept);
                currentEncounter.getEncounterAdditionalInfo().setLastVisitDate(new Date());

            }

            concept = provider.getRandomCodeFromValueSet("HL70430");
            if (concept != null) {
                currentEncounter.getEncounterAdditionalInfo().setDischargeMode(concept);
            }

            concept = provider.getRandomCodeFromValueSet(FACILITY);
            if (concept != null) {
                currentEncounter.getEncounterAdditionalInfo().setEstablishmentOfDestination(concept);
            }

            concept = provider.getRandomCodeFromValueSet("HL73302");
            if (concept != null) {
                currentEncounter.getEncounterAdditionalInfo().setLegalCareMode(concept);
            }

            //Patient PHR

            concept = provider.getRandomCodeFromValueSet("HL79101");
            if (concept != null) {
                currentEncounter.getPatientsPHRInfo().setStatus(concept);
            }
        }

    }

    public String displayCurrentPage(String id) {
        if (id == null) {
            return "/automaton/pamAutomaton.seam";
        } else {
            return "/automaton/pamAutomaton.seam?graphExecutionResultsId=" + id;
        }
    }

    public boolean isProcessEnded() {

        if (graphExecutionResults.getGraphExecution().isValidateTransactionInstance() && graphExecutionResults.getExecutionStatus().equals
                (GraphExecutionStatus.VALIDATED)) {
            return true;
        } else if (!graphExecutionResults.getGraphExecution().isValidateTransactionInstance() && graphExecutionResults.getExecutionStatus().equals
                (GraphExecutionStatus.COMPLETED)) {
            return true;
        }
        return false;
    }

    private static List<TestStep> updateGraphExecutionResultsId(List<TestStep> testStepList, GraphExecutionResults graphExecutionResults) {
        List<TestStep> newTestStepList = new ArrayList<>();
        for (TestStep currentTestStep : testStepList) {
            TestStep newTestStep = currentTestStep;
            newTestStep.setExecutionResult(graphExecutionResults);
            newTestStepList.add(newTestStep);
        }
        return newTestStepList;
    }

    public static boolean isBP6ModeEnabled() {
        return SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.BP6);
    }

    public void createNewPhoneNumber() {
        this.newPhoneNumber = new PatientPhoneNumber();
        newPhoneNumber.setPatient(currentEncounter.getPatient());
    }

    public void addNewPhoneNumber() {
        if (this.newPhoneNumber.getValue() == null || this.newPhoneNumber.getValue().isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Phone number shall have a value");
        } else {
            currentEncounter.getPatient().addPhoneNumber(newPhoneNumber);
            newPhoneNumber = null;
        }
    }


    protected Class<? extends HL7Domain<?>> getHL7DomainClass() {
        return isBP6ModeEnabled()
                ? BP6HL7Domain.class
                : IHEHL7Domain.class;
    }


}
