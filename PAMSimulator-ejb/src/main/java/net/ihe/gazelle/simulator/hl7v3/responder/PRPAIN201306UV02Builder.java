package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.coctmt150002UV01.COCTMT150002UV01Organization;
import net.ihe.gazelle.hl7v3.coctmt150003UV03.COCTMT150003UV03Organization;
import net.ihe.gazelle.hl7v3.datatypes.AD;
import net.ihe.gazelle.hl7v3.datatypes.CD;
import net.ihe.gazelle.hl7v3.datatypes.CE;
import net.ihe.gazelle.hl7v3.datatypes.CS;
import net.ihe.gazelle.hl7v3.datatypes.EnGiven;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.INT;
import net.ihe.gazelle.hl7v3.datatypes.PN;
import net.ihe.gazelle.hl7v3.datatypes.TS;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01Subject1;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01Subject2;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02AdministrativeObservation;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02OtherIDs;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02QueryMatchObservation;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Subject;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Subject4;
import net.ihe.gazelle.hl7v3.voc.ActClass;
import net.ihe.gazelle.hl7v3.voc.ActClassControlAct;
import net.ihe.gazelle.hl7v3.voc.ActClassObservation;
import net.ihe.gazelle.hl7v3.voc.ActMood;
import net.ihe.gazelle.hl7v3.voc.EntityClass;
import net.ihe.gazelle.hl7v3.voc.EntityClassOrganization;
import net.ihe.gazelle.hl7v3.voc.EntityDeterminer;
import net.ihe.gazelle.hl7v3.voc.NullFlavor;
import net.ihe.gazelle.hl7v3.voc.ParticipationTargetSubject;
import net.ihe.gazelle.hl7v3.voc.XActMoodIntentEvent;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.common.utils.SVSConsumer;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageBuilder;
import net.ihe.gazelle.simulator.hl7v3.messages.MCCIMT00300UV01PartBuilder;
import net.ihe.gazelle.simulator.hl7v3.messages.MessageIdGenerator;
import net.ihe.gazelle.simulator.pam.dao.PersonDAO;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.Person;
import net.ihe.gazelle.simulator.pdqv3.pdc.PDQv3MessageBuilder;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>Abstract PRPAIN201306UV02Builder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class PRPAIN201306UV02Builder extends HL7v3MessageBuilder {

    /** Constant <code>OTHERIDS_VALUE_XPATH="/PRPA_IN201305UV02/controlActProcess/qu"{trunked}</code> */
    protected static final String OTHERIDS_VALUE_XPATH = "/PRPA_IN201305UV02/controlActProcess/queryByParameter/parameterList/otherIDsScopingOrganization[$index$]";
    /** Constant <code>CH_DOMAIN_KEYWORD="EPD"</code> */
    protected static final String CH_DOMAIN_KEYWORD = "EPD";
    private static Logger log = LoggerFactory.getLogger(PRPAIN201306UV02Builder.class);
    protected String pdqDomain = "ITI";
    /** Constant <code>PDQV3_QUERY_TRANSACTION="ITI-47"</code> */
    protected static final String PDQV3_QUERY_TRANSACTION = "ITI-47";
    /** Constant <code>XCPD_TRANSACTION="ITI-55"</code> */
    protected static final String XCPD_TRANSACTION = "ITI-55";
    private String transactionKeyword;

    /**
     * <p>Constructor for PRPAIN201306UV02Builder.</p>
     *
     * @param inTransactionKeyword a {@link java.lang.String} object.
     * @param inDomainKeyword a {@link java.lang.String} object.
     */
    public PRPAIN201306UV02Builder(String inTransactionKeyword, String inDomainKeyword) {
        this.pdqDomain = inDomainKeyword;
        this.transactionKeyword = inTransactionKeyword;
    }


    /**
     * <p>buildFindCandidatesQueryResponse.</p>
     *
     * @param messageId a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
     * @param patients a {@link java.util.List} object.
     * @param unknownDomains a {@link java.util.List} object.
     * @param domainsToReturn a {@link java.util.List} object.
     * @param queryByParameter a {@link net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter} object.
     * @param sutDeviceOid a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     */
    public PRPAIN201306UV02Type buildFindCandidatesQueryResponse(II messageId, List<Patient> patients,
                                                                 List<Integer> unknownDomains, List<HierarchicDesignator> domainsToReturn,
                                                                 PRPAMT201306UV02QueryByParameter queryByParameter,
                                                                 String sutDeviceOid) {
        SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyyMMddHHmmss");
        PRPAIN201306UV02Type response = new PRPAIN201306UV02Type();
        response.setITSVersion(PDQv3MessageBuilder.VERSION);
        response.addReceiver(MCCIMT00300UV01PartBuilder.buildReceiver(sutDeviceOid));
        response.setCreationTime(new TS(sdfDateTime.format(new Date())));
        response.setInteractionId(new II(MCCIIN000002UV01Builder.INTERACTION_ID_NAMESPACE, "PRPA_IN201306UV02"));
        response.setProcessingCode(new CS("T", null, null));
        response.setProcessingModeCode(new CS("T", null, null));
        response.setAcceptAckCode(new CS("NE", null, null));
        MessageIdGenerator generator = MessageIdGenerator.getGeneratorForActor(PatientManagerConstants.PDS);
        if (generator != null) {
            response.setId(new II(generator.getOidForMessage(), generator.getNextId()));
        } else {
            log.error("No MessageIdGenerator instance found for actor PDS");
        }

        PRPAIN201306UV02MFMIMT700711UV01ControlActProcess controlActProcess = new PRPAIN201306UV02MFMIMT700711UV01ControlActProcess();
        controlActProcess.setQueryByParameter(queryByParameter);
        controlActProcess.setClassCode(ActClassControlAct.CACT);
        controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);
        controlActProcess.setCode(new CD("PRPA_TE201306UV02", MCCIIN000002UV01Builder.INTERACTION_ID_NAMESPACE, null));
        buildControlActWrapper(messageId, patients, unknownDomains, domainsToReturn, queryByParameter, response, controlActProcess);
        response.setControlActProcess(controlActProcess);
        return response;
    }

    /**
     * <p>buildControlActWrapper.</p>
     *
     * @param messageId a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
     * @param patients a {@link java.util.List} object.
     * @param unknownDomains a {@link java.util.List} object.
     * @param domainsToReturn a {@link java.util.List} object.
     * @param queryByParameter a {@link net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter} object.
     * @param response a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     * @param controlActProcess a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess} object.
     */
    protected abstract void buildControlActWrapper(II messageId, List<Patient> patients, List<Integer> unknownDomains,
                                                   List<HierarchicDesignator> domainsToReturn,
                                                   PRPAMT201306UV02QueryByParameter queryByParameter,
                                                   PRPAIN201306UV02Type response,
                                                   PRPAIN201306UV02MFMIMT700711UV01ControlActProcess controlActProcess);

    /**
     * <p>populateSubjectOf1.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Subject} object.
     */
    protected PRPAMT201310UV02Subject populateSubjectOf1() {
        PRPAMT201310UV02Subject subject = new PRPAMT201310UV02Subject();
        subject.setQueryMatchObservation(populateQueryMatchObservation());
        return subject;
    }

    private PRPAMT201310UV02QueryMatchObservation populateQueryMatchObservation() {
        PRPAMT201310UV02QueryMatchObservation queryMatchObservation = new PRPAMT201310UV02QueryMatchObservation();
        queryMatchObservation.setClassCode(ActClassObservation.COND);
        queryMatchObservation.setMoodCode(ActMood.EVN);
        queryMatchObservation.setCode(new CD("IHE_PDQ", null, null));
        queryMatchObservation.setValue(new INT(100));
        return queryMatchObservation;
    }

    /**
     * <p>populateProviderOrganization.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.coctmt150003UV03.COCTMT150003UV03Organization} object.
     */
    public static COCTMT150003UV03Organization populateProviderOrganization() {
        COCTMT150003UV03Organization organization = COCTMT150003UV03PartBuilder.populateProviderOrganization(PreferenceService.getString("application_url"), true);
        return organization;
    }

    /**
     * <p>populateSubject.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01Subject1} object.
     */
    protected PRPAIN201306UV02MFMIMT700711UV01Subject1 populateSubject() {
        PRPAIN201306UV02MFMIMT700711UV01Subject1 subject = new PRPAIN201306UV02MFMIMT700711UV01Subject1();
        subject.setTypeCode("SUBJ");
        subject.setContextConductionInd(false);
        return subject;
    }

    /**
     * <p>populateRegistrationEvent.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent} object.
     */
    protected PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent populateRegistrationEvent() {
        PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent event = new PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent();
        event.setClassCode(ActClass.REG);
        event.setMoodCode(ActMood.EVN);
        event.setStatusCode(new CS("active", null, null));
        return event;
    }

    /**
     * <p>populateSubject1.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01Subject2} object.
     */
    protected PRPAIN201306UV02MFMIMT700711UV01Subject2 populateSubject1() {
        PRPAIN201306UV02MFMIMT700711UV01Subject2 subject1 = new PRPAIN201306UV02MFMIMT700711UV01Subject2();
        subject1.setTypeCode(ParticipationTargetSubject.SBJ);
        return subject1;
    }

    /**
     * <p>populatePatient.</p>
     *
     * @param currentPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param domainsToReturn a {@link java.util.List} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient} object.
     */
    protected PRPAMT201310UV02Patient populatePatient(Patient currentPatient,
                                                      List<HierarchicDesignator> domainsToReturn) {
        PRPAMT201310UV02Patient msgPatient = new PRPAMT201310UV02Patient();
        msgPatient.setClassCode("PAT");
        currentPatient = EntityManagerService.provideEntityManager().find(Patient.class, currentPatient.getId());
        // for CH:XCPD, the patientPerson shall be only populated with a nullflavor name
        if (!CH_DOMAIN_KEYWORD.equals(pdqDomain) || PDQV3_QUERY_TRANSACTION.equals(transactionKeyword)) {
            msgPatient.setPatientPerson(populatePersonPatient(currentPatient));
        } else {
            // CH-XCPD-004
            PRPAMT201310UV02Person emptyPatientPerson = new PRPAMT201310UV02Person();
            emptyPatientPerson.setClassCode(EntityClass.PSN);
            emptyPatientPerson.setDeterminerCode(EntityDeterminer.INSTANCE);
            PN nullFlavorName = new PN();
            nullFlavorName.setNullFlavor(NullFlavor.NA);
            emptyPatientPerson.addName(nullFlavorName);
            msgPatient.setPatientPerson(emptyPatientPerson);
        }
        buildPatientId(currentPatient, domainsToReturn, msgPatient);
        msgPatient.setStatusCode(new CS("active", null, null));
        msgPatient.setProviderOrganization(populateProviderOrganization());
        msgPatient.addSubjectOf1(populateSubjectOf1());
        if (SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.SEHE)) {
            msgPatient.addSubjectOf2(populatedSubjectOf2(currentPatient));
        }
        return msgPatient;
    }

    /**
     * <p>buildPatientId.</p>
     *
     * @param currentPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param domainsToReturn a {@link java.util.List} object.
     * @param msgPatient a {@link net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient} object.
     */
    protected abstract void buildPatientId(Patient currentPatient, List<HierarchicDesignator> domainsToReturn, PRPAMT201310UV02Patient msgPatient);

    /**
     * <p>populateAsOtherIDsForNotFoundID.</p>
     *
     * @param domain a {@link net.ihe.gazelle.simulator.pam.model.HierarchicDesignator} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02OtherIDs} object.
     */
    protected PRPAMT201310UV02OtherIDs populateAsOtherIDsForNotFoundID(HierarchicDesignator domain) {
        PRPAMT201310UV02OtherIDs otherIDs = createAsOtherIDs(domain);
        II nullflavor = new II();
        nullflavor.setNullFlavor(NullFlavor.UNK);
        otherIDs.addId(nullflavor);
        return otherIDs;
    }

    /**
     * <p>populateAsOtherIDs.</p>
     *
     * @param identifier a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02OtherIDs} object.
     */
    protected PRPAMT201310UV02OtherIDs populateAsOtherIDs(PatientIdentifier identifier) {
        PRPAMT201310UV02OtherIDs otherIDs = createAsOtherIDs(identifier.getDomain());
        if (identifier.getDomain() != null){
            II id = new II(identifier.getDomain().getUniversalID(), identifier.getIdNumber());
            otherIDs.addId(id);
        }
        return otherIDs;
    }

    private PRPAMT201310UV02OtherIDs createAsOtherIDs(HierarchicDesignator domain) {
        PRPAMT201310UV02OtherIDs otherIDs = new PRPAMT201310UV02OtherIDs();
        otherIDs.setClassCode("PSN");
        COCTMT150002UV01Organization organization = new COCTMT150002UV01Organization();
        if (domain != null){
            organization.addId(new II(domain.getUniversalID(), null));
        }
        organization.setClassCode(EntityClassOrganization.ORG);
        organization.setDeterminerCode(EntityDeterminer.INSTANCE);
        otherIDs.setScopingOrganization(organization);
        return otherIDs;
    }

    private PRPAMT201310UV02Subject4 populatedSubjectOf2(Patient currentPatient) {
        PRPAMT201310UV02Subject4 subjectOf2 = new PRPAMT201310UV02Subject4();
        subjectOf2.setTypeCode(ParticipationTargetSubject.SBJ);
        PRPAMT201310UV02AdministrativeObservation observation = new PRPAMT201310UV02AdministrativeObservation();
        observation.setClassCode(ActClassObservation.OBS);
        observation.setCode(new CD("882-1", "ABO+Rh group", "1.3.6.1.4.1.12009.10.2.3"));
        observation.getCode().setCodeSystemName("LOINC");
        if (currentPatient.getBloodGroup() != null) {
            Concept bloodGroup = SVSConsumer.getConceptForCode("2.16.840.1.113883.3.3731.1.203.18", "en-sa",
                    currentPatient.getBloodGroup());
            if (bloodGroup != null) {
                observation.setValue(new CE(bloodGroup.getCode(), bloodGroup.getDisplayName(), bloodGroup
                        .getCodeSystem()));
                ((CE) observation.getValue()).setCodeSystemName(bloodGroup.getCodeSystemName());
            } else {
                log.warn("Recorded blood group : %s does not match any value from 2.16.840.1.113883.3.3731.1.203.18", currentPatient.getBloodGroup());
                observation.setValue(new CE());
                observation.getValue().setNullFlavor(NullFlavor.NAV);
            }
        } else {
            observation.setValue(new CE());
            observation.getValue().setNullFlavor(NullFlavor.NAV);
        }
        subjectOf2.setAdministrativeObservation(observation);
        return subjectOf2;
    }

    private PRPAMT201310UV02Person populatePersonPatient(Patient currentPatient) {
        PRPAMT201310UV02Person patientPerson = new PRPAMT201310UV02Person();
        patientPerson.setClassCode(EntityClass.PSN);
        patientPerson.setDeterminerCode(EntityDeterminer.INSTANCE);
        if (!currentPatient.getFirstName().isEmpty() || !currentPatient.getLastName().isEmpty()) {
            PN patientName;
            patientName = buildPersonName(currentPatient.getFirstName(), currentPatient.getLastName(), null);
            if (currentPatient.getSecondName() != null && !currentPatient.getSecondName().isEmpty()) {
                EnGiven second = new EnGiven();
                second.addMixed(currentPatient.getSecondName());
                patientName.addGiven(second);
            }
            if (currentPatient.getThirdName() != null && !currentPatient.getThirdName().isEmpty()) {
                EnGiven third = new EnGiven();
                third.addMixed(currentPatient.getThirdName());
                patientName.addGiven(third);
            }
            patientPerson.addName(patientName);
        }
        if ((currentPatient.getAlternateFirstName() != null && !currentPatient.getAlternateFirstName().isEmpty())
                || (currentPatient.getAlternateLastName() != null && !currentPatient.getAlternateLastName().isEmpty())) {
            PN alternatePatientName = buildPersonName(currentPatient.getAlternateFirstName(),
                    currentPatient.getAlternateLastName(), null);
            if (currentPatient.getAlternateSecondName() != null && !currentPatient.getAlternateSecondName().isEmpty()) {
                EnGiven second = new EnGiven();
                second.addMixed(currentPatient.getAlternateSecondName());
                alternatePatientName.addGiven(second);
            }
            if (currentPatient.getAlternateThirdName() != null && !currentPatient.getAlternateThirdName().isEmpty()) {
                EnGiven third = new EnGiven();
                third.addMixed(currentPatient.getAlternateThirdName());
                alternatePatientName.addGiven(third);
            }
            patientPerson.addName(alternatePatientName);
        }
        String genderCode = currentPatient.getGenderCode();
        if (genderCode != null && !genderCode.isEmpty()) {
            patientPerson.setAdministrativeGenderCode(populateCEFromSVS(genderCode, "HL70001"));
        }
        if (currentPatient.getDateOfBirth() != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            patientPerson.setBirthTime(new TS(sdf.format(currentPatient.getDateOfBirth())));
        }
        // SeHe spec forbids the use of the address in the XCPD response
        if (!SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.SEHE)) {
            AD addr = populateAddr(currentPatient.getAddressLine(), currentPatient.getCity(), currentPatient.getState(),
                    currentPatient.getCountryCode(), currentPatient.getZipCode());
            if (addr != null) {
                patientPerson.addAddr(addr);
            }
            if (currentPatient.getPhoneNumbers() != null && !currentPatient.getPhoneNumbers().isEmpty()) {
                for (PatientPhoneNumber phoneNumbers : currentPatient.getPhoneNumbers()) {
                    patientPerson.addTelecom(populateTelecom(phoneNumbers));
                }
            }
        }
        List<Person> patientRelatives = PersonDAO.getRelationsForPatient(currentPatient);
        if (patientRelatives != null) {
            for (Person relative : patientRelatives) {
                PRPAMT201310UV02PersonalRelationship personalRelationship = buildPersonRelationshipForFindCandidatesResponse(relative);
                patientPerson.addPersonalRelationship(personalRelationship);
            }
        }
        // FIXME asOtherIDs
        return patientPerson;
    }
}
