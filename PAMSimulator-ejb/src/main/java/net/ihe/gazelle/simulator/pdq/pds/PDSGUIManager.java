package net.ihe.gazelle.simulator.pdq.pds;

import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pdq.util.AbstractPDQPDSGuiManager;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * <p>PDSGUIManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pdqpdsGUIManagerBean")
@Scope(ScopeType.PAGE)
public class PDSGUIManager extends AbstractPDQPDSGuiManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** {@inheritDoc} */
	@Override
    public String getUrlForHL7Messages(SimulatorResponderConfiguration conf) {
        return "/messages/browser.seam?simulatedActor=" + conf.getSimulatedActor().getId()
                + "&domain=" + conf.getDomain().getId();
    }

    public String getUrlForHL7Messages(){
		Actor pds = Actor.findActorWithKeyword(PatientManagerConstants.PDS);
		return "/messages/browser.seam?simulatedActor=" + pds.getId() + "&responder=" + pds.getId();
	}

	/** {@inheritDoc} */
	@Override
	@Create
	public void getSimulatorResponderConfiguration() {
		getSimulatorResponderConfigurations(Actor.findActorWithKeyword(PatientManagerConstants.PDS), null, null);
	}
}
