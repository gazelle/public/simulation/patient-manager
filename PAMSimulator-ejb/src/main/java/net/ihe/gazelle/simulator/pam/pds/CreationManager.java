package net.ihe.gazelle.simulator.pam.pds;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.simulator.application.PatientService;
import net.ihe.gazelle.patient.AddressType;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.application.PatientService;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.action.SendindException;
import net.ihe.gazelle.simulator.pam.model.*;
import net.ihe.gazelle.simulator.pam.action.SendindException;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.model.*;
import net.ihe.gazelle.simulator.pam.util.PatientIdentifierUtil;
import net.ihe.gazelle.simulator.pam.utils.Pair;
import org.apache.log4j.Logger;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>CreationManager class.</p>
 *
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 * @author Anne-Gaëlle Bergé
 * @version $Id: $Id
 */
@Name("creationManager")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 60000)
public class CreationManager extends PDS implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String MESSAGE_TYPE = "ADT^A28^ADT_A05";

    private static Logger log = Logger.getLogger(CreationManager.class);

    private boolean displayEditPatientPanel;

    // id management
    private String newPatientIdentifier;
    private List<Pair<Integer, String>> patientIdentifiers;
    private Integer idForUpdate;

    /**
     * <p>Getter for the field <code>idForUpdate</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getIdForUpdate() {
        return idForUpdate;
    }

    /**
     * <p>Setter for the field <code>idForUpdate</code>.</p>
     *
     * @param idForUpdate a {@link java.lang.Integer} object.
     */
    public void setIdForUpdate(Integer idForUpdate) {
        this.idForUpdate = idForUpdate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void updateSelectedEvent() {
        this.canSendMessage = false;
        ITI30ManagerLocal bean = (ITI30ManagerLocal) Component.getInstance("iti30Manager");
        bean.setSelectedEvent(ITI30Manager.ITI30EVENT.A28);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createNewRelationship() {
        setCurrentRelationship(new Person(selectedPatient));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveRelationship() {
        selectedPatient.addPersonalRelationship(getCurrentRelationship());
        setCurrentRelationship(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDisplayDDSPanel(boolean displayDDSPanel) {
        this.displayDDSPanel = displayDDSPanel;
    }

    /**
     * <p>getDisplayDDSPanel.</p>
     *
     * @return a boolean.
     */
    public boolean getDisplayDDSPanel() {
        return displayDDSPanel;
    }

    /**
     * <p>Setter for the field <code>displayEditPatientPanel</code>.</p>
     *
     * @param displayEditPatientPanel a boolean.
     */
    public void setDisplayEditPatientPanel(boolean displayEditPatientPanel) {
        this.displayEditPatientPanel = displayEditPatientPanel;
        if (displayEditPatientPanel == true) {
            generatePatientIdentifiersList();
        }
    }

    /**
     * <p>Getter for the field <code>displayEditPatientPanel</code>.</p>
     *
     * @return a boolean.
     */
    public boolean getDisplayEditPatientPanel() {
        return displayEditPatientPanel;
    }

    /**
     * <p>Getter for the field <code>newPatientIdentifier</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNewPatientIdentifier() {
        return newPatientIdentifier;
    }

    /**
     * <p>Setter for the field <code>newPatientIdentifier</code>.</p>
     *
     * @param newPatientIdentifier a {@link java.lang.String} object.
     */
    public void setNewPatientIdentifier(String newPatientIdentifier) {
        this.newPatientIdentifier = newPatientIdentifier;
    }

    /**
     * <p>Setter for the field <code>patientIdentifiers</code>.</p>
     *
     * @param patientIdentifiers a {@link java.util.List} object.
     */
    public void setPatientIdentifiers(List<Pair<Integer, String>> patientIdentifiers) {
        this.patientIdentifiers = patientIdentifiers;
    }

    /**
     * <p>Getter for the field <code>patientIdentifiers</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Pair<Integer, String>> getPatientIdentifiers() {
        return patientIdentifiers;
    }

    @Override
    protected List<TransactionInstance> send() {
        if (selectedPatient.getId() == null) {
            PatientHistory h = patientService.createPatientVersion(selectedPatient);
            selectedPatient = h.getCurrentPatient();
        }
        try {
            List<TransactionInstance> transactions = patientService.sendADTMessage(
                    new PatientService.Exchange(
                            selectedSUT,
                            sendingApplication,
                            sendingFacility,
                            sendingActor,
                            receivingActor,
                            simulatedTransaction,
                            getHL7Domain(),
                            PatientService.ADT.A28,
                            selectedPatient
                    ));
            selectedPatient = null;
            displayPatientsList = false;
            return transactions;
        } catch (HL7Exception e) {
            throw new SendindException(e.getMessage(),e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionOnSelectedPatient(Patient inPatient) {
        selectedPatient = inPatient;
        this.canSendMessage = true;
        sendMessage();
    }

    /**
     * <p>generateAndSavePatient.</p>
     */
    public void generateAndSavePatient() {
        selectedPatient = generateNewPatient(sendingActor);
        if (selectedPatient != null) {
            displayEditPatientPanel = true;
            displayDDSPanel = false;
            displayPatientsList = false;
            this.canSendMessage = true;
            generatePatientIdentifiersList();
        }
    }



    /**
     * <p>displayDDSPanel.</p>
     */
    public void displayDDSPanel() {
        displayDDSPanel = true;
        selectedPatient = null;
    }

    private void generatePatientIdentifiersList() {
        patientIdentifiers = PatientIdentifierUtil.generatePatientIdentifierList(selectedPatient);
    }

    /**
     * <p>changeIdentifier.</p>
     *
     * @param pidIndex    a {@link java.lang.Integer} object.
     * @param newIdString a {@link java.lang.String} object.
     */
    public void changeIdentifier(Integer pidIndex, String newIdString) {
        String[] pidComponents = newIdString.split("\\^");
        String fullId = null;
        String newIdentifierTypeCode = null;
        if (pidComponents.length > 4) {
            int cx5Index = newIdString.lastIndexOf("^");
            fullId = newIdString.substring(0, cx5Index);
            newIdentifierTypeCode = newIdString.substring(cx5Index + 1, newIdString.length());
        } else {
            fullId = newIdString;
            newIdentifierTypeCode = null;
        }
        PatientIdentifier newIdentifier = new PatientIdentifier(fullId, newIdentifierTypeCode);
        PatientIdentifier modifiedPid = selectedPatient.getPatientIdentifiers().get(pidIndex);
        if (!newIdentifier.equals(modifiedPid)) {
            removeIdentifier(modifiedPid);
            selectedPatient.getPatientIdentifiers().add(newIdentifier);
            generatePatientIdentifiersList();
        }
        idForUpdate = null;
    }

    /**
     * <p>addIdentifier.</p>
     */
    public void addIdentifier() {
        if ((newPatientIdentifier != null) && !newPatientIdentifier.isEmpty()) {
            String[] pidComponents = newPatientIdentifier.split("\\^");
            String newIdentifierTypeCode = null;
            if (pidComponents.length > 4) {
                int cx5Index = newPatientIdentifier.lastIndexOf("^");
                newIdentifierTypeCode = newPatientIdentifier.substring(cx5Index + 1, newPatientIdentifier.length());
            } else {
                newIdentifierTypeCode = null;
            }
            PatientIdentifier id = new PatientIdentifier(newPatientIdentifier, newIdentifierTypeCode);
            if (selectedPatient.getPatientIdentifiers() == null) {
                List<PatientIdentifier> identifiers = new ArrayList<PatientIdentifier>();
                identifiers.add(id);
                selectedPatient.setPatientIdentifiers(identifiers);
            } else {
                selectedPatient.getPatientIdentifiers().add(id);
            }
            newPatientIdentifier = null;
        }
        generatePatientIdentifiersList();
    }

    /**
     * <p>removeIdentifier.</p>
     *
     * @param pidIndex a {@link java.lang.Integer} object.
     */
    public void removeIdentifier(Integer pidIndex) {
        PatientIdentifier removedPid = selectedPatient.getPatientIdentifiers().get(pidIndex);
        removeIdentifier(removedPid);
        generatePatientIdentifiersList();
    }

    private void removeIdentifier(PatientIdentifier identifier) {
        if (selectedPatient.getPatientIdentifiers().contains(identifier)) {
            selectedPatient.getPatientIdentifiers().remove(identifier);
        } else {
            log.error("this identifier is not linked to the current patient");
        }
    }
}
