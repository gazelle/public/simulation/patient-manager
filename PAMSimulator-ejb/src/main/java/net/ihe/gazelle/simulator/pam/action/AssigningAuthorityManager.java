package net.ihe.gazelle.simulator.pam.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignatorQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;

/**
 * <b>Class Description : </b>AssigningAuthorityManager<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 22/04/16
 *


 */
@Name("assigningAuthorityManager")
@Scope(ScopeType.PAGE)
public class AssigningAuthorityManager implements QueryModifier<HierarchicDesignator>{

    private HierarchicDesignator hierarchicDesignator;
    private Filter<HierarchicDesignator> filter;
    private boolean displayOnlyForTool;
    private ApplicationConfiguration appAccountNumber;
    private ApplicationConfiguration appVisitNumber;
    private ApplicationConfiguration appMovementNumber;
    private ApplicationConfiguration appPatientId;
    private ApplicationConfiguration tosLink;
    private boolean displayOnlyForCat;

    @Create
    public void init(){
        appAccountNumber = ApplicationConfiguration.getApplicationConfigurationByVariable("account_assigning_authority");
        appVisitNumber = ApplicationConfiguration.getApplicationConfigurationByVariable("visit_assigning_authority");
        appMovementNumber = ApplicationConfiguration.getApplicationConfigurationByVariable("movement_assigning_authority");
        appPatientId = ApplicationConfiguration.getApplicationConfigurationByVariable("patient_assigning_authority");
    }

    public HierarchicDesignator getHierarchicDesignator() {
        return hierarchicDesignator;
    }

    public void setHierarchicDesignator(HierarchicDesignator hierarchicDesignator) {
        this.hierarchicDesignator = hierarchicDesignator;
    }

    public String getTosLink() {
        return ApplicationConfiguration.getValueOfVariable("link_to_cgu");
    }

    public Filter<HierarchicDesignator> getFilter() {
        if (this.filter == null){
            filter = new Filter<HierarchicDesignator>(getHQLCriterionsForFilter());
        }
        return filter;
    }

    public FilterDataModel<HierarchicDesignator> getHierarchicDesignators(){
        return new FilterDataModel<HierarchicDesignator>(getFilter()) {
            @Override
            protected Object getId(HierarchicDesignator hierarchicDesignator) {
                return hierarchicDesignator.getId();
            }
        };
    }

    private HQLCriterionsForFilter<HierarchicDesignator> getHQLCriterionsForFilter() {
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        query.usage().neq(DesignatorType.FILTERED_DOMAIN);
        HQLCriterionsForFilter<HierarchicDesignator>  criteria = query.getHQLCriterionsForFilter();
        criteria.addQueryModifier(this);
        return criteria;
    }

    public void createNewHierarchicDesignator(){
        hierarchicDesignator = new HierarchicDesignator();
        hierarchicDesignator.setToolAssigningAuthority(true);
        hierarchicDesignator.setUsage(DesignatorType.PATIENT_ID);
        hierarchicDesignator.setIndex(0);
    }

    public void save(){
        try {
            hierarchicDesignator.save(EntityManagerService.provideEntityManager(), true);
            hierarchicDesignator = null;
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Assigning authority has been saved");
        }catch (Exception e){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
        }
    }

    public void editHierarchicDesignator(HierarchicDesignator hd){
        this.hierarchicDesignator = hd;
    }

    public void resetIndex(){
        hierarchicDesignator.setIndex(0);
        try {
            hierarchicDesignator.save(EntityManagerService.provideEntityManager(), false);
        } catch (Exception e){
            // must never been thrown in that case
        }
    }

    public boolean isDisplayOnlyForTool() {
        return displayOnlyForTool;
    }

    public void setDisplayOnlyForTool(boolean displayOnlyForTool) {
        this.displayOnlyForTool = displayOnlyForTool;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<HierarchicDesignator> queryBuilder, Map<String, Object> filterValuesApplied) {
        HierarchicDesignatorQuery query = new HierarchicDesignatorQuery();
        if (displayOnlyForTool){
            queryBuilder.addRestriction(query.toolAssigningAuthority().eqRestriction(true));
        }
        if (displayOnlyForCat){
            queryBuilder.addRestriction(query.catUsage().eqRestriction(true));
        }
    }

    public ApplicationConfiguration getAppAccountNumber() {
        return appAccountNumber;
    }

    public void setAppAccountNumber(ApplicationConfiguration appAccountNumber) {
        this.appAccountNumber = appAccountNumber;
    }

    public ApplicationConfiguration getAppVisitNumber() {
        return appVisitNumber;
    }

    public void setAppVisitNumber(ApplicationConfiguration appVisitNumber) {
        this.appVisitNumber = appVisitNumber;
    }

    public ApplicationConfiguration getAppMovementNumber() {
        return appMovementNumber;
    }

    public void setAppMovementNumber(ApplicationConfiguration appMovementNumber) {
        this.appMovementNumber = appMovementNumber;
    }

    public ApplicationConfiguration getAppPatientId() {
        return appPatientId;
    }

    public void setAppPatientId(ApplicationConfiguration appPatientId) {
        this.appPatientId = appPatientId;
    }

    public List<SelectItem> getAvailableUsages(){
        return DesignatorType.listDesignatorType();
    }

    public void deactivateAssigningAuthority(HierarchicDesignator hd){
        hd.setDefaultAssigningAuthority(false);
        hd.setToolAssigningAuthority(false);
        try {
            hd.save(EntityManagerService.provideEntityManager(), false);
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "This assigning authority is not marked as 'owned by the tool' anymore");
        } catch (Exception e){
            // must never been thrown in that case
        }
    }

    public List<SelectItem> getAssigningAuthoritiesForUsage(String designatorType){
        DesignatorType type = DesignatorType.getDesignatorTypeByValue(designatorType);
        return HierarchicDesignatorDAO.getAssigningAuthorities(type);
    }

    private ApplicationConfiguration saveApplicationConfiguration(ApplicationConfiguration pref){
        EntityManager em = EntityManagerService.provideEntityManager();
        return em.merge(pref);
    }

    public void updateForAccount(){
        appAccountNumber = saveApplicationConfiguration(appAccountNumber);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Default assigning authority for account numbers hass been set");
    }

    public void updateForVisit(){
        appVisitNumber = saveApplicationConfiguration(appVisitNumber);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Default assigning authority for visit numbers has been set");
    }

    public void updateForMovement(){
        appMovementNumber = saveApplicationConfiguration(appMovementNumber);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Default assigning authority for movement identifiers has been set");
    }

    public void updateForPatient(){
        appPatientId = saveApplicationConfiguration(appPatientId);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Default assigning authority for patient identifiers has been set");
    }

    public boolean isCatFeatureEnabled(){
        return SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.CONNECTATHON);
    }

    public boolean isDisplayOnlyForCat() {
        return displayOnlyForCat;
    }

    public void setDisplayOnlyForCat(boolean displayOnlyForCat) {
        this.displayOnlyForCat = displayOnlyForCat;
    }
}
