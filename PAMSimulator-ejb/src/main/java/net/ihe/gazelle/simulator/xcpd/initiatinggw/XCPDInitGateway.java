package net.ihe.gazelle.simulator.xcpd.initiatinggw;

import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.messages.MCCIIN000002UV01Parser;
import net.ihe.gazelle.simulator.hl7v3.messages.PRPAIN201306UV02Parser;
import net.ihe.gazelle.simulator.hl7v3.sut.action.HL7v3ResponderSUTConfigurationDAO;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pdq.util.AbstractPDQPDC;
import net.ihe.gazelle.simulator.pdqv3.pdc.PDQv3MessageBuilder;
import net.ihe.gazelle.simulator.testdata.model.TestData;
import net.ihe.gazelle.simulator.xcpd.common.DeferredTransactionDAO;
import net.ihe.gazelle.xua.dao.PicketLinkCredentialsDAO;
import net.ihe.gazelle.xua.model.PicketLinkCredentials;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>XCPDInitGateway<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 14/12/15
 */
@Name("xcpdInitGateway")
@Scope(ScopeType.PAGE)
public class XCPDInitGateway extends AbstractPDQPDC implements Serializable {

    /** Constant <code>SINGLE_PAGE=1</code> */
    public static final int SINGLE_PAGE = 1;
    private static Logger log = LoggerFactory.getLogger(XCPDInitGateway.class);


    private boolean deferredMode = false;
    private List<TransactionInstance> messages;
    private HL7v3ResponderSUTConfiguration selectedSUT;
    private List<HL7v3ResponderSUTConfiguration> availableSystems;
    private String senderDeviceId;
    private String organizationOid;
    private String homeCommunityId;
    private PDQv3MessageBuilder messageBuilder;
    private XCPDInitGatewaySender sender;
    private String principalCareProviderExtension;
    private String principalCareProviderRoot;
    private String ackCode;
    private PicketLinkCredentials selectedCredentials;
    private boolean useXua = false;
    private boolean xuaFeatureEnabled;

    /** {@inheritDoc} */
    @Override
    public void sendMessage() {
        if (selectedSUT != null) {
            messageBuilder = new PDQv3MessageBuilder(patientCriteria, selectedSUT, deferredMode);
            messageBuilder.setPrincipalCareProviderExtension(principalCareProviderExtension);
            messageBuilder.setPrincipalCareProviderRoot(principalCareProviderRoot);
            PRPAIN201305UV02Type request = messageBuilder.buildPatientRegistryFindCandidatesQuery();
            sender = new XCPDInitGatewaySender(selectedSUT);
            try {
                if (deferredMode) {
                    sender.sendDeferredCrossGatewayPatientDiscoveryRequest(request, selectedCredentials);
                } else {
                    sender.sendCrossGatewayPatientDiscoveryRequest(request, selectedCredentials);
                }
            } catch (SoapSendException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error sending message to SUT : " + e.getMessage());
                return;
            }
            TransactionInstance instance = sender.getTransactionInstance();
            messages = new ArrayList<TransactionInstance>();
            messages.add(instance);
            pageNumber = SINGLE_PAGE;
            // at the end
            try {
                parseResponse(instance.getResponse().getContent(), instance.getResponse().getType());
                if (deferredMode && instance.getResponse() != null) {
                    DeferredTransactionDAO.buildAndSaveDeferredTransaction(request, instance, ackCode);
                }
            } catch (NullPointerException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,"No response received from the responding gateway");
            } catch (JAXBException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Unable to parse the response");
                log.error(e.getMessage(), e);
            }
            displayCriteriaForm = false;
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN,
                    "Please, first select the system under test to which you want to send the message");
        }
    }

    /** {@inheritDoc} */
    @Override
    public void cleanUpQuery() {
        super.cleanUpQuery();
        principalCareProviderExtension = null;
        principalCareProviderRoot = null;
    }

    private void parseResponse(byte[] response, String responseType) throws JAXBException {
        if (response != null) {
            if (responseType.equals("PRPA_IN201306UV02")) {
                PRPAIN201306UV02Parser parser = new PRPAIN201306UV02Parser(FacesMessages.instance());
                List<Patient> patients = parser.extractPatients(response);
                listReceivedPatients(parser, patients);
            } else if (responseType.equals("MCCI_IN000002UV01")) {
                ackCode = MCCIIN000002UV01Parser.parseXCPDDeferredAcknowledgement(response);
                if (ackCode == null) {
                    FacesMessages.instance().add(StatusMessage.Severity.INFO,"Message has been acknowledged. Waiting for response in background");
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.WARN,"Receiver rejected the messages: " + ackCode);
                }
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Received " + responseType + ": cannot be parsed");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,"No response received from the supplier");
        }
    }

    /** {@inheritDoc} */
    @Override
    public void getNextResults() {
        // not used in XCPD
    }

    /** {@inheritDoc} */
    @Override
    public void cancelQuery() {
        // not used in XCPD
    }

    /** {@inheritDoc} */
    @Override
    @Create
    public void initializeRequest() {
        super.initPage();
        if (selectedTransaction == null) {
            selectedTransaction = Transaction.GetTransactionByKeyword("ITI-55");
        }
        if (availableSystems == null || availableSystems.isEmpty()) {
            availableSystems = HL7v3ResponderSUTConfigurationDAO.getSUTForTransactionDomain("ITI-55", "IHE");

        }
        initializePatientCriteria();
        messages = null;
        receivedPatients = null;
        displayCriteriaForm = true;
        ackCode = null;
        principalCareProviderRoot = null;
        principalCareProviderExtension = null;
        xuaFeatureEnabled = SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.XUA);

    }

    /**
     * <p>Getter for the field <code>messages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TransactionInstance> getMessages() {
        return messages;
    }

    /**
     * <p>Getter for the field <code>availableSystems</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HL7v3ResponderSUTConfiguration> getAvailableSystems() {
        return availableSystems;
    }

    /**
     * <p>Getter for the field <code>senderDeviceId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSenderDeviceId() {
        if (senderDeviceId == null) {
            this.senderDeviceId = PreferenceService.getString("hl7v3_xcpd_initgw_device_id");
        }
        return senderDeviceId;
    }

    /**
     * <p>Getter for the field <code>organizationOid</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOrganizationOid() {
        if (organizationOid == null) {
            this.organizationOid = PreferenceService.getString("hl7v3_organization_oid");
        }
        return organizationOid;
    }

    /**
     * <p>Getter for the field <code>homeCommunityId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getHomeCommunityId() {
        if (homeCommunityId == null) {
            this.homeCommunityId = PreferenceService.getString("hl7v3_xcpd_initgw_home_community_id");
        }
        return homeCommunityId;
    }

    /**
     * <p>isDeferredMode.</p>
     *
     * @return a boolean.
     */
    public boolean isDeferredMode() {
        return deferredMode;
    }

    /**
     * <p>Setter for the field <code>deferredMode</code>.</p>
     *
     * @param deferredMode a boolean.
     */
    public void setDeferredMode(boolean deferredMode) {
        this.deferredMode = deferredMode;
    }

    /**
     * <p>Getter for the field <code>selectedSUT</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
     */
    public HL7v3ResponderSUTConfiguration getSelectedSUT() {
        return selectedSUT;
    }

    /**
     * <p>Setter for the field <code>selectedSUT</code>.</p>
     *
     * @param selectedSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
     */
    public void setSelectedSUT(HL7v3ResponderSUTConfiguration selectedSUT) {
        this.selectedSUT = selectedSUT;
    }

    /**
     * <p>Getter for the field <code>principalCareProviderExtension</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPrincipalCareProviderExtension() {
        return principalCareProviderExtension;
    }

    /**
     * <p>Setter for the field <code>principalCareProviderExtension</code>.</p>
     *
     * @param principalCareProviderExtension a {@link java.lang.String} object.
     */
    public void setPrincipalCareProviderExtension(String principalCareProviderExtension) {
        this.principalCareProviderExtension = principalCareProviderExtension;
    }

    /**
     * <p>Getter for the field <code>principalCareProviderRoot</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPrincipalCareProviderRoot() {
        return principalCareProviderRoot;
    }

    /**
     * <p>Setter for the field <code>principalCareProviderRoot</code>.</p>
     *
     * @param principalCareProviderRoot a {@link java.lang.String} object.
     */
    public void setPrincipalCareProviderRoot(String principalCareProviderRoot) {
        this.principalCareProviderRoot = principalCareProviderRoot;
    }

    /**
     * <p>getAvailableSTSCredentials.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PicketLinkCredentials> getAvailableSTSCredentials() {
        return PicketLinkCredentialsDAO.listAllCredentials();
    }

    /**
     * <p>Getter for the field <code>selectedCredentials</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.xua.model.PicketLinkCredentials} object.
     */
    public PicketLinkCredentials getSelectedCredentials() {
        return selectedCredentials;
    }

    /**
     * <p>Setter for the field <code>selectedCredentials</code>.</p>
     *
     * @param selectedCredentials a {@link net.ihe.gazelle.xua.model.PicketLinkCredentials} object.
     */
    public void setSelectedCredentials(PicketLinkCredentials selectedCredentials) {
        this.selectedCredentials = selectedCredentials;
    }

    /**
     * <p>isUseXua.</p>
     *
     * @return a boolean.
     */
    public boolean isUseXua() {
        return useXua;
    }

    /**
     * <p>Setter for the field <code>useXua</code>.</p>
     *
     * @param useXua a boolean.
     */
    public void setUseXua(boolean useXua) {
        this.useXua = useXua;
        // make sure no assertion will be sent if useXua is false
        if (!this.useXua) {
            selectedCredentials = null;
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void setDeferredOption(TestData testData) {
        testData.setDeferredOption(deferredMode);
    }

    /** {@inheritDoc} */
    @Override
    protected void setCredentialsForTestData(TestData testData) {
        testData.setCredentials(selectedCredentials);
    }

    /** {@inheritDoc} */
    @Override
    protected void initializePageUsingTestData(TestData inTestData) {
        super.initializePageUsingTestData(inTestData);
        this.deferredMode = inTestData.isDeferredOption();
        this.selectedCredentials = inTestData.getCredentials();
        this.useXua = (selectedCredentials != null);
    }

    /**
     * <p>isXuaFeatureEnabled.</p>
     *
     * @return a boolean.
     */
    public boolean isXuaFeatureEnabled() {
        return this.xuaFeatureEnabled;
    }
}
