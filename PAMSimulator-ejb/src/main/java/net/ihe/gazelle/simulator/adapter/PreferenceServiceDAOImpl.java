package net.ihe.gazelle.simulator.adapter;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.application.PreferenceServiceDAO;
import org.jboss.seam.annotations.Name;

/**
 * DAO for Application preferences
 */
@Name("preferenceServiceDAO")
public class PreferenceServiceDAOImpl implements PreferenceServiceDAO {

    /**
     * Empty Constructor for injection.
     */
    public PreferenceServiceDAOImpl() {
    //Empty
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String retrievePreferenceServiceValue(String preferenceName){
        return PreferenceService.getString(preferenceName);
    }
}
