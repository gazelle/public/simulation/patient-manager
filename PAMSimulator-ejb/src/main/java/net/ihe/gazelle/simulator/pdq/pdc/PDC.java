package net.ihe.gazelle.simulator.pdq.pdc;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.ACK;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.exception.HL7v2ParsingException;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfigurationQuery;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message.QBP_Q21;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message.RSP_K21;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message.RSP_ZV2;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.PID;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pdq.hl7.PDCMessageBuilder;
import net.ihe.gazelle.simulator.pdq.hl7.RSPMessageDecoder;
import net.ihe.gazelle.simulator.pdq.util.AbstractPDQPDC;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * <p>PDC class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pdqpdc")
@Scope(ScopeType.PAGE)
public class PDC extends AbstractPDQPDC implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5229238212708176997L;

    private static Logger log = LoggerFactory.getLogger(PDC.class);

    private List<Transaction> transactions;
    private List<TransactionInstance> hl7Messages;
    private Encounter encounterCriteria;
    private Movement movementCriteria;
    private HL7V2ResponderSUTConfiguration selectedSUT;
    private String continuationPointer;
    private PatientIdentifier accountNumber;
    private List<HL7V2ResponderSUTConfiguration> availableSystems;
    // not used in JSF
    private QBP_Q21 sentMessage;
    private String messageType;


    /**
     * <p>initializeRequest.</p>
     */
    @Create
    public void initializeRequest() {
        super.initPage();
        if (patientCriteria == null) {
            initializePatientCriteria();
            transactions = new ArrayList<Transaction>();
            transactions.add(Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI21));
            transactions.add(Transaction.GetTransactionByKeyword(PatientManagerConstants.ITI22));
            selectedTransaction = transactions.get(0);
            accountNumber = new PatientIdentifier();
            accountNumber.setDomain(new HierarchicDesignator());
        }
        if (encounterCriteria == null) {
            encounterCriteria = new Encounter();
        }
        if (movementCriteria == null) {
            movementCriteria = new Movement();
        }
        continuationPointer = null;
        currentDomainReturned = new AssigningAuthority();
        hl7Messages = null;
        receivedPatients = null;
        displayCriteriaForm = true;
        sentMessage = null;
        cancellation = null;
        availableSystems = getLisOfSystemsUnderTest();
    }

    private List<HL7V2ResponderSUTConfiguration> getLisOfSystemsUnderTest() {
        HL7V2ResponderSUTConfigurationQuery query = new HL7V2ResponderSUTConfigurationQuery();
        query.listUsages().transaction().in(transactions);
        query.name().order(true);
        return query.getList();
    }

    /**
     * Build, send the message and parse the response
     */
    public void sendMessage() {
        if (selectedSUT != null) {
            // we do not used this formatting of the field when building the query but we may want to use it to display the query parameters
            buildAccountNumber();
            addPatientIdentifierToList();
            if (!limit) {
                limitValue = null;
            }
            if (selectedTransaction.getKeyword().equals(PatientManagerConstants.ITI21)) {
                messageType = "QBP^Q22^QBP_Q21";
            } else {
                messageType = "QBP^ZV1^QBP_Q21";
            }
            PDCMessageBuilder messageBuilder = new PDCMessageBuilder(patientCriteria, encounterCriteria,
                    movementCriteria, domainsReturned, limitValue);
            messageBuilder.setAccountNumber(accountNumber);
            try {
                sentMessage = (QBP_Q21) messageBuilder.buildQBPMessageWithoutPointer(selectedSUT, selectedTransaction);
                if ((sentMessage.getQPD().getDemographicsFields() == null)
                        || (sentMessage.getQPD().getDemographicsFields().length == 0)) {
                    FacesMessages.instance().add(StatusMessage.Severity.WARN, "You must specify at least one criteria");
                    return;
                }
            } catch (HL7Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to build QBP message for the following reason: " + e.getMessage());
                log.error(e.getMessage(), e);
                return;
            }
            try {
                // TODO message type depends on transaction
                Initiator hl7Initiator = new Initiator(selectedSUT, simulatorDomainKeyword, "PatientManager",
                        Actor.findActorWithKeyword("PDC"), selectedTransaction, sentMessage.encode(), messageType,
                        simulatorDomainKeyword, Actor.findActorWithKeyword(PatientManagerConstants.PDS));
                TransactionInstance message = hl7Initiator.sendMessageAndGetTheHL7Message();
                hl7Messages = new ArrayList<TransactionInstance>();
                hl7Messages.add(message);
                if (message.getResponse().getType().endsWith("RSP_K21") || message.getResponse().getType().endsWith("RSP_ZV2")) {
                    pageNumber = 1;
                    parseResult(message.getResponse().getContentAsString(), message.getResponse().getType());
                }
                displayCriteriaForm = false;
            } catch (HL7Exception e) {
                FacesMessages.instance().add(e.getMessage());
                log.error(e.getMessage(), e);
                return;
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN,
                    "Please, first select the system under test to which you want to send the message");
        }
    }

    private void parseResult(String receivedMessageAsString, String responseType) {
        PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();
        try {
            if (responseType.endsWith("RSP_K21")) {
                RSP_K21 response = (RSP_K21) pipeParser.parseForSpecificPackage(receivedMessageAsString,
                        "net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message");
                int pidRep = response.getQUERY_RESPONSEReps();
                if (pidRep > 0) {
                    if (receivedPatients == null) {
                        receivedPatients = new HashMap<Integer, List<Patient>>();
                    }
                    List<Patient> patients = new ArrayList<Patient>();
                    for (int repIndex = 0; repIndex < pidRep; repIndex++) {
                        PID pidSegment = response.getQUERY_RESPONSE(repIndex).getPID();
                        try {
                            Patient patient = RSPMessageDecoder.createPatientFromPIDSegment(pidSegment);
                            patients.add(patient);
                        } catch (HL7v2ParsingException e) {
                            log.warn("Cannot parse PID[{}]", repIndex);
                        }
                    }
                    receivedPatients.put(pageNumber, patients);
                    if ((response.getDSC() != null) && (response.getDSC().encode().length() > 3)
                            && !response.getDSC().getContinuationPointer().getValue().isEmpty()) {
                        continuationPointer = response.getDSC().getContinuationPointer().getValue();
                        moreResultsAvailable = true;
                    } else {
                        continuationPointer = null;
                        moreResultsAvailable = false;
                    }
                } else {
                    moreResultsAvailable = false;
                    continuationPointer = null;
                    receivedPatients = null;
                }
            } else // RSP_ZV2
            {
                RSP_ZV2 response = (RSP_ZV2) pipeParser.parseForSpecificPackage(receivedMessageAsString,
                        "net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message");
                int responseReps = response.getQUERY_RESPONSE().getPATIENTReps();
                log.info("nb of responses in RSP_ZV2: " + responseReps);
                if (responseReps > 0) {
                    if (receivedPatients == null) {
                        receivedPatients = new HashMap<Integer, List<Patient>>();
                    }
                    List<Patient> patients = new ArrayList<Patient>();
                    for (int repIndex = 0; repIndex < responseReps; repIndex++) {
                        Patient patient ;
                        try {
                            patient = RSPMessageDecoder.createPatientFromPIDSegment(response.getQUERY_RESPONSE()
                                    .getPATIENT(repIndex).getPID());

                            Encounter encounter = new Encounter();
                            encounter = RSPMessageDecoder.fillEncounterWithPV1Segment(response.getQUERY_RESPONSE()
                                    .getPATIENT(repIndex).getVISIT().getPV1(), encounter);
                            patient.setEncounters(new ArrayList<Encounter>());
                            patient.getEncounters().add(encounter);
                            patients.add(patient);
                        } catch (HL7v2ParsingException e) {
                            log.warn("Cannot parse patient from PID[{}]", repIndex);
                        }
                    }
                    receivedPatients.put(pageNumber, patients);
                    if ((response.getDSC() != null) && (response.getDSC().encode().length() > 3)
                            && !response.getDSC().getContinuationPointer().getValue().isEmpty()) {
                        continuationPointer = response.getDSC().getContinuationPointer().getValue();
                        moreResultsAvailable = true;
                    } else {
                        continuationPointer = null;
                        moreResultsAvailable = false;
                    }
                } else {
                    moreResultsAvailable = false;
                    continuationPointer = null;
                    receivedPatients = null;
                }
            }
            selectedPage = pageNumber;
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The application has not been able to parse the received response");
            receivedPatients = null;
            log.error(e.getMessage(), e);
            return;
        }
    }

    /**
     * Using the continuation pointer, query the PDS to get the next results
     */
    public void getNextResults() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            PDCMessageBuilder.fillDSCSegment(sentMessage.getDSC(), continuationPointer);
            sentMessage.getMSH().getDateTimeOfMessage().getTime().setValue(sdf.format(new Date()));
            sentMessage.getMSH().getMessageControlID().setValue(sdf.format(new Date()));
            // the nb of hits to return may have been updated
            if (limit && (limitValue != null)) {
                sentMessage.getRCP().getQuantityLimitedRequest().getQuantity().setValue(limitValue.toString());
            }
            sendContinuationMessage();
        } catch (DataTypeException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to build message for the following reason: " + e.getMessage());
            log.error(e.getMessage());
        }

    }

    private void sendContinuationMessage() {
        try {
            Initiator hl7Initiator = new Initiator(selectedSUT, simulatorDomainKeyword, PatientManagerConstants.ISSUER_DEFAULT_NAME,
                    Actor.findActorWithKeyword(PatientManagerConstants.PDC), selectedTransaction, sentMessage.encode(), messageType,
                    simulatorDomainKeyword, Actor.findActorWithKeyword(PatientManagerConstants.PDS));
            TransactionInstance message = hl7Initiator.sendMessageAndGetTheHL7Message();
            hl7Messages = new ArrayList<TransactionInstance>();
            hl7Messages.add(message);
            nextPage();
            parseResult(message.getResponse().getContentAsString(), message.getResponse().getType());
        } catch (HL7Exception e) {
            FacesMessages.instance().add(e.getMessage());
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Send QCN^J01 message to PDS
     */
    public void cancelQuery() {
        receivedPatients = null;
        moreResultsAvailable = false;
        continuationPointer = null;
        try {
            Message cancelMessage = PDCMessageBuilder.buildCancellationMessage(sentMessage, selectedSUT);
            sendCancelQuery(cancelMessage);
        } catch (DataTypeException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "The application has not been able to build the cancellation message for the following reason: "
                            + e.getMessage());
            log.error(e.getMessage());
        }

    }

    private void sendCancelQuery(Message cancelMessage) {
        try {
            Initiator hl7Initiator = new Initiator(selectedSUT, simulatorDomainKeyword, PatientManagerConstants.ISSUER_DEFAULT_NAME,
                    Actor.findActorWithKeyword(PatientManagerConstants.PDC), selectedTransaction, cancelMessage.encode(),
                    "QCN^J01^QCN_J01", simulatorDomainKeyword, Actor.findActorWithKeyword(PatientManagerConstants.PDS));
            TransactionInstance message = hl7Initiator.sendMessageAndGetTheHL7Message();
            hl7Messages = new ArrayList<TransactionInstance>();
            hl7Messages.add(message);
            if ((message.getAckMessageType() != null) && message.getAckMessageType().contains("ACK")) {
                parseAck(message.getReceivedMessageContent());
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The received response is not of type ACK");
                cancellation = false;
            }
        } catch (HL7Exception e) {
            FacesMessages.instance().add(e.getMessage());
            cancellation = false;
            log.error(e.getMessage(), e);
        }
    }

    private void addPatientIdentifierToList() {
        if (patientCriteria.getPatientIdentifiers() != null && !patientCriteria.getPatientIdentifiers().isEmpty()) {
            PatientIdentifier currentIdentifier = patientCriteria.getPatientIdentifiers().get(0);
            StringBuilder identifier = new StringBuilder();
            if (currentIdentifier.getFullPatientId() != null) {
                identifier.append(currentIdentifier.getFullPatientId());
            }
            identifier.append("^^^");
            identifier.append(currentIdentifier.getDomain().toString().replace("^", "&"));
            if (identifier.toString().equals("^^^&&")) {
                patientCriteria.setPatientIdentifiers(null);
            } else {
                PatientIdentifier identifierCriteria = new PatientIdentifier(identifier.toString(),
                        currentIdentifier.getIdentifierTypeCode());
                identifierCriteria.setDomain(currentIdentifier.getDomain());
                patientCriteria.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
                patientCriteria.getPatientIdentifiers().add(identifierCriteria);
            }
        }
    }


    private void parseAck(String ackMessage) {
        try {
            ACK ack = (ACK) PipeParser.getInstanceWithNoValidation().parse(ackMessage);
            if (ack.getMSA().getAcknowledgmentCode().getValue().equals("AA")) {
                cancellation = true;
            } else {
                cancellation = false;
            }
        } catch (HL7Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The received acknowledgment cannot be parsed");
        }
    }

    private void buildAccountNumber() {
        StringBuilder identifier = new StringBuilder();
        if (accountNumber.getIdNumber() != null) {
            identifier.append(accountNumber.getIdNumber());
        }
        identifier.append("^^^");
        identifier.append(accountNumber.getDomain().toString().replace("^", "&"));
        if (identifier.toString().equals("^^^&&")) {
            patientCriteria.setAccountNumber(null);
        } else {
            patientCriteria.setAccountNumber(identifier.toString());
        }
    }

    /**
     * <p>Getter for the field <code>encounterCriteria</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public Encounter getEncounterCriteria() {
        return encounterCriteria;
    }

    /**
     * <p>Setter for the field <code>encounterCriteria</code>.</p>
     *
     * @param encounterCriteria a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public void setEncounterCriteria(Encounter encounterCriteria) {
        this.encounterCriteria = encounterCriteria;
    }

    /**
     * <p>Getter for the field <code>selectedSUT</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     */
    public HL7V2ResponderSUTConfiguration getSelectedSUT() {
        return selectedSUT;
    }

    /**
     * <p>Setter for the field <code>selectedSUT</code>.</p>
     *
     * @param selectedSUT a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     */
    public void setSelectedSUT(HL7V2ResponderSUTConfiguration selectedSUT) {
        this.selectedSUT = selectedSUT;
    }

    /**
     * <p>Getter for the field <code>hl7Messages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TransactionInstance> getHl7Messages() {
        return hl7Messages;
    }

    /**
     * <p>Getter for the field <code>continuationPointer</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getContinuationPointer() {
        return continuationPointer;
    }

    /**
     * <p>Setter for the field <code>movementCriteria</code>.</p>
     *
     * @param movementCriteria a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
     */
    public void setMovementCriteria(Movement movementCriteria) {
        this.movementCriteria = movementCriteria;
    }

    /**
     * <p>Getter for the field <code>movementCriteria</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
     */
    public Movement getMovementCriteria() {
        return movementCriteria;
    }

    /**
     * <p>Getter for the field <code>transactions</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Transaction> getTransactions() {
        return transactions;
    }

    /**
     * <p>Setter for the field <code>accountNumber</code>.</p>
     *
     * @param accountNumber a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     */
    public void setAccountNumber(PatientIdentifier accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * <p>Getter for the field <code>accountNumber</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     */
    public PatientIdentifier getAccountNumber() {
        return accountNumber;
    }

    /**
     * <p>Getter for the field <code>availableSystems</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HL7V2ResponderSUTConfiguration> getAvailableSystems() {
        return availableSystems;
    }
}
