package net.ihe.gazelle.simulator.pdqv3.pdc;

import net.ihe.gazelle.hl7v3.datatypes.*;
import net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01EntityRsp;
import net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01RespondTo;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02QUQIMT021001UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201306UV02.*;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01CancelType;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01Type;
import net.ihe.gazelle.hl7v3.quqimt000001UV01.QUQIMT000001UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.quqimt000001UV01.QUQIMT000001UV01QueryContinuation;
import net.ihe.gazelle.hl7v3.voc.ActClassControlAct;
import net.ihe.gazelle.hl7v3.voc.CommunicationFunctionType;
import net.ihe.gazelle.hl7v3.voc.EntityClassRoot;
import net.ihe.gazelle.hl7v3.voc.EntityDeterminer;
import net.ihe.gazelle.hl7v3.voc.PersonalRelationshipRoleType;
import net.ihe.gazelle.hl7v3.voc.XActMoodIntentEvent;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.initiator.MCCIMT000100UV01PartBuilder;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3Constants;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageBuilder;
import net.ihe.gazelle.simulator.hl7v3.messages.MCCIMT00300UV01PartBuilder;
import net.ihe.gazelle.simulator.hl7v3.messages.MessageIdGenerator;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.Person;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>PDQv3MessageBuilder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PDQv3MessageBuilder extends HL7v3MessageBuilder {

    private Patient criteria;
    private Integer limitValue;
    private HL7v3ResponderSUTConfiguration sut;
    private SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyyMMddHHmmss");
    private SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
    private List<AssigningAuthority> otherIDsScopingOrganizations;
    private II queryId = null;
    private II targetMessageId = null;
    private MessageIdGenerator generator = null;
    private boolean deferredMode;
    private String principalCareProviderExtension = null;
    private String principalCareProviderRoot = null;
    private String actorKeyword;

    private static Logger log = LoggerFactory.getLogger(PDQv3MessageBuilder.class);

    /**
     * <p>Constructor for PDQv3MessageBuilder.</p>
     *
     * @param inCriteria a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param inLimitValue a {@link java.lang.Integer} object.
     * @param inSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
     * @param whatDomainsReturned a {@link java.util.List} object.
     */
    public PDQv3MessageBuilder(Patient inCriteria, Integer inLimitValue, HL7v3ResponderSUTConfiguration inSUT,
                               List<AssigningAuthority> whatDomainsReturned) {
        this.criteria = inCriteria;
        this.limitValue = inLimitValue;
        this.sut = inSUT;
        this.otherIDsScopingOrganizations = whatDomainsReturned;
        this.deferredMode = false;
        this.actorKeyword = HL7V3Constants.PDC;
    }

    /**
     * <p>Constructor for PDQv3MessageBuilder.</p>
     *
     * @param inCriteria a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param inSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
     * @param deferredMode a boolean.
     */
    public PDQv3MessageBuilder(Patient inCriteria, HL7v3ResponderSUTConfiguration inSUT, boolean deferredMode) {
        this.criteria = inCriteria;
        this.limitValue = null;
        this.sut = inSUT;
        this.otherIDsScopingOrganizations = null;
        this.deferredMode = deferredMode;
        this.actorKeyword = HL7V3Constants.INIT_GW;
    }

    /**
     * <p>Constructor for PDQv3MessageBuilder.</p>
     *
     * @param patientIdentifier a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     * @param inSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
     * @param deferredMode a boolean.
     */
    public PDQv3MessageBuilder(PatientIdentifier patientIdentifier, HL7v3ResponderSUTConfiguration inSUT, boolean deferredMode){
        this.criteria = new Patient();
        this.criteria.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
        this.criteria.getPatientIdentifiers().add(patientIdentifier);
        this.limitValue = null;
        this.sut = inSUT;
        this.otherIDsScopingOrganizations = null;
        this.deferredMode = deferredMode;
        this.actorKeyword = HL7V3Constants.INIT_GW;
    }

    /**
     * <p>buildPatientRegistryFindCandidatesQuery.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type} object.
     */
    public PRPAIN201305UV02Type buildPatientRegistryFindCandidatesQuery() {

        PRPAIN201305UV02QUQIMT021001UV01ControlActProcess controlActProcess = new PRPAIN201305UV02QUQIMT021001UV01ControlActProcess();
        controlActProcess.setClassCode(ActClassControlAct.CACT);
        controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);
        controlActProcess.setCode(new CD(HL7V3Constants.PRPA_TE_201305_UV_02, HL7V3Constants.INTERACTION_ID, null));

        PRPAIN201305UV02Type request = new PRPAIN201305UV02Type();
        request.setITSVersion(VERSION);

        generator = MessageIdGenerator.getGeneratorForActor(actorKeyword);
        if (generator != null) {
            controlActProcess.setQueryByParameter(buildQueryByParameter(generator));
            request.setId(new II(generator.getOidForMessage(), generator.getNextId()));
            if (actorKeyword.equals(HL7V3Constants.INIT_GW)) {
                String homeCommunityId = PreferenceService.getString(HL7V3Constants.XCPD_INITGW_HOME_COMMUNITY_ID);
                request.setSender(MCCIMT000100UV01PartBuilder.buildSender(generator.getOid(), homeCommunityId));
            } else {
                request.setSender(MCCIMT000100UV01PartBuilder.buildSender(generator.getOid()));
            }
        } else {
            log.error("No MessageIdGenerator instance found for actor " + actorKeyword);
            controlActProcess.setQueryByParameter(buildQueryByParameter(null));
            if (actorKeyword.equals(HL7V3Constants.PDC)) {
                request.setSender(MCCIMT000100UV01PartBuilder.buildSender(
                        PreferenceService.getString(HL7V3Constants.PDQ_PDC_DEVICE_ID)));
            } else {
                request.setSender(MCCIMT000100UV01PartBuilder.buildSender(
                        PreferenceService.getString(HL7V3Constants.XCPD_INIT_DEVICE_ID)));
            }
        }
        request.setCreationTime(new TS(sdfDateTime.format(new Date())));
        request.setInteractionId(new II(HL7V3Constants.INTERACTION_ID, HL7V3Constants.PRPA_IN_201305_UV_02));
        request.setProcessingCode(new CS(HL7V3Constants.DEFAULT_PROCESSING_MODE, null, null));
        request.setProcessingModeCode(new CS(HL7V3Constants.DEFAULT_PROCESSING_MODE, null, null));
        request.setAcceptAckCode(new CS(HL7V3Constants.DEFAULT_ACK_CODE, null, null));
        request.addReceiver(MCCIMT000100UV01PartBuilder.buildReceiver(sut));
        request.setControlActProcess(controlActProcess);
        if (deferredMode) {
            request.addRespondTo(buildRespondTo());
        }
        return request;
    }

    private MCCIMT000100UV01RespondTo buildRespondTo() {
        MCCIMT000100UV01RespondTo respondTo = new MCCIMT000100UV01RespondTo();
        respondTo.setTelecom(new TEL());
        respondTo.getTelecom().setValue(PreferenceService.getString(HL7V3Constants.XCPD_INITGW_URL));
        respondTo.setTypeCode(CommunicationFunctionType.RSP);
        MCCIMT000100UV01EntityRsp entity = new MCCIMT000100UV01EntityRsp();
        entity.setClassCode(EntityClassRoot.ENT);
        entity.setDeterminerCode(EntityDeterminer.INSTANCE);
        respondTo.addEntityRsp(entity);
        return respondTo;
    }

    /**
     * <p>buildGeneralQueryActivateQueryContinue.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01Type} object.
     */
    public QUQIIN000003UV01Type buildGeneralQueryActivateQueryContinue() {
        QUQIIN000003UV01Type request = new QUQIIN000003UV01Type();
        populateContinueOrCancelMessage(request, HL7V3Constants.STATUS_CONTINUE);
        return request;
    }

    /**
     * <p>buildGeneralQueryActivateQueryCancel.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01CancelType} object.
     */
    public QUQIIN000003UV01CancelType buildGeneralQueryActivateQueryCancel() {
        QUQIIN000003UV01CancelType request = new QUQIIN000003UV01CancelType();
        populateContinueOrCancelMessage(request, HL7V3Constants.STATUS_CANCEL);
        return request;
    }

    private <T extends QUQIIN000003UV01Type> void populateContinueOrCancelMessage(T request, String status) {

        request.setITSVersion(VERSION);

        if (generator != null) {
            request.setId(new II(generator.getOidForMessage(), generator.getNextId()));
            request.setSender(MCCIMT00300UV01PartBuilder.buildSender(generator.getOid(), null));
        } else {
            log.error("No MessageIdGenerator instance found for actor PDS");
            request.setSender(MCCIMT00300UV01PartBuilder.buildSender(
                    PreferenceService.getString(HL7V3Constants.PDQ_PDC_DEVICE_ID), null));
        }

        request.setCreationTime(new TS(sdfDateTime.format(new Date())));
        request.setInteractionId(new II(HL7V3Constants.INTERACTION_ID, HL7V3Constants.QUQI_IN_000003_UV_01));
        request.setProcessingCode(new CS(HL7V3Constants.DEFAULT_PROCESSING_MODE, null, null));
        request.setProcessingModeCode(new CS(HL7V3Constants.DEFAULT_PROCESSING_MODE, null, null));
        request.setAcceptAckCode(new CS(HL7V3Constants.DEFAULT_ACK_CODE, null, null));
        request.addReceiver(MCCIMT00300UV01PartBuilder.buildReceiver(sut));

        request.addAcknowledgement(MCCIMT00300UV01PartBuilder.populateAcknowledgement(targetMessageId, HL7V3Constants.ACCEPT_ACK_CODE));

        QUQIMT000001UV01ControlActProcess controlActProcess = new QUQIMT000001UV01ControlActProcess();
        controlActProcess.setClassCode(ActClassControlAct.CACT);
        controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);
        controlActProcess.setCode(new CD(HL7V3Constants.PRPA_TE_000003_UV_01, HL7V3Constants.INTERACTION_ID, null));
        controlActProcess.setQueryContinuation(buildQueryContinuation(status));

        request.setControlActProcess(controlActProcess);
    }

    private QUQIMT000001UV01QueryContinuation buildQueryContinuation(String status) {
        QUQIMT000001UV01QueryContinuation queryContinuation = new QUQIMT000001UV01QueryContinuation();
        queryContinuation.setQueryId(queryId);
        queryContinuation.setContinuationQuantity(new INT(limitValue));
        queryContinuation.setStatusCode(new CS(status, null, null));
        return queryContinuation;
    }

    /**
     * <p>Setter for the field <code>limitValue</code>.</p>
     *
     * @param limitValue a {@link java.lang.Integer} object.
     */
    public void setLimitValue(Integer limitValue) {
        this.limitValue = limitValue;
    }


    private PRPAMT201306UV02QueryByParameter buildQueryByParameter(MessageIdGenerator generator) {
        PRPAMT201306UV02QueryByParameter queryByParameter = new PRPAMT201306UV02QueryByParameter();
        if (generator != null) {
            queryId = new II(generator.getOidForQuery(), generator.getNextId());
            queryByParameter.setQueryId(queryId);
        } else {
            log.error("queryId cannot be generated because no generator has been found");
        }
        queryByParameter.setStatusCode(new CS(HL7V3Constants.NEW, null, null));
        queryByParameter.setResponseModalityCode(new CS(HL7V3Constants.REAL_TIME, null, null));
        if (deferredMode) {
            queryByParameter.setResponsePriorityCode(new CS(HL7V3Constants.DEFFERED, null, null));
        } else {
            queryByParameter.setResponsePriorityCode(new CS(HL7V3Constants.IMMEDIATE, null, null));
        }
        if (limitValue != null) {
            queryByParameter.setInitialQuantity(new INT(limitValue));
        }
        queryByParameter.setParameterList(buildParameterList());
        return queryByParameter;
    }

    private PRPAMT201306UV02ParameterList buildParameterList() {
        PRPAMT201306UV02ParameterList parameterList = new PRPAMT201306UV02ParameterList();
        if (criteria.getGenderCode() != null && !criteria.getGenderCode().isEmpty()) {
            PRPAMT201306UV02LivingSubjectAdministrativeGender gender = new PRPAMT201306UV02LivingSubjectAdministrativeGender();
            gender.addValue(new CE(criteria.getGenderCode(), null, null));
            gender.setSemanticsText(buildST(HL7V3Constants.LIVING_SUBJECT_ADMINISTRATIVE_GENDER));
            parameterList.addLivingSubjectAdministrativeGender(gender);
        }
        // TODO LivingSubjectBirthPlaceName
        if (criteria.getDateOfBirth() != null) {
            PRPAMT201306UV02LivingSubjectBirthTime birthTime = new PRPAMT201306UV02LivingSubjectBirthTime();
            birthTime.addValue(new IVLTS(sdfDate.format(criteria.getDateOfBirth())));
            birthTime.setSemanticsText(buildST(HL7V3Constants.LIVING_SUBJECT_BIRTH_TIME));
            parameterList.addLivingSubjectBirthTime(birthTime);
        }
        // TODO LivingSubjectDeceasedTime
        if (criteria.getPatientIdentifiers() != null && !criteria.getPatientIdentifiers().isEmpty()) {
            List<PatientIdentifier> identifiers = criteria.getPatientIdentifiers();
            for (PatientIdentifier identifier : identifiers) {
                PRPAMT201306UV02LivingSubjectId subjectId = new PRPAMT201306UV02LivingSubjectId();
                II ii = new II();
                if (identifier.getIdNumber() != null && !identifier.getIdNumber().isEmpty()) {
                    ii.setExtension(identifier.getIdNumber());
                }
                if (identifier.getDomain() != null && identifier.getDomain().getUniversalID() != null && !identifier.getDomain().getUniversalID().isEmpty()) {
                    ii.setRoot(identifier.getDomain().getUniversalID());
                }
                subjectId.addValue(ii);
                subjectId.setSemanticsText(buildST(HL7V3Constants.LIVING_SUBJECT_ID));
                parameterList.addLivingSubjectId(subjectId);
            }
        }
        if ((criteria.getFirstName() != null && !criteria.getFirstName().isEmpty())
                || (criteria.getLastName() != null && !criteria.getLastName().isEmpty())) {
            PRPAMT201306UV02LivingSubjectName name = new PRPAMT201306UV02LivingSubjectName();
            name.setSemanticsText(buildST(HL7V3Constants.LIVING_SUBJECT_NAME));
            PN personName = buildPersonName(criteria, null);
            name.addValue(personName);
            parameterList.addLivingSubjectName(name);
        }
        if ((criteria.getAlternateFirstName() != null && !criteria.getAlternateFirstName().isEmpty())
                || (criteria.getAlternateLastName() != null && !criteria.getAlternateLastName().isEmpty())) {
            PRPAMT201306UV02LivingSubjectName name = new PRPAMT201306UV02LivingSubjectName();
            name.setSemanticsText(buildST(HL7V3Constants.LIVING_SUBJECT_NAME));
            PN altPersonName = buildAlternatePersonName(criteria);
            name.addValue(altPersonName);
            parameterList.addLivingSubjectName(name);
        }
        String motherMaidenName = criteria.getMotherMaidenName();
        if (motherMaidenName != null && !motherMaidenName.isEmpty()) {
            PRPAMT201306UV02MothersMaidenName mothersMaidenName = new PRPAMT201306UV02MothersMaidenName();
            mothersMaidenName.setSemanticsText(buildST(HL7V3Constants.PERSON_MOTHERS_MAIDEN_NAME));
            mothersMaidenName.addValue(buildPersonName(null, motherMaidenName, null));
            parameterList.addMothersMaidenName(mothersMaidenName);
        }
        // Patient address
        PRPAMT201306UV02PatientAddress patientAddress = new PRPAMT201306UV02PatientAddress();
        for (PatientAddress criteriaAddress : criteria.getAddressList()) {
            AD address = createAD(criteriaAddress);
            // it may occur that addresses come here with no parameter set, discard them
            if (!address.getMixed().isEmpty()) {
                if (criteriaAddress.isBirthPlace()) {
                    // we need to create a birthplace element
                    PRPAMT201306UV02LivingSubjectBirthPlaceAddress birthPlaceAddress = new PRPAMT201306UV02LivingSubjectBirthPlaceAddress();
                    birthPlaceAddress.addValue(address);
                    birthPlaceAddress.setSemanticsText(buildST(HL7V3Constants.LIVING_SUBJECT_BIRTH_PLACE));
                    parameterList.addLivingSubjectBirthPlaceAddress(birthPlaceAddress);
                } else {
                    patientAddress.addValue(address);
                }
            }
        }
        if (!patientAddress.getValue().isEmpty()) {
            patientAddress.setSemanticsText(buildST(HL7V3Constants.PATIENT_ADDR));
            parameterList.addPatientAddress(patientAddress);
        }

        // Patient Telecom
        PRPAMT201306UV02PatientTelecom patientTelecom = new PRPAMT201306UV02PatientTelecom();
        for (PatientPhoneNumber phoneNumber : criteria.getPhoneNumbers()) {
            if (phoneNumber.getValue() != null && !phoneNumber.getValue().isEmpty()) {
                TEL telecom = populateTelecom(phoneNumber);
                patientTelecom.addValue(telecom);
            }
        }
        if (!patientTelecom.getValue().isEmpty()) {
            patientTelecom.setSemanticsText(buildST(HL7V3Constants.PATIENT_TELECOM));
            parameterList.addPatientTelecom(patientTelecom);
        }
        // TODO patientStatusCode ?
        // principalCareProviderId
        if ((principalCareProviderExtension != null && !principalCareProviderExtension.isEmpty())
                || (principalCareProviderRoot != null && !principalCareProviderRoot.isEmpty()))

        {
            PRPAMT201306UV02PrincipalCareProviderId principalCareProviderElt = new PRPAMT201306UV02PrincipalCareProviderId();
            principalCareProviderElt.getValue().add(new II(principalCareProviderRoot, principalCareProviderExtension));
            principalCareProviderElt.setSemanticsText(buildST(HL7V3Constants.ASSIGNED_PROVIDER_ID));
            parameterList.addPrincipalCareProviderId(principalCareProviderElt);
        }
        // TODO principalCareProvisionId
        if (otherIDsScopingOrganizations != null && !otherIDsScopingOrganizations.isEmpty())
        {
            for (AssigningAuthority authority : otherIDsScopingOrganizations) {
                PRPAMT201306UV02OtherIDsScopingOrganization scopingOrganization = new PRPAMT201306UV02OtherIDsScopingOrganization();
                scopingOrganization.addValue(new II(authority.getUniversalId(), null));
                scopingOrganization.setSemanticsText(buildST(HL7V3Constants.OTHER_IDS_SCOPING_ORGANIZATION_ID));
                parameterList.addOtherIDsScopingOrganization(scopingOrganization);
            }
        }
        return parameterList;
    }

    private AD createAD(PatientAddress addCriteria) {
        AD address = new AD();
        if (addCriteria.getCity() != null && !addCriteria.getCity().isEmpty()) {
            AdxpCity city = new AdxpCity();
            city.getMixed().add(addCriteria.getCity());
            address.addCity(city);
        }
        if (addCriteria.getState() != null && !addCriteria.getState().isEmpty()) {
            AdxpState state = new AdxpState();
            state.getMixed().add(addCriteria.getState());
            address.addState(state);
        }
        if (addCriteria.getAddressLine() != null && !addCriteria.getAddressLine().isEmpty()) {
            AdxpStreetAddressLine street = new AdxpStreetAddressLine();
            street.getMixed().add(addCriteria.getAddressLine());
            address.addStreetAddressLine(street);
        }
        if (addCriteria.getCountryCode() != null && !addCriteria.getCountryCode().isEmpty()) {
            AdxpCountry country = new AdxpCountry();
            country.getMixed().add(addCriteria.getCountryCode());
            address.addCountry(country);
        }
        if (addCriteria.getZipCode() != null && !addCriteria.getZipCode().isEmpty()) {
            AdxpPostalCode postalCode = new AdxpPostalCode();
            postalCode.getMixed().add(addCriteria.getZipCode());
            address.addPostalCode(postalCode);
        }
        return address;
    }

    /**
     * <p>Setter for the field <code>targetMessageId</code>.</p>
     *
     * @param targetMessageId a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
     */
    public void setTargetMessageId(II targetMessageId) {
        this.targetMessageId = targetMessageId;
    }

    /**
     * <p>Setter for the field <code>principalCareProviderExtension</code>.</p>
     *
     * @param principalCareProviderExtension a {@link java.lang.String} object.
     */
    public void setPrincipalCareProviderExtension(String principalCareProviderExtension) {
        this.principalCareProviderExtension = principalCareProviderExtension;
    }

    /**
     * <p>Setter for the field <code>principalCareProviderRoot</code>.</p>
     *
     * @param principalCareProviderRoot a {@link java.lang.String} object.
     */
    public void setPrincipalCareProviderRoot(String principalCareProviderRoot) {
        this.principalCareProviderRoot = principalCareProviderRoot;
    }
}
