package net.ihe.gazelle.simulator.hl7v3.sut.action;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.cat.dao.SystemUnderTestPreferencesDAO;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfigurationQuery;
import net.ihe.gazelle.simulator.sut.action.AbstractSystemConfigurationManager;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.simulator.sut.model.UsageQuery;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <p>HL7v3ResponderSUTConfManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("hl7v3ResponderSUTConfManager")
@Scope(ScopeType.PAGE)
public class HL7v3ResponderSUTConfManager extends
		AbstractSystemConfigurationManager<HL7v3ResponderSUTConfiguration> implements Serializable, UserAttributeCommon {

	/**
	 *
	 */
	private static final long serialVersionUID = -8203169928429200398L;

	@In(value="gumUserService")
	private UserService userService;

	GazelleIdentity identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");

	/** {@inheritDoc} */
	@Override
	public void deleteSelectedSystemConfiguration() {
		if (selectedSystemConfiguration != null) {
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			HL7v3ResponderSUTConfiguration configToRemove = entityManager.find(
					HL7v3ResponderSUTConfiguration.class, selectedSystemConfiguration.getId());
			int configId = configToRemove.getId();
			SystemUnderTestPreferencesDAO.deletePreferencesForSUT(configId);
			entityManager.remove(configToRemove);
			entityManager.flush();
			selectedSystemConfiguration = null;
			reset();
			FacesMessages.instance().add(StatusMessage.Severity.INFO,"SUT configuration successfully deleted");
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.WARN,"No SUT selected for deletion");
		}
	}

    /** {@inheritDoc} */
    @Override
    protected HQLCriterionsForFilter<HL7v3ResponderSUTConfiguration> getHQLCriterionsForFilter() {
        HL7v3ResponderSUTConfigurationQuery query = new HL7v3ResponderSUTConfigurationQuery();
        HQLCriterionsForFilter<HL7v3ResponderSUTConfiguration> criterionsForFilter = query.getHQLCriterionsForFilter();
		criterionsForFilter.addPath("owner", query.owner());
        criterionsForFilter.addQueryModifier(this);
        return criterionsForFilter;
    }

    /** {@inheritDoc} */
    @Override
	public void copySelectedConfiguration(HL7v3ResponderSUTConfiguration inConfiguration) {
		if (inConfiguration != null) {
			selectedSystemConfiguration = new HL7v3ResponderSUTConfiguration(inConfiguration);
			editSelectedConfiguration();
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR,"No configuration selected");
		}
	}

	/** {@inheritDoc} */
	@Override
	public void addConfiguration() {
		selectedSystemConfiguration = new HL7v3ResponderSUTConfiguration();
		displayEditPanel = true;
		displayListPanel = false;
	}

	/** {@inheritDoc} */
	@Override
	public List<Usage> getPossibleListUsages() {
		UsageQuery query = new UsageQuery();
		List<Usage> usages = query.getList();
		Collections.sort(usages);
		return usages;
	}


	/**
	 * Used in editSystemConfig_common.xhtml.xhtml
	 */
	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}

    /** {@inheritDoc} */
    @Override
    public void modifyQuery(HQLQueryBuilder<HL7v3ResponderSUTConfiguration> hqlQueryBuilder, Map<String, Object> map) {
        HL7v3ResponderSUTConfigurationQuery query = new HL7v3ResponderSUTConfigurationQuery();
        if (!Identity.instance().isLoggedIn()) {
            hqlQueryBuilder.addRestriction(query.isPublic().eqRestriction(true));
        } else if (!Identity.instance().hasRole("admin_role")) {
            hqlQueryBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("isPublic", true),
                    HQLRestrictions.eq("ownerCompany", identity.getOrganisationKeyword())));
        }
        if (filteredUsage != null){
            hqlQueryBuilder.addRestriction(query.listUsages().id().eqRestriction(filteredUsage.getId()));
        }
    }
}
