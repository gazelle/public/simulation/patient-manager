package net.ihe.gazelle.simulator.pam.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.demographic.ws.DemographicDataServerServiceStub;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.patient.PatientGenerator;
import net.ihe.gazelle.simulator.application.PatientBuilder;
import net.ihe.gazelle.simulator.application.PatientBuilderFactory;
import net.ihe.gazelle.simulator.application.PatientService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.hl7.HL7Domain;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.*;
import net.ihe.gazelle.ssov7.gum.client.interlay.filter.UserValueFormatter;
import org.jboss.seam.annotations.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>Abstract PatientSelectorManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@GenerateInterface(value = "PatientSelectorManagerLocal", isLocal = false, isRemote = false, extendsInterface = "net.ihe.gazelle.patient.PatientGeneratorLocal")
public abstract class PatientSelectorManager extends PatientGenerator implements QueryModifier<Patient> {
    private static final Logger log = LoggerFactory.getLogger(PatientSelectorManager.class);
    public static final String OWNER = "owner";

    protected transient FilterDataModel<Patient> patients;
    protected transient Filter<Patient> filter;
    protected transient boolean displayPatientsList;
    protected transient boolean displayDDSPanel;
    protected transient boolean useIdsFromDDS;
    private String searchedPatientId;
    protected transient boolean generateIdsForAuthorities;
    protected transient boolean generateIdsForConnectathon;
    protected transient List<HierarchicDesignator> selectedAuthorities;
    private List<HierarchicDesignator> availableAuthorities;
    protected PatientPhoneNumber newPhoneNumber;
    private Person currentRelationship;
    private String relationshipLastName;

    @In(create = true)
    protected transient PatientService patientService;
    @In(create = true)
    protected transient PatientBuilderFactory patientBuilderFactory;


    /**
     * <p>getLunarWeeks.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getLunarWeeks() {
        return LunarWeek.getLunarWeekValues();
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<Patient> getFilter() {
        if (filter == null) {
            filter = new Filter<>(getFilterCriteria());
            filter.getFormatters().put(OWNER,new UserValueFormatter(filter, OWNER));
        }
        return filter;
    }

    /**
     * <p>getSendingActor.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public abstract Actor getSendingActor();

    /**
     * <p>createNewRelationship.</p>
     */
    public abstract void createNewRelationship();

    /**
     * <p>saveRelationship.</p>
     */
    public abstract void saveRelationship();

    private HQLCriterionsForFilter<Patient> getFilterCriteria() {
        PatientQuery query = new PatientQuery();
        HQLCriterionsForFilter<Patient> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("firstname", query.firstName());
        criteria.addPath("lastname", query.lastName());
        criteria.addPath(OWNER, query.creator());
        criteria.addPath("gender", query.genderCode());
        criteria.addPath("country", query.countryCode());
        criteria.addPath("date", query.creationDate());
        criteria.addPath("mothersmaidenname", query.motherMaidenName());
        criteria.addQueryModifier(this);
        return criteria;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifyQuery(HQLQueryBuilder<Patient> queryBuilder, Map<String, Object> params) {
        PatientQuery query = new PatientQuery();
        if (searchedPatientId != null && !searchedPatientId.isEmpty()) {
            queryBuilder.addRestriction(query.patientIdentifiers().fullPatientId().likeRestriction(searchedPatientId, HQLRestrictionLikeMatchMode.ANYWHERE));
        }
        queryBuilder.addRestriction(query.stillActive().eqRestriction(true));
        if (getSendingActor() != null && getSendingActor().getKeyword().equals("PES")) {
            queryBuilder.addRestriction(HQLRestrictions.or(query.simulatedActor().keyword().eqRestriction("PES"),
                    query.simulatedActor().keyword().eqRestriction("PDC")));
        } else if (getSendingActor() != null) {
            queryBuilder.addRestriction(query.simulatedActor().eqRestriction(getSendingActor()));
        }
        if (relationshipLastName != null && !relationshipLastName.isEmpty()) {
            queryBuilder.addRestriction(query.personalRelationships().lastName().likeRestriction(relationshipLastName,
                    HQLRestrictionLikeMatchMode.ANYWHERE));
        }
    }

    /**
     * <p>Getter for the field <code>patients</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<Patient> getPatients() {
        return new FilterDataModel<Patient>(getFilter()) {
            @Override
            protected Object getId(Patient patient) {
                return patient.getId();
            }
        };
    }

    /**
     * <p>isDisplayPatientsList.</p>
     *
     * @return a boolean.
     */
    public boolean isDisplayPatientsList() {
        return displayPatientsList;
    }

    /**
     * <p>Setter for the field <code>displayPatientsList</code>.</p>
     *
     * @param displayPatientsList a boolean.
     */
    public void setDisplayPatientsList(boolean displayPatientsList) {
        this.displayPatientsList = displayPatientsList;
    }

    /**
     * <p>isDisplayDDSPanel.</p>
     *
     * @return a boolean.
     */
    public boolean isDisplayDDSPanel() {
        return displayDDSPanel;
    }

    /**
     * <p>Setter for the field <code>displayDDSPanel</code>.</p>
     *
     * @param displayDDSPanel a boolean.
     */
    public void setDisplayDDSPanel(boolean displayDDSPanel) {
        this.displayDDSPanel = displayDDSPanel;
    }

    /**
     * <p>Getter for the field <code>searchedPatientId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSearchedPatientId() {
        return searchedPatientId;
    }

    /**
     * <p>Setter for the field <code>searchedPatientId</code>.</p>
     *
     * @param searchedPatientId a {@link java.lang.String} object.
     */
    public void setSearchedPatientId(String searchedPatientId) {
        this.searchedPatientId = searchedPatientId;
    }

    /**
     * <p>initialize.</p>
     */
    public void initialize() {
        displayDDSPanel = false;
        displayPatientsList = true;
        generateIdsForConnectathon = false;
        useIdsFromDDS = Boolean.parseBoolean(ApplicationConfiguration.getValueOfVariable("use_ids_from_dds"));
        generateIdsForAuthorities = Boolean.parseBoolean(ApplicationConfiguration.getValueOfVariable("use_predefined_authorities"));
        selectedAuthorities = HierarchicDesignatorDAO.getToolsDefaultAssigningAuthorities(DesignatorType.PATIENT_ID);
        initializeListOfAvailableAuthorities();
        super.initializeGenerator();
    }

    protected void initializeListOfAvailableAuthorities() {
        availableAuthorities = HierarchicDesignatorDAO.getToolsAssigningAuthoritiesNotForConnectathon(DesignatorType.PATIENT_ID);
    }

    /**
     * <p>generateNewPatient.</p>
     *
     * @param sendingActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    protected Patient generateNewPatient(Actor sendingActor) {
        return patientBuilderFactory
                .make(getHL7Domain())
                .build(new PatientBuilder.Context(this));
    }

    /**
     * <p>resetFilter.</p>
     */
    public void resetFilter() {
        getFilter().clear();
    }

    /**
     * <p>isUseIdsFromDDS.</p>
     *
     * @return a boolean.
     */
    public boolean isUseIdsFromDDS() {
        return useIdsFromDDS;
    }

    /**
     * <p>Setter for the field <code>useIdsFromDDS</code>.</p>
     *
     * @param useIdsFromDDS a boolean.
     */
    public void setUseIdsFromDDS(boolean useIdsFromDDS) {
        this.useIdsFromDDS = useIdsFromDDS;
    }

    /**
     * <p>switchToGenerationMode.</p>
     */
    public void switchToGenerationMode() {
        displayPatientsList = false;
        displayDDSPanel = true;
    }

    /**
     * <p>switchToSelectionMode.</p>
     */
    public void switchToSelectionMode() {
        displayPatientsList = true;
        displayDDSPanel = false;
    }

    /**
     * <p>isGenerateIdsForAuthorities.</p>
     *
     * @return a boolean.
     */
    public boolean isGenerateIdsForAuthorities() {
        return generateIdsForAuthorities;
    }

    /**
     * <p>Setter for the field <code>generateIdsForAuthorities</code>.</p>
     *
     * @param generateIdsForAuthorities a boolean.
     */
    public void setGenerateIdsForAuthorities(boolean generateIdsForAuthorities) {
        this.generateIdsForAuthorities = generateIdsForAuthorities;
    }

    /**
     * <p>Getter for the field <code>selectedAuthorities</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HierarchicDesignator> getSelectedAuthorities() {
        return selectedAuthorities;
    }

    /**
     * <p>Setter for the field <code>selectedAuthorities</code>.</p>
     *
     * @param selectedAuthorities a {@link java.util.List} object.
     */
    public void setSelectedAuthorities(List<HierarchicDesignator> selectedAuthorities) {
        this.selectedAuthorities = selectedAuthorities;
    }

    public void addSelectedAuthorities(HierarchicDesignator selectedAuthority) {
        if (selectedAuthorities == null) {
            selectedAuthorities = new ArrayList<>();
        }
        selectedAuthorities.add(selectedAuthority);
    }

    /**
     * <p>Getter for the field <code>availableAuthorities</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HierarchicDesignator> getAvailableAuthorities() {
        return availableAuthorities;
    }

    /**
     * <p>getPhoneTypes.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getPhoneTypes() {
        return PhoneNumberType.getListAsSelectItems();
    }

    /**
     * <p>Getter for the field <code>newPhoneNumber</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber} object.
     */
    public PatientPhoneNumber getNewPhoneNumber() {
        return newPhoneNumber;
    }

    /**
     * <p>Setter for the field <code>newPhoneNumber</code>.</p>
     *
     * @param newPhoneNumber a {@link net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber} object.
     */
    public void setNewPhoneNumber(PatientPhoneNumber newPhoneNumber) {
        this.newPhoneNumber = newPhoneNumber;
    }

    /**
     * <p>Getter for the field <code>currentRelationship</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     */
    public Person getCurrentRelationship() {
        return currentRelationship;
    }

    /**
     * <p>Setter for the field <code>currentRelationship</code>.</p>
     *
     * @param currentRelationship a {@link net.ihe.gazelle.simulator.pam.model.Person} object.
     */
    public void setCurrentRelationship(Person currentRelationship) {
        this.currentRelationship = currentRelationship;
    }

    /**
     * <p>Getter for the field <code>relationshipLastName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRelationshipLastName() {
        return relationshipLastName;
    }

    /**
     * <p>Setter for the field <code>relationshipLastName</code>.</p>
     *
     * @param relationshipLastName a {@link java.lang.String} object.
     */
    public void setRelationshipLastName(String relationshipLastName) {
        this.relationshipLastName = relationshipLastName;
    }

    public void setAvailableAuthorities(List<HierarchicDesignator> availableAuthorities) {
        this.availableAuthorities = availableAuthorities;
    }

    public boolean isGenerateIdsForConnectathon() {
        return generateIdsForConnectathon;
    }

    public void setGenerateIdsForConnectathon(boolean generateIdsForConnectathon) {
        this.generateIdsForConnectathon = generateIdsForConnectathon;
    }

    public boolean isConnectathonFeatureEnabled() {
        return SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.CONNECTATHON);
    }

    protected abstract Class<? extends HL7Domain<?>> getHL7DomainClass();

    protected HL7Domain<?> getHL7Domain() {
        try {
            return getHL7DomainClass().newInstance();
        } catch (InstantiationException|IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public DemographicDataServerServiceStub getDdsStub() {
        return this.ddsStub;
    }
}
