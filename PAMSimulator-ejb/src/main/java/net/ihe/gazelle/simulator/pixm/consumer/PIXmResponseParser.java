package net.ihe.gazelle.simulator.pixm.consumer;

import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.Reference;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>PIXmResponseParser class.</p>
 *
 * @author aberge
 * @version 1.0: 13/11/17
 */

public class PIXmResponseParser {

    /**
     * <p>Getter for the field <code>referencedPatients</code>.</p>
     *
     * @param resp a {@link org.hl7.fhir.dstu3.model.Parameters} object.
     *
     * @return a {@link java.util.List} object.
     */
    public static List<String> getReferencedPatients(Parameters resp) {

        List<String> res = new ArrayList<String>();

        List<Parameters.ParametersParameterComponent> parameterList = resp.getParameter();
        for (Parameters.ParametersParameterComponent currentParameter : parameterList) {
            String currentReferencedPatient;
            if (currentParameter.getName().equals("targetId")) {
                Reference targetIdentifier = ((Reference) currentParameter.getValue());
                byte[] referenceFromMessage = targetIdentifier.getReference().getBytes(StandardCharsets.UTF_8);
                currentReferencedPatient = new String(referenceFromMessage, StandardCharsets.UTF_8);
                res.add(currentReferencedPatient);
            }
        }
        return res;
    }

    /**
     * <p>getReceivedIdentifiers.</p>
     *
     * @param resp a {@link org.hl7.fhir.dstu3.model.Parameters} object.
     *
     * @return a {@link java.util.List} object.
     */
    public static List<PatientIdentifier> getReceivedIdentifiers(Parameters resp) {

        List<PatientIdentifier> res = new ArrayList<PatientIdentifier>();

        List<Parameters.ParametersParameterComponent> parameterList = resp.getParameter();
        for (Parameters.ParametersParameterComponent currentParameter : parameterList) {
            PatientIdentifier currentPatientIdentifier = new PatientIdentifier();
            if (currentParameter.getName().equals("targetIdentifier")) {
                Identifier targetIdentifier = ((Identifier) currentParameter.getValue());
                currentPatientIdentifier.setFullPatientId(targetIdentifier.getValue());
                HierarchicDesignator currentDomain = new HierarchicDesignator();
                currentDomain.setNamespaceID(targetIdentifier.getSystem());
                currentPatientIdentifier.setDomain(currentDomain);
                res.add(currentPatientIdentifier);
            }
        }
        return res;
    }

}
