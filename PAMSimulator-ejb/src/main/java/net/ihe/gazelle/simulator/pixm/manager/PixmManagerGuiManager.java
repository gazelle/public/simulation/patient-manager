package net.ihe.gazelle.simulator.pixm.manager;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pdq.util.AbstractPDQPDSGuiManager;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * <p>PixmManagerGuiManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pixmManagerGuiManager")
@Scope(ScopeType.PAGE)
public class PixmManagerGuiManager extends AbstractPDQPDSGuiManager {

    /**
     *
     */
    private static final long serialVersionUID = -2657551111877383278L;
    private String endpointUrl;

    /**
     * <p>getUrlForHL7Messages.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUrlForHL7Messages() {
        Actor actor = Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR");
        Transaction transaction = Transaction.GetTransactionByKeyword("ITI-83");
        return "/messages/browser.seam?simulatedActor=" + actor.getId() + "&transaction=" + transaction.getId();
    }

    /** {@inheritDoc} */
    @Override
    @Create
    public void getSimulatorResponderConfiguration() {
        this.endpointUrl = PreferenceService.getString("pixm_mgr_server_url");
    }

    /**
     * <p>Getter for the field <code>endpointUrl</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEndpointUrl() {
        return endpointUrl;
    }

    @Override
    public String linkToPatients(){
        Actor actor = Actor.findActorWithKeyword("PAT_IDENTITY_X_REF_MGR");
        return "/patient/allPatients.seam?actor=" + actor.getId();
    }
}
