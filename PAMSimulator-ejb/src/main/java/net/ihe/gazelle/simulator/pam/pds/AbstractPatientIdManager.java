package net.ihe.gazelle.simulator.pam.pds;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.application.PatientService;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.action.SendindException;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientHistory;
import net.ihe.gazelle.simulator.pam.model.PatientHistory.ActionPerformed;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.Person;
import org.jboss.seam.security.Identity;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * <p>AbstractPatientIdManager class.</p>
 * Common methods for Patient ID Managers
 *
 * @author pdt
 * @version $Id: $Id
 */
public abstract class AbstractPatientIdManager extends PDS {

    // not used in xhtml
    protected PatientIdentifier correctPatientIdentifier;
    protected PatientIdentifier incorrectPatientIdentifier;

    protected List<TransactionInstance> send() {
        try {
            List<TransactionInstance> transactions = patientService.sendADTMessage(
                    new PatientService.Exchange(
                        selectedSUT,sendingApplication,sendingFacility,
                        sendingActor,receivingActor,simulatedTransaction, getHL7Domain(), PatientService.ADT.A47,selectedPatient,secondaryPatient,
                    correctPatientIdentifier,incorrectPatientIdentifier
                    ));
            displayPatientsList = false;
            return transactions;
        } catch (HL7Exception e) {
            throw new SendindException(e.getMessage(),e);
        }
    }

    
    protected void upgradePatientVersion(Patient inPatient) {
        try {
            PatientHistory h = patientService.upgradePatientIdVersion(inPatient, sendingActor);
            secondaryPatient = h.getCurrentPatient();
            selectedPatient = h.getOtherPatient();
        } catch (Exception e) {
            secondaryPatient = null;
            selectedPatient = null;
        }
    }



    /**
     * <p>generateAPatient.</p>
     */
    // TODO Refactor to extract patient generation into patientService
    public void generateAPatient() {
        selectedPatient = super.generateNewPatient(sendingActor);
        if (selectedPatient != null) {
            String username = null;
            if (Identity.instance().isLoggedIn()) {
                username = Identity.instance().getCredentials().getUsername();
            }
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            selectedPatient = selectedPatient.savePatient(entityManager);
            PatientHistory history = new PatientHistory(selectedPatient, null, ActionPerformed.CREATED, null, username,
                    null, null);
            history.save(entityManager);
            displayDDSPanel = false;
            displayPatientsList = true; // always show all patients with ins identifier
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void createNewRelationship() {
        setCurrentRelationship(new Person(selectedPatient));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveRelationship() {
        selectedPatient.addPersonalRelationship(getCurrentRelationship());
        setCurrentRelationship(null);
    }
}
