package net.ihe.gazelle.simulator.pdq.hl7;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.QCN_J01;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message.QBP_Q21;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.DSC;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.MSH;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.QPD;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.RCP;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pdq.util.AssigningAuthority;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>PDCMessageBuilder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PDCMessageBuilder extends SegmentBuilder {

	private Patient patient;
	private Encounter encounter;
	private Movement movement;
	private List<AssigningAuthority> returnedDomains;
	private Integer limit;
	private PatientIdentifier accountNumber;

	/**
	 * <p>Constructor for PDCMessageBuilder.</p>
	 *
	 * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
	 * @param encounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
	 * @param movement a {@link net.ihe.gazelle.simulator.pam.iti31.model.Movement} object.
	 * @param returnedDomains a {@link java.util.List} object.
	 * @param limit a {@link java.lang.Integer} object.
	 */
	public PDCMessageBuilder(Patient patient, Encounter encounter, Movement movement,
			List<AssigningAuthority> returnedDomains, Integer limit) {
		this.patient = patient;
		this.encounter = encounter;
		this.movement = movement;
		this.returnedDomains = returnedDomains;
		this.limit = limit;
	}

	/**
	 * <p>buildQBPMessageWithoutPointer.</p>
	 *
	 * @param sut a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
	 * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
	 * @return a {@link ca.uhn.hl7v2.model.Message} object.
	 * @throws ca.uhn.hl7v2.model.DataTypeException if any.
	 * @throws ca.uhn.hl7v2.HL7Exception if any.
	 */
	public Message buildQBPMessageWithoutPointer(HL7V2ResponderSUTConfiguration sut, Transaction transaction)
			throws DataTypeException, HL7Exception {
		QBP_Q21 qbpMessage = new QBP_Q21();
		fillMSHSegment(qbpMessage.getMSH(), sut, transaction);
		fillQPDSegment(qbpMessage.getQPD(), transaction.getKeyword());
		fillRCPSegment(qbpMessage.getRCP());
		return qbpMessage;
	}

	private void fillMSHSegment(MSH mshSegment, HL7V2ResponderSUTConfiguration sut, Transaction transaction)
			throws DataTypeException, HL7Exception {
		String triggerEvent;
		if (transaction.getKeyword().equals("ITI-21")) {
			triggerEvent = "Q22";
		} else {
			triggerEvent = "ZV1";
		}
		ca.uhn.hl7v2.model.v25.message.QBP_Q21 transientMessage = new ca.uhn.hl7v2.model.v25.message.QBP_Q21();
		fillMSHSegment(transientMessage.getMSH(), sut.getApplication(), sut.getFacility(), "PatientManager", "IHE",
				"QBP", triggerEvent, "QBP_Q21", "2.5", new SimpleDateFormat("yyyyMMddHHmmss"), sut.getCharset()
						.getHl7Code());
		String mshSegmentAsString = transientMessage.getMSH().encode();
		mshSegment.getFieldSeparator().setValue("|");
		mshSegment.getEncodingCharacters().setValue("^~\\&");
		mshSegment.parse(mshSegmentAsString);
	}

	private void fillQPDSegment(QPD qpdSegment, String transactionKeyword) throws DataTypeException {
		// query tag
		qpdSegment.getQueryTag().setValue("GazellePDQPDC." + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));

		// parameter fields for demographics
		int rep = 0;
		if ((patient.getFirstName() != null) && !patient.getFirstName().isEmpty()) {
			qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.5.2");
			qpdSegment.getDemographicsFields(rep).getValues().setValue(patient.getFirstName());
			rep++;
		}
		if ((patient.getLastName() != null) && !patient.getLastName().isEmpty()) {
			qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.5.1.1");
			qpdSegment.getDemographicsFields(rep).getValues().setValue(patient.getLastName());
			rep++;
		}
		String motherMaidenName = patient.getMotherMaidenName();
		if ((motherMaidenName != null) && !motherMaidenName.isEmpty()) {
			qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.6.1.1");
			qpdSegment.getDemographicsFields(rep).getValues().setValue(motherMaidenName);
			rep++;
		}
		if (patient.getDateOfBirth() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.7.1");
			qpdSegment.getDemographicsFields(rep).getValues().setValue(sdf.format(patient.getDateOfBirth()));
			rep++;
		}
		if ((patient.getGenderCode() != null) && !patient.getGenderCode().isEmpty()) {
			qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.8");
			qpdSegment.getDemographicsFields(rep).getValues().setValue(patient.getGenderCode());
			rep++;
		}
		for (PatientPhoneNumber phoneNumber: patient.getPhoneNumbers()) {
			qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.13.9");
			qpdSegment.getDemographicsFields(rep).getValues().setValue(phoneNumber.getValue());
			rep++;
			// only add one value for now
			break;
		}
		if (patient.getPatientIdentifiers() != null && !patient.getPatientIdentifiers().isEmpty()) {
			Map<String, String> fields = patient.getPatientIdentifiers().get(0).splitPatientId();
			if ((fields.get("CX-1") != null) && !fields.get("CX-1").isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.3.1");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(fields.get("CX-1"));
				rep++;
			}
			if (fields.containsKey("CX-4-1")) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.3.4.1");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(fields.get("CX-4-1"));
				rep++;
			}
			if (fields.containsKey("CX-4-2")) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.3.4.2");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(fields.get("CX-4-2"));
				rep++;
			}
			if (fields.containsKey("CX-4-3")) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.3.4.3");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(fields.get("CX-4-3"));
				rep++;
			}
			if ((patient.getPatientIdentifiers().get(0).getIdentifierTypeCode() != null)
					&& !patient.getPatientIdentifiers().get(0).getIdentifierTypeCode().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.3.5");
				qpdSegment.getDemographicsFields(rep).getValues()
						.setValue(patient.getPatientIdentifiers().get(0).getIdentifierTypeCode());
				rep++;
			}
		}
		if (accountNumber != null) {
			HierarchicDesignator hd = accountNumber.getDomain();
			if (accountNumber.getIdNumber() != null && !accountNumber.getIdNumber().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.18.1");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(accountNumber.getIdNumber());
				rep++;
			}
			if (hd.getNamespaceID() != null && !hd.getNamespaceID().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.18.4.1");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(hd.getNamespaceID());
				rep++;
			}
			if (hd.getUniversalID() != null && !hd.getUniversalID().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.18.4.2");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(hd.getUniversalID());
				rep++;
			}
			if (hd.getUniversalIDType() != null && !hd.getUniversalIDType().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.18.4.3");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(hd.getUniversalIDType());
				rep++;
			}
		}
		for (PatientAddress patientAddress: patient.getAddressList()) {
			String addressLine = patientAddress.getAddressLine();
			if ((addressLine != null) && !addressLine.isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.11.1");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(addressLine);
				rep++;
			}
			String city = patientAddress.getCity();
			if ((city != null) && !city.isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.11.3");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(city);
				rep++;
			}
			String state = patientAddress.getState();
			if ((state != null) && !state.isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.11.4");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(state);
				rep++;
			}
			String zipCode = patientAddress.getZipCode();
			if ((zipCode != null) && !zipCode.isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.11.5");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(zipCode);
				rep++;
			}
			String countryCode = patientAddress.getCountryCode();
			if ((countryCode != null) && !countryCode.isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PID.11.6");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(countryCode);
				rep++;
			}
			// currently add only one occurrence
			break;
		}
		// Message query name
		if (transactionKeyword.equals("ITI-21")) {
			qpdSegment.getMessageQueryName().getIdentifier().setValue("IHE PDQ Query");
		} else {
			qpdSegment.getMessageQueryName().getIdentifier().setValue("IHE PDVQ Query");
			// additional parameter fields for visit
			if ((encounter.getPatientClassCode() != null) && !encounter.getPatientClassCode().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.2");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(encounter.getPatientClassCode());
				rep++;
			}
			if ((encounter.getHospitalService() != null) && !encounter.getHospitalService().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.10");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(encounter.getHospitalService());
				rep++;
			}
			// visit number
			if ((encounter.getVisitNumber() != null) && !encounter.getVisitNumber().isEmpty()) {
				if (encounter.getVisitNumber().contains("^")) {
					String[] components = encounter.getVisitNumber().split("\\^");
					// set CX-1
					qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.19.1");
					qpdSegment.getDemographicsFields(rep).getValues().setValue(components[0]);
					rep++;
					if (components.length > 3) {
						String[] hdSubComponents = components[3].split("\\&");
						if (hdSubComponents.length > 0) {
							qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.19.4.1");
							qpdSegment.getDemographicsFields(rep).getValues().setValue(hdSubComponents[0]);
							rep++;
						}
						if (hdSubComponents.length > 1) {
							qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.19.4.2");
							qpdSegment.getDemographicsFields(rep).getValues().setValue(hdSubComponents[1]);
							rep++;
						}
						if (hdSubComponents.length > 2) {
							qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.19.4.3");
							qpdSegment.getDemographicsFields(rep).getValues().setValue(hdSubComponents[2]);
							rep++;
						}
					}
				} else {
					qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.19.1");
					qpdSegment.getDemographicsFields(rep).getValues().setValue(encounter.getVisitNumber());
					rep++;
				}
			}
			// assigned location
			if ((movement.getPointOfCareCode() != null) && !movement.getPointOfCareCode().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.3.1");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(movement.getPointOfCareCode());
				rep++;
			}
			if ((movement.getRoomCode() != null) && !movement.getRoomCode().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.3.2");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(movement.getRoomCode());
				rep++;
			}
			if ((movement.getBedCode() != null) && !movement.getBedCode().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.3.3");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(movement.getBedCode());
				rep++;
			}
			if ((movement.getFacilityCode() != null) && !movement.getFacilityCode().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.3.4");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(movement.getFacilityCode());
				rep++;
			}
			// doctors
			if ((encounter.getAdmittingDoctorCode() != null) && !encounter.getAdmittingDoctorCode().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.17.2.1");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(encounter.getAdmittingDoctorCode());
				rep++;
			}
			if ((encounter.getReferringDoctorCode() != null) && !encounter.getReferringDoctorCode().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.8.2.1");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(encounter.getReferringDoctorCode());
				rep++;
			}
			if ((encounter.getAttendingDoctorCode() != null) && !encounter.getAttendingDoctorCode().isEmpty()) {
				qpdSegment.getDemographicsFields(rep).getSegmentFieldName().setValue("@PV1.17.2.1");
				qpdSegment.getDemographicsFields(rep).getValues().setValue(encounter.getAttendingDoctorCode());
				rep++;
			}
		}
		// what domains returned
		if ((returnedDomains != null) && !returnedDomains.isEmpty()) {
			int domrep = 0;
			for (AssigningAuthority domain : returnedDomains) {
				if ((domain.getNamespace() != null) && !domain.getNamespace().isEmpty()) {
					qpdSegment.getWhatDomainsReturned(domrep).getAssigningAuthority().getNamespaceID()
							.setValue(domain.getNamespace());
				}
				if ((domain.getUniversalId() != null) && !domain.getUniversalId().isEmpty()) {
					qpdSegment.getWhatDomainsReturned(domrep).getAssigningAuthority().getUniversalID()
							.setValue(domain.getUniversalId());
				}
				if ((domain.getUniversalIdType() != null) && !domain.getUniversalIdType().isEmpty()) {
					qpdSegment.getWhatDomainsReturned(domrep).getAssigningAuthority().getUniversalIDType()
							.setValue(domain.getUniversalIdType());
				}
				domrep++;
			}
		}
	}

	private void fillRCPSegment(RCP rcpSegment) throws DataTypeException {
		rcpSegment.getQueryPriority().setValue("I"); // immediate mode
		if (limit != null) {
			rcpSegment.getQuantityLimitedRequest().getCq1_Quantity().setValue(limit.toString());
			rcpSegment.getQuantityLimitedRequest().getCq2_Units().getCe1_Identifier().setValue("RD");
		}
	}

	/**
	 * <p>fillDSCSegment.</p>
	 *
	 * @param dscSegment a {@link net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.DSC} object.
	 * @param continuationPointer a {@link java.lang.String} object.
	 * @throws ca.uhn.hl7v2.model.DataTypeException if any.
	 */
	public static void fillDSCSegment(DSC dscSegment, String continuationPointer) throws DataTypeException {
		dscSegment.getContinuationPointer().setValue(continuationPointer);
		dscSegment.getContinuationStyle().setValue("I"); // interactive
	}

	/**
	 * <p>buildCancellationMessage.</p>
	 *
	 * @param queryMessage a {@link net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message.QBP_Q21} object.
	 * @param sut a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
	 * @return a {@link ca.uhn.hl7v2.model.Message} object.
	 * @throws ca.uhn.hl7v2.model.DataTypeException if any.
	 */
	public static Message buildCancellationMessage(QBP_Q21 queryMessage, HL7V2ResponderSUTConfiguration sut)
			throws DataTypeException {
		QCN_J01 qcnMessage = new QCN_J01();
		fillMSHSegment(qcnMessage.getMSH(), sut.getApplication(), sut.getFacility(), "PatientManager", "IHE", "QCN",
				"J01", "QCN_J01", "2.5", new SimpleDateFormat("yyyyMMddHHmmss"), sut.getCharset().getHl7Code());
		qcnMessage.getQID().getQueryTag().setValue(queryMessage.getQPD().getQueryTag().getValue());
		qcnMessage.getQID().getMessageQueryName().getIdentifier()
				.setValue(queryMessage.getQPD().getMessageQueryName().getIdentifier().getValue());
		return qcnMessage;
	}

	/**
	 * <p>Setter for the field <code>accountNumber</code>.</p>
	 *
	 * @param accountNumber a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
	 */
	public void setAccountNumber(PatientIdentifier accountNumber) {
		this.accountNumber = accountNumber;
	}
}
