package net.ihe.gazelle.simulator.pixv3.manager;

import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mfmimt700701UV01.MFMIMT700701UV01PriorRegisteredRole;
import net.ihe.gazelle.hl7v3.mfmimt700701UV01.MFMIMT700701UV01PriorRegistration;
import net.ihe.gazelle.hl7v3.mfmimt700701UV01.MFMIMT700701UV01ReplacementOf;
import net.ihe.gazelle.hl7v3.mfmimt700701UV01.MFMIMT700701UV01Subject3;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02MFMIMT700701UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02MFMIMT700701UV01RegistrationEvent;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02MFMIMT700701UV01Subject1;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02MFMIMT700701UV01Subject2;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201303UV02.PRPAMT201303UV02OtherIDs;
import net.ihe.gazelle.hl7v3.prpamt201303UV02.PRPAMT201303UV02Patient;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException;
import net.ihe.gazelle.simulator.hl7v3.responder.PIXV3QueryHandler;
import net.ihe.gazelle.simulator.pam.dao.PatientDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>PRPAIN201304UV02Parser class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PRPAIN201304UV02Parser extends ITI44MessageParser {

    private final String CORRECT_PATIENT_XPATH = "/PRPA_IN201304UV02/controlActProcess/subject/registrationEvent/subject1/Patient";
    /**
     * Constant <code>FIRST_OCCURENCE=0</code>
     */
    public final int FIRST_OCCURENCE = 0;
    /**
     * Constant <code>SINGLE_VALUE=1</code>
     */
    public final int SINGLE_VALUE = 1;

    /**
     * <p>mergePatients.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type} object.
     * @throws net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException if any.
     */
    public void mergePatients(PRPAIN201304UV02Type request) throws HL7V3ParserException {
        // first parse the control act process
        List<PatientIdentifier> correctPatientIdentifiers = parseControlActProcess(request.getControlActProcess());
        // store patient and its identifiers
        Actor pixManager = Actor.findActorWithKeyword(PIX_MANAGER_ACTOR_KEYWORD);
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Patient dbPatient = null;
        for (PatientIdentifier pid : correctPatientIdentifiers) {
            pid.setFullPatientIdentifierIfEmpty();
            dbPatient = PatientDAO.getActivePatientByIdentifierByActor(pid, entityManager, pixManager);
            if (dbPatient != null) {
                // we've found one patient with the given identifier
                break;
            }
        }
        if (dbPatient != null) {
            II priorRegisteredRoleId = parseReplacementOf(request.getControlActProcess());
            PatientIdentifier incorrectPatientIdentifier = getPatientIdentifierFromII(priorRegisteredRoleId);
            incorrectPatientIdentifier.setFullPatientIdentifierIfEmpty();
            Patient incorrectPatient = PatientDAO.getActivePatientByIdentifierByActor(incorrectPatientIdentifier, entityManager, pixManager);
            if (incorrectPatient != null) {
                incorrectPatient.deactivatePatient();
                // add identifiers to the "surviving" patient
                PatientIdentifier pid = PatientIdentifierDAO.getIdentifierByFullId(incorrectPatientIdentifier.getFullPatientId());
                dbPatient.getPatientIdentifiers().add(pid);
                dbPatient.savePatient(entityManager);
            }
            // else there is nothing to do, this obsolete patient is unknown
        }
    }

    private II parseReplacementOf(PRPAIN201304UV02MFMIMT700701UV01ControlActProcess controlActProcess) throws HL7V3ParserException {
        // if we reach this point, we have one and only one subject element
        PRPAIN201304UV02MFMIMT700701UV01Subject1 subject1 = controlActProcess.getSubject().get(FIRST_OCCURENCE);
        List<MFMIMT700701UV01ReplacementOf> replacementOfItems = subject1.getRegistrationEvent().getReplacementOf();
        if (replacementOfItems.isEmpty()) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02/controlActProcess/subject/registrationEvent", "The replacementOf element is required");
        } else if (replacementOfItems.size() > 1) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02/controlActProcess/subject/registrationEvent", "Only one replacementOf element is allowed");
        } else {
            return getPriorRegisteredRoleId(replacementOfItems.get(FIRST_OCCURENCE));
        }
    }

    private II getPriorRegisteredRoleId(MFMIMT700701UV01ReplacementOf replacementOf) throws HL7V3ParserException {
        MFMIMT700701UV01PriorRegistration priorRegistration = replacementOf.getPriorRegistration();
        if (priorRegistration == null) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02/controlActProcess/subject/registrationEvent/replacementOf",
                    "There SHALL be a priorRegistration element");
        }
        MFMIMT700701UV01Subject3 subject1 = priorRegistration.getSubject1();
        if (subject1 == null) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02/controlActProcess/subject/registrationEvent/replacementOf/priorRegistration",
                    "There SHALL be a subject1 element");
        }
        MFMIMT700701UV01PriorRegisteredRole registeredRole = subject1.getPriorRegisteredRole();
        if (registeredRole == null) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02/controlActProcess/subject/registrationEvent/replacementOf/priorRegistration/subject1",
                    "There SHALL be a PriorRegisteredRole role");
        }
        List<II> registeredRoleIds = registeredRole.getId();
        if (registeredRoleIds.size() != SINGLE_VALUE) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02/controlActProcess/subject/registrationEvent/replacementOf/priorRegistration/subject1/priorRegisteredRole",
                    "There SHALL be a single PriorRegisteredRole.id attribute, representing the subsumed patient identifier");
        } else {
            return registeredRoleIds.get(FIRST_OCCURENCE);
        }
    }

    private List<PatientIdentifier> parseControlActProcess(PRPAIN201304UV02MFMIMT700701UV01ControlActProcess controlActProcess) throws
            HL7V3ParserException {
        if (controlActProcess == null) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02", "Received message does not contain ControlActProcess element");
        } else if (controlActProcess.getSubject().isEmpty()) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02/controlActProcess", "The ControlActProcess does not contain Subject element");
        } else if (controlActProcess.getSubject().size() > 1) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02/controlActProcess", "Only one subject element is allowed");
        } else {
            PRPAIN201304UV02MFMIMT700701UV01Subject1 subject1 = controlActProcess.getSubject().get(FIRST_OCCURENCE);
            return parseSubject(subject1);
        }
    }

    private List<PatientIdentifier> parseSubject(PRPAIN201304UV02MFMIMT700701UV01Subject1 subject) throws HL7V3ParserException {
        if (subject.getRegistrationEvent() == null) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02/controlActProcess/subject", "Subject does not contain any RegistrationEvent element");
        }
        return parseRegistrationEvent(subject.getRegistrationEvent());
    }

    private List<PatientIdentifier> parseRegistrationEvent(PRPAIN201304UV02MFMIMT700701UV01RegistrationEvent registrationEvent) throws HL7V3ParserException {
        if (registrationEvent.getSubject1() == null) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02/controlActProcess/subject/registrationEvent", "RegistrationEvent does not contain " +
                    "any subject1 element");
        }
        return parseSubject1(registrationEvent.getSubject1());
    }

    private List<PatientIdentifier> parseSubject1(PRPAIN201304UV02MFMIMT700701UV01Subject2 subject1) throws HL7V3ParserException {
        if (subject1.getPatient() == null) {
            throw new HL7V3ParserException("/PRPA_IN201304UV02/controlActProcess/subject/registrationEvent/subject1", "Subject1 does not contain " +
                    "any Patient element");
        } else {
            return parsePatient(subject1.getPatient());
        }
    }

    private void checkEprSpidForPatientIdentifiers(List<PatientIdentifier> identifierList) throws HL7V3ParserException {
        boolean epr = SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD);
        if (epr) {
            PatientIdentifier eprSpid = null;
            for (PatientIdentifier pid : identifierList) {
                if (pid.getDomain() != null && PIXV3QueryHandler.SPID_ROOT.equals(pid.getDomain().getUniversalID())) {
                    eprSpid = pid;
                }
            }
            if (eprSpid == null) {
                throw new HL7V3ParserException(PATIENT_XPATH, "Patient is expected to contain an EPR-SPID");
            }
        }
    }

    private List<PatientIdentifier> parsePatient(PRPAMT201303UV02Patient subject1Patient) throws HL7V3ParserException {
        List<PatientIdentifier> identifierList = parsePatientIdentifier(subject1Patient.getId());
        if (subject1Patient.getPatientNonPersonLivingSubject() != null) {
            throw new HL7V3ParserException(CORRECT_PATIENT_XPATH, "Patient is expected to be a patientPerson");
        } else if (subject1Patient.getPatientPerson() == null) {
            throw new HL7V3ParserException(CORRECT_PATIENT_XPATH, "Subject1 does not contain a patientPerson element");
        } else {
            List<PatientIdentifier> othersIds = parseOtherIds(subject1Patient.getPatientPerson().getAsOtherIDs());
            if (!othersIds.isEmpty()) {
                identifierList.addAll(othersIds);
            }
        }
        if (identifierList.size() != 2) {
            throw new HL7V3ParserException(PATIENT_XPATH, "A patient must contain exactly 2 patient identifier (" + identifierList.size() + " provided)");
        }
        checkEprSpidForPatientIdentifiers(identifierList);
        return identifierList;
    }

    private List<PatientIdentifier> parseOtherIds(List<PRPAMT201303UV02OtherIDs> asOtherIDs) {
        if (asOtherIDs.isEmpty()) {
            return new ArrayList<PatientIdentifier>();
        } else {
            List<PatientIdentifier> identifierList = new ArrayList<PatientIdentifier>();
            for (PRPAMT201303UV02OtherIDs otherId : asOtherIDs) {
                List<PatientIdentifier> localIdentifiers = parsePatientIdentifier(otherId.getId());
                if (!localIdentifiers.isEmpty()) {
                    identifierList.addAll(localIdentifiers);
                }
            }
            return identifierList;
        }
    }
}
