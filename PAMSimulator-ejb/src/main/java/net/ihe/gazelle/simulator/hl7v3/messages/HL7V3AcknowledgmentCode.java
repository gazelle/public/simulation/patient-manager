package net.ihe.gazelle.simulator.hl7v3.messages;


public enum HL7V3AcknowledgmentCode {
    AA("AA", "Application Accept"),
    AE("AE","Application Error"),
    AR("AR","Application Reject"),
    CA("CA","Commit Accept"),
    CE("CE","Commit Error"),
    CR("CR","Commit Reject");

    private final String value;
    private final String message;

    HL7V3AcknowledgmentCode(String value, String message) {
        this.value = value;
        this.message = message;
    }

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

}
