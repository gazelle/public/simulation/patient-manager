package net.ihe.gazelle.simulator.application;

import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import java.util.List;

/**
 * Application Bean for FHIR Responder Configuration search.
 */
@Name("fhirResponderSUTConfigurationFinder")
public class FHIRResponderSUTConfigurationFinder {

    @In(create = true, value = "fhirResponderSUTConfigurationDAO")
    private FHIRResponderSUTConfigurationDAO sutDAO;

    /**
     * Empty Constructor for injection
     */
    public FHIRResponderSUTConfigurationFinder() {
        //Empty
    }

    /**
     * Getter for teh sutDAO property.
     *
     * @return the value of the property.
     */
    public FHIRResponderSUTConfigurationDAO getSutDAO() {
        return sutDAO;
    }

    /**
     * Setter for the sutDAO property.
     *
     * @param sutDAO value to set to the property.
     */
    public void setSutDAO(FHIRResponderSUTConfigurationDAO sutDAO) {
        this.sutDAO = sutDAO;
    }

    /**
     * Retrieve all SUT linked to the transaction with the given keyword.
     *
     * @param transactionKeyword keyword of the transaction for which we want to get SUT.
     * @return the list of SUT linked to given transaction.
     */
    public List<FHIRResponderSUTConfiguration> retrieveSUTByTransactionKeyword(String transactionKeyword) {
        return sutDAO.retrieveSUTByTransaction(transactionKeyword);
    }
}
