package net.ihe.gazelle.simulator.pam.dao;

import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;

/**
 * <p>MovementDAO class.</p>
 *
 * @author aberge
 * @version 1.0: 04/10/17
 */

public class MovementDAO {

    public static void randomlyFillMovement(Movement selectedMovement, boolean isBP6Mode) {
        ValueSetProvider provider = ValueSetProvider.getInstance();
        String concept = provider.getRandomCodeFromValueSet("BED");
        if (concept != null) {
            selectedMovement.setBedCode(concept);
        }
        concept = provider.getRandomCodeFromValueSet("FACILITY");
        if (concept != null) {
            selectedMovement.setFacilityCode(concept);
        }
        concept = provider.getRandomCodeFromValueSet("ROOM");
        if (concept != null) {
            selectedMovement.setRoomCode(concept);
        }
        concept = provider.getRandomCodeFromValueSet("POINT_OF_CARE");
        if (concept != null) {
            selectedMovement.setPointOfCareCode(concept);
        }
        if (isBP6Mode) {
            selectedMovement.setNatureOfMovement("HMS");
            selectedMovement.setNursingCareWard(provider.getRandomCodeFromValueSet("NURSING_WARD"));
            selectedMovement.setWardCode(provider.getRandomCodeFromValueSet("MEDICAL_WARD"));
            selectedMovement.getEncounter().getPatientsPHRInfo().setStatus("INEXISTANT");
            selectedMovement.getEncounter().getDrgMovement().setPmsiAdmissionMode("8");
        }
    }
}
