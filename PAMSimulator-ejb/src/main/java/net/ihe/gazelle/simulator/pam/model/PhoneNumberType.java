package net.ihe.gazelle.simulator.pam.model;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 09/03/17.
 */
public enum PhoneNumberType {

    HOME("PRN", "HP", "Primary Residence Number"),
    OTHER("ORN", "H", "Other Residence Number"),
    WORK("WPN", "WP", "Work Number"),
    VACATION("VHN", "HV", "Vacation Home Number"),
    MOBILE("PRS", "MC", "Mobile Phone Number"),
    EMERGENCY("EMR", "EC", "Emergency Number"),
    BEEPER("BPN", "PG", "Beeper Number"),;

    private String hl7v2Code;
    private String hl7v3Code;
    private String label;

    PhoneNumberType(String hl7v2Code, String hl7v3Code, String label) {
        this.hl7v2Code = hl7v2Code;
        this.hl7v3Code = hl7v3Code;
        this.label = label;
    }

    public String getHl7v2Code() {
        return hl7v2Code;
    }

    public String getHl7v3Code() {
        return hl7v3Code;
    }

    public String getLabel() {
        return label;
    }

    public static PhoneNumberType getTypeForHL7v3(String use) {
        for (PhoneNumberType type : values()) {
            if (type.getHl7v3Code().equals(use)) {
                return type;
            }
        }
        return null;
    }

    public static List<SelectItem> getListAsSelectItems() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (PhoneNumberType type: values()){
            items.add(new SelectItem(type, type.label));
        }
        return items;
    }

    public static PhoneNumberType getTypeForHL7v2(String value) {
        for (PhoneNumberType type: values()){
            if (type.getHl7v2Code().equals(value)){
                return type;
            }
        }
        return null;
    }
}
