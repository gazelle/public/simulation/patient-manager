package net.ihe.gazelle.simulator.pdq.hl7;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.v25.datatype.CX;
import ca.uhn.hl7v2.model.v25.datatype.XAD;
import ca.uhn.hl7v2.model.v25.datatype.XPN;
import ca.uhn.hl7v2.model.v25.datatype.XTN;
import net.ihe.gazelle.HL7Common.exception.HL7v2ParsingException;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.PID;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.PV1;
import net.ihe.gazelle.simulator.pam.hl7.HL7MessageDecoder;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import net.ihe.gazelle.simulator.pam.model.PhoneNumberType;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>RSPMessageDecoder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class RSPMessageDecoder extends HL7MessageDecoder {

    private static Log log = Logging.getLog(RSPMessageDecoder.class);

    private RSPMessageDecoder() {

    }

    /**
     * Returns the patient contains in the PID segment given as parameter
     *
     * @param segment a {@link net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.PID} object.
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public static Patient createPatientFromPIDSegment(PID segment) throws HL7v2ParsingException {
        try {
            if (segment == null || segment.isEmpty()) {
                throw new HL7v2ParsingException("PID segment is empty");
            }
        } catch (HL7Exception e) {
            throw new HL7v2ParsingException(e);
        }
        Patient patient = new Patient();
        patient.setCreationDate(new Date());
        XPN[] patientNames = segment.getPatientName();
        if ((patientNames != null) && (patientNames.length > 0)) {
            XPN patientName = patientNames[0];
            patient.setFirstName(patientName.getGivenName().getValue());
            patient.setLastName(patientName.getFamilyName().getSurname().getValue());
        }
        patient.setMotherMaidenName(segment.getMotherSMaidenName(0).getFamilyName().getSurname().getValue());

        try {
            Date dob = segment.getDateTimeOfBirth().getTime().getValueAsDate();
            patient.setDateOfBirth(dob);
        } catch (DataTypeException e) {
            log.warn("Unable to parse date of birth");
        }
        patient.setGenderCode(segment.getAdministrativeSex().getValue());
        patient.setRaceCode(segment.getRace(0).getIdentifier().getValue());

        patient.setReligionCode(segment.getReligion().getIdentifier().getValue());
        if (segment.getPhoneNumberHomeReps() > 0) {
            for (XTN xtn : segment.getPhoneNumberHome()) {
                PatientPhoneNumber phoneNumber = new PatientPhoneNumber();
                phoneNumber.setValue(xtn.getLocalNumber().getValue());
                if (xtn.getTelecommunicationUseCode() != null && !xtn.getTelecommunicationUseCode().getValue().isEmpty()) {
                    PhoneNumberType type = PhoneNumberType.getTypeForHL7v2(xtn.getTelecommunicationUseCode().getValue());
                    phoneNumber.setType(type);
                }
                phoneNumber.setPatient(patient);
                patient.addPhoneNumber(phoneNumber);
            }
        }
        patient.setEmail(segment.getPhoneNumberHome(0).getEmailAddress().getValue());

        XAD[] addresses = segment.getPatientAddress();
        if ((addresses != null) && (addresses.length > 0)) {
            setPatientAddresses(patient, addresses);
        }
        CX[] identifiers = segment.getPatientIdentifierList();
        if ((identifiers != null) && (identifiers.length > 0)) {
            List<PatientIdentifier> patientIdentifiers = new ArrayList<PatientIdentifier>();
            for (CX cxEntry: identifiers) {
                PatientIdentifier pid = HL7MessageDecoder.buildPatientIdentifier(cxEntry);
                if (pid != null) {
                    patientIdentifiers.add(pid);
                }
            }
            patient.setPatientIdentifiers(patientIdentifiers);
        } else {
            patient.setPatientIdentifiers(null);
        }
        return patient;
    }

    /**
     * <p>fillEncounterWithPV1Segment.</p>
     *
     * @param pv1Segment a {@link net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.PV1} object.
     * @param encounter  a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public static Encounter fillEncounterWithPV1Segment(PV1 pv1Segment, Encounter encounter) {
        if (pv1Segment != null) {
            try {
                String segmentAsString = pv1Segment.encode();
                // if the received message does not contain a PV1 segment, segmentAsString = "PV1"
                if ((segmentAsString == null) || segmentAsString.isEmpty() || (segmentAsString.length() < 4)) {
                    return null;
                }
            } catch (HL7Exception e) {
                log.error("unable to encode PV1 segment");
                return null;
            }
        } else {
            return null;
        }
        encounter.setPatientClassCode(pv1Segment.getPatientClass().getValue());
        try {
            encounter.setAttendingDoctorCode(pv1Segment.getAttendingDoctor(0).encode());
        } catch (HL7Exception e) {
            log.info("No attending doctor given");
        }
        try {
            encounter.setReferringDoctorCode(pv1Segment.getReferringDoctor(0).encode());
        } catch (HL7Exception e) {
            log.info("No referring doctor given");
        }
        try {
            encounter.setVisitNumber(pv1Segment.getVisitNumber().encode());
        } catch (HL7Exception e) {
            log.warn("visit number is not given !!!");
            encounter.setVisitNumber(null);
        }
        try {
            encounter.setAdmitDate(pv1Segment.getAdmitDateTime().getTime().getValueAsDate());
        } catch (NullPointerException e) {
            log.info("No admit date given");
        } catch (DataTypeException e) {
            log.info("No admit date given");
        }
        try {
            encounter.setDischargeDate(pv1Segment.getDischargeDateTime(0).getTime().getValueAsDate());
        } catch (NullPointerException e) {
            log.info("No discharge date given");
        } catch (DataTypeException e) {
            log.info("No discharge date given");
        }
        try {
            encounter.setAdmittingDoctorCode(pv1Segment.getAdmittingDoctor(0).encode());
        } catch (HL7Exception e) {
            log.info("No admitting doctor provided");
        }
        Movement movement = new Movement(encounter);
        try {
            movement.setAssignedPatientLocation(pv1Segment.getAssignedPatientLocation().encode());
        } catch (HL7Exception e) {
            movement.setAssignedPatientLocation(null);
        }
        try {
            movement.setPriorPatientLocation(pv1Segment.getPriorPatientLocation().encode());
        } catch (HL7Exception e) {
            log.info("cannot encode prior patient location");
        }
        try {
            movement.setPendingPatientLocation(pv1Segment.getPendingLocation().encode());
        } catch (HL7Exception e) {
            log.info("cannot encode pending patient location");
        }
        try {
            movement.setPriorTemporaryLocation(pv1Segment.getPriorTemporaryLocation().encode());
        } catch (HL7Exception e) {
            log.info("cannot encode prior temporary location");
        }
        try {
            movement.setTemporaryPatientLocation(pv1Segment.getTemporaryLocation().encode());
        } catch (HL7Exception e) {
            log.info("cannot encode temporary patient location");
        }
        encounter.setMovements(new ArrayList<Movement>());
        encounter.getMovements().add(movement);
        return encounter;
    }

}
