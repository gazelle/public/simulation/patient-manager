package net.ihe.gazelle.simulator.pam.action;

import ca.uhn.fhir.parser.IParser;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.fhir.constants.FhirParserProvider;
import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.fhir.util.ToFhirResourceConverter;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerPages;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.filter.UserValueFormatter;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Resource;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * <p>PatientManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("patientManager")
@Scope(ScopeType.PAGE)
public class PatientManager implements Serializable, QueryModifier<Patient>, UserAttributeCommon {

    private static final Logger LOG = LoggerFactory.getLogger(PatientManager.class);

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String ACTOR_FILTER_KEY = "actor";
    public static final String OWNER = "owner";

    private Patient selectedPatient = null;
    private Encounter selectedEncounter;
    private Boolean enableWorklist = null;
    private boolean displayLinks = false;
    protected FilterDataModel<Patient> patients;
    protected Filter<Patient> filter;
    private String searchedPatientId;
    private String relationshipLastName;
    private boolean onlyActivePatients = true;
    private boolean filterOnConnectathonOnly;

    @In(value="gumUserService")
    private UserService userService;

    @Create
    public void initializePage(){
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String catAsString = urlParams.get("cat");
        String actorIdParam = urlParams.get(ACTOR_FILTER_KEY);
        if (actorIdParam != null){
            try {
                Actor catActor = Actor.findActorWithKeyword(PatientManagerConstants.CONNECTATHON);
                if (catActor.getId().toString().equals(actorIdParam)){
                    filterOnConnectathonOnly = true;
                }
            }catch (NumberFormatException e){
                LOG.warn(actorIdParam + " is not a correct number");
            }
        }
        else if (catAsString != null){
            filterOnConnectathonOnly = Boolean.parseBoolean(catAsString);
        } else {
            filterOnConnectathonOnly = false;
        }
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<Patient> getFilter() {
        if (filter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            filter = new Filter<Patient>(getFilterCriteria(), params);
            filter.getFormatters().put(OWNER, new UserValueFormatter(filter, OWNER));
        }
        return filter;
    }

    private HQLCriterionsForFilter<Patient> getFilterCriteria() {
        PatientQuery query = new PatientQuery();
        HQLCriterionsForFilter<Patient> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("firstname", query.firstName());
        criteria.addPath("lastname", query.lastName());
        criteria.addPath(OWNER, query.creator());
        criteria.addPath("gender", query.genderCode());
        criteria.addPath("country", query.countryCode());
        criteria.addPath("mothersmaidenname", query.motherMaidenName());
        criteria.addPath("testData", query.testData(), false, false);
        criteria.addQueryModifier(this);
        if (filterOnConnectathonOnly){
            Actor catActor = Actor.findActorWithKeyword(PatientManagerConstants.CONNECTATHON);
            criteria.addPath(ACTOR_FILTER_KEY, query.simulatedActor(), catActor, catActor);
        } else {
            criteria.addPath(ACTOR_FILTER_KEY, query.simulatedActor());
        }
        return criteria;
    }

    /** {@inheritDoc} */
    @Override
    public void modifyQuery(HQLQueryBuilder<Patient> queryBuilder, Map<String, Object> params) {
        PatientQuery query = new PatientQuery();
        if (searchedPatientId != null && !searchedPatientId.isEmpty()) {
            queryBuilder.addRestriction(query.patientIdentifiers().fullPatientId().likeRestriction(searchedPatientId, HQLRestrictionLikeMatchMode
                    .ANYWHERE));
        }
        if (relationshipLastName != null && !relationshipLastName.isEmpty()) {
            queryBuilder.addRestriction(query.personalRelationships().lastName().likeRestriction(relationshipLastName,
                    HQLRestrictionLikeMatchMode.ANYWHERE));
        }
        if (onlyActivePatients){
            queryBuilder.addRestriction(query.stillActive().eqRestriction(true));
        } else {
            queryBuilder.addRestriction(query.stillActive().isNotNullRestriction());
        }
        if (!filterOnConnectathonOnly){
            queryBuilder.addRestriction(query.simulatedActor().keyword().neqRestriction(PatientManagerConstants.CONNECTATHON));
        }
    }

    /**
     * <p>Getter for the field <code>patients</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<Patient> getPatients() {
        return new FilterDataModel<Patient>(getFilter()) {
            @Override
            protected Object getId(Patient patient) {
                return patient.getId();
            }
        };
    }

    /**
     * <p>Setter for the field <code>displayLinks</code>.</p>
     *
     * @param displayLinks a boolean.
     */
    public void setDisplayLinks(boolean displayLinks) {
        this.displayLinks = displayLinks;
    }

    /**
     * <p>isDisplayLinks.</p>
     *
     * @return a boolean.
     */
    public boolean isDisplayLinks() {
        return displayLinks;
    }

    /**
     * <p>isEPD.</p>
     *
     * @return a boolean.
     */
    public boolean isEPD(){
        return SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD);
    }

    public boolean isWorklistFeatureEnabled(){
        return SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.MOD_WORKLIST);
    }

    /**
     * <p>Getter for the field <code>selectedPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    /**
     * <p>Setter for the field <code>selectedPatient</code>.</p>
     *
     * @param selectedPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }


    /**
     * Used in sutAssigingAuthorities.xhtml
     */
    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    /**
     * <p>displayPatient.</p>
     *
     * @param inPatient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public void displayPatient(Patient inPatient) {
        selectedPatient = inPatient;
    }

    /**
     * <p>reset.</p>
     */
    public void reset() {
        getFilter().clear();
        searchedPatientId = null;
        onlyActivePatients = true;
    }


    /**
     * <p>Getter for the field <code>searchedPatientId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSearchedPatientId() {
        return searchedPatientId;
    }


    /**
     * <p>Setter for the field <code>searchedPatientId</code>.</p>
     *
     * @param searchedPatientId a {@link java.lang.String} object.
     */
    public void setSearchedPatientId(String searchedPatientId) {
        this.searchedPatientId = searchedPatientId;
    }

    /**
     * <p>isWorklistEnabled.</p>
     *
     * @return a boolean.
     */
    public boolean isWorklistEnabled() {
        if (enableWorklist == null) {
            enableWorklist = SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.MOD_WORKLIST);
        }
        return enableWorklist;
    }

    /**
     * <p>Getter for the field <code>relationshipLastName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRelationshipLastName() {
        return relationshipLastName;
    }

    /**
     * <p>Setter for the field <code>relationshipLastName</code>.</p>
     *
     * @param relationshipLastName a {@link java.lang.String} object.
     */
    public void setRelationshipLastName(String relationshipLastName) {
        this.relationshipLastName = relationshipLastName;
    }

    /**
     * <p>deactivateSelectedPatients.</p>
     */
    public void deactivateSelectedPatients(){
        List<Patient> toDeactivate = getFilteredPatients();
        if (!toDeactivate.isEmpty()){
            for (Patient patient: toDeactivate){
                patient.deactivatePatient();
            }
        }
    }

    /**
     * <p>activateSelectedPatients.</p>
     */
    public void activateSelectedPatients(){
        List<Patient> toActivate = getFilteredPatients();
        if (!toActivate.isEmpty()){
            for (Patient patient: toActivate){
                patient.activatePatient();
            }
        }
    }

    private List<Patient> getFilteredPatients(){
        FilterDataModel<Patient> dataModel = getPatients();
        return dataModel.getAllItems(FacesContext.getCurrentInstance());
    }

    public String redirectToWorklistPage(Patient inPatient){
        return "/patient/createWorklist.seam?pid=" + inPatient.getId();
    }

    public boolean isOnlyActivePatients() {
        return onlyActivePatients;
    }

    public void setOnlyActivePatients(boolean onlyActivePatients) {
        this.onlyActivePatients = onlyActivePatients;
    }

    public boolean isFilterOnConnectathonOnly() {
        return filterOnConnectathonOnly;
    }

    public String shareSinglePatient(Patient patient){
        String basePage = PatientManagerPages.CAT_SHARE.getLink();
        StringBuilder url = new StringBuilder(basePage);
        url.append("?id=");
        url.append(patient.getId());
        return url.toString();
    }

    public String shareSelectedPatients(){
        String basePage = PatientManagerPages.CAT_SHARE.getLink();
        String parameters = getFilter().getUrlParameters();
        StringBuilder url = new StringBuilder(basePage);
        url.append('?');
        if (searchedPatientId != null && !searchedPatientId.isEmpty()){
            url.append("pidpart=");
            url.append(searchedPatientId);
            if (!parameters.isEmpty()){
                url.append('&');
            }
        }
        url.append(parameters);
        return url.toString();
    }

    public void getSelectedPatientsAsFHIR(){
        List<Patient> patients = getFilteredPatients();
        Bundle bundle = ToFhirResourceConverter.patientListToFhirBundle(patients);
        downloadResource(bundle);
    }

    private void downloadResource(Resource resource) {
        IParser parser = FhirParserProvider.getXmlParser();
        String xmlResource = parser.encodeResourceToString(resource);

        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.setContentType("application/xml");
            response.setHeader("Content-Disposition", "attachment;filename=selectedPatients.xml");

            ServletOutputStream servletOutputStream;
            servletOutputStream = response.getOutputStream();
            servletOutputStream.write(xmlResource.getBytes(StandardCharsets.UTF_8));
            servletOutputStream.flush();
            servletOutputStream.close();

            context.responseComplete();
        } catch (IOException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to create resource for selected patients");
        }
    }


    public void getFhirResource(Patient patient){
        PatientFhirIHE fhirPatient = ToFhirResourceConverter.patientToFhirResource(patient);
        downloadResource(fhirPatient);
    }

    public String editPatient(Patient patient){
        StringBuilder pageUrl = new StringBuilder(PatientManagerPages.ADMIN_PATIENT_CREATION.getLink());
        pageUrl.append("?id=");
        pageUrl.append(patient.getId());
        return pageUrl.toString();
    }
}
