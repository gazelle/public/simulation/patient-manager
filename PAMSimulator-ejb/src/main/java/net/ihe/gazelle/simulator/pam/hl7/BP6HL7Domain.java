package net.ihe.gazelle.simulator.pam.hl7;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.AbstractMessage;
import ca.uhn.hl7v2.model.v25.datatype.HD;
import ca.uhn.hl7v2.model.v25.datatype.IS;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;

import java.util.Arrays;
import java.util.List;

public class BP6HL7Domain implements HL7Domain<BP6HL7MessageGenerator> {

    public static class INS {
        private static final List<String> authorities = Arrays.asList("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.9","1.2.250.1.213.1.4.10","1.2.250.1.213.1.4.11");
        public static List<String> getAuthorities() {
            return authorities;
        }
        public static boolean hasINSAuthority(PatientIdentifier pi) {
            return authorities.contains(pi.splitPatientId().get("CX-4-2"));
        }
        private INS() { super(); }
    }

    private static final String FACILITY = "PAM_FR";
    public static boolean isBP6Facility(String facility) {
        return BP6HL7Domain.FACILITY.equals(facility);
    }
    public static boolean isBP6Facility(HD facility) {
        return isBP6Facility(facility.getNamespaceID());
    }
    private static boolean isBP6Facility(IS namespaceID) {
        return isBP6Facility(namespaceID.getValue());
    }
    public static boolean isBP6Facility(AbstractMessage msg) {
        try {
            return isBP6Facility(new Terser(msg).get("/.MSH-6-1"));
        } catch (HL7Exception e) {
            return false;
        }
    }
    public static boolean isBP6Facility(Domain domain) {
        return PatientManagerConstants.ITI_FR.equals(domain.getKeyword());
    }

    private final BP6HL7MessageGenerator generator;

    public BP6HL7Domain() {
        generator = new BP6HL7MessageGenerator();
    }

    @Override
    public BP6HL7MessageGenerator getFrameGenerator() {
        return generator;
    }

    @Override
    public String getDomainKeyword() {
        return PatientManagerConstants.ITI_FR;
    }

}
