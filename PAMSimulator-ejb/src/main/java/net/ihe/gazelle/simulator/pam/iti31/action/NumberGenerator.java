package net.ihe.gazelle.simulator.pam.iti31.action;

import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>NumberGenerator class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class NumberGenerator implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -613864983079581778L;

	/**
	 * <p>generate.</p>
	 *
	 * @param numberType a {@link net.ihe.gazelle.simulator.pam.model.DesignatorType} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String generate(DesignatorType numberType) {
		HierarchicDesignator hd = HierarchicDesignatorDAO.getAssigningAuthority(numberType);
		if (hd == null){
			return null;
		}
		StringBuilder generatedNumber = new StringBuilder(hd.nextId());
		switch (numberType){
			case ACCOUNT_NUMBER:
			case VISIT_NUMBER:
				generatedNumber.append("^^^");
				generatedNumber.append(hd.toString().replace('^', '&'));
				generatedNumber.append('^');
				generatedNumber.append(hd.getType());
				break;
			case PATIENT_ID:
				generatedNumber.append("^^^");
				generatedNumber.append(hd.toString().replace('^', '&'));
				break;
			case MOVEMENT_ID:
			default:
				generatedNumber.append('^');
				generatedNumber.append(hd.toString());
				break;
		}
		return generatedNumber.toString();
	}

	/**
	 * <p>splitVisitNumber.</p>
	 *
	 * @param visitNumber a {@link java.lang.String} object.
	 * @return a {@link java.util.Map} object.
	 */
	public static Map<String, String> splitVisitNumber(String visitNumber) {
		return PatientIdentifierDAO.splitCXField(visitNumber);
	}

	/**
	 * <p>splitMovementId.</p>
	 *
	 * @param movementId a {@link java.lang.String} object.
	 * @return a {@link java.util.Map} object.
	 */
	public static Map<String, String> splitMovementId(String movementId) {
		String[] subfields = movementId.split("\\^");
		Map<String, String> result = new HashMap<String, String>();
		result.put("EI-1", subfields[0]);
		result.put("EI-2", subfields[1]);
		result.put("EI-3", subfields[2]);
		result.put("EI-4", subfields[3]);
		return result;
	}

}
