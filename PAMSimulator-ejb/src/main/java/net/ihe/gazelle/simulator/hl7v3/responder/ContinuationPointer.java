package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;

import java.util.Date;
import java.util.List;

/**
 * This internal class is a structure to store the required information about a given pointer
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ContinuationPointer {
    /**
     * value of the continuation pointer (has given in the QBP message for PDQ or queryId@root and queryId@extension for ITI-47)
     */
    private String value;
    /**
     * nb of results to return each time the pointer is received
     */
    private Integer nbOfHitsToReturn;
    /**
     * the list of patients associated to the query (ITI-21/ITI-47)
     */
    private List<Patient> patients;
    /**
     * the list of encounters associated to the query (ITI-22)
     */
    private List<Movement> movements;
    /**
     * when the last request has been performed (in order to clean the list with old requests)
     */
    private Date lastRequestTime;
    /**
     * For PDQv3 the queryByParameter is not sent again by the PDC but is mandatory in the response
     */
    private PRPAMT201306UV02QueryByParameter queryByParameter;
    /**
     * The initial number of results
     */
    private Integer resultTotalQuantity;

    /**
     * the initial list of domains to return, required to properly build the response
     */
    private List<HierarchicDesignator> domainsToReturn;

    /**
     * <p>Constructor for ContinuationPointer.</p>
     *
     * @param value             a {@link java.lang.String} object.
     * @param nbOfHintsToReturn a {@link java.lang.Integer} object.
     */
    public ContinuationPointer(String value, Integer nbOfHintsToReturn) {
        this.value = value;
        this.nbOfHitsToReturn = nbOfHintsToReturn;
        this.lastRequestTime = new Date();
    }

    /**
     * <p>Getter for the field <code>value</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValue() {
        return value;
    }

    /**
     * <p>Getter for the field <code>nbOfHitsToReturn</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getNbOfHitsToReturn() {
        return nbOfHitsToReturn;
    }

    /**
     * <p>Getter for the field <code>patients</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Patient> getPatients() {
        return patients;
    }

    /**
     * <p>Setter for the field <code>patients</code>.</p>
     *
     * @param patients a {@link java.util.List} object.
     */
    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    /**
     * <p>Getter for the field <code>movements</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Movement> getMovements() {
        return movements;
    }

    /**
     * <p>Setter for the field <code>movements</code>.</p>
     *
     * @param movements a {@link java.util.List} object.
     */
    public void setMovements(List<Movement> movements) {
        this.movements = movements;
    }

    /**
     * <p>Getter for the field <code>lastRequestTime</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getLastRequestTime() {
        if (lastRequestTime == null){
            return null;
        } else {
            return (Date) lastRequestTime.clone();
        }
    }

    /**
     * <p>Setter for the field <code>lastRequestTime</code>.</p>
     *
     * @param lastRequestTime a {@link java.util.Date} object.
     */
    public void setLastRequestTime(Date lastRequestTime) {
        if (lastRequestTime != null) {
            this.lastRequestTime = (Date) lastRequestTime.clone();
        } else {
            this.lastRequestTime = null;
        }
    }

    /**
     * <p>Getter for the field <code>domainsToReturn</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HierarchicDesignator> getDomainsToReturn() {
        return domainsToReturn;
    }

    /**
     * <p>Setter for the field <code>domainsToReturn</code>.</p>
     *
     * @param domainsToReturn a {@link java.util.List} object.
     */
    public void setDomainsToReturn(List<HierarchicDesignator> domainsToReturn) {
        this.domainsToReturn = domainsToReturn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ContinuationPointer other = (ContinuationPointer) obj;
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

    /**
     * <p>Setter for the field <code>nbOfHitsToReturn</code>.</p>
     *
     * @param nbOfHitsToReturn a {@link java.lang.Integer} object.
     */
    public void setNbOfHitsToReturn(Integer nbOfHitsToReturn) {
        this.nbOfHitsToReturn = nbOfHitsToReturn;
    }

    /**
     * <p>Getter for the field <code>queryByParameter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter} object.
     */
    public PRPAMT201306UV02QueryByParameter getQueryByParameter() {
        return queryByParameter;
    }

    /**
     * <p>Setter for the field <code>queryByParameter</code>.</p>
     *
     * @param queryByParameter a {@link net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter} object.
     */
    public void setQueryByParameter(PRPAMT201306UV02QueryByParameter queryByParameter) {
        this.queryByParameter = queryByParameter;
    }

    /**
     * <p>Getter for the field <code>resultTotalQuantity</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getResultTotalQuantity() {
        return resultTotalQuantity;
    }

    /**
     * <p>Setter for the field <code>resultTotalQuantity</code>.</p>
     *
     * @param resultTotalQuantity a {@link java.lang.Integer} object.
     */
    public void setResultTotalQuantity(Integer resultTotalQuantity) {
        this.resultTotalQuantity = resultTotalQuantity;
    }

    /**
     * <p>buildContinuationPointer.</p>
     *
     * @param queryId a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
     *
     * @return a {@link java.lang.String} object.
     */
    public static String buildContinuationPointer(II queryId) {
        StringBuilder pointerBuilder = new StringBuilder();
        if (queryId.getExtension() != null) {
            pointerBuilder.append(queryId.getExtension());
        }
        if (queryId.getRoot() != null) {
            pointerBuilder.append('^');
            pointerBuilder.append(queryId.getRoot());
        }
        return pointerBuilder.toString();
    }

}
