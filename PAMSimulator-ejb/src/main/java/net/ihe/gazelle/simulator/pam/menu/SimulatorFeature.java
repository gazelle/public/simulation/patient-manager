package net.ihe.gazelle.simulator.pam.menu;

import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

/**
 * Created by aberge on 17/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Entity
@Name("simulatorFeature")
@Table(name = "pam_simulator_feature", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "pam_simulator_feature_sequence", sequenceName = "pam_simulator_feature_id_seq", allocationSize = 1)
public class SimulatorFeature implements Serializable {

    @Id
    @GeneratedValue(generator = "pam_simulator_feature_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "keyword")
    @Enumerated(EnumType.STRING)
    private PatientManagerAuthorizations keyword;

    @Column(name = "label")
    private String label;

    @Column(name = "enabled")
    private boolean enabled;

    /**
     * <p>Constructor for SimulatorFeature.</p>
     */
    public SimulatorFeature(){
        this.enabled = true;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>keyword</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations} object.
     */
    public PatientManagerAuthorizations getKeyword() {
        return keyword;
    }

    /**
     * <p>Setter for the field <code>keyword</code>.</p>
     *
     * @param keyword a {@link net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations} object.
     */
    public void setKeyword(PatientManagerAuthorizations keyword) {
        this.keyword = keyword;
    }

    /**
     * <p>Getter for the field <code>label</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return label;
    }

    /**
     * <p>Setter for the field <code>label</code>.</p>
     *
     * @param label a {@link java.lang.String} object.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * <p>isEnabled.</p>
     *
     * @return a boolean.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * <p>Setter for the field <code>enabled</code>.</p>
     *
     * @param enabled a boolean.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SimulatorFeature)) {
            return false;
        }

        SimulatorFeature that = (SimulatorFeature) o;

        if (enabled != that.enabled) {
            return false;
        }
        if (keyword != that.keyword) {
            return false;
        }
        return label != null ? label.equals(that.label) : that.label == null;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = keyword != null ? keyword.hashCode() : 0;
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        return result;
    }
}
