package net.ihe.gazelle.simulator.pam.dao;

import net.ihe.gazelle.fhir.constants.FhirConstants;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifierQuery;
import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.Reference;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>PatientIdentifierDAO class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("patientIdentifierDAO")
@Scope(ScopeType.STATELESS)
public class PatientIdentifierDAO {

    /**
     * <p>getIdentifierByFullId.</p>
     *
     * @param fullPatientId a {@link java.lang.String} object.
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     */
    public static PatientIdentifier getIdentifierByFullId(String fullPatientId) {
        PatientIdentifierQuery query = new PatientIdentifierQuery();
        query.fullPatientId().eq(fullPatientId);
        return query.getUniqueResult();
    }

    /**
     * <p>createListFromNationalAndDDSIdentifiers.</p>
     *
     * @param npiString   a {@link java.lang.String} object.
     * @param ddsIdString a {@link java.lang.String} object.
     *
     * @return a {@link java.util.List} object.
     */
    public static List<PatientIdentifier> createListFromNationalAndDDSIdentifiers(String npiString, String ddsIdString) {
        List<PatientIdentifier> identifiers = new ArrayList<PatientIdentifier>();
        if (npiString != null) {
            identifiers.add(new PatientIdentifier(npiString, "NH"));
        }
        if (ddsIdString != null) {
            identifiers.add(new PatientIdentifier(ddsIdString, "PI"));
        }
        if (identifiers.isEmpty()) {
            return null;
        } else {
            return identifiers;
        }
    }

    /**
     * <p>getPatientIdentifiers.</p>
     *
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     *
     * @return a {@link java.util.List} object.
     */
    public static List<PatientIdentifier> getPatientIdentifiers(Patient patient) {
        if ((patient != null) && (patient.getId() != null)) {
            PatientIdentifierQuery query;
            List<PatientIdentifier> identifiers = new ArrayList<PatientIdentifier>();
            for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
                query = new PatientIdentifierQuery();
                query.id().eq(pid.getId());
                identifiers.add(query.getUniqueResult());
            }
            return identifiers;
        } else {
            return null;
        }
    }

    /**
     * <p>getPatientIdentifiers.</p>
     * Returns the identifiers which are from the given domain
     * @param patient a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     * @param domain  a {@link net.ihe.gazelle.simulator.pam.model.HierarchicDesignator} object.
     *
     * @return a {@link java.util.List} object.
     */
    public static List<PatientIdentifier> getPatientIdentifiers(Patient patient, HierarchicDesignator domain) {
        if ((patient != null) && (patient.getId() != null)) {
            PatientIdentifierQuery query;
            List<PatientIdentifier> identifiers = new ArrayList<PatientIdentifier>();
            for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
                if (domain.equals(pid.getDomain())) {
                    query = new PatientIdentifierQuery();
                    query.id().eq(pid.getId());
                    identifiers.add(query.getUniqueResult());
                }
            }
            return identifiers;
        } else {
            return null;
        }
    }

    /**
     * <p>savePatientIdentifierList.</p>
     *
     * @param pidList a {@link java.util.List} object.
     *
     * @return a {@link java.util.List} object.
     */
    public static List<PatientIdentifier> savePatientIdentifierList(List<PatientIdentifier> pidList) {
        if ((pidList != null) && !pidList.isEmpty()) {
            List<PatientIdentifier> returnedList = new ArrayList<PatientIdentifier>();
            for (PatientIdentifier pid : pidList) {
                if (pid != null) {
                    pid = pid.save();
                    returnedList.add(pid);
                }
            }
            return returnedList;
        } else {
            return null;
        }
    }

    /**
     * <p>splitCXField.</p>
     *
     * @param fullPatientId a {@link java.lang.String} object.
     *
     * @return a {@link java.util.Map} object.
     */
    public static Map<String, String> splitCXField(String fullPatientId) {
        String[] subfields = fullPatientId.split("\\^");
        if (subfields.length > 0) {
            Map<String, String> result = new HashMap<String, String>();
            result.put("CX-1", subfields[0]);
            if (subfields.length > 3) {
                String parts[] = subfields[3].split("&");
                if (parts.length > 0) {
                    result.put("CX-4-1", parts[0]);
                }
                if (parts.length > 1) {
                    result.put("CX-4-2", parts[1]);
                }
                if (parts.length > 2) {
                    result.put("CX-4-3", parts[2]);
                }
            }
            if (subfields.length > 4) {
                result.put("CX-5", subfields[4]);
            }
            return result;
        } else {
            return null;
        }
    }

    public static String urnToOid(String system) {
        if (system == null || system.isEmpty()){
            return null;
        } else {
            return StringUtils.removeStart(system, "urn:oid:");
        }
    }

    public static Parameters toParametersList(Parameters parameters, List<PatientIdentifier> identifierList) {
        for (PatientIdentifier pid: identifierList){
           parameters.addParameter().setName(FhirConstants.PARAM_TARGET_IDENTIFIER).setValue(asFhirIdentifier(pid));
        }
        return parameters;
    }

    private static Identifier asFhirIdentifier(PatientIdentifier pid) {
        Identifier identifier = new Identifier();
        identifier.setSystem(pid.getDomain().getUniversalIDAsUrn());
        identifier.setValue(pid.getIdNumber());
        identifier.setUse(Identifier.IdentifierUse.OFFICIAL);
        final String namespaceID = pid.getDomain().getNamespaceID();
        if (namespaceID != null && !namespaceID.isEmpty()){
            Reference namespace = new Reference();
            namespace.setDisplay(namespaceID);
            identifier.setAssigner(namespace);
        }
        return identifier;
    }

    public static List<PatientIdentifier> getPatientIdentifiersForRestrictedDomains(Patient patient, List<HierarchicDesignator> domains) {
        List<PatientIdentifier> identifiers = new ArrayList<PatientIdentifier>();
        if (domains == null || domains.isEmpty()){
            identifiers = getPatientIdentifiers(patient);
        } else {
            for (HierarchicDesignator domain : domains) {
                List<PatientIdentifier> identifiersForDomain = getPatientIdentifiers(patient, domain);
                if (identifiersForDomain != null) {
                    identifiers.addAll(identifiersForDomain);
                }
            }
        }
        return identifiers;
    }
}
