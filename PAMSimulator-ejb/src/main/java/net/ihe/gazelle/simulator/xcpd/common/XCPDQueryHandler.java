package net.ihe.gazelle.simulator.xcpd.common;

import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.responder.*;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import net.ihe.gazelle.simulator.xcpd.model.DeferredTransaction;
import net.ihe.gazelle.simulator.xcpd.model.DeferredTransactionQuery;
import net.ihe.gazelle.simulator.xcpd.model.DeferredTransactionStatus;
import net.ihe.gazelle.xcpd.plq.PatientLocationQueryRequestType;
import net.ihe.gazelle.xcpd.plq.PatientLocationQueryResponseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import javax.xml.ws.handler.MessageContext;
import java.io.ByteArrayOutputStream;

/**
 * <b>Class Description : </b>XCPDQueryHandler<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 13/01/16
 */
public class XCPDQueryHandler extends QueryHandler {

    private static Logger log = LoggerFactory.getLogger(XCPDQueryHandler.class);

    /**
     * <p>Constructor for XCPDQueryHandler.</p>
     *
     * @param inDomain         a {@link Domain} object.
     * @param inSimulatedActor a {@link Actor} object.
     * @param inSutActor       a {@link Actor} object.
     * @param inTransaction    a {@link Transaction} object.
     * @param servletRequest   a {@link String} object.
     * @param messageContext   a {@link MessageContext} object.
     */
    public XCPDQueryHandler(Domain inDomain, Actor inSimulatedActor, Actor inSutActor, Transaction inTransaction,
                            HttpServletRequest servletRequest, MessageContext messageContext) {
        super(inDomain, inSimulatedActor, inSutActor, inTransaction, servletRequest, messageContext);
    }


    /**
     * <p>Constructor for XCPDQueryHandler.</p>
     *
     * @param inDomain         a {@link Domain} object.
     * @param inSimulatedActor a {@link Actor} object.
     * @param inSutActor       a {@link Actor} object.
     * @param inTransaction    a {@link Transaction} object.
     * @param servletRequest   a {@link String} object.
     */
    public XCPDQueryHandler(Domain inDomain, Actor inSimulatedActor, Actor inSutActor, Transaction inTransaction,
                            HttpServletRequest servletRequest) {
        super(inDomain, inSimulatedActor, inSutActor, inTransaction, servletRequest);
    }

    public XCPDQueryHandler(Domain inDomain, Actor inSimulatedActor, Actor inSutActor, Transaction inTransaction,
                            String issuer, String issuerIP) {
        super(inDomain, inSimulatedActor, inSutActor, inTransaction, issuer, issuerIP);
    }

    /**
     * <p>handleDeferredCrossGatewayPatientDiscoveryQuery.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     */
    public MCCIIN000002UV01Type handleDeferredCrossGatewayPatientDiscoveryQuery(PRPAIN201305UV02Type request) {
        if (request == null) {
            return null;
        } else {
            try {
                updateTransactionInstance(request, "MCCI_IN000002UV01", false);
                String url = getInitiatingGatewayUrlFromRequest(request);
                String sutDeviceOid = null;
                Object device = null;
                if (request.getSender() != null && request.getSender().getDevice() != null
                        && !request.getSender().getDevice().getId().isEmpty()) {
                    sutDeviceOid = request.getSender().getDevice().getId().get(0).getRoot();
                }
                if (request.getReceiver() != null && !request.getReceiver().isEmpty()) {
                    device = request.getReceiver().get(0);
                }
                MCCIIN000002UV01Type response = MCCIIN000002UV01Builder.buildAcceptAcknowledgement(sutDeviceOid, request.getId(), device, "AA");
                sendResponse(MCCIIN000002UV01Type.class, response);
                DeferredTransaction deferredTransaction = new DeferredTransaction(Actor.findActorWithKeyword("RESP_GW"),
                        instance, url);
                if (request.getId() != null && url != null) {
                    deferredTransaction.setMessageIdExtension(request.getId().getExtension());
                    deferredTransaction.setMessageIdRoot(request.getId().getRoot());
                    deferredTransaction.setStatus(DeferredTransactionStatus.WAITING);
                } else if (url == null) {
                    deferredTransaction.setStatus(DeferredTransactionStatus.DISCARDED);
                    deferredTransaction.setDiscardedRreason("The URL of the Initiating Gateway endpoint is not provided in the request");
                } else if (request.getId() != null) {
                    deferredTransaction.setStatus(DeferredTransactionStatus.DISCARDED);
                    deferredTransaction.setDiscardedRreason("The initial request does not contain an id element");
                }
                deferredTransaction.save();
                HL7V3Utils.saveMessageId(request.getId(), instance.getRequest());
                HL7V3Utils.saveMessageId(response.getId(), instance.getResponse());
                return response;
            } catch (JAXBException e) {
                log.error(e.getMessage(), e);
                return null;
            }
        }
    }


    /**
     * <p>handleCrossGatewayPatientDiscoveryQuery.</p>
     *
     * @param request                 a {@link net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type} object.
     * @param saveTransactionInstance a boolean.
     * @param domain                  a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     */
    public PRPAIN201306UV02Type handleCrossGatewayPatientDiscoveryQuery(PRPAIN201305UV02Type request, boolean saveTransactionInstance,
                                                                        Domain domain) {
        FindCandidateQueryParser parser = new FindCandidateQueryParser();
        IHEQueryParserCode code = parser.parsePRPAIN201305UV02(request, PatientManagerConstants.PDS, domain.getKeyword());
        String deviceOid;
        try {
            deviceOid = request.getSender().getDevice().getId().get(0).getRoot();
        } catch (NullPointerException e) {
            log.warn("sender/device/id not provided in received message");
            deviceOid = null;
        }
        XCPDRespGwResponseBuilder responseBuilder = new XCPDRespGwResponseBuilder(instance.getDomain().getKeyword());
        PRPAIN201306UV02Type response = responseBuilder.buildFindCandidatesQueryResponse(request.getId(),
                parser.getFoundPatients(), parser.getQueryByParameter(), deviceOid, parser.isWildcardUsed());
        if (saveTransactionInstance) {
            try {
                updateTransactionInstance(request, "PRPA_IN201306UV02", false);
            } catch (JAXBException e) {
                log.error(e.getMessage(), e);
            }
            response = sendResponse(PRPAIN201306UV02Type.class, response);
            HL7V3Utils.saveMessageId(request.getId(), instance.getRequest());
            HL7V3Utils.saveMessageId(response.getId(), instance.getResponse());
            return response;
        } else {
            return response;
        }
    }

    /**
     * <p>handlePatientLocationQuery.</p>
     *
     * @param request a {@link net.ihe.gazelle.xcpd.plq.PatientLocationQueryRequestType} object.
     * @return a {@link net.ihe.gazelle.xcpd.plq.PatientLocationQueryResponseType} object.
     */
    public PatientLocationQueryResponseType handlePatientLocationQuery(PatientLocationQueryRequestType request) {
        PatientLocationQueryResponseType response = new PatientLocationQueryResponseType();
        return sendResponse(PatientLocationQueryResponseType.class, response);
    }

    private String getInitiatingGatewayUrlFromRequest(PRPAIN201305UV02Type request) {
        if (!request.getRespondTo().isEmpty()
                && request.getRespondTo().get(0).getTelecom() != null) {
            return request.getRespondTo().get(0).getTelecom().getValue();
        } else {
            log.error("The URL of the Initiating Gateway endpoint is not provided in the request");
            return null;
        }
    }

    /**
     * <p>handleCrossGatewayPatientDiscoveryDeferredResponse.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     */
    public MCCIIN000002UV01Type handleCrossGatewayPatientDiscoveryDeferredResponse(PRPAIN201306UV02Type request) {
        DeferredTransaction deferredTransaction = getTransactionForDeferredMessage(request);
        try {
            updateTransactionInstance(request, "MCCI_IN000002UV01");
        } catch (JAXBException e) {
            log.error(e.getMessage(), e);
        }
        String sutDeviceOid = null;
        Object device = null;
        if (request.getSender() != null && request.getSender().getDevice() != null
                && !request.getSender().getDevice().getId().isEmpty()) {
            sutDeviceOid = request.getSender().getDevice().getId().get(0).getRoot();
        }
        if (request.getReceiver() != null && !request.getReceiver().isEmpty()) {
            device = request.getReceiver().get(0);
        }
        MCCIIN000002UV01Type response = MCCIIN000002UV01Builder.buildAcceptAcknowledgement(sutDeviceOid, request.getId(), device, "AA");
        sendResponse(MCCIIN000002UV01Type.class, response);
        if (deferredTransaction != null) {
            deferredTransaction.setStatus(DeferredTransactionStatus.DONE);
            deferredTransaction.setResponse(instance);
            deferredTransaction.save();
        }
        HL7V3Utils.saveMessageId(request.getId(), instance.getRequest());
        HL7V3Utils.saveMessageId(response.getId(), instance.getResponse());
        return response;
    }

    private DeferredTransaction getTransactionForDeferredMessage(PRPAIN201306UV02Type request) {
        if (!request.getAcknowledgement().isEmpty()) {
            if (request.getAcknowledgement().get(0).getTargetMessage() != null
                    && request.getAcknowledgement().get(0).getTargetMessage().getId() != null) {
                II id = request.getAcknowledgement().get(0).getTargetMessage().getId();
                DeferredTransactionQuery query = new DeferredTransactionQuery();
                query.messageIdExtension().eq(id.getExtension());
                query.messageIdRoot().eq(id.getRoot());
                query.simulatedActor().keyword().eq(PatientManagerConstants.INIT_GW);
                query.status().eq(DeferredTransactionStatus.WAITING);
                return query.getUniqueResult();
            } else {
                log.warn("Acknowledgement does not contain a valid TargetMessage element");
                return null;
            }
        } else {
            log.warn("Received message does not contain required Acknowledgement element");
            return null;
        }
    }

    private void updateTransactionInstance(PRPAIN201305UV02Type request, String responseType, boolean deferredOption) throws JAXBException {
        ByteArrayOutputStream requestStream = new ByteArrayOutputStream();
        HL7V3Transformer.marshallMessage(PRPAIN201305UV02Type.class, requestStream, request);
        instance.getRequest().setContent(requestStream.toByteArray());
        if (deferredOption) {
            instance.getRequest().setType("PRPA_IN201305UV02 (deferred)");
        } else {
            instance.getRequest().setType("PRPA_IN201305UV02");
        }
        instance.getResponse().setType(responseType);
    }

    private void updateTransactionInstance(PRPAIN201306UV02Type request, String responseType) throws JAXBException {
        ByteArrayOutputStream requestStream = new ByteArrayOutputStream();
        HL7V3Transformer.marshallMessage(PRPAIN201306UV02Type.class, requestStream, request);
        instance.getRequest().setContent(requestStream.toByteArray());
        instance.getRequest().setType("PRPA_IN201306UV02");
        instance.getResponse().setType(responseType);
    }
}
