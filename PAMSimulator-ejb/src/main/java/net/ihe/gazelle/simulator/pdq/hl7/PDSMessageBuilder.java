package net.ihe.gazelle.simulator.pdq.hl7;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.v25.message.ACK;
import ca.uhn.hl7v2.model.v25.message.QCN_J01;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message.QBP_Q21;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message.RSP_K21;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.message.RSP_ZV2;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.DSC;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.ERR;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.MSA;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.MSH;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.PV1;
import net.ihe.gazelle.iti.pdqpds.hl7v2.model.v25.segment.QAK;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientPhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * <p>PDSMessageBuilder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PDSMessageBuilder extends SegmentBuilder {

	private static final AcknowledgmentCode APPLICATION_ACCEPT_CODE = AcknowledgmentCode.AA;
	private static final AcknowledgmentCode APPLICATION_ERROR_CODE = AcknowledgmentCode.AE;
	private static Logger LOG = LoggerFactory.getLogger(PDSMessageBuilder.class);

	private String pointer;
	private AcknowledgmentCode ack;
	private List<Patient> patients;
	private List<Movement> movements;

	/**
	 * for error in QPD-8: which domain to return are unknown
	 */
	private List<Integer> fieldRepetition;

	/**
	 * <p>Constructor for PDSMessageBuilder.</p>
	 *
	 * @param pointer a {@link java.lang.String} object.
	 * @param ackCode a {@link java.lang.String} object.
	 */
	public PDSMessageBuilder(String pointer, AcknowledgmentCode ackCode) {
		this.pointer = pointer;
		this.ack = ackCode;
		this.fieldRepetition = null;
		this.patients = null;
		this.movements = null;
	}

	/**
	 * <p>build.</p>
	 *
	 * @param incomingMessage a {@link ca.uhn.hl7v2.model.Message} object.
	 * @param trigger a {@link java.lang.String} object.
	 * @param domainsToReturn a {@link java.util.List} object.
	 * @return a {@link ca.uhn.hl7v2.model.Message} object.
	 */
	public Message build(Message incomingMessage, String trigger, List<HierarchicDesignator> domainsToReturn) {
		if (trigger == null) {
			return null;
		} else if (trigger.equals("Q22")) {
			return builResponseToQ22((QBP_Q21) incomingMessage, domainsToReturn);
		} else if (trigger.equals("ZV1")) {
			return buildResponseToZV1((QBP_Q21) incomingMessage, domainsToReturn);
		} else if (trigger.equals("J01")) {
			return builResponseToJ01((QCN_J01) incomingMessage);
		} else {
			return null;
		}
	}


	private ACK builResponseToJ01(QCN_J01 incomingMessage) {
		ACK response = new ACK();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		try {
			fillMSHSegment(response.getMSH(), incomingMessage.getMSH().getSendingApplication().getNamespaceID()
					.getValue(), incomingMessage.getMSH().getSendingFacility().getNamespaceID().getValue(),
					incomingMessage.getMSH().getReceivingApplication().getNamespaceID().getValue(), incomingMessage
							.getMSH().getReceivingFacility().getNamespaceID().getValue(), "ACK", "J01", "ACK", "2.5",
					sdf, incomingMessage.getMSH().getCharacterSet(0).getValue());
			response.getMSA().getAcknowledgmentCode().setValue(ack.name());
			response.getMSA().getMessageControlID().setValue(incomingMessage.getMSH().getMessageControlID().getValue());
			if (!AcknowledgmentCode.AA.equals(ack)) {
				response.getERR().getErrorLocation(0).getSegmentID().setValue("QID");
				response.getERR().getErrorLocation(0).getFieldPosition().setValue("1");
				response.getERR().getErrorLocation(0).getSegmentSequence().setValue("1");
				response.getERR().getHL7ErrorCode().getIdentifier().setValue("204");
				response.getERR().getHL7ErrorCode().getText().setValue("Unknown Key Identifier");
				response.getERR().getSeverity().setValue("E");
				response.getERR().getUserMessage().setValue("Unknown query tag");
			}
		} catch (HL7Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
		return response;
	}

	private RSP_K21 builResponseToQ22(QBP_Q21 incomingMessage, List<HierarchicDesignator> domainsToReturn) {
		RSP_K21 response = new RSP_K21();
		try {
			fillMSHSegment(response.getMSH(), incomingMessage.getMSH(), "K22");
			fillMSASegment(response.getMSA(), incomingMessage.getMSH());
			if ((fieldRepetition != null) && !fieldRepetition.isEmpty()) {
				int errRep = 0;
				for (Integer qpdRep : fieldRepetition) {
					fillERRSegment(response.getERR(errRep), qpdRep);
					errRep++;
				}
			} else if (patients != null) {
				int patientRep = 0;
				EntityManager entityManager = EntityManagerService.provideEntityManager();
				for (Patient patient : patients) {
					patient = entityManager.find(Patient.class, patient.getId());
					fillPIDSegment(response.getQUERY_RESPONSE(patientRep).getPID(), patient, patientRep + 1,
							domainsToReturn);
					patientRep++;
				}
			} else if (APPLICATION_ERROR_CODE.equals(ack)) {
				fillERRSegment(response.getERR(), null);
			}
			// other segments
			fillQAKSegment(response.getQAK(), incomingMessage.getQPD());
			response.getQPD().parse(incomingMessage.getQPD().encode());
			// pointer if needed
			if (APPLICATION_ACCEPT_CODE.equals(ack) && (pointer != null)) {
				fillDSCSegment(response.getDSC());
			}
		} catch (HL7Exception e) {
			LOG.error(e.getMessage(), e);
			LOG.error(e.getMessage());
			return null;
		}
		return response;
	}

	private RSP_ZV2 buildResponseToZV1(QBP_Q21 incomingMessage, List<HierarchicDesignator> domainsToReturn) {
		RSP_ZV2 response = new RSP_ZV2();
		try {
			fillMSHSegment(response.getMSH(), incomingMessage.getMSH(), "ZV2");
			fillMSASegment(response.getMSA(), incomingMessage.getMSH());
			if ((fieldRepetition != null) && !fieldRepetition.isEmpty()) {
				int errRep = 0;
				for (Integer qpdRep : fieldRepetition) {
					fillERRSegment(response.getERR(errRep), qpdRep);
					errRep++;
				}
			} else if (movements != null) {
				int patientRep = 0;
				EntityManager entityManager = EntityManagerService.provideEntityManager();
				for (Movement movement : movements) {
					movement = entityManager.find(Movement.class, movement.getId());
					fillPIDSegment(response.getQUERY_RESPONSE().getPATIENT(patientRep).getPID(), movement
							.getEncounter().getPatient(), patientRep + 1, domainsToReturn);
					fillPV1Segment(response.getQUERY_RESPONSE().getPATIENT(patientRep).getVISIT().getPV1(), movement,
							entityManager);
					patientRep++;
				}
			} else if (APPLICATION_ERROR_CODE.equals(ack)) {
				fillERRSegment(response.getERR(), null);
			}

			// other segments
			fillQAKSegment(response.getQAK(), incomingMessage.getQPD());
			response.getQPD().parse(incomingMessage.getQPD().encode());
			// pointer if needed
			if (APPLICATION_ACCEPT_CODE.equals(ack) && (pointer != null)) {
				fillDSCSegment(response.getDSC());
			}
		} catch (HL7Exception e) {
			LOG.error(e.getMessage(), e);
			LOG.error(e.getMessage());
			return null;
		}
		return response;
	}

	private void fillMSHSegment(MSH mshSegment, MSH incomingMSH, String triggerEvent) throws DataTypeException,
			HL7Exception {
		String messageStructure = null;
		if (triggerEvent.equals("ZV2")) {
			messageStructure = "RSP_ZV2";
		} else {
			messageStructure = "RSP_K21";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		mshSegment.getFieldSeparator().setValue("|");
		mshSegment.getEncodingCharacters().setValue("^~\\&");
		mshSegment.getSendingApplication().getNamespaceID()
				.setValue(incomingMSH.getReceivingApplication().getNamespaceID().getValue());
		mshSegment.getSendingFacility().getNamespaceID()
				.setValue(incomingMSH.getReceivingFacility().getNamespaceID().getValue());
		mshSegment.getReceivingApplication().getNamespaceID()
				.setValue(incomingMSH.getSendingApplication().getNamespaceID().getValue());
		mshSegment.getReceivingFacility().getNamespaceID()
				.setValue(incomingMSH.getSendingFacility().getNamespaceID().getValue());
		mshSegment.getDateTimeOfMessage().getTime().setValue(sdf.format(Calendar.getInstance().getTime()));
		mshSegment.getMessageType().getMessageCode().setValue("RSP");
		mshSegment.getMessageType().getTriggerEvent().setValue(triggerEvent);
		mshSegment.getMessageType().getMessageStructure().setValue(messageStructure);
		mshSegment.getProcessingID().getProcessingID().setValue("P");
		mshSegment.getMessageControlID().setValue(sdf.format(Calendar.getInstance().getTime()));
		mshSegment.getVersionID().getVersionID().setValue("2.5");
		mshSegment.getCharacterSet(0).setValue(incomingMSH.getCharacterSet(0).getValue());
	}

	private void fillMSASegment(MSA msaSegment, Segment incomingMSH) throws HL7Exception {
		msaSegment.getAcknowledgmentCode().setValue(ack.name());
		msaSegment.getMessageControlID().setValue(Terser.get(incomingMSH, 10, 0, 1, 1));
	}

	private void fillERRSegment(ERR errSegment, Integer fieldRepetition) throws DataTypeException {
		if (fieldRepetition != null) {
			errSegment.getErrorLocation(0).getSegmentID().setValue("QPD");
			errSegment.getErrorLocation(0).getSegmentSequence().setValue("1");
			errSegment.getErrorLocation(0).getFieldPosition().setValue("8");
			errSegment.getErrorLocation(0).getFieldRepetition().setValue(fieldRepetition.toString());
			errSegment.getUserMessage().setValue("Unknown domain");
		} else // unknown continuation pointer
		{
			errSegment.getErrorLocation(0).getSegmentID().setValue("DSC");
			errSegment.getErrorLocation(0).getSegmentSequence().setValue("1");
			errSegment.getErrorLocation(0).getFieldPosition().setValue("1");
			errSegment.getErrorLocation(0).getFieldRepetition().setValue("0");
			errSegment.getUserMessage().setValue("Unknown continuation pointer");
		}
		errSegment.getHL7ErrorCode().getIdentifier().setValue("204");
		errSegment.getHL7ErrorCode().getText().setValue("Unknown key identifier");
		errSegment.getHL7ErrorCode().getNameOfCodingSystem().setValue("HL7");
		errSegment.getSeverity().setValue("E");
	}

	private void fillQAKSegment(QAK qakSegment, Segment incomingQPD) throws HL7Exception {
		if (APPLICATION_ACCEPT_CODE.equals(ack) && ((patients != null) || (movements != null))) {
			qakSegment.getQueryResponseStatus().setValue("OK");
		} else if (APPLICATION_ACCEPT_CODE.equals(ack)) {
			qakSegment.getQueryResponseStatus().setValue("NF"); // PAM-73
		} else {
			qakSegment.getQueryResponseStatus().setValue(ack.name());
		}
		qakSegment.getQueryTag().setValue(Terser.get(incomingQPD, 2, 0, 1, 1));
	}

	private void fillPIDSegment(Segment pid, Patient patient, Integer patientRep,
			List<HierarchicDesignator> domainsToReturn) throws HL7Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		// set ID
		Terser.set(pid, 1, 0, 1, 1, patientRep.toString());
		// Patient identifier list
		int pid3Rep = 0;
		for (PatientIdentifier identifier : patient.getPatientIdentifiers()) {
			if (domainsToReturn.isEmpty() || domainsToReturn.contains(identifier.getDomain())) {
				Map<String, String> pidFields = identifier.splitPatientId();
				if (pidFields.containsKey("CX-1")) {
					Terser.set(pid, 3, pid3Rep, 1, 1, pidFields.get("CX-1"));
				}
				if (pidFields.containsKey("CX-4-1")) {
					Terser.set(pid, 3, pid3Rep, 4, 1, pidFields.get("CX-4-1"));
				}
				if (pidFields.containsKey("CX-4-2")) {
					Terser.set(pid, 3, pid3Rep, 4, 2, pidFields.get("CX-4-2"));
				}
				if (pidFields.containsKey("CX-4-3")) {
					Terser.set(pid, 3, pid3Rep, 4, 3, pidFields.get("CX-4-3"));
				}
				Terser.set(pid, 3, pid3Rep, 5, 1, identifier.getIdentifierTypeCode());
				pid3Rep++;
			} else {
				LOG.info("identifier " + identifier.getFullPatientId() + " will not be sent");
			}
		}
		// Patient name
		Terser.set(pid, 5, 0, 1, 1, patient.getLastName());
		Terser.set(pid, 5, 0, 7, 1, "L");
		Terser.set(pid, 5, 0, 2, 1, patient.getFirstName());
		Terser.set(pid, 6, 0, 1, 1, patient.getMotherMaidenName());
		Terser.set(pid, 6, 0, 7, 1, "L");
		// Date/Time of birth
		if (patient.getDateOfBirth() != null) {
			Terser.set(pid, 7, 0, 1, 1, sdf.format(patient.getDateOfBirth()));
		}
		// Administrative sex
		Terser.set(pid, 8, 0, 1, 1, patient.getGenderCode());
		// Patient address
		Terser.set(pid, 11, 0, 1, 1, patient.getAddressLine());
		Terser.set(pid, 11, 0, 3, 1, patient.getCity());
		Terser.set(pid, 11, 0, 4, 1, patient.getState());
		Terser.set(pid, 11, 0, 5, 1, patient.getZipCode());
		Terser.set(pid, 11, 0, 6, 1, patient.getCountryCode());
		// patient account number
		if (patient.getAccountNumber() != null) {
			pid.getField(18, 0).parse(patient.getAccountNumber());
		}
		// tel/email
		int pid13Rep = 0;
		if (patient.getEmail() != null && !patient.getEmail().isEmpty()){
			Terser.set(pid, 13, pid13Rep, 4, 1, patient.getEmail());
			Terser.set(pid, 13, pid13Rep, 2, 1, "NET");
			Terser.set(pid, 13, pid13Rep, 3, 1, "Internet");
			pid13Rep++;
		}
		PatientPhoneNumber principal = patient.getPrincipalPhoneNumber();
		if (principal != null){
			Terser.set(pid, 13, pid13Rep, 9, 1, principal.getValue());
			Terser.set(pid, 13, pid13Rep, 2, 1, principal.getType().getHl7v2Code());
			Terser.set(pid, 13, pid13Rep, 3, 1, "PH");
		}
		if (patient.getMultipleBirthIndicator() != null){
			String indicator = (patient.getMultipleBirthIndicator()) ? "Y":"N";
			Terser.set(pid, 24, 0, 1, 1, indicator);
			if (patient.getMultipleBirthIndicator() && patient.getBirthOrder() != null){
				Terser.set(pid, 25, 0, 1, 1, patient.getBirthOrder().toString());
			}
		}
	}

	private void fillPV1Segment(PV1 pv1, Movement movement, EntityManager entityManager) throws HL7Exception {
		pv1.getPatientClass().setValue(movement.getEncounter().getPatientClassCode());
		pv1.getAssignedPatientLocation().parse(movement.getAssignedPatientLocation());
		if (movement.getEncounter().getAdmittingDoctorCode() != null) {
			pv1.getAttendingDoctor(0).parse(movement.getEncounter().getAttendingDoctorCode());
		}
		if (movement.getEncounter().getReferringDoctorCode() != null) {
			pv1.getReferringDoctor(0).parse(movement.getEncounter().getReferringDoctorCode());
		}
		pv1.getHospitalService().setValue(movement.getEncounter().getHospitalServiceCode());
		if (movement.getEncounter().getAdmittingDoctorCode() != null) {
			pv1.getAdmittingDoctor(0).parse(movement.getEncounter().getAdmittingDoctorCode());
		}
		pv1.getVisitNumber().parse(movement.getEncounter().getVisitNumber());
	}

	private void fillDSCSegment(DSC dsc) throws DataTypeException {
		dsc.getContinuationPointer().setValue(pointer);
	}

	/**
	 * <p>Setter for the field <code>patients</code>.</p>
	 *
	 * @param patients a {@link java.util.List} object.
	 */
	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}

	/**
	 * <p>Setter for the field <code>fieldRepetition</code>.</p>
	 *
	 * @param fieldRepetition a {@link java.util.List} object.
	 */
	public void setFieldRepetition(List<Integer> fieldRepetition) {
		this.fieldRepetition = fieldRepetition;
	}

	/**
	 * <p>Setter for the field <code>movements</code>.</p>
	 *
	 * @param movements a {@link java.util.List} object.
	 */
	public void setMovements(List<Movement> movements) {
		this.movements = movements;
	}

}
