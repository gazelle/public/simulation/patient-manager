package net.ihe.gazelle.simulator.application;

import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

/**
 * Application Bean for TransactionInstance search.
 */
@Name("transactionInstanceFinder")
public class TransactionInstanceFinder {

    @In(create = true, value = "transactionInstanceDAO")
    private TransactionInstanceDAO transactionInstanceDAO;

    /**
     * Empty Constructor for injection
     */
    public TransactionInstanceFinder() {
        //Empty
    }

    /**
     * Getter for the transactionInstanceDAO property.
     *
     * @return the value of the property.
     */
    public TransactionInstanceDAO getTransactionInstanceDAO() {
        return transactionInstanceDAO;
    }

    /**
     * Setter for the transactionInstanceDAO property.
     *
     * @param transactionInstanceDAO value to set to the property.
     */
    public void setTransactionInstanceDAO(TransactionInstanceDAO transactionInstanceDAO) {
        this.transactionInstanceDAO = transactionInstanceDAO;
    }

    /**
     * Get a Transaction Instance by its identifier.
     *
     * @param id identifier of the transaction instance to retrieve.
     * @return the retrieved {@link TransactionInstance}
     */
    public TransactionInstance retreiveRecordById(String id) {
        return transactionInstanceDAO.retrieveTransactionInstanceById(id);
    }
}
