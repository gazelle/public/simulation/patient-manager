package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.datatypes.CD;
import net.ihe.gazelle.hl7v3.datatypes.CE;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01ActOrderRequired;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01DetectedIssueEvent;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01Requires;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01Reason;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01Subject1;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01Subject2;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter;
import net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient;
import net.ihe.gazelle.hl7v3.voc.ActClass;
import net.ihe.gazelle.hl7v3.voc.ActClassRoot;
import net.ihe.gazelle.hl7v3.voc.ActMood;
import net.ihe.gazelle.hl7v3.voc.ActRelationshipReason;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3AcknowledgmentCode;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ResponseCode;
import net.ihe.gazelle.simulator.hl7v3.messages.MCCIMT00300UV01PartBuilder;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;

import java.util.List;

/**
 * <b>Class Description : </b>XCPDRespGwResponseBuilder<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 20/01/16
 */
public class XCPDRespGwResponseBuilder extends PRPAIN201306UV02Builder {

    private boolean wildcardUsed;

    /**
     * <p>Constructor for XCPDRespGwResponseBuilder.</p>
     *
     * @param inDomainKeyword a {@link java.lang.String} object.
     */
    public XCPDRespGwResponseBuilder(String inDomainKeyword) {
        super(XCPD_TRANSACTION, inDomainKeyword);
    }

    /**
     * <p>buildFindCandidatesQueryResponse.</p>
     *
     * @param messageId        a {@link net.ihe.gazelle.hl7v3.datatypes.II} object.
     * @param patients         a {@link java.util.List} object.
     * @param queryByParameter a {@link net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02QueryByParameter} object.
     * @param sutDeviceOid     a {@link java.lang.String} object.
     * @param wildcardUsed     a boolean.
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type} object.
     */
    public PRPAIN201306UV02Type buildFindCandidatesQueryResponse(II messageId, List<Patient> patients,
                                                                 PRPAMT201306UV02QueryByParameter queryByParameter,
                                                                 String sutDeviceOid, boolean wildcardUsed) {
        this.wildcardUsed = wildcardUsed;
        return super.buildFindCandidatesQueryResponse(messageId, patients, null, null, queryByParameter, sutDeviceOid);
    }

    /**
     * {@inheritDoc}
     */
    protected void buildControlActWrapper(II messageId, List<Patient> patients, List<Integer> unknownDomains,
                                          List<HierarchicDesignator> domainsToReturn,
                                          PRPAMT201306UV02QueryByParameter queryByParameter,
                                          PRPAIN201306UV02Type response,
                                          PRPAIN201306UV02MFMIMT700711UV01ControlActProcess controlActProcess) {
        if (queryByParameter == null) {
            response.addAcknowledgement(MCCIMT00300UV01PartBuilder.populateAcknowledgement(messageId, HL7V3AcknowledgmentCode.AR.getValue()));
            controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(null, null, HL7V3ResponseCode.QE.getValue(), "aborted",
                    null));
        } else if (patients != null) {
            // AA (application accept) is returned in Acknowledgement.typeCode (transmission wrapper)
            response.addAcknowledgement(MCCIMT00300UV01PartBuilder.populateAcknowledgement(messageId, HL7V3AcknowledgmentCode.AA.getValue()));
            // OK (data found, no errors) is returned in QueryAck.queryResponseCode (control act wrapper)
            controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(
                    queryByParameter.getQueryId().getRoot(), queryByParameter.getQueryId().getExtension(), HL7V3ResponseCode.OK.getValue(),
                    "deliveredResponse", null));
            // If too many results are found and wild card was used, return DetectedIssueEvent
            if (patients.size() > 3 && wildcardUsed) {
                MFMIMT700711UV01Reason reasonOf = new MFMIMT700711UV01Reason();
                reasonOf.setTypeCode(ActRelationshipReason.RSON);
                reasonOf.setDetectedIssueEvent(buildDetectedIssueForRequestedParams(queryByParameter));
                controlActProcess.addReasonOf(reasonOf);
            } else {
                // One Registration Event is returned from the patient information source for each patient record found
                for (Patient currentPatient : patients) {
                    PRPAIN201306UV02MFMIMT700711UV01Subject1 subject = populateSubject();
                    PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent event = populateRegistrationEvent();
                    event.setCustodian(MFMIMT700711UV01PartBuilder.populateCustodian());
                    PRPAIN201306UV02MFMIMT700711UV01Subject2 subject1 = populateSubject1();
                    PRPAMT201310UV02Patient msgPatient = populatePatient(currentPatient, null);
                    subject1.setPatient(msgPatient);
                    event.setSubject1(subject1);
                    subject.setRegistrationEvent(event);
                    controlActProcess.addSubject(subject);
                }
            }
        } else {
            // AA (application accept) is returned in Acknowledgement.typeCode (transmission wrapper)
            response.addAcknowledgement(MCCIMT00300UV01PartBuilder.populateAcknowledgement(messageId, HL7V3AcknowledgmentCode.AA.getValue()));
            // OK (data found, no errors) is returned in QueryAck.queryResponseCode (control act wrapper)
            controlActProcess.setQueryAck(MFMIMT700711UV01PartBuilder.populateQueryAck(
                    queryByParameter.getQueryId().getRoot(), queryByParameter.getQueryId().getExtension(), HL7V3ResponseCode.NF.getValue(),
                    "deliveredResponse", null));
        }
        response.setSender(MCCIMT00300UV01PartBuilder.buildSender(
                PreferenceService.getString("hl7v3_xcpd_respgw_device_id"), PreferenceService.getString("hl7v3_xcpd_respgw_url")));
    }

    private MCAIMT900001UV01DetectedIssueEvent buildDetectedIssueForRequestedParams(PRPAMT201306UV02QueryByParameter queryByParameter) {
        MCAIMT900001UV01DetectedIssueEvent event = new MCAIMT900001UV01DetectedIssueEvent();
        event.setClassCode(ActClass.ALRT);
        event.setMoodCode(ActMood.EVN);
        event.setCode(new CD("ActAdministrativeDetectedIssueCode", null, "2.16.840.1.113883.5.4"));
        if (queryByParameter.getParameterList().getLivingSubjectAdministrativeGender().isEmpty()) {
            event.addTriggerFor(buildTrigger(XCPDRequestedParam.GENDER));
        }
        if (queryByParameter.getParameterList().getMothersMaidenName().isEmpty()) {
            event.addTriggerFor(buildTrigger(XCPDRequestedParam.MOTHERS_MAIDEN_NAME));
        }
        return event;
    }

    private MCAIMT900001UV01Requires buildTrigger(XCPDRequestedParam param) {
        MCAIMT900001UV01Requires trigger = new MCAIMT900001UV01Requires();
        trigger.setTypeCode("TRIG");
        MCAIMT900001UV01ActOrderRequired act = new MCAIMT900001UV01ActOrderRequired();
        act.setClassCode(ActClassRoot.ACT);
        act.setMoodCode(ActMood.RQO);
        act.setCode(new CE(param.getValue(), null, "1.3.6.1.4.1.19376.1.2.27.1"));
        trigger.setActOrderRequired(act);
        return trigger;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void buildPatientId(Patient currentPatient, List<HierarchicDesignator> domainsToReturn, PRPAMT201310UV02Patient msgPatient) {
        List<PatientIdentifier> identifiers = PatientIdentifierDAO.getPatientIdentifiers(currentPatient);
        boolean isEPDMode = SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.EPD);
        if (isEPDMode) {
            buildPatientIdForEPR(currentPatient, msgPatient);
        } else if (identifiers != null && !identifiers.isEmpty()) {
            PatientIdentifier refId = identifiers.get(0);
            if (refId.getDomain() != null) {
                addPatientIdElement(msgPatient, refId);
            }
        }
    }

    private void buildPatientIdForEPR(Patient currentPatient, PRPAMT201310UV02Patient msgPatient) {
        // 1.10.3.1 The MPI-PID MUST be returned, if there is a match from the EPR-SPID.
        boolean mpipidInserted = false;
        for (PatientIdentifier pid : currentPatient.getPatientIdentifiers()) {
            if (pid.getDomain() != null && !PIXV3QueryHandler.SPID_ROOT.equals(pid.getDomain().getUniversalID())) {
                if (mpipidInserted) {
                    // All other patient identifiers shall be specified in the OtherIDs.id attribute
                    msgPatient.getPatientPerson().addAsOtherIDs(populateAsOtherIDs(pid));
                } else {
                    addPatientIdElement(msgPatient, pid);
                    mpipidInserted = true;
                }
            }
        }
    }

    private void addPatientIdElement(PRPAMT201310UV02Patient msgPatient, PatientIdentifier pid) {
        msgPatient.addId(new II(pid.getDomain().getUniversalID(), pid.getIdNumber()));
    }
}
