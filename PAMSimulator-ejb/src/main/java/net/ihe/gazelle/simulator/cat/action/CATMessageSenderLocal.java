package net.ihe.gazelle.simulator.cat.action;

import net.ihe.gazelle.simulator.cat.model.SharingResult;
import net.ihe.gazelle.simulator.cat.model.SystemUnderTestPreferences;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.model.Patient;

import javax.ejb.Asynchronous;
import javax.ejb.Local;
import java.util.List;

@Local
public interface CATMessageSenderLocal {

	@Asynchronous
	void sendMessages(SharingResult sharingResult);

	void setBp6Mode(boolean bp6Mode);

}
