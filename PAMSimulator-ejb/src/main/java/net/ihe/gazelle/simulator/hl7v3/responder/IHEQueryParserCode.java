package net.ihe.gazelle.simulator.hl7v3.responder;

/**
 * <p>EFindCandidateQueryParserCode class.</p>
 *
 * @author aberge
 * @version 1.0: 06/11/17
 */

public enum IHEQueryParserCode {
    NO_PATIENT_FOUND,
    UNKNOWN_DOMAINS,
    FORBIDDEN_FIELD,
    NO_QUERY_PARAMETER,
    RETURN_ALL_PATIENTS,
    RETURN_SUBSET_OF_PATIENTS,
    UNKNOWN_SOURCE_ASSIGNING_AUTHORITY,
    UNKNOWN_SOURCE_PATIENT_ID
}
