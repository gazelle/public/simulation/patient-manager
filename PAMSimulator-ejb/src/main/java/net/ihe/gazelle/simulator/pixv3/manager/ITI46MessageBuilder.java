package net.ihe.gazelle.simulator.pixv3.manager;

import net.ihe.gazelle.hl7v3.datatypes.CD;
import net.ihe.gazelle.hl7v3.datatypes.CS;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.TS;
import net.ihe.gazelle.hl7v3.prpain201302UV02.*;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientId;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientPatientPerson;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientStatusCode;
import net.ihe.gazelle.hl7v3.voc.*;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.hl7v3.initiator.MCCIMT000100UV01PartBuilder;
import net.ihe.gazelle.simulator.hl7v3.initiator.MFMIMT700701UV01PartBuilder;
import net.ihe.gazelle.simulator.hl7v3.initiator.RevisePatientMessageBuilder;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3Constants;
import net.ihe.gazelle.simulator.hl7v3.messages.MessageIdGenerator;
import net.ihe.gazelle.simulator.hl7v3.responder.COCTMT150003UV03PartBuilder;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeature;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by gthomazon on 24/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ITI46MessageBuilder extends RevisePatientMessageBuilder {

	/**
	 * <p>Constructor for ITI46MessageBuilder.</p>
	 *
	 * @param inSUT a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
	 * @param active a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
	 */
	public ITI46MessageBuilder(HL7v3ResponderSUTConfiguration inSUT, Patient active) {
		super(inSUT, active);
	}

	/** {@inheritDoc} */
	@Override
	protected PRPAMT201302UV02PatientPatientPerson populatePatientPersonForRevisePatientRecord() {
		PRPAMT201302UV02PatientPatientPerson person = new PRPAMT201302UV02PatientPatientPerson();
		person.setClassCode(EntityClass.PSN);
		person.setDeterminerCode(EntityDeterminer.INSTANCE);
		if (!activePatient.getFirstName().isEmpty() || !activePatient.getLastName().isEmpty()) {
			person.addName(buildPersonName(activePatient, null));
		}
		if (activePatient.getAlternateFirstName() != null || activePatient.getAlternateLastName() != null) {
			person.addName(buildAlternatePersonName(activePatient));
		}
		return person;
	}

	/** {@inheritDoc} */
	@Override
	protected void addPatientIdToList(PRPAMT201302UV02Patient patient, PatientIdentifier pid) {
		PRPAMT201302UV02PatientId id = new PRPAMT201302UV02PatientId(pid.getDomain().getUniversalID(),
				pid.getIdNumber());
		id.setAssigningAuthorityName(pid.getDomain().getNamespaceID());
		patient.addId(id);
		II ii = new II(pid.getDomain().getUniversalID(), null);
		if (!patient.getProviderOrganization().getId().contains(ii)) {
			patient.getProviderOrganization().addId(ii);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected String getActorKeyword() {
		return "PAT_IDENTITY_X_REF_MGR";
	}

	/** {@inheritDoc} */
	@Override
	protected String getOIDPreferenceName() {
		return "hl7v3_pix_mgr_device_id";
	}

}
