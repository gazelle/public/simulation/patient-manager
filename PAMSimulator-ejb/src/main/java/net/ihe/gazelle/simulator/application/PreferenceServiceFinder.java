package net.ihe.gazelle.simulator.application;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

/**
 * Application Bean for preference search.
 */
@Name("preferenceServiceFinder")
public class PreferenceServiceFinder {

    @In(create = true, value = "preferenceServiceDAO")
    private PreferenceServiceDAO preferenceServiceDAO;

    /**
     * Empty Constructor for injection
     */
    public PreferenceServiceFinder() {
        //Empty
    }

    /**
     * Getter for the preferenceServiceDAO property.
     *
     * @return the value of the property.
     */
    public PreferenceServiceDAO getPreferenceServiceDAO() {
        return preferenceServiceDAO;
    }

    /**
     * Setter for the preferenceServiceDAO property.
     *
     * @param preferenceServiceDAO value to set to the property.
     */
    public void setPreferenceServiceDAO(PreferenceServiceDAO preferenceServiceDAO) {
        this.preferenceServiceDAO = preferenceServiceDAO;
    }

    /**
     * Get the preference value
     *
     * @param preferenceName name of the preference.
     * @return retrieve the preference value
     */
    public String retrievePreferenceValue (String preferenceName) {
        return preferenceServiceDAO.retrievePreferenceServiceValue(preferenceName);
    }

}

