package net.ihe.gazelle.simulator.cat.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.cat.dao.SystemUnderTestPreferencesDAO;
import net.ihe.gazelle.simulator.cat.model.SystemUnderTestPreferences;
import net.ihe.gazelle.simulator.cat.model.SystemUnderTestPreferencesQuery;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerPages;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.util.Map;

/**
 * <p>SystemUnderTestPreferencesManager class.</p>
 *
 * @author aberge
 * @version 1.0: 29/11/17
 */

@Name("sutPreferencesManager")
@Scope(ScopeType.PAGE)
public class SystemUnderTestPreferencesManager implements QueryModifier<SystemUnderTestPreferences>, UserAttributeCommon {

    private Filter<SystemUnderTestPreferences> filter;

    GazelleIdentity gazelleIdentity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");

    SystemUnderTestPreferences selectedPreferences;

    @In(value="gumUserService")
    private UserService userService;

    public SystemUnderTestPreferences getSelectedPreferences() {
        return selectedPreferences;
    }

    public void setSelectedPreferences(SystemUnderTestPreferences selectedPreferences) {
        this.selectedPreferences = selectedPreferences;
    }

    public Filter<SystemUnderTestPreferences> getFilter() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (filter == null){
            filter = new Filter<SystemUnderTestPreferences>(getCriteriaForFilter(), urlParams);
        }
        return filter;
    }

    private HQLCriterionsForFilter<SystemUnderTestPreferences> getCriteriaForFilter(){
        SystemUnderTestPreferencesQuery query = new SystemUnderTestPreferencesQuery();
        query.systemUnderTest().name().order(true);
        HQLCriterionsForFilter<SystemUnderTestPreferences> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("sut", query.systemUnderTest());
        criteria.addPath("available", query.systemUnderTest().isAvailable(), true, true);

        if (gazelleIdentity.isLoggedIn()) {
            criteria.addPath("owner", query.systemUnderTest().owner());
            criteria.addPath("company", query.systemUnderTest().ownerCompany());
        }
        criteria.addQueryModifier(this);
        return criteria;
    }

    public FilterDataModel<SystemUnderTestPreferences> getSutPreferences(){
        return new FilterDataModel<SystemUnderTestPreferences>(getFilter()) {
            @Override
            protected Object getId(SystemUnderTestPreferences systemUnderTestPreferences) {
                return systemUnderTestPreferences.getId();
            }
        };
    }

    public void reset(){
        getFilter().clear();
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<SystemUnderTestPreferences> queryBuilder, Map<String, Object> filterValuesApplied) {
        SystemUnderTestPreferencesQuery query = new SystemUnderTestPreferencesQuery();
        if (gazelleIdentity.isLoggedIn()){
            if (! (gazelleIdentity.hasRole("admin_role") || gazelleIdentity.hasRole("monitor_role"))){
                String username = gazelleIdentity.getUsername();
                String company = gazelleIdentity.getOrganisationKeyword();
                queryBuilder.addRestriction(HQLRestrictions.or(
                        query.systemUnderTest().owner().eqRestriction(username),
                        query.systemUnderTest().ownerCompany().eqRestriction(company),
                        query.systemUnderTest().isPublic().eqRestriction(true)
                ));
            }
        } else {
            queryBuilder.addRestriction(query.systemUnderTest().isPublic().eqRestriction(true));
        }
    }

    /**
     * Called in patientColumns.xhtml
     */
    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    public String editPreferences(SystemUnderTestPreferences preferences){
        StringBuilder url = new StringBuilder("/PatientManager");
        url.append(goToCreationPage());
        url.append('?');
        url.append(SystemUnderTestPreferencesEditor.SUT_KEY_NAME);
        url.append('=');
        url.append(preferences.getId());
        return url.toString();
    }

    public void removePreferences(){
        if (selectedPreferences != null){
            SystemUnderTestPreferencesDAO.deletePreferencesForSUT(selectedPreferences.getSystemUnderTest().getId());

        }
    }

    public String goToCreationPage(){
        return PatientManagerPages.CAT_EDIT_SUT_PREF.getLink();
    }

    public boolean isConnectathonFeatureActivated(){
        return SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.CONNECTATHON);
    }
}
