package net.ihe.gazelle.simulator.hl7v3.initiator;

import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.TEL;
import net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Agent;
import net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Device;
import net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Organization;
import net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Receiver;
import net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Sender;
import net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Agent;
import net.ihe.gazelle.hl7v3.mccimt000300UV01.MCCIMT000300UV01Organization;
import net.ihe.gazelle.hl7v3.voc.*;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7v3MessageBuilder;
import net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration;

/**
 * Created by aberge on 17/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class MCCIMT000100UV01PartBuilder extends HL7v3MessageBuilder {

	/**
	 * build <receiver>
	 *
	 * @param sut a {@link net.ihe.gazelle.simulator.hl7v3.sut.model.HL7v3ResponderSUTConfiguration} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Receiver} object.
	 */
	public static MCCIMT000100UV01Receiver buildReceiver(HL7v3ResponderSUTConfiguration sut) {
		MCCIMT000100UV01Receiver receiver = new MCCIMT000100UV01Receiver();
		receiver.setTypeCode(CommunicationFunctionType.RCV);
		receiver.setDevice(buildDevice(sut.getDeviceOID(), sut.getUrl(), null));
		return receiver;
	}

	/**
	 * build <sender>
	 *
	 * @param senderOid a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Sender} object.
	 */
	public static MCCIMT000100UV01Sender buildSender(String senderOid) {
		MCCIMT000100UV01Sender sender = new MCCIMT000100UV01Sender();
		sender.setTypeCode(CommunicationFunctionType.SND);
		sender.setDevice(buildDevice(senderOid, null, null));
		return sender;
	}

    /**
     * <p>buildSender.</p>
     *
     * @param senderOid a {@link java.lang.String} object.
     * @param homeCommunityId a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Sender} object.
     */
    public static MCCIMT000100UV01Sender buildSender(String senderOid, String homeCommunityId) {
		MCCIMT000100UV01Sender sender = new MCCIMT000100UV01Sender();
		sender.setTypeCode(CommunicationFunctionType.SND);
		sender.setDevice(buildDevice(senderOid, null, homeCommunityId));
		return sender;
	}

	/**
	 * build <device>
	 *
	 * @param deviceOID a {@link java.lang.String} object.
	 * @param url a {@link java.lang.String} object.
	 * @param homeCommunityId a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Device} object.
	 */
	public static MCCIMT000100UV01Device buildDevice(String deviceOID, String url, String homeCommunityId) {
		MCCIMT000100UV01Device device = new MCCIMT000100UV01Device();
		device.setClassCode(EntityClassDevice.DEV);
		device.setDeterminerCode(EntityDeterminer.INSTANCE);
		device.addId(new II(deviceOID, null));
		if (url != null && !url.isEmpty()) {
			TEL telecom = new TEL();
			telecom.setValue(url);
			device.addTelecom(telecom);
		}
        if (homeCommunityId != null && !homeCommunityId.isEmpty()){
            MCCIMT000100UV01Agent agent = new MCCIMT000100UV01Agent();
            MCCIMT000100UV01Organization organization = new MCCIMT000100UV01Organization();
            organization.addId(new II(homeCommunityId, null));
            organization.setClassCode(EntityClassOrganization.ORG);
            organization.setDeterminerCode(EntityDeterminer.INSTANCE);
            agent.setRepresentedOrganization(organization);
            agent.setClassCode(RoleClassAgent.AGNT);
            device.setAsAgent(agent);
        }
		return device;
	}
}
