package net.ihe.gazelle.simulator.pam.iti31.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <b>Class Description : </b>DRGMovement<br>
 * <br>
 * This class owns the attributes to be used in the ZFM segment described in ITI Vol4 section4 (french extension)
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 22/10/15
 */
@Entity
@Name("drgMovement")
@Table(name = "pam_fr_drg_movement", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pam_fr_drg_movement_sequence", sequenceName = "pam_fr_drg_movement_id_seq", allocationSize = 1)
public class DRGMovement implements Serializable{

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(generator = "pam_fr_drg_movement_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    private Integer id;

    @Column(name = "pmsi_admission_mode")
    private String pmsiAdmissionMode;

    @Column(name = "pmsi_discharge_mode")
    private String pmsiDischargeMode;

    @Column(name = "pmsi_establishment_of_origin_mode")
    private String pmsiEstablishmentOfOriginMode;

    @Column(name = "pmsi_destination_mode")
    private String pmsiDestinationMode;

    @OneToOne(targetEntity = Encounter.class, mappedBy = "drgMovement")
    private Encounter encounter;

    /**
     * <p>Getter for the field <code>encounter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public Encounter getEncounter() {
        return encounter;
    }

    /**
     * <p>Setter for the field <code>encounter</code>.</p>
     *
     * @param encounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public void setEncounter(Encounter encounter) {
        this.encounter = encounter;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>pmsiAdmissionMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPmsiAdmissionMode() {
        return pmsiAdmissionMode;
    }

    /**
     * <p>Setter for the field <code>pmsiAdmissionMode</code>.</p>
     *
     * @param pmsiAdmissionMode a {@link java.lang.String} object.
     */
    public void setPmsiAdmissionMode(String pmsiAdmissionMode) {
        this.pmsiAdmissionMode = pmsiAdmissionMode;
    }

    /**
     * <p>Getter for the field <code>pmsiDischargeMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPmsiDischargeMode() {
        return pmsiDischargeMode;
    }

    /**
     * <p>Setter for the field <code>pmsiDischargeMode</code>.</p>
     *
     * @param pmsiDischargeMode a {@link java.lang.String} object.
     */
    public void setPmsiDischargeMode(String pmsiDischargeMode) {
        this.pmsiDischargeMode = pmsiDischargeMode;
    }

    /**
     * <p>Getter for the field <code>pmsiEstablishmentOfOriginMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPmsiEstablishmentOfOriginMode() {
        return pmsiEstablishmentOfOriginMode;
    }

    /**
     * <p>Setter for the field <code>pmsiEstablishmentOfOriginMode</code>.</p>
     *
     * @param pmsiEstablishmentOfOriginMode a {@link java.lang.String} object.
     */
    public void setPmsiEstablishmentOfOriginMode(String pmsiEstablishmentOfOriginMode) {
        this.pmsiEstablishmentOfOriginMode = pmsiEstablishmentOfOriginMode;
    }

    /**
     * <p>Getter for the field <code>pmsiDestinationMode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPmsiDestinationMode() {
        return pmsiDestinationMode;
    }

    /**
     * <p>Setter for the field <code>pmsiDestinationMode</code>.</p>
     *
     * @param pmsiDestinationMode a {@link java.lang.String} object.
     */
    public void setPmsiDestinationMode(String pmsiDestinationMode) {
        this.pmsiDestinationMode = pmsiDestinationMode;
    }
}
