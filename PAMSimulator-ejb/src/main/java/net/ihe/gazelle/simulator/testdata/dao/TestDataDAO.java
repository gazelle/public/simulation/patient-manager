package net.ihe.gazelle.simulator.testdata.dao;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.Person;
import net.ihe.gazelle.simulator.testdata.model.PatientIdentifierTestData;
import net.ihe.gazelle.simulator.testdata.model.PatientTestData;
import net.ihe.gazelle.simulator.testdata.model.TestData;
import net.ihe.gazelle.simulator.testdata.model.TestDataQuery;

import javax.persistence.EntityManager;
import java.util.Iterator;
import java.util.List;

/**
 * Created by aberge on 09/03/17.
 */
public class TestDataDAO {

    public static void saveEntity(TestData testData){
        prepareEntityBeforeSave(testData);
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        testData.updateModifierAndDate();
        entityManager.merge(testData);
        entityManager.flush();
    }

    public static List<TestData> getAvailableTestData(Transaction appliesTo, String username) {
        TestDataQuery query = new TestDataQuery();
        query.name().order(true);
        if (appliesTo != null) {
            query.applyToTransaction().id().eq(appliesTo.getId());
        }
        if (username == null){
            query.shared().eq(true);
        } else {
            query.addRestriction(HQLRestrictions.or(query.shared().eqRestriction(true),
                    query.lastModifier().eqRestriction(username)));
        }
        return query.getList();
    }

    public static void deleteEntity(TestData selectedTestData) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        TestData toDelete = entityManager.find(selectedTestData.getClass(), selectedTestData.getId());
        entityManager.remove(toDelete);
    }

    private static void prepareEntityBeforeSave(TestData testData){
        if (testData instanceof PatientTestData){
            Patient patient = ((PatientTestData) testData).getPatient();
            // first set PatientAddress to null if empty
            if (patient.getAddressList().isEmpty()) {
                patient.setAddressList(null);
            }
            // the same for the PhoneNumber
            if (patient.getPhoneNumbers().isEmpty()){
                patient.setPhoneNumbers(null);
            }
            // and for the identifiers
            if (patient.getPatientIdentifiers() == null || patient.getPatientIdentifiers().isEmpty()){
                patient.setPatientIdentifiers(null);
            }
            // for the relationship
            if (patient.getPersonalRelationships().isEmpty()){
                patient.setPersonalRelationships(null);
            } else {
                Iterator<Person> relations = patient.getPersonalRelationships().iterator();
                while (relations.hasNext()){
                    Person person = relations.next();
                    if (!person.isDefined()){
                        patient.removePersonalRelationship(person);
                    } else {
                        person.setPatient(patient);
                    }
                }
            }
        } else if (testData instanceof PatientIdentifierTestData){
            ((PatientIdentifierTestData) testData).getPatientIdentifier().setFullPatientIdentifierIfEmpty();
        }
    }

    public static boolean entryExistsWithName(String name) {
        TestDataQuery query = new TestDataQuery();
        query.name().eq(name);
        return query.getCount() > 0;
    }
}
