package net.ihe.gazelle.simulator.pam.iti31.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <b>Class Description : </b>PatientsPHRInfo<br>
 * <br>
 * This class owns the attributes used to populate the ZFA segment as described in ITI VOl4 section 4 (French extension)
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 22/10/15
 */

@Entity
@Name("patientsPHRInfo")
@Table(name = "pam_fr_patients_phr_info", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pam_fr_patients_phr_info_sequence", sequenceName = "pam_fr_patients_phr_info_id_seq", allocationSize = 1)
public class PatientsPHRInfo implements Serializable{

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @NotNull
    @GeneratedValue(generator = "pam_fr_patients_phr_info_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "status")
    private String status;

    @Column(name = "status_collection_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statusCollectionDate;

    @Column(name = "closing_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closingDate;

    @Column(name = "facility_access")
    private Boolean facilityAccess;

    @Column(name = "facility_access_collection_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date facilityAccessCollectionDate;

    @Column(name = "bris_de_glace")
    private Boolean brisDeGlace;

    @Column(name = "opposition_collection_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date oppositionCollectionDate;

    @Column(name="centre_15")
    private Boolean centre15;

    @OneToOne(targetEntity = Encounter.class, mappedBy = "patientsPHRInfo")
    private Encounter encounter;

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>status</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getStatus() {
        return status;
    }

    /**
     * <p>Setter for the field <code>status</code>.</p>
     *
     * @param status a {@link java.lang.String} object.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * <p>Getter for the field <code>statusCollectionDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getStatusCollectionDate() {
        return statusCollectionDate;
    }

    /**
     * <p>Setter for the field <code>statusCollectionDate</code>.</p>
     *
     * @param statusCollectionDate a {@link java.util.Date} object.
     */
    public void setStatusCollectionDate(Date statusCollectionDate) {
        if (statusCollectionDate != null) {
            this.statusCollectionDate = (Date) statusCollectionDate.clone();
        } else {
            this.statusCollectionDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>closingDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getClosingDate() {
        return closingDate;
    }

    /**
     * <p>Setter for the field <code>closingDate</code>.</p>
     *
     * @param closingDate a {@link java.util.Date} object.
     */
    public void setClosingDate(Date closingDate) {
        if (closingDate != null) {
            this.closingDate = (Date) closingDate.clone();
        } else {
            this.closingDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>facilityAccess</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getFacilityAccess() {
        return facilityAccess;
    }

    /**
     * <p>Setter for the field <code>facilityAccess</code>.</p>
     *
     * @param facilityAccess a {@link java.lang.Boolean} object.
     */
    public void setFacilityAccess(Boolean facilityAccess) {
        this.facilityAccess = facilityAccess;
    }

    /**
     * <p>Getter for the field <code>facilityAccessCollectionDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getFacilityAccessCollectionDate() {
        return facilityAccessCollectionDate;
    }

    /**
     * <p>Setter for the field <code>facilityAccessCollectionDate</code>.</p>
     *
     * @param facilityAccessCollectionDate a {@link java.util.Date} object.
     */
    public void setFacilityAccessCollectionDate(Date facilityAccessCollectionDate) {
        if (facilityAccessCollectionDate != null) {
            this.facilityAccessCollectionDate = (Date) facilityAccessCollectionDate.clone();
        } else {
            this.facilityAccessCollectionDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>brisDeGlace</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getBrisDeGlace() {
        return brisDeGlace;
    }

    /**
     * <p>Setter for the field <code>brisDeGlace</code>.</p>
     *
     * @param brisDeGlace a {@link java.lang.Boolean} object.
     */
    public void setBrisDeGlace(Boolean brisDeGlace) {
        this.brisDeGlace = brisDeGlace;
    }

    /**
     * <p>Getter for the field <code>oppositionCollectionDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getOppositionCollectionDate() {
        return oppositionCollectionDate;
    }

    /**
     * <p>Setter for the field <code>oppositionCollectionDate</code>.</p>
     *
     * @param oppositionCollectionDate a {@link java.util.Date} object.
     */
    public void setOppositionCollectionDate(Date oppositionCollectionDate) {
        if (oppositionCollectionDate != null) {
            this.oppositionCollectionDate = (Date) oppositionCollectionDate.clone();
        } else {
            this.oppositionCollectionDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>centre15</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getCentre15() {
        return centre15;
    }

    /**
     * <p>Setter for the field <code>centre15</code>.</p>
     *
     * @param centre15 a {@link java.lang.Boolean} object.
     */
    public void setCentre15(Boolean centre15) {
        this.centre15 = centre15;
    }

    /**
     * <p>Getter for the field <code>encounter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public Encounter getEncounter() {
        return encounter;
    }

    /**
     * <p>Setter for the field <code>encounter</code>.</p>
     *
     * @param encounter a {@link net.ihe.gazelle.simulator.pam.iti31.model.Encounter} object.
     */
    public void setEncounter(Encounter encounter) {
        this.encounter = encounter;
    }
}
