package net.ihe.gazelle.simulator.pam.pds;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import java.io.Serializable;

@Stateful
/**
 * <p>ITI30Manager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("iti30Manager")
@Scope(ScopeType.SESSION)
@GenerateInterface("ITI30ManagerLocal")
public class ITI30Manager implements Serializable, ITI30ManagerLocal {

    /**
     *
     */
    private static final Logger LOG = LoggerFactory.getLogger(ITI30Manager.class);

    private static final long serialVersionUID = 6844674348612435138L;
    private Actor selectedActor = null;
    private ITI30EVENT selectedEvent = null;
    private Boolean bp6Mode = null;


    /**
     * <p>initialize.</p>
     */
    @Create
    public void initialize() {
    }

    /**
     * <p>isBp6Authorized.</p>
     *
     * @return a boolean.
     */
    public boolean isBp6Authorized() {
        return SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.BP6);
    }

    /**
     * <p>isBp6Mode.</p>
     *
     * @return a boolean.
     */
    public boolean isBp6Mode() {
        return SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.BP6) && (bp6Mode==null || bp6Mode);
    }


    public void setBp6Mode(boolean bp6Mode) {
        this.bp6Mode = bp6Mode;
    }


    @Override
    public String getPage(ITI30EVENT event) {
        selectedEvent = event;
        String url = "/pds/";
        switch (event) {
            case A28:
                url = url + "createPatient";
                break;
            case A24:
                url = url + "linkPatient";
                break;
            case A31:
                url = url + "updatePatient";
                break;
            case A40:
                url = url + "mergePatient";
                break;
            case A47:
                url = url + "changeId";
                break;
            case A47_INS:
                url = url + "removeInsIds";
                break;
            default:
                selectedEvent = ITI30EVENT.A28;
                url = url + "createPatient";
                break;
        }
        return url;
    }


    public void setSelectedEvent(ITI30EVENT selectedEvent) {
        LOG.info("setting selected event to " + selectedEvent);
        this.selectedEvent = selectedEvent;
    }


    @Override
    public String getPage(ITI30EVENT event, String actorKeyword) {
        StringBuilder url = new StringBuilder(getPage(event));
        url.append(".seam?initiator=");
        url.append(actorKeyword);
        return url.toString();
    }


    @Override
    public String getPageForActor(String actorKeyword) {
        if (selectedEvent == null) {
            selectedEvent = ITI30EVENT.A28;
        }
        StringBuilder url = new StringBuilder(getPage(selectedEvent));
        url.append(".seam?initiator=");
        url.append(actorKeyword);
        return url.toString();
    }


    @Override
    public String getStyleClass(ITI30EVENT event) {
        if (event.equals(selectedEvent)) {
            return "gzl-btn gzl-btn-blue";
        } else {
            return "gzl-btn";
        }
    }

    public enum ITI30EVENT {
        A28,
        A31,
        A24,
        A40,
        A47,
        A47_INS
    }


    @Override
    public ITI30EVENT getSelectedEvent() {
        return selectedEvent;
    }


    @Override
    public Actor getSelectedActor() {
        return selectedActor;
    }


    @Override
    @Destroy
    @Remove
    public void destroy() {

    }
}
