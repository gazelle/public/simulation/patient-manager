package net.ihe.gazelle.simulator.pam.utils;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.AbstractMessage;
import ca.uhn.hl7v2.model.AbstractSegment;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.Type;
import ca.uhn.hl7v2.model.v25.datatype.CE;
import ca.uhn.hl7v2.model.v25.datatype.CX;
import ca.uhn.hl7v2.model.v25.datatype.DLD;
import ca.uhn.hl7v2.model.v25.datatype.IS;
import ca.uhn.hl7v2.model.v25.datatype.NA;
import ca.uhn.hl7v2.model.v25.datatype.TS;
import ca.uhn.hl7v2.model.v25.message.ADT_A05;
import ca.uhn.hl7v2.model.v25.message.ADT_A24;
import ca.uhn.hl7v2.model.v25.message.ADT_A30;
import ca.uhn.hl7v2.model.v25.message.ADT_A37;
import ca.uhn.hl7v2.model.v25.message.ADT_A39;
import ca.uhn.hl7v2.model.v25.message.ADT_A43;
import ca.uhn.hl7v2.parser.CustomModelClassFactory;
import ca.uhn.hl7v2.parser.ModelClassFactory;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.exception.DuplicateKeyIdentifierException;
import net.ihe.gazelle.HL7Common.exception.HL7v2ParsingException;
import net.ihe.gazelle.HL7Common.exception.MissingRequiredFieldException;
import net.ihe.gazelle.HL7Common.exception.MissingRequiredSegmentException;
import net.ihe.gazelle.HL7Common.exception.UnexpectedMessageStructureException;
import net.ihe.gazelle.HL7Common.exception.UnknownKeyIdentifierException;
import net.ihe.gazelle.HL7Common.utils.ACKBuilder;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFS;
import net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A01;
import net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A02;
import net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A03;
import net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A06;
import net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A09;
import net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A12;
import net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A21;
import net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A38;
import net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A52;
import net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A54;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.EVN;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.PID;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.PV1;
import net.ihe.gazelle.pam.hl7v2.model.v25.segment.ZBE;
import net.ihe.gazelle.simulator.common.model.ReceiverConsole;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.pam.dao.EncounterDAO;
import net.ihe.gazelle.simulator.pam.dao.LegalCareModeDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.hl7.BP6HL7Domain;
import net.ihe.gazelle.simulator.pam.hl7.HL7MessageDecoder;
import net.ihe.gazelle.simulator.pam.iti31.action.NumberGenerator;
import net.ihe.gazelle.simulator.pam.iti31.model.ActionType;
import net.ihe.gazelle.simulator.pam.iti31.model.DRGMovement;
import net.ihe.gazelle.simulator.pam.iti31.model.Encounter;
import net.ihe.gazelle.simulator.pam.iti31.model.EncounterAdditionalInfo;
import net.ihe.gazelle.simulator.pam.iti31.model.LegalCareMode;
import net.ihe.gazelle.simulator.pam.iti31.model.Movement;
import net.ihe.gazelle.simulator.pam.iti31.model.PatientsPHRInfo;
import net.ihe.gazelle.simulator.pam.iti31.util.TriggerEventEnum;
import net.ihe.gazelle.simulator.pam.model.AdditionalDemographics;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.LunarWeek;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientHistory;
import net.ihe.gazelle.simulator.pam.model.PatientHistory.ActionPerformed;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientLink;
import net.ihe.gazelle.simulator.pam.model.PatientStatus;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.apache.xmlbeans.impl.jam.internal.elements.ParameterImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static net.ihe.gazelle.simulator.pam.hl7.BP6HL7Domain.INS.hasINSAuthority;


/**
 * This class is used to decode the received ADT messages and to perform the right action
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ADTDecoder extends HL7MessageDecoder {

    private static final String MOVEMENT_ID_LOCATION = "ZBE^1^1";
    private static final String PID_3 = "PID-3";
    private static final String NO_ACTIVE_ENCOUNTER_FOUND_MESSAGE_HAS_BEEN_DISCARDED = "No active encounter found : message has been discarded";
    private static final String PV1_7 = "PV1-7";
    private static final String ATTENDING_DOCTOR_LOCATION = "PV1^1^7^1";
    private static final String EXPECTED_LOA_DATE = "PV2-47";
    private static final String PID_3_LOCATION = "PID^1^3";
    private static final String CANNOT_RETRIEVE_MOVEMENT_EFFECTIVE_DATE = "Cannot retrieve movement effective date";
    private static final String PV1_19_LOCATION = "PV1^1^19^1";
    private static final Logger log = LoggerFactory.getLogger(ADTDecoder.class);
    private static final String PAM_HL7_PACKAGE_NAME = "net.ihe.gazelle.pam.hl7v2.model";

    class RequirementException extends HL7Exception {
        public RequirementException(String message) {
            super(message);
        }

        public RequirementException(Throwable cause) {
            super(cause);
        }
    }

    private EntityManager entityManager;
    private Actor simulatedActor;
    private Patient newPatient;
    private Patient deactivatedPatient;
    private Domain domain;
    private Transaction transaction;
    private ACKBuilder ackBuilder;
    private String sendingApplication;
    private String sendingFacility;
    private String creator;
    private String messageControlId;
    private Consumer consumer;

    /**
     * The ADT Decoder is instantiated when the message is received by the HL7 responder
     *
     * @param domain         a {@link Domain} object.
     * @param transaction    a {@link Transaction} object.
     * @param simulatedActor a {@link Actor} object.
     * @param ackBuilder
     * @param entityManager  a {@link EntityManager} object.
     */
    public ADTDecoder(Domain domain, Transaction transaction, Actor simulatedActor,
                      ACKBuilder ackBuilder, EntityManager entityManager, Consumer consumer) {
        this.ackBuilder = ackBuilder;
        this.entityManager = entityManager;
        this.simulatedActor = simulatedActor;
        this.newPatient = null;
        this.deactivatedPatient = null;
        this.domain = domain;
        this.transaction = transaction;
        this.consumer = consumer;
    }

    /**
     * <p>decode.</p>
     *
     * @param incomingMessage a {@link ca.uhn.hl7v2.model.Message} object.
     * @param eventCode       a {@link java.lang.String} object.
     * @return a {@link ca.uhn.hl7v2.model.Message} object.
     */
    public Message decode(Message incomingMessage, String eventCode) {
        getSenderInfo(incomingMessage);
        try {
            if (eventCode.equals(PatientManagerConstants.TRIGGER_CREATE)) {
                processCreateNewPatientEvent(incomingMessage);
            } else if (eventCode.equals("A31")) {
                processUpdatePatientEvent(incomingMessage);
            } else if (eventCode.equals("A47")) {
                processChangeIdentifierEvent(incomingMessage);
            } else if (eventCode.equals("A24")) {
                processLinkPatientEvent(incomingMessage);
            } else if (eventCode.equals("A37")) {
                processUnlinkPatientEvent(incomingMessage);
            } else if (eventCode.equals("A40")) {
                processMergePatientEvent(incomingMessage);
            } else {
                processITI31Event(incomingMessage, eventCode);
            }
        } catch (HL7v2ParsingException e) {
            return ackBuilder.buildNack(incomingMessage, e);
        }
        return ackBuilder.buildAck(incomingMessage);
    }

    private void getSenderInfo(Message incomingMessage) {
        Terser terser = new Terser(incomingMessage);
        try {
            sendingApplication = terser.get("/.MSH-3-1");
            sendingFacility = terser.get("/.MSH-4-1");
            creator = sendingApplication + "_" + sendingFacility;
            messageControlId = terser.get("/.MSH-10");
        } catch (HL7Exception e) {
            log.warn("Cannot extract informations from MSH");
        }
    }

    private void processITI31Event(Message incomingMessage, String eventCode) throws HL7v2ParsingException {
        TriggerEventEnum eventEnum = TriggerEventEnum.getEventForTrigger(eventCode);
        if (eventEnum != null) {
            switch (eventEnum) {
                case A01:
                    processAdmitPatientEvent(incomingMessage);
                    break;
                case A04:
                    processRegisterPatientEvent(incomingMessage);
                    break;
                case A03:
                    processDischargePatientEvent(incomingMessage);
                    break;
                case A08:
                    processA08Event(incomingMessage);
                    break;
                case A11:
                    processCancelNewEncounterEvent(incomingMessage);
                    break;
                case A13:
                    processCancelDischargeEvent(incomingMessage);
                    break;
                case A02:
                    processTransferEvent(incomingMessage);
                    break;
                case A05:
                    processPreAdmitEvent(incomingMessage);
                    break;
                case A06:
                    processChangeToInpatientEvent(incomingMessage);
                    break;
                case A07:
                    processChangeToOutpatientEvent(incomingMessage);
                    break;
                case A12:
                    processCancelTransferEvent(incomingMessage);
                    break;
                case A38:
                    processCancelPreAdmitEvent(incomingMessage);
                    break;
                case A21:
                    processLeaveOfAbsence(incomingMessage);
                    break;
                case A52:
                    processCancelLeaveOfAbsence(incomingMessage);
                    break;
                case A22:
                    processReturnFromLeaveOfAbsence(incomingMessage);
                    break;
                case A53:
                    processCancelReturnFromLeaveOfAbsence(incomingMessage);
                    break;
                case A54:
                    processChangeAttendingDoctor(incomingMessage);
                    break;
                case A55:
                    processCancelChangeAttendingDoctor(incomingMessage);
                    break;
                case A44:
                    processAccountMove(incomingMessage);
                    break;
                case Z99:
                    processUpdateMovement(incomingMessage);
                    break;
                default:
                    log.warn("Event {} is not yet supported by this simulator", eventEnum.getTriggerEvent());
            }
        }
    }

    /**
     * Assert  {% requirementId.html id="PAM-032" coverage=site.data.gazelle-test-bed.patient-manager-coverage %} As a SUT operator, if my system sends an ADT message containing an INS (NIR and/or NIA) and PID-32 is not set to "VALI", I expect the simulator not to store the INS identifiers and to send back an acknowledgement message with MSA-1 = AA if the message is successfully integrated furthermore.
     *
     * @param pid
     * @throws RequirementException
     */
    public void assertINSRequirements(AbstractSegment pid) throws RequirementException {
        if (BP6HL7Domain.isBP6Facility(consumer.getReceivingFacility())) {
            try {
                for (Type f : pid.getField(3)) {
                    CX cx = (CX) f;
                    if (BP6HL7Domain.INS.getAuthorities().contains(cx.getAssigningAuthority().getUniversalID().getValue())) {
                        if (!"VALI".equals(((IS) pid.getField(32, 0)).getValue())) {
                            throw new RequirementException("PAM-32");
                        } else {
                            break;
                        }
                    }
                }
            } catch (HL7Exception e) {
                throw new RequirementException(e);
            }
        }
    }

    public boolean isMessageBp6Compliant(AbstractSegment pid) {
        try {
            assertINSRequirements(pid);
            return true;
        } catch (RequirementException e) {
            return false;
        }
    }

    public List<PatientIdentifier> getPatientIdentifierListWithoutInsIds(List<PatientIdentifier> patientIdentifiers, boolean isMessageBp6Compliant) {
        if (isMessageBp6Compliant) {
            return patientIdentifiers;
        }
        List<PatientIdentifier> patientIdentifierListWithoutInsIds = new ArrayList<>();
        for (PatientIdentifier id : patientIdentifiers) {
            if (!hasINSAuthority(id)) {
                patientIdentifierListWithoutInsIds.add(id);
            }
        }
        return patientIdentifierListWithoutInsIds;
    }

    /**
     * Condition predicate: At least one of the fields PID-18 “Patient Account Number” or PV1-19
     * “Visit Number” shall be valued in the messages of transaction [ITI-31] that use the PV1
     * segment. (ITI TF Vol 2b section 3.30.5.4 rev 15.0)
     *
     * @param pidSegment
     * @param pv1Segment
     * @return the visit number from PV1-19 or PID-18
     * @throws MissingRequiredFieldException if neither PV1-19 nor PID-18 is populated
     */
    private String getVisitNumberForADTMessage(PID pidSegment, PV1 pv1Segment) throws MissingRequiredFieldException {
        try {
            if (!pv1Segment.isEmpty() && !pv1Segment.getVisitNumber().isEmpty()) {
                return pv1Segment.getVisitNumber().encode();
            }
        } catch (HL7Exception e) {
            log.warn(e.getMessage());
        }
        // if visitNumber has not been returned, try to read PID-18
        try {
            if (!pidSegment.isEmpty() && !pidSegment.getPatientAccountNumber().isEmpty()) {
                return pidSegment.getPatientAccountNumber().encode();
            }
        } catch (HL7Exception e) {
            log.warn(e.getMessage());
        }
        throw new MissingRequiredFieldException("visit number", null);
    }

    /**
     * PAM-566: this method is no more specific to BP6, renamed it
     *
     * @param newMovement
     * @param incomingMessage
     */
    private void addWardInformation(Movement newMovement, Message incomingMessage) {
        Terser terser = new Terser(incomingMessage);
        try {
            Segment zbefr = terser.getSegment("/.ZBE");
            newMovement.setNatureOfMovement(zbefr.getField(9, 0).encode());
            newMovement.setNursingCareWard(zbefr.getField(8, 0).encode());
            newMovement.setWardCode(zbefr.getField(7, 0).encode());
        } catch (HL7Exception e) {
            log.debug("ZBE parsing: {}", e.getMessage());
        }
    }

    private void addBp6Attributes(Encounter encounter, Message incomingMessage) {
        Terser terser = new Terser(incomingMessage);
        if (isBp6(terser)) {
            extractPatientsPHRInfoFromZFS(encounter, terser);
            extractEncounterAdditionalInfoFromZFV(encounter, terser);
            extractPMSIInfoFromZFM(encounter, terser);
        }
    }

    private void extractPMSIInfoFromZFM(Encounter encounter, Terser terser) {
        try {
            Segment zfm = terser.getSegment("/.ZFM");
            if (zfm != null && !zfm.isEmpty()) {
                DRGMovement drg = encounter.getDrgMovement();
                drg.setPmsiAdmissionMode(getISOrIDValue(zfm.getField(1, 0)));
                drg.setPmsiDischargeMode(getISOrIDValue(zfm.getField(2, 0)));
                drg.setPmsiEstablishmentOfOriginMode(getISOrIDValue(zfm.getField(3, 0)));
                drg.setPmsiDestinationMode(getISOrIDValue(zfm.getField(4, 0)));
            }
        } catch (HL7Exception e) {
            log.debug("ZFM parsing: {}", e.getMessage());
        }
    }

    private void extractEncounterAdditionalInfoFromZFV(Encounter encounter, Terser terser) {
        try {
            Segment zfv = terser.getSegment("/.ZFV");
            if (zfv != null && !zfv.isEmpty()) {
                EncounterAdditionalInfo add = encounter.getEncounterAdditionalInfo();
                add.setRelatedEncounter(encounter);
                add.setLastVisitDate(getDateFromDLDField(zfv.getField(1, 0)));
                add.setEstablishmentOfOrigin(getISFromDLDField(zfv.getField(1, 0)));
                add.setTransportMode(zfv.getField(2, 0).encode());
                add.setPlacementStartDate(getDateFromTSField(zfv.getField(4, 0)));
                add.setPlacementEndDate(getDateFromTSField(zfv.getField(5, 0)));
                add.setEstablishmentOfOriginAddress(getAddressFromXAD(zfv.getField(6), "ORI"));
                add.setEstablishmentOfDestinationAddress(getAddressFromXAD(zfv.getField(6), "DST"));
                add.setOriginAccountNumber(zfv.getField(7, 0).encode());
                add.setDischargeMode(getISOrIDValue(zfv.getField(9, 0)));
                add.setLegalCareMode(getISOrIDValue(zfv.getField(10, 0)));
                add.setCareDuringTransport(getCodeFromCE(zfv.getField(11, 0)));

            }
        } catch (HL7Exception e) {
            log.debug("ZFV parsing: {}", e.getMessage());
        }
    }

    private void extractPatientsPHRInfoFromZFS(Encounter encounter, Terser terser) {
        try {
            Segment zfa = terser.getSegment("/.ZFA");
            if (zfa != null && !zfa.isEmpty()) {
                PatientsPHRInfo phr = encounter.getPatientsPHRInfo();
                phr.setStatus(zfa.getField(1, 0).encode());
                phr.setStatusCollectionDate(getDateFromTSField(zfa.getField(2, 0)));
                phr.setClosingDate(getDateFromTSField(zfa.getField(3, 0)));
                phr.setFacilityAccess(getBooleanFromIDField(zfa.getField(4, 0)));
                phr.setFacilityAccessCollectionDate(getDateFromTSField(zfa.getField(5, 0)));
                phr.setBrisDeGlace(getBooleanFromIDField(zfa.getField(6, 0)));
                phr.setCentre15(getBooleanFromIDField(zfa.getField(7, 0)));
                phr.setOppositionCollectionDate(getDateFromTSField(zfa.getField(8, 0)));
            }
        } catch (HL7Exception e) {
            log.debug("ZFA parsing: {}", e.getMessage());
        }
    }

    private String getCodeFromCE(Type field) {
        if (field != null) {
            CE ce = new CE(field.getMessage());
            return ce.getIdentifier().getValue();
        } else {
            return null;
        }
    }

    private void addBp6Attributes(Patient patient, Message incomingMessage) {
        Terser terser = new Terser(incomingMessage);
        if (isBp6(terser)) {
            AdditionalDemographics add = patient.getAdditionalDemographics();
            extractAdditionDemographicsFromZFP(terser, add);
            extractAdditionalDemographicsFromZFD(terser, add);
            // ZFS segment is only used in ADT_A05 and it is repeatable, using the structure is faster than using Tersers
            extractLegalCareModeList(patient, incomingMessage);
        }
    }

    private void extractLegalCareModeList(Patient patient, Message incomingMessage) {
        if (incomingMessage instanceof ADT_A05) {
            PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();
            try {
                net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A05 fradtA05 =
                        (net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A05) pipeParser.parseForSpecificPackage(incomingMessage.encode(),
                                "net.ihe.gazelle.pam.fr.hl7v2.model.v25.message");
                if (fradtA05.getZFSReps() > 0) {
                    for (ZFS zfs : fradtA05.getZFSAll()) {
                        createLegalCareModeFromZFS(patient, zfs);
                    }
                }
            } catch (HL7Exception e) {
                log.debug("Cannot extract ZFS repetitions: {}", e.getMessage());
            }
        }
    }

    private void createLegalCareModeFromZFS(Patient patient, ZFS zfs) throws HL7Exception {
        LegalCareMode mode = null;
        // if patient is stored in DB, first check if this legalcaremode exist for this patient
        if (patient.getId() != null) {
            String legalCareModeIdentifier = zfs.getLegalModeOfCareIdentifier().encode();
            mode = LegalCareModeDAO.getCareModeForPatientByIdentifier(legalCareModeIdentifier, patient);
        }
        if (mode == null) {
            mode = patient.addLegalCareMode();
            mode.setIdentifier(zfs.getLegalModeOfCareIdentifier().encode());
            String setIdAsString = zfs.getSetIDZFS().getValue();
            try {
                mode.setSetId(Integer.parseInt(setIdAsString));
            } catch (NumberFormatException e) {
                log.debug("SetID is not an integer: {}", setIdAsString);
            }
        }
        mode.setMode(zfs.getLegalModeOfCare().getIdentifier().getValue());
        mode.setCareStartDateTime(zfs.getLegalModeOfCareStartDateAndHour().getTime().getValueAsDate());
        mode.setCareEndDateTime(zfs.getLegalModeOfCareEndDateAndHour().getTime().getValueAsDate());
        mode.setAction(ActionType.valueOf(zfs.getLegalModeOfCareAction().getValue()));
        mode.setRimpCode(zfs.getLegalModeOfCareRIMPCode().getIdentifier().getValue());
        mode.setComment(zfs.getComment().getValueOrEmpty());
    }

    private void extractAdditionalDemographicsFromZFD(Terser terser, AdditionalDemographics add) {
        try {
            Segment zfd = terser.getSegment("/.ZFD");
            if (zfd != null && !zfd.isEmpty()) {
                add.setSmsConsent(getBooleanFromIDField(zfd.getField(3, 0)));
                add.setNumberOfWeeksOfGestation(getIntegerFromNM(zfd.getField(2, 0)));
                Type lunarDate = zfd.getField(1, 0);
                add.setMonth(getIntegerFromNA(lunarDate, 2));
                add.setYearOfBirth(getIntegerFromNA(lunarDate, 3));
                Integer day = getIntegerFromNA(lunarDate, 1);
                if (day != null) {
                    add.setWeekInMonth(LunarWeek.getWeekBasedOnDay(day));
                }
                add.setDateOfBirthCorrected(getBooleanFromIDField(zfd.getField(4, 0)));
                add.setDateOfTheInsiWebserviceRequest(getDateFromTSFieldWithTimezoneCorrected(zfd.getField(6, 0)));
                add.setDocumentExpirationDate(getDateFromTSFieldWithTimezoneCorrected(zfd.getField(8, 0)));
                add.setProofOfIdentity(getISOrIDValue(zfd.getField(7, 0)));
                add.setIdentityAcquisitionMode(getISOrIDValue(zfd.getField(5, 0)));
            }
        } catch (Exception e) {
            log.warn("ZFD parsing: {}", e.getMessage());
        }
    }

    private void extractAdditionDemographicsFromZFP(Terser terser, AdditionalDemographics add) {
        try {
            Segment zfp = terser.getSegment("/.ZFP");
            if (zfp != null && !zfp.isEmpty()) {
                add.setSocioProfessionalGroup(getISOrIDValue(zfp.getField(2, 0)));
                add.setSocioProfessionalOccupation(getISOrIDValue(zfp.getField(1, 0)));
            }
        } catch (Exception e) {
            log.debug("ZFP parsing: {}", e.getMessage());
        }
    }

    private Integer getIntegerFromNA(Type type, int position) {
        if (type != null) {
            try {
                String encoded = type.encode();
                NA na = new NA(type.getMessage());
                na.parse(encoded);
                switch (position) {
                    case 1:
                        return getIntegerFromNM(na.getNa1_Value1());
                    case 2:
                        return getIntegerFromNM(na.getNa2_Value2());
                    case 3:
                        return getIntegerFromNM(na.getNa3_Value3());
                    default:
                        return null;
                }
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    private Integer getIntegerFromNM(Type type) {
        if (type != null) {
            try {
                String number = type.encode();
                return Integer.parseInt(number);
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    private boolean isBp6(AbstractMessage msg) {
        return isBp6(new Terser(msg));
    }

    private boolean isBp6(Terser terser) {
        try {
            Segment msh = terser.getSegment("/.MSH");
            if (msh != null && !msh.isEmpty()) {
                String version = msh.getField(12, 0).encode();
                return version.contains(PatientManagerConstants.FRA);
            } else {
                return false;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    private String getAddressFromXAD(Type[] types, String addressType) {
        if (types != null && types.length > 0) {
            for (Type type : types) {
                try {
                    String value = type.encode();
                    if (value.contains("^" + addressType)) {
                        return value;
                    }
                } catch (HL7Exception e) {
                    log.error(e.getMessage(), e);
                    return null;
                }
            }
            return null;
        } else {
            return null;
        }
    }

    private String getISFromDLDField(Type type) {
        if (type != null) {
            DLD dld = new DLD(type.getMessage());
            try {
                return dld.getDischargeLocation().getValue();
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    private String getISOrIDValue(Type type) {
        if (type != null) {
            try {
                return type.encode();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return null;
            }
        } else {
            return null;
        }
    }

    private Date getDateFromDLDField(Type type) {
        if (type != null) {
            DLD dld = new DLD(type.getMessage());
            try {
                return dld.getEffectiveDate().getTime().getValueAsDate();
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    private Date getDateFromTSField(Type type) {
        if (type != null) {
            try {
                TS ts = new TS(type.getMessage());
                ts.parse(type.encode());
                return ts.getTime().getValueAsDate();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return null;
            }
        } else {
            return null;
        }
    }

    private Date getDateFromTSFieldWithTimezoneCorrected(Type type) {
        if (type != null) {
            try {
                TS ts = new TS(type.getMessage());
                ts.parse(type.encode());
                Calendar calendar = ts.getTime().getValueAsCalendar();
                calendar.setTimeZone(TimeZone.getDefault());
                return calendar.getTime();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return null;
            }
        } else {
            return null;
        }
    }

    private Boolean getBooleanFromIDField(Type type) {
        if (type != null) {
            String identifier = getISOrIDValue(type);
            if (identifier == null) {
                return null;
            } else if (identifier.equals("Y")) {
                return true;
            } else if (identifier.equals("N")) {
                return false;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private void processUpdateMovement(Message incomingMessage) throws HL7v2ParsingException {
        PipeParser parser = getParserForPAMMessage();
        ADT_A01 adtMessage;
        try {
            adtMessage = (ADT_A01) parser.parse(incomingMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        ZBE zbe = adtMessage.getZBE();
        if (zbe.getMovementID() == null || zbe.getMovementID().length == 0) {
            throw new MissingRequiredFieldException("ZBE-1", MOVEMENT_ID_LOCATION);
        }
        String movementId;
        try {
            movementId = zbe.getMovementID(0).encode();
        } catch (HL7Exception e) {
            throw new HL7v2ParsingException("ZBE-1", MOVEMENT_ID_LOCATION);
        }
        try {
            Movement movementToUpdate = Movement.getMovementByUniqueIdByActor(movementId, simulatedActor, entityManager);
            if (movementToUpdate == null || movementToUpdate.getActionType().equals(ActionType.CANCEL)) {
                throw new UnknownKeyIdentifierException(movementId, MOVEMENT_ID_LOCATION);
            } else {
                movementToUpdate.setActionType(ActionType.UPDATE);
                movementToUpdate.setLastChanged(new Date());
                movementToUpdate.setWardCode(zbe.getResponsibleMedicalWard().encode());
                movementToUpdate.setAssignedPatientLocation(adtMessage.getPV1().getAssignedPatientLocation().encode());
                addWardInformation(movementToUpdate, incomingMessage);
                movementToUpdate = movementToUpdate.save(entityManager);

                String updatedTrigger = zbe.getOriginalTriggerEventCode().getValue();
                TriggerEventEnum updatedEvent = TriggerEventEnum.getEventForTrigger(updatedTrigger);
                Encounter encounterToUpdate = movementToUpdate.getEncounter();
                PV1 pv1 = adtMessage.getPV1();
                encounterToUpdate.setPatientClassCode(pv1.getPatientClass().getValue());
                encounterToUpdate.setAdmitDate(pv1.getAdmitDateTime().getTime().getValueAsDate());
                encounterToUpdate.setAdmittingDoctorCode(pv1.getAdmittingDoctor(0).encode());
                encounterToUpdate.setReferringDoctorCode(pv1.getReferringDoctor(0).encode());
                encounterToUpdate.setAttendingDoctorCode(pv1.getAttendingDoctor(0).encode());
                switch (updatedEvent) {
                    case A22:
                        encounterToUpdate.setLoaReturnDate(adtMessage.getEVN().getEventOccurred().getTime().getValueAsDate());
                        break;
                    case A21:
                        encounterToUpdate.setLeaveOfAbsenceDate(adtMessage.getEVN().getEventOccurred().getTime().getValueAsDate());
                        encounterToUpdate.setExpectedLOAReturn(adtMessage.getPV2().getExpectedLOAReturnDateTime().getTime().getValueAsDate());
                        break;
                    case A05:
                        encounterToUpdate.setExpectedAdmitDate(adtMessage.getPV2().getExpectedAdmitDateTime().getTime().getValueAsDate());
                        break;
                    case A03:
                        encounterToUpdate.setDischargeDate(adtMessage.getPV1().getDischargeDateTime(0).getTime().getValueAsDate());
                        break;
                    default:
                        log.warn("No specific treatment for trigger event: {}", updatedTrigger);
                }
                addBp6Attributes(encounterToUpdate, incomingMessage);
                encounterToUpdate.save(entityManager);
            }
        } catch (HL7Exception e) {
            throw new HL7v2ParsingException(e);
        }
    }

    private void processAccountMove(Message incomingMessage) throws HL7v2ParsingException {
        PipeParser parser = PipeParser.getInstanceWithNoValidation();
        ADT_A43 adtMessage;
        try {
            adtMessage = (ADT_A43) parser.parse(incomingMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        PatientIdentifier identifier;
        try {
            identifier = HL7MessageDecoder.buildPatientIdentifier(adtMessage.getPATIENT(0).getPID().getPatientIdentifierList
                    (0));
        } catch (NullPointerException e) {
            throw new MissingRequiredFieldException(PID_3, PID_3_LOCATION);
        }
        Patient remainingPatient = PatientDAO.getActivePatientByIdentifierByActor(identifier, entityManager, simulatedActor);
        if (remainingPatient != null) {
            String accountToMerge;
            try {
                accountToMerge = adtMessage.getPATIENT(0).getMRG().getPriorPatientAccountNumber().encode();
            } catch (HL7Exception e) {
                throw new MissingRequiredFieldException("MRG-3", "MRG^1^3");
            }

            PatientIdentifier identifierMerged = HL7MessageDecoder.buildPatientIdentifier(adtMessage.getPATIENT(0).getMRG()
                    .getPriorPatientIdentifierList(0));
            Patient patient = PatientDAO.getActivePatientByIdentifierByActor(identifierMerged, entityManager, simulatedActor);
            if (patient != null && patient.getAccountNumber().equals(accountToMerge)) {
                List<Encounter> encounters = EncounterDAO.getEncountersByAccountNumber(patient);
                if (encounters != null) {
                    for (Encounter encounter : encounters) {
                        encounter.setPatient(remainingPatient);
                        encounter.save(entityManager);
                    }
                }
                patient.setAccountNumber(NumberGenerator.generate(DesignatorType.ACCOUNT_NUMBER));
                patient.savePatient(entityManager);
            } else if (patient == null) {
                logAction("A44", AcknowledgmentCode.AA, "Message discarded: No active patient with identifier "
                        + identifierMerged.getFullPatientId());
            } else if (!patient.getAccountNumber().equals(accountToMerge)) {
                logAction("A44", AcknowledgmentCode.AA, "Message discarded: Account "
                        + accountToMerge + " is not associated to patient with identifier "
                        + identifierMerged.getFullPatientId());
            }

        } else {
            logAction("A44", AcknowledgmentCode.AA, "Message discarded: No active patient with identifier: " + identifier.getFullPatientId());
        }
    }

    private void reOpenPreviousMovement(Movement movement) {
        Movement previousMovement = movement.getPreviousValidMovement(entityManager, false);
        if (previousMovement != null) {
            previousMovement.setCurrentMovement(true);
            previousMovement.setLastChanged(new Date());
            previousMovement.save(entityManager);
        }
    }

    private void cancelMovement(Movement movementToCancel) {
        if (movementToCancel != null) {
            movementToCancel.setLastChanged(new Date());
            movementToCancel.setCurrentMovement(false);
            movementToCancel.setActionType(ActionType.CANCEL);
            movementToCancel.save(entityManager);
        }
    }

    private void processCancelChangeAttendingDoctor(Message incomingMessage) throws HL7v2ParsingException {
        ADT_A52 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A52) parser.parse(incomingMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        isPV1Present(incomingMessage, "A55");
        if (adtMessage.getPV1().getAttendingDoctorReps() == 0) {
            throw new MissingRequiredFieldException(PV1_7, ATTENDING_DOCTOR_LOCATION);
        }
        Encounter encounter = getEncounterWithVisitNumber(adtMessage.getPV1(), adtMessage.getPID());
        if (encounter != null) {
            Movement movementToCancel = encounter.getMovementToCancel("A54", entityManager);
            if (movementToCancel != null) {
                reOpenPreviousMovement(movementToCancel);
                cancelMovement(movementToCancel);
            }
            try {
                encounter.setAttendingDoctorCode(adtMessage.getPV1().getAttendingDoctor(0).encode());
                encounter.setPriorAttendingDoctorCode(null);
                encounter.save(entityManager);
            } catch (HL7Exception e) {
                // shall never occur since we've checked before that the field was not empty
                log.error("Attending doctor not present in message");
            }
        } else {
            logAction("A55", AcknowledgmentCode.AA, NO_ACTIVE_ENCOUNTER_FOUND_MESSAGE_HAS_BEEN_DISCARDED);
        }
    }

    private Encounter getEncounterWithVisitNumber(PV1 pv1, PID pid) throws MissingRequiredFieldException {
        String visitNumber = getVisitNumberForADTMessage(pid, pv1);
        return EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber, simulatedActor, entityManager, null);
    }

    private void processChangeAttendingDoctor(Message incomingMessage) throws HL7v2ParsingException {
        ADT_A54 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A54) parser.parse(incomingMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        isPV1Present(incomingMessage, "A54");
        Encounter encounter = getEncounterWithVisitNumber(adtMessage.getPV1(), adtMessage.getPID());
        if (encounter != null) {
            try {
                String attendingDoctor = adtMessage.getPV1().getAttendingDoctor(0).encode();
                Movement movement = newMovement(encounter, adtMessage.getPV1(), adtMessage.getZBE(), adtMessage.getEVN(), ActionType.INSERT,
                        "A54");
                closePreviousMovement(movement, true);
                encounter.setPriorAttendingDoctorCode(encounter.getAttendingDoctorCode());
                encounter.setAttendingDoctorCode(attendingDoctor);
                encounter.save(entityManager);
            } catch (HL7Exception e) {
                logAction("A54", AcknowledgmentCode.AE, "Unable to read Attending Doctor value");
                throw new MissingRequiredFieldException(PV1_7, ATTENDING_DOCTOR_LOCATION);
            }
        } else {
            logAction("A55", AcknowledgmentCode.AA, NO_ACTIVE_ENCOUNTER_FOUND_MESSAGE_HAS_BEEN_DISCARDED);
        }
    }

    private void processCancelReturnFromLeaveOfAbsence(Message incomingMessage) throws HL7v2ParsingException {
        ADT_A52 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A52) parser.parse(incomingMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        isPV1Present(incomingMessage, "A53");
        Encounter encounter = getEncounterWithVisitNumber(adtMessage.getPV1(), adtMessage.getPID());
        if (encounter != null) {
            boolean stop;
            try {
                stop = adtMessage.getPV2().getExpectedLOAReturnDateTime().isEmpty();
            } catch (HL7Exception e) {
                stop = true;
            }
            if (stop) {
                throw new MissingRequiredFieldException(EXPECTED_LOA_DATE, "PV2^1^47^1");
            } else {
                Movement movementToCancel = encounter.getMovementToCancel("A22", entityManager);
                if (movementToCancel != null) {
                    reOpenPreviousMovement(movementToCancel);
                    cancelMovement(movementToCancel);
                }
                encounter.setLoaReturnDate(null);
                encounter.setPriorPatientStatus(encounter.getPatientStatus());
                encounter.setPatientStatus(PatientStatus.STATUS_TEMPORARILY_ABSENT);
                encounter.setLeaveOfAbsence(true);
                try {
                    encounter.setExpectedLOAReturn(adtMessage.getPV2().getExpectedLOAReturnDateTime().getTime().getValueAsDate());
                } catch (HL7Exception e) {
                    // shall not occur, we've already checked that the field isn't  empty
                    log.error("Cannot update expected LOA return time");
                }
                encounter.save(entityManager);
            }
        } else {
            logAction("A53", AcknowledgmentCode.AA, NO_ACTIVE_ENCOUNTER_FOUND_MESSAGE_HAS_BEEN_DISCARDED);
        }
    }

    private void processReturnFromLeaveOfAbsence(Message incomingMessage) throws HL7v2ParsingException {
        ADT_A21 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A21) parser.parse(incomingMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        isPV1Present(incomingMessage, "A22");

        String visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
        Encounter encounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber, simulatedActor, entityManager, null);
        if (encounter != null && encounter.getPatientStatus().equals(PatientStatus.STATUS_TEMPORARILY_ABSENT)) {
            Movement movement = newMovement(encounter, adtMessage.getPV1(), adtMessage.getZBE(), adtMessage.getEVN(), ActionType.INSERT, "A22");
            closePreviousMovement(movement, true);
            encounter.setPatientStatus(encounter.getPriorPatientStatus());
            encounter.setPriorPatientStatus(PatientStatus.STATUS_TEMPORARILY_ABSENT);
            encounter.setLeaveOfAbsence(false);
            try {
                encounter.setLoaReturnDate(adtMessage.getEVN().getEventOccurred().getTime().getValueAsDate());
            } catch (HL7Exception e) {
                log.warn("LOA return date is not provided");
            }
            encounter.save(entityManager);
        } else {
            logAction("A52", AcknowledgmentCode.AA, "Patient is not absent or no active encounter matches : message has been discarded");
        }
    }

    private void processCancelLeaveOfAbsence(Message incomingMessage) throws HL7v2ParsingException {
        ADT_A52 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A52) parser.parse(incomingMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        isPV1Present(incomingMessage, "A52");
        Encounter encounter = getEncounterWithVisitNumber(adtMessage.getPV1(), adtMessage.getPID());
        if (encounter != null) {
            Movement movementToCancel = encounter.getMovementToCancel("A21", entityManager);
            if (movementToCancel != null) {
                reOpenPreviousMovement(movementToCancel);
                cancelMovement(movementToCancel);
            }
            encounter.setLoaReturnDate(null);
            encounter.setExpectedLOAReturn(null);
            encounter.setPatientStatus(encounter.getPriorPatientStatus());
            encounter.setPriorPatientStatus(null);
            encounter.setLeaveOfAbsence(false);
            encounter.save(entityManager);
        } else {
            logAction("A52", AcknowledgmentCode.AA, NO_ACTIVE_ENCOUNTER_FOUND_MESSAGE_HAS_BEEN_DISCARDED);
        }
    }

    private void processLeaveOfAbsence(Message incomingMessage) throws HL7v2ParsingException {
        ADT_A21 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A21) parser.parse(incomingMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        isPV1Present(incomingMessage, "A21");
        String visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
        Encounter encounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber, simulatedActor, entityManager, null);
        if (encounter != null) {
            Movement movement = newMovement(encounter, adtMessage.getPV1(), adtMessage.getZBE(), adtMessage.getEVN(), ActionType.INSERT, "A21");
            closePreviousMovement(movement, true);
            encounter.setPriorPatientStatus(encounter.getPatientStatus());
            encounter.setPatientStatus(PatientStatus.STATUS_TEMPORARILY_ABSENT);
            encounter.setLeaveOfAbsence(true);
            encounter.setLoaReturnDate(null);
            try {
                encounter.setLeaveOfAbsenceDate(adtMessage.getEVN().getEventOccurred().getTime().getValueAsDate());
            } catch (Exception e) {
                log.warn("Leave of absence date is not provided");
            }
            try {
                encounter.setExpectedLOAReturn(adtMessage.getPV2().getExpectedLOAReturnDateTime().getTime().getValueAsDate());
            } catch (Exception e) {
                log.warn("Expected LOA return date is not provided");
            }
            encounter.save(entityManager);
        } else {
            logAction("A21", AcknowledgmentCode.AA, NO_ACTIVE_ENCOUNTER_FOUND_MESSAGE_HAS_BEEN_DISCARDED);
        }
    }

    /**
     * @param message ADT^A40^ADT_A39
     * @return
     */
    private void processMergePatientEvent(Message message) throws HL7v2ParsingException {
        // check the message structure is the one expected by IHE
        ADT_A39 adtMessage;
        if (message instanceof ADT_A39) {
            adtMessage = (ADT_A39) message;
        } else {
            // message structure is not the one expected (ADT_A39)
            throw new UnexpectedMessageStructureException();
        }
        // check the message contains only one occurrence of PATIENT group
        if (adtMessage.getPATIENTReps() != 1) {
            throw new HL7v2ParsingException(null, "PATIENT segment group is repeated and should not");
        } else {
            PatientIdentifier incorrectPatientIdentifier = HL7MessageDecoder.buildPatientIdentifier(adtMessage
                    .getPATIENT().getMRG().getPriorPatientIdentifierList()[0]);
            PatientIdentifier correctPatientIdentifier = HL7MessageDecoder.buildPatientIdentifier(adtMessage
                    .getPATIENT().getPID().getPatientIdentifierList()[0]);
            Patient incorrectPatient;
            Patient correctPatient;
            List<Encounter> encounters = null;
            // check the incorrect patient identifier exists and is used by an active patient
            if (incorrectPatientIdentifier != null && incorrectPatientIdentifier.alreadyExists() != null) {
                incorrectPatient = PatientDAO.getActivePatientByIdentifierByActor(incorrectPatientIdentifier,
                        entityManager, simulatedActor);
                if (incorrectPatient != null) {
                    incorrectPatient.setStillActive(false);
                    if (!simulatedActor.getKeyword().equals("PDC")) {
                        encounters = incorrectPatient.getAllEncountersForPatient(entityManager);
                    }
                    incorrectPatient.savePatient(entityManager);
                    PatientHistory history = new PatientHistory(incorrectPatient, null, ActionPerformed.RECEIVED,
                            "ADT^A40^ADT_A39", null, sendingFacility, sendingApplication);
                    history.save(entityManager);
                }
            } else {
                incorrectPatient = null;
            }
            // check the correct patient identifier exists and is used by an active patient
            if (correctPatientIdentifier != null && correctPatientIdentifier.alreadyExists() != null) {
                correctPatient = PatientDAO.getActivePatientByIdentifierByActor(correctPatientIdentifier,
                        entityManager, simulatedActor);
            } else {
                correctPatient = null;
            }

            if (correctPatient != null) {
                // both patients exists => correctPatient remains
                // only correct patient exists => nothing to do
                if (incorrectPatient != null) {
                    PatientHistory history = new PatientHistory(incorrectPatient, correctPatient,
                            ActionPerformed.MERGE, null, null, null, null);
                    history.save(entityManager);
                    deactivatedPatient = incorrectPatient;
                }
                PatientHistory history = new PatientHistory(correctPatient, null, ActionPerformed.RECEIVED,
                        "ADT^A40^ADT_A39", null, sendingFacility, sendingApplication);
                history.save(entityManager);
            } else if (incorrectPatient == null) {
                newPatient = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPATIENT().getPID());
                newPatient.setCreator(creator);
                newPatient.setSimulatedActor(simulatedActor);
                newPatient.setStillActive(true);
                newPatient.setPatientIdentifiers(PatientIdentifierDAO.savePatientIdentifierList(newPatient
                        .getPatientIdentifiers()));
                newPatient = newPatient.savePatient(entityManager);
                PatientHistory history = new PatientHistory(newPatient, null, ActionPerformed.RECEIVED,
                        "ADT^A40^ADT_A39", null, sendingFacility, sendingApplication);
                history.save(entityManager);
            } else {
                // if the receiver does not recognize the target patient identifiers, it shall performed a Change Patient Identifier List
                // instead of a Merge
                newPatient = new Patient(incorrectPatient, simulatedActor);
                newPatient.setCreator(creator);
                newPatient.getPatientIdentifiers().remove(incorrectPatientIdentifier);
                newPatient.getPatientIdentifiers().add(correctPatientIdentifier);
                newPatient = newPatient.savePatient(entityManager);
                PatientHistory history = new PatientHistory(incorrectPatient, newPatient, ActionPerformed.CHANGEID,
                        null, null, null, null);
                history.save(entityManager);
            }
            if (!simulatedActor.getKeyword().equals("PDC") && (correctPatient != null) && (encounters != null)) {
                for (Encounter encounter : encounters) {
                    encounter.setPatient(correctPatient);
                    encounter.save(entityManager);
                }

            }
        }
    }

    /**
     * @param message ADT^A37^ADT_A37
     * @return
     */
    private void processUnlinkPatientEvent(Message message) throws HL7v2ParsingException {
        // check the message structure is the one expected by IHE
        if (message instanceof ADT_A37) {
            ADT_A37 adtMessage = (ADT_A37) message;
            Patient patientOne = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());
            Patient patientTwo = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID2());
            patientOne.setPatientIdentifiers(PatientIdentifierDAO.savePatientIdentifierList(patientOne
                    .getPatientIdentifiers()));
            patientTwo.setPatientIdentifiers(PatientIdentifierDAO.savePatientIdentifierList(patientTwo
                    .getPatientIdentifiers()));
            PatientLink.unlinkPatients(patientOne, patientTwo, simulatedActor);

        } else {
            throw new UnexpectedMessageStructureException();
        }
    }

    /**
     * @param message ADT^A24^ADT_A24
     * @return
     */
    private void processLinkPatientEvent(Message message) throws HL7v2ParsingException {
        // check the message structure is the one expected by IHE
        if (message instanceof ADT_A24) {
            ADT_A24 adtMessage = (ADT_A24) message;
            Patient patientOne = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());
            Patient patientTwo = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID2());

            patientOne.setPatientIdentifiers(PatientIdentifierDAO.savePatientIdentifierList(patientOne
                    .getPatientIdentifiers()));

            patientTwo.setPatientIdentifiers(PatientIdentifierDAO.savePatientIdentifierList(patientTwo
                    .getPatientIdentifiers()));
            PatientLink.linkPatients(patientOne, patientTwo, simulatedActor);

        } else {
            // message structure is not the one expected (ADT_A24)
            throw new UnexpectedMessageStructureException();
        }
    }

    /**
     * @param message ADT^A47^ADT_A30
     * @return
     */
    private void processChangeIdentifierEvent(Message message) throws HL7v2ParsingException {
        // we check the message structure is the one expected
        ADT_A30 adtMessage;
        if (message instanceof ADT_A30) {
            adtMessage = (ADT_A30) message;
        } else {
            // message structure is not the one expected (ADT_A30)
            throw new UnexpectedMessageStructureException();
        }
        // we check that the MRG-1 field has only one occurrence
        try {
            if (adtMessage.getMRG().isEmpty()) {
                throw new MissingRequiredSegmentException("MRG", "MRG");
            }
            if (adtMessage.getMRG().getPriorPatientIdentifierList().length != 1) {
                throw new HL7v2ParsingException(null, "MRG-1 field should contain a single incorrect supplier identifier");
            }
        } catch (HL7Exception e) {
            throw new HL7v2ParsingException(e);
        }

        // we check that the PID-3 List of identifier contains an identifier
        // with the same Type and Authority as the MRG-1 identifier
        assertMatchingIdentifierExists(adtMessage.getMRG().getMrg1_PriorPatientIdentifierList()[0],adtMessage.getPID().getPatientIdentifierList());

        newPatient = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());

        // we check that the new patient identifier is not already used by an active patient
        if ((newPatient.getPatientIdentifiers() != null) && !newPatient.getPatientIdentifiers().isEmpty()) {
            PatientIdentifier foundId = newPatient.getPatientIdentifiers().get(0).alreadyExists();
            List<PatientIdentifier> allId = newPatient.getPatientIdentifiers();

            if (foundId != null
                    && PatientDAO.getActivePatientByIdentifierByActor(foundId, entityManager, simulatedActor) != null
                    && !isRemovalIdentifierList(allId)) {
                // then we send an error message, ADT^A40 should have been used
                throw new HL7v2ParsingException(null, "A merge (A40) should have been sent instead of a change");
            }
        }

        // if we reach this point, the "correct target patient ID" is unknown
        PatientIdentifier incorrectPid = HL7MessageDecoder.buildPatientIdentifier(
                adtMessage
                        .getMRG()
                        .getPriorPatientIdentifierList()[0]
        );

        // check the given Patient ID exists in database
        PatientIdentifier foundPid = incorrectPid.alreadyExists();
        if (foundPid != null) {
            // check an active patient used this patient ID
            Patient foundPatient = PatientDAO.getActivePatientByIdentifierByActor(foundPid, entityManager,
                    simulatedActor);
            if (foundPatient != null) {
                // if such a patient exists, copy it, deactivate it and update the identifier list
                foundPatient.setStillActive(false);
                foundPatient = foundPatient.savePatient(entityManager);
                newPatient = new Patient(foundPatient, simulatedActor);
                newPatient.getPatientIdentifiers().remove(foundPid);
                newPatient.setCreator(creator);
                // get the new identifier
                PatientIdentifier newPid = HL7MessageDecoder.buildPatientIdentifier(adtMessage.getPID()
                        .getPatientIdentifierList()[0]);
                if (isRemovalIdentifier(newPid)) {
                    // it's a removal message
                    if (BP6HL7Domain.isBP6Facility(adtMessage.getMSH().getReceivingFacility())
                            && hasINSAuthority(newPid)) {
                        // it's an INS identifier removal:  we must remove all INS identifiers
                        for (PatientIdentifier identifier : newPatient.getPatientIdentifiers().toArray(new PatientIdentifier[]{})) {
                            if (hasINSAuthority(identifier)) {
                                newPatient.getPatientIdentifiers().remove(identifier);
                            }
                        }
                    }
                    if(adtMessage.getPID().getIdentityReliabilityCode() != null && adtMessage.getPID().getIdentityReliabilityCode().length > 0) {
                        newPatient.setIdentityReliabilityCode(adtMessage.getPID().getIdentityReliabilityCode()[0].getValue());
                    }
                } else {
                    newPid = newPid.save();
                    newPatient.getPatientIdentifiers().add(newPid);
                }
                newPatient = newPatient.savePatient(entityManager);
                PatientHistory history = new PatientHistory(foundPatient, newPatient,
                        ActionPerformed.CHANGEID, null, null, null, null);
                history.save(entityManager);
                history = new PatientHistory(newPatient, null, ActionPerformed.RECEIVED, "ADT^A47^ADT_A30",
                        null, sendingFacility, sendingApplication);
                history.save(entityManager);
            }
        } else {
            // if we reach this line that means that we must used the patient as described in PID
            List<PatientIdentifier> mergedList = new ArrayList<PatientIdentifier>();
            for (PatientIdentifier id : newPatient.getPatientIdentifiers()) {
                id = id.save();
                mergedList.add(id);
            }
            newPatient.setPatientIdentifiers(mergedList);
            newPatient.setStillActive(true);
            newPatient.setSimulatedActor(simulatedActor);
            newPatient.setCreator(creator);
            newPatient = newPatient.savePatient(entityManager);
            PatientHistory history = new PatientHistory(newPatient, null, ActionPerformed.RECEIVED,
                    "ADT^A47^ADT_A30", null, sendingFacility, sendingApplication);
            history.save(entityManager);
        }

    }
    private void assertMatchingIdentifierExists(CX prior, CX[] patientIdentifierList) throws HL7v2ParsingException {
        for (CX cx:patientIdentifierList) {
            if (equals(prior.getAssigningAuthority().getUniversalID().getValue(),cx.getAssigningAuthority().getUniversalID().getValue())
                    && equals(prior.getIdentifierTypeCode().getValue(),cx.getIdentifierTypeCode().getValue())
            ) {
                return;
            }
        }
        throw new HL7v2ParsingException(null, "No corresponding correct identifier found in PID-3");
    }

    private boolean isRemovalIdentifierList(List<PatientIdentifier> allId) {
        for (PatientIdentifier pi : allId) {
            if(hasINSAuthority(pi) && pi.alreadyExists() != null) {
                if (isRemovalIdentifier(pi)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean equals(Object a, Object b) {
        return (a==null&&b==null)
                || a!=null && b!=null && a.equals(b);
    }
    private boolean isRemovalIdentifier(PatientIdentifier pi) {
        return "\"\"".equals(pi.getIdNumber()) || pi.getIdNumber() == null && pi.getFullPatientId().startsWith("\"\"^");
    }

    /**
     * update
     *
     * @param message ADT^A31^ADT_A05
     * @return
     */
    private void processUpdatePatientEvent(Message message) throws HL7v2ParsingException {
        ADT_A05 adtMessage;
        if (message instanceof ADT_A05) {
            adtMessage = (ADT_A05) message;
        } else {
            // message structure is not the one expected (ADT_A05)
            throw new UnexpectedMessageStructureException();
        }
        Patient patient = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());
        boolean validBp6Message = isMessageBp6Compliant(adtMessage.getPID());
        // the patient for which one identifier at least match one of the received patient
        if (patient.getPatientIdentifiers() != null) {
            Patient foundPatient = searchActivePatientByIdentifier(patient);

            // if one of the received patient's identifiers match an existing patient, the latter is updated
            if (foundPatient != null) {
                // deactivate the found patient
                foundPatient.setStillActive(false);
                foundPatient.savePatient(entityManager);
                // merge unsaved identifiers
                List<PatientIdentifier> mergedIdentifiers = PatientIdentifierDAO.savePatientIdentifierList(getPatientIdentifierListWithoutInsIds(patient
                        .getPatientIdentifiers(), validBp6Message));

                // new patient
                patient.setPatientIdentifiers(mergedIdentifiers);
                patient.setSimulatedActor(simulatedActor);
                patient.setCreator(creator);
                patient.setStillActive(true);
                addBp6Attributes(patient, message);
                patient = patient.savePatient(entityManager);
                PatientHistory history = new PatientHistory(foundPatient, patient, ActionPerformed.UPDATE,
                        null, null, null, null);
                history.save(entityManager);
                history = new PatientHistory(patient, null, ActionPerformed.RECEIVED, "ADT^A31^ADT_A05", null,
                        sendingFacility, sendingApplication);
                history.save(entityManager);
                newPatient = patient;
                deactivatedPatient = foundPatient;
            } else {
                // we create a new patient
                List<PatientIdentifier> mergedIdentifiers = PatientIdentifierDAO.savePatientIdentifierList(patient
                        .getPatientIdentifiers());
                patient.setPatientIdentifiers(mergedIdentifiers);
                patient.setCreator(creator);
                patient.setSimulatedActor(simulatedActor);
                patient.setStillActive(true);
                addBp6Attributes(patient, message);
                patient = patient.savePatient(entityManager);

                PatientHistory history = new PatientHistory(patient, null, ActionPerformed.RECEIVED, "ADT^A31^ADT_A05", null,
                        sendingFacility, sendingApplication);
                history.save(entityManager);
                newPatient = patient;
            }
        } else {
            // the provided patient has no identifiers -- reject the message
            throw new MissingRequiredFieldException(PID_3, PID_3_LOCATION);
        }
    }

    private Patient searchActivePatientByIdentifier(Patient patient) {
        Patient foundPatient = null;
        for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
            PatientIdentifier foundIdentifier = pid.alreadyExists();
            if (foundIdentifier != null) {
                foundPatient = PatientDAO.getActivePatientByIdentifierByActor(foundIdentifier, entityManager,
                        simulatedActor);
                if (foundPatient != null) {
                    break;
                }
            }
        }
        return foundPatient;
    }


    /**
     * create
     *
     * @param message ADT^A28^ADT_A05
     * @return
     */
    private void processCreateNewPatientEvent(Message message) throws HL7v2ParsingException {
        ADT_A05 adtMessage;
        if (message instanceof ADT_A05) {
            adtMessage = (ADT_A05) message;
        } else {
            // message structure is not the one expected (ADT_A05)
            throw new UnexpectedMessageStructureException();
        }

        Patient patient = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());
        if (patient.getPatientIdentifiers() == null) {
            // the provided patient has no identifiers -- reject the message
            throw new MissingRequiredFieldException(PID_3, PID_3_LOCATION);
        }
        for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
            if (pid.alreadyExists() != null) {
                if (PatientDAO.getActivePatientByIdentifierByActor(pid, entityManager, simulatedActor) != null) {
                    // this id is already used by an active patient
                    throw new DuplicateKeyIdentifierException(pid.getFullPatientId(), PID_3_LOCATION);
                }
            }
        }
        // if we reach this point, none of the provided patient identifiers match an existing one
        // we can then save the identifiers and the patient
        boolean validBp6Message = isMessageBp6Compliant(adtMessage.getPID());
        List<PatientIdentifier> mergedIdentifiers = PatientIdentifierDAO.savePatientIdentifierList(getPatientIdentifierListWithoutInsIds(patient
                .getPatientIdentifiers(), validBp6Message));
        patient.setPatientIdentifiers(mergedIdentifiers);
        patient.setStillActive(true);
        patient.setSimulatedActor(simulatedActor);
        patient.setCreator(creator);
        addBp6Attributes(patient, message);
        patient = patient.savePatient(entityManager);

        // update history
        PatientHistory history = new PatientHistory(patient, null, ActionPerformed.RECEIVED,
                "ADT^A28^ADT_A05", null, sendingFacility, sendingApplication);
        history.save(entityManager);
        history = new PatientHistory(patient, null, ActionPerformed.CREATED, null, null, null, null);
        history.save(entityManager);
        newPatient = patient;
        // create ack
    }

    private void processCancelDischargeEvent(Message receivedMessage) throws HL7v2ParsingException {
        ADT_A01 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A01) parser.parse(receivedMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        isPV1Present(receivedMessage, "A13");
        // extract the visit number and check that this visit exists and is open
        String visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
        Encounter encounter = EncounterDAO.getClosedEncounterByActorByNumber(visitNumber, simulatedActor, entityManager);
        if (encounter == null) {
            logAction("A13", AcknowledgmentCode.AA, "Message discarded: no such movement to cancel");
        } else {
            Movement movementToCancel = null;
            movementToCancel = encounter.getMovementToCancel("A03", entityManager);
            if (movementToCancel == null) {
                throw new HL7v2ParsingException(null, "No valid movement found for cancel");
            } else {
                encounter.setOpen(true);
                encounter.setPatientStatus(encounter.getPriorPatientStatus());
                encounter.setDischargeDate(null);
                encounter.save(entityManager);
                Movement previousMovement = movementToCancel.getPreviousValidMovement(entityManager, false);
                if (previousMovement != null) {
                    previousMovement.setCurrentMovement(true);
                    previousMovement.setLastChanged(new Date());
                    previousMovement.save(entityManager);
                }
                movementToCancel.setCurrentMovement(false);
                movementToCancel.setLastChanged(new Date());
                movementToCancel.setActionType(ActionType.CANCEL);
                movementToCancel.save(entityManager);
            }
        }
    }

    private void processCancelNewEncounterEvent(Message receivedMessage) throws HL7v2ParsingException {
        PipeParser parser = getParserForPAMMessage();
        ADT_A09 adtMessage;
        try {
            adtMessage = (ADT_A09) parser.parse(receivedMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        // check the PV1 segment is present and the visit number exists
        isPV1Present(receivedMessage, "A11");
        // extract the visit number and check that this visit exists and is open
        String visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
        Encounter encounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber, simulatedActor, entityManager,
                null);
        if (encounter == null) {
            logAction("A11", AcknowledgmentCode.AA, "Message discarded: no movement to cancel");
        } else {
            Movement movementToCancel;
            movementToCancel = encounter.getMovementToCancel("A01", entityManager);
            if (movementToCancel == null) {
                throw new HL7v2ParsingException("No valid movement found for cancellation");
            } else if (encounter.getPriorPatientStatus() != null) {
                PatientStatus priorStatus = encounter.getPriorPatientStatus();
                encounter.setPriorPatientStatus(encounter.getPatientStatus());
                encounter.setPatientStatus(priorStatus);
                encounter.save(entityManager);
                Movement previousMovement = movementToCancel.getPreviousValidMovement(entityManager, false);
                if (previousMovement != null) {
                    previousMovement.setCurrentMovement(true);
                    previousMovement.setLastChanged(new Date());
                    previousMovement.save(entityManager);
                }
            } else {
                encounter.setOpen(false);
                encounter.setPatientStatus(PatientStatus.STATUS_ENCOUNTER_CANCELLED);
                encounter.save(entityManager);
            }
            movementToCancel.setCurrentMovement(false);
            movementToCancel.setLastChanged(new Date());
            movementToCancel.setActionType(ActionType.CANCEL);
            movementToCancel.save(entityManager);
        }
    }

    private void processA08Event(Message receivedMessage) throws HL7v2ParsingException {
        ca.uhn.hl7v2.model.v25.message.ADT_A01 adtMessage;
        if (receivedMessage instanceof ca.uhn.hl7v2.model.v25.message.ADT_A01) {
            adtMessage = (ca.uhn.hl7v2.model.v25.message.ADT_A01) receivedMessage;
        } else {
            // message structure is not the one expected (ADT_A01)
            throw new UnexpectedMessageStructureException();
        }
        Patient patient = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());
        // the patient for which one identifier at least match one of the received patient
        if (patient.getPatientIdentifiers() == null) {
            throw new MissingRequiredFieldException(PID_3, PID_3_LOCATION);
        }
        Patient foundPatient = searchActivePatientByIdentifier(patient);

        // if one of the received patient's identifiers match an existing patient, the latter is updated
        if (foundPatient != null) {
            // deactivate the found patient
            foundPatient.setStillActive(false);
            foundPatient.savePatient(entityManager);
            deactivatedPatient = foundPatient;
            // merge unsaved identifiers
            boolean validBp6Message = isMessageBp6Compliant(adtMessage.getPID());
            List<PatientIdentifier> mergedIdentifiers = PatientIdentifierDAO.savePatientIdentifierList(getPatientIdentifierListWithoutInsIds(patient
                    .getPatientIdentifiers(), validBp6Message));

            // new patient
            patient.setPatientIdentifiers(mergedIdentifiers);
            patient.setSimulatedActor(simulatedActor);
            patient.setCreator(creator);
            patient.setStillActive(true);
            patient = patient.savePatient(entityManager);
            newPatient = patient;
            // move encounters from oldPatient to the newly created patient
            List<Encounter> encounters = foundPatient.getAllEncountersForPatient(entityManager);
            if (encounters != null) {
                for (Encounter encounter : encounters) {
                    encounter.setPatient(patient);
                    encounter.save(entityManager);
                }
            }
            PatientHistory history = new PatientHistory(foundPatient, patient, ActionPerformed.UPDATE,
                    null, null, null, null);
            history.save(entityManager);
            history = new PatientHistory(patient, null, ActionPerformed.RECEIVED, "ADT^A08^ADT_A01", null,
                    sendingFacility, sendingApplication);
            history.save(entityManager);
        } else {
            throw new UnknownKeyIdentifierException(PID_3, "No admitted nor registered patient match the given patient");
        }
    }

    private void processDischargePatientEvent(Message receivedMessage) throws HL7v2ParsingException {
        ADT_A03 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A03) parser.parse(receivedMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }

        // check the PV1 segment is present and the visit number exists
        isPV1Present(receivedMessage, "A03");

        // extract the visit number and check that this visit exists and is open
        String visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
        Encounter encounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber, simulatedActor, entityManager,
                null);
        if (encounter == null) {
            logAction("A03", AcknowledgmentCode.AA, "Message discarded: no such active encounter");
        } else {
            encounter.setOpen(false);
            try {
                encounter.setDischargeDate(adtMessage.getPV1().getDischargeDateTime(0).getTime().getValueAsDate());
            } catch (DataTypeException e) {
                log.warn("cannot read discharge date/time");
            }
            encounter.setPriorPatientStatus(encounter.getPatientStatus());
            encounter.setPatientStatus(PatientStatus.DISCHARGED);
            encounter = encounter.save(entityManager);
            Movement movement = new Movement(encounter);
            movement = HL7MessageDecoder.fillMovementWithPV1AndZBE(movement, adtMessage.getPV1(), adtMessage.getZBE());
            movement.setTriggerEvent("A03");
            movement.setCreator(creator);
            movement.setLastChanged(new Date());
            movement.setActionType(ActionType.INSERT);
            movement.setPatientClassCode(encounter.getPatientClassCode());
            movement.setCurrentMovement(true);
            movement.setPendingMovement(false);
            movement.setSimulatedActor(simulatedActor);
            addWardInformation(movement, receivedMessage);
            try {
                movement.setMovementDate(adtMessage.getEVN().getEventOccurred().getTime().getValueAsDate());
            } catch (NullPointerException | DataTypeException e) {
                log.info(CANNOT_RETRIEVE_MOVEMENT_EFFECTIVE_DATE);
            }
            closePreviousMovement(movement, true);
        }
    }


    private void processRegisterPatientEvent(Message receivedMessage) throws HL7v2ParsingException {
        ADT_A01 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A01) parser.parse(receivedMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        isPV1Present(receivedMessage, "A04");
        Encounter encounterFromMessage = HL7MessageDecoder.populateEncounterFromPV1Segment(adtMessage.getPV1());
        String visitNumber = encounterFromMessage.getVisitNumber();
        Encounter selectedEncounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber,
                simulatedActor, entityManager, PatientStatus.STATUS_PRE_ADMITTED);

        if ((selectedEncounter == null)
                && EncounterDAO.isVisitNumberAlreadyUsed(visitNumber, simulatedActor, entityManager)) {
            throw new DuplicateKeyIdentifierException(visitNumber, PV1_19_LOCATION);
        }
        // confirm a pre-admission
        else if (selectedEncounter != null) {
            selectedEncounter.setPriorPatientStatus(PatientStatus.STATUS_PRE_ADMITTED);
            selectedEncounter.setPatientStatus(PatientStatus.STATUS_REGISTERED);
            selectedEncounter.setAdmitDate(encounterFromMessage.getAdmitDate());
            selectedEncounter.setPatientClassCode(encounterFromMessage.getPatientClassCode());
            addBp6Attributes(selectedEncounter, receivedMessage);
            selectedEncounter = selectedEncounter.save(entityManager);

            // creation of a new movement
            Movement movement = new Movement(selectedEncounter);
            // close previous movement
            Movement previousMovement = movement.getPreviousValidMovement(entityManager, true);
            if (previousMovement != null) {
                previousMovement.setCurrentMovement(false);
                previousMovement.setLastChanged(new Date());
                previousMovement.save(entityManager);
            }
            // fill out new movement
            movement.setTriggerEvent("A04");
            movement.setCreator(creator);
            movement.setLastChanged(new Date());
            movement.setActionType(ActionType.INSERT);
            movement.setPatientClassCode(selectedEncounter.getPatientClassCode());
            movement.setCurrentMovement(true);
            movement.setPendingMovement(false);
            movement.setSimulatedActor(simulatedActor);
            movement = HL7MessageDecoder.fillMovementWithPV1AndZBE(movement, adtMessage.getPV1(),
                    adtMessage.getZBE());
            addWardInformation(movement, receivedMessage);
            setMovementDateIfAvailable(adtMessage, movement);
            movement.save(entityManager);

        } else {
            // create the patient from the PID segment
            Patient pidPatient = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());
            Patient selectedPatient = null;
            // retrieve the patient if it is known by the application
            for (PatientIdentifier pid : pidPatient.getPatientIdentifiers()) {
                if (pid.alreadyExists() != null) {
                    selectedPatient = PatientDAO.getActivePatientByIdentifierByActor(pid, entityManager, simulatedActor);
                    break;
                }
            }
            // if the patient is unknown, create it using PID segment
            if (selectedPatient == null) {
                boolean validBp6Message = isMessageBp6Compliant(adtMessage.getPID());
                // we have a new patient
                createNewPatientFromPID(pidPatient, validBp6Message);
                addBp6Attributes(pidPatient, receivedMessage);
                selectedPatient = pidPatient.savePatient(entityManager);
            }

            // creation of a new encounter for the patient
            Encounter encounter;
            encounter = createEncounterForPatientFromPV1Segment(adtMessage.getPV1(), selectedPatient);
            encounter.setCreator(creator);
            encounter.setPatientStatus(PatientStatus.STATUS_REGISTERED);
            encounter.setSimulatedActor(simulatedActor);
            encounter.setOpen(true);
            addBp6Attributes(encounter, receivedMessage);
            encounter = encounter.save(entityManager);

            // creation of a new movement
            Movement movement = new Movement(encounter);
            movement.setTriggerEvent("A04");
            movement.setCreator(creator);
            movement.setLastChanged(new Date());
            movement.setActionType(ActionType.INSERT);
            movement.setPatientClassCode(encounter.getPatientClassCode());
            movement.setCurrentMovement(true);
            movement.setPendingMovement(false);
            movement.setSimulatedActor(simulatedActor);
            addWardInformation(movement, receivedMessage);
            movement = HL7MessageDecoder.fillMovementWithPV1AndZBE(movement, adtMessage.getPV1(), adtMessage.getZBE());
            setMovementDateIfAvailable(adtMessage, movement);
            movement.save(entityManager);
        }
    }

    /**
     * Admit a patient: message ADT^A01^ADT_A01
     *
     * @param receivedMessage
     * @return
     */
    private void processAdmitPatientEvent(Message receivedMessage) throws HL7v2ParsingException {
        ADT_A01 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A01) parser.parse(receivedMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        isPV1Present(receivedMessage, "A01");
        String visitNumber;
        visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
        Encounter selectedEncounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber,
                simulatedActor, entityManager, PatientStatus.STATUS_PRE_ADMITTED);
        if ((selectedEncounter == null)
                && EncounterDAO.isVisitNumberAlreadyUsed(visitNumber, simulatedActor, entityManager)) {
            throw new DuplicateKeyIdentifierException(visitNumber, PV1_19_LOCATION);
        }
        // confirm a pre-admission
        else if (selectedEncounter != null) {
            selectedEncounter.setPriorPatientStatus(PatientStatus.STATUS_PRE_ADMITTED);
            selectedEncounter.setPatientStatus(PatientStatus.STATUS_ADMITTED);
            // create the encounter from the PV1 segment to retrieve the missing values
            Encounter dumpEncounter = createEncounterForPatientFromPV1Segment(adtMessage.getPV1(),
                    selectedEncounter.getPatient());
            selectedEncounter.setAdmitDate(dumpEncounter.getAdmitDate());
            selectedEncounter.setPatientClassCode(dumpEncounter.getPatientClassCode());
            addBp6Attributes(selectedEncounter, receivedMessage);
            selectedEncounter = selectedEncounter.save(entityManager);

            // creation of a new movement
            Movement movement = new Movement(selectedEncounter);
            addWardInformation(movement, receivedMessage);
            // close previous movement
            Movement previousMovement = movement.getPreviousValidMovement(entityManager, true);
            if (previousMovement != null) {
                previousMovement.setCurrentMovement(false);
                previousMovement.setLastChanged(new Date());
                previousMovement.save(entityManager);
            }
            // fill out new movement
            movement.setTriggerEvent("A01");
            movement.setCreator(creator);
            movement.setLastChanged(new Date());
            movement.setActionType(ActionType.INSERT);
            movement.setPatientClassCode(selectedEncounter.getPatientClassCode());
            movement.setCurrentMovement(true);
            movement.setPendingMovement(false);
            movement.setSimulatedActor(simulatedActor);
            movement = HL7MessageDecoder.fillMovementWithPV1AndZBE(movement, adtMessage.getPV1(),
                    adtMessage.getZBE());
            setMovementDateIfAvailable(adtMessage, movement);
            movement.save(entityManager);

        } else {
            // create the patient from the PID segment
            Patient pidPatient = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());
            Patient selectedPatient = null;
            // retrieve the patient if it is known by the application
            for (PatientIdentifier pid : pidPatient.getPatientIdentifiers()) {
                if (pid.alreadyExists() != null) {
                    selectedPatient = PatientDAO.getActivePatientByIdentifierByActor(pid, entityManager, simulatedActor);
                    break;
                }
            }
            boolean validBp6Message = isMessageBp6Compliant(adtMessage.getPID());
            // if the patient is unknown, create it using PID segment
            if (selectedPatient == null) {
                selectedPatient = createNewPatientFromPID(pidPatient, validBp6Message);
                addBp6Attributes(selectedPatient, receivedMessage);
                selectedPatient = selectedPatient.savePatient(entityManager);
            }

            // 3.31.7.1.4 in case this movement conflicts with an existing current movement
            // for the patient, the message is discarded and an error condition is raised
            if (EncounterDAO.isPatientAlreadyAdmitted(selectedPatient, simulatedActor, entityManager)) {
                throw new HL7v2ParsingException(null, "An admission is already opened for this patient");
            } else {
                // creation of a new encounter for the patient
                Encounter encounter = createEncounterForPatientFromPV1Segment(adtMessage.getPV1(), selectedPatient);
                encounter.setOpen(true);
                encounter.setCreator(creator);
                encounter.setPatientStatus(PatientStatus.STATUS_ADMITTED);
                encounter.setSimulatedActor(simulatedActor);
                addBp6Attributes(encounter, receivedMessage);
                encounter = encounter.save(entityManager);
                // creation of a new movement
                Movement movement = new Movement(encounter);
                movement.setTriggerEvent("A01");
                movement.setCreator(creator);
                movement.setLastChanged(new Date());
                movement.setActionType(ActionType.INSERT);
                movement.setPatientClassCode(encounter.getPatientClassCode());
                movement.setCurrentMovement(true);
                movement.setPendingMovement(false);
                movement.setSimulatedActor(simulatedActor);
                movement = HL7MessageDecoder.fillMovementWithPV1AndZBE(movement, adtMessage.getPV1(),
                        adtMessage.getZBE());
                addWardInformation(movement, receivedMessage);
                setMovementDateIfAvailable(adtMessage, movement);
                movement.save(entityManager);
            }

        }
    }

    private void setMovementDateIfAvailable(ADT_A01 adtMessage, Movement movement) {
        try {
            movement.setMovementDate(adtMessage.getEVN().getEventOccurred().getTime().getValueAsDate());
        } catch (NullPointerException e) {
            log.info("Movement effective date is missing");
        } catch (DataTypeException e) {
            log.info(CANNOT_RETRIEVE_MOVEMENT_EFFECTIVE_DATE);
        }
    }

    private void isPV1Present(Message incomingMessage, String triggerEvent) throws HL7v2ParsingException {
        try {
            Terser terser = new Terser(incomingMessage);
            Segment pv1 = terser.getSegment("/.PV1");
            String segmentAsString = pv1.encode();
            // if the received message does not contain a PV1 segment, segmentAsString = "PV1"
            if ((segmentAsString == null) || segmentAsString.isEmpty() || (segmentAsString.length() < 4)) {
                logAction(triggerEvent, AcknowledgmentCode.AR, "PV1 segment is missing or empty");
                throw new MissingRequiredSegmentException("PV1", "PV1");
            }
        } catch (HL7Exception e) {
            logAction(triggerEvent, AcknowledgmentCode.AR, "PV1 segment is missing or empty");
            throw new HL7v2ParsingException(e);
        }
    }

    /**
     * Cancel the pre-admission of the patient (ADT^A38^ADT_A38)
     *
     * @param receivedMessage
     * @return
     */
    private void processCancelPreAdmitEvent(Message receivedMessage) throws HL7v2ParsingException {
        ADT_A38 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A38) parser.parse(receivedMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        // check the PV1 segment is present and the visit number exists
        isPV1Present(receivedMessage, "A38");
        String visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
        Encounter encounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber, simulatedActor, entityManager,
                null);
        if (encounter != null) {
            cancelMovement(encounter.getMovementToCancel("A05", entityManager));
            encounter.setPatientStatus(PatientStatus.STATUS_ENCOUNTER_CANCELLED);
            encounter.setOpen(false);
            encounter.save(entityManager);
        } else {
            logAction("A38", AcknowledgmentCode.AA, "Message discarded: no such event to cancel");
        }
    }

    /**
     * Cancel the transfer of the patient (ADT^A12^ADT_A12)
     *
     * @param receivedMessage
     * @return
     */
    private void processCancelTransferEvent(Message receivedMessage) throws HL7v2ParsingException {
        ADT_A12 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A12) parser.parse(receivedMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        // check the PV1 segment is present and the visit number exists
        isPV1Present(receivedMessage, "A12");
        // extract the visit number and check that this visit exists and is open
        String visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
        Encounter encounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber, simulatedActor, entityManager,
                null);
        if (encounter != null) {
            Movement movementToCancel = encounter.getMovementToCancel("A02", entityManager);
            if (movementToCancel != null) {
                cancelMovement(movementToCancel);
                reOpenPreviousMovement(movementToCancel);
            }
        } else {
            logAction("A12", AcknowledgmentCode.AA, "Message discarded: no such movement to cancel");
        }
    }

    /**
     * Change patient class from inpatient to outpatient (ADT^A07^ADT_A06)
     *
     * @param receivedMessage
     * @return
     */
    private void processChangeToOutpatientEvent(Message receivedMessage) throws HL7v2ParsingException {
        ADT_A06 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A06) parser.parse(receivedMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        isPV1Present(receivedMessage, "A07");
        // extract the visit number and check that this visit exists and is open
        String visitNumber;
        visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
        // if MRG-3 is populated, close the prior visit
        try {
            String priorVisitNumber = adtMessage.getMRG().getPriorPatientAccountNumber().encode();
            if ((priorVisitNumber != null) && !priorVisitNumber.isEmpty()) {
                Encounter priorEncounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(priorVisitNumber,
                        simulatedActor, entityManager, null);
                if (priorEncounter != null) {
                    priorEncounter.setOpen(false);
                    priorEncounter.setPatientStatus(PatientStatus.STATUS_DISCHARGE);
                    priorEncounter.save(entityManager);
                }
            }
        } catch (HL7Exception e) {
            log.info("No account number provided");
        }
        Encounter encounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber, simulatedActor, entityManager,
                null);

        if (encounter == null) {
            // create the patient from the PID segment
            Patient pidPatient = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());
            Patient selectedPatient = null;
            // retrieve the patient if it is known by the application
            for (PatientIdentifier pid : pidPatient.getPatientIdentifiers()) {
                if (pid.alreadyExists() != null) {
                    selectedPatient = PatientDAO.getActivePatientByIdentifierByActor(pid, entityManager,
                            simulatedActor);
                    break;
                }
            }
            // if the patient is unknown, create it using PID segment
            if (selectedPatient == null) {
                // we have a new patient
                selectedPatient = createNewPatientFromPID(pidPatient);
                addBp6Attributes(selectedPatient, receivedMessage);
                selectedPatient = selectedPatient.savePatient(entityManager);
            }
            encounter = createEncounterForPatientFromPV1Segment(adtMessage.getPV1(), selectedPatient);
            encounter.setOpen(true);
            encounter.setCreator(creator);
            encounter.setPatientStatus(PatientStatus.STATUS_REGISTERED);
            encounter.setSimulatedActor(simulatedActor);
            addBp6Attributes(encounter, receivedMessage);
            encounter = encounter.save(entityManager);
        } else {
            encounter.setPriorPatientClassCode(encounter.getPatientClassCode());
            encounter.setPatientClassCode(adtMessage.getPV1().getPatientClass().getValue());
            encounter.setPatientStatus(PatientStatus.STATUS_REGISTERED);
            addBp6Attributes(encounter, receivedMessage);
            encounter = encounter.save(entityManager);
        }

        Movement movement = newMovement(encounter, adtMessage.getPV1(), adtMessage.getZBE(), adtMessage.getEVN(), ActionType.INSERT, "A07");
        addWardInformation(movement, receivedMessage);
        try {
            movement.setMovementDate(adtMessage.getEVN().getEventOccurred().getTime().getValueAsDate());
        } catch (NullPointerException e) {
            log.info("Movement effective date is missing");
        } catch (DataTypeException e) {
            log.info(CANNOT_RETRIEVE_MOVEMENT_EFFECTIVE_DATE);
        }
        closePreviousMovement(movement, true);
    }

    /**
     * Change patient class from outpatient to inpatient (ADT^A06^ADT_A06)
     *
     * @param receivedMessage
     * @return
     */
    private void processChangeToInpatientEvent(Message receivedMessage) throws HL7v2ParsingException {
        ADT_A06 adtMessage;
        try {
            PipeParser parser = getParserForPAMMessage();
            adtMessage = (ADT_A06) parser.parse(receivedMessage.encode());
        } catch (Exception e) {
            throw new UnexpectedMessageStructureException();
        }
        isPV1Present(receivedMessage, "A06");
        // extract the visit number and check that this visit exists and is open
        Encounter encounter = null;
        try {
            String visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
            // if MRG-3 is populated, close the prior visit
            String priorVisitNumber = adtMessage.getMRG().getPriorPatientAccountNumber().encode();
            if ((priorVisitNumber != null) && !priorVisitNumber.isEmpty()) {
                Encounter priorEncounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(priorVisitNumber,
                        simulatedActor, entityManager, null);
                if (priorEncounter != null) {
                    priorEncounter.setOpen(false);
                    priorEncounter.setPatientStatus(PatientStatus.DISCHARGED);
                    priorEncounter.save(entityManager);
                }
            }
            encounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber, simulatedActor, entityManager,
                    null);
        } catch (HL7Exception e) {
            log.error("unable to retrieve the visit number");
        }
        if (encounter == null) {
            // create the patient from the PID segment
            Patient pidPatient = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());
            Patient selectedPatient = null;
            // retrieve the patient if it is known by the application
            for (PatientIdentifier pid : pidPatient.getPatientIdentifiers()) {
                if (pid.alreadyExists() != null) {
                    selectedPatient = PatientDAO.getActivePatientByIdentifierByActor(pid, entityManager,
                            simulatedActor);
                    break;
                }
            }
            // if the patient is unknown, create it using PID segment
            if (selectedPatient == null) {
                selectedPatient = createNewPatientFromPID(pidPatient);
                addBp6Attributes(selectedPatient, receivedMessage);
                selectedPatient = selectedPatient.savePatient(entityManager);
            }
            encounter = createEncounterForPatientFromPV1Segment(adtMessage.getPV1(), selectedPatient);
            encounter.setOpen(true);
            encounter.setCreator(creator);
            encounter.setPatientStatus(PatientStatus.STATUS_ADMITTED);
            encounter.setSimulatedActor(simulatedActor);
            addBp6Attributes(encounter, receivedMessage);
            encounter.save(entityManager);
        } else {
            encounter.setPriorPatientClassCode(encounter.getPatientClassCode());
            encounter.setPatientClassCode(adtMessage.getPV1().getPatientClass().getValue());
            encounter.setPatientStatus(PatientStatus.STATUS_ADMITTED);
            addBp6Attributes(encounter, receivedMessage);
            encounter = encounter.save(entityManager);
        }

        Movement movement = newMovement(encounter, adtMessage.getPV1(), adtMessage.getZBE(), adtMessage.getEVN(), ActionType.INSERT, "A06");
        addWardInformation(movement, receivedMessage);
        movement.save(entityManager);
        try {
            movement.setMovementDate(adtMessage.getEVN().getEventOccurred().getTime().getValueAsDate());
        } catch (NullPointerException e) {
            log.info("Movement effective date is missing");
        } catch (DataTypeException e) {
            log.info(CANNOT_RETRIEVE_MOVEMENT_EFFECTIVE_DATE);
        }
        closePreviousMovement(movement, true);
    }

    /**
     * Pre-admit the patient (ADT^A05^ADT_A05)
     *
     * @param receivedMessage
     * @return
     */
    private void processPreAdmitEvent(Message receivedMessage) throws HL7v2ParsingException {
        ModelClassFactory mcf = new CustomModelClassFactory(PAM_HL7_PACKAGE_NAME);
        Parser parser = new PipeParser(mcf);
        net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A05 adtMessage;
        try {
            adtMessage = (net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A05) parser.parse(receivedMessage.encode());
        } catch (HL7Exception e) {
            throw new UnexpectedMessageStructureException();
        }

        // check the PV1 segment is present and the visit number is not already used
        isPV1Present(receivedMessage, "A05");
        String visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
        if (EncounterDAO.isVisitNumberAlreadyUsed(visitNumber, simulatedActor, entityManager)) {
            throw new DuplicateKeyIdentifierException(visitNumber, PV1_19_LOCATION);
        }

        // create the patient from the PID segment
        Patient pidPatient = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());
        Patient selectedPatient = null;
        // retrieve the patient if it is known by the application
        for (PatientIdentifier pid : pidPatient.getPatientIdentifiers()) {
            if (pid.alreadyExists() != null) {
                selectedPatient = PatientDAO.getActivePatientByIdentifierByActor(pid, entityManager, simulatedActor);
                break;
            }
        }
        // if the patient is unknown, create it using PID segment
        if (selectedPatient == null) {
            boolean validBp6Message = isMessageBp6Compliant(adtMessage.getPID());
            selectedPatient = createNewPatientFromPID(pidPatient, validBp6Message);
        }

        // creation of a new encounter for the patient
        Encounter encounter = createEncounterForPatientFromPV1Segment(adtMessage.getPV1(), selectedPatient);
        encounter.setOpen(true);
        encounter.setCreator(creator);
        encounter.setPatientStatus(PatientStatus.STATUS_PRE_ADMITTED);
        encounter.setSimulatedActor(simulatedActor);
        encounter = encounter.save(entityManager);
        // look for the expected admit date PV2-8
        Date expectedAdmitDate;
        try {
            String pv2Segment = adtMessage.getPV2().encode();
            if (pv2Segment.length() > 3) {
                expectedAdmitDate = setExpectedAdmitDate(adtMessage);
            } else {
                expectedAdmitDate = null;
            }
        } catch (HL7Exception e) {
            expectedAdmitDate = null;
        }
        encounter.setExpectedAdmitDate(expectedAdmitDate);
        // creation of a new movement
        Movement movement = new Movement(encounter);
        movement.setTriggerEvent("A05");
        movement.setCreator(creator);
        movement.setLastChanged(new Date());
        movement.setActionType(ActionType.INSERT);
        movement.setPatientClassCode(encounter.getPatientClassCode());
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);
        movement.setSimulatedActor(simulatedActor);
        movement = HL7MessageDecoder.fillMovementWithPV1AndZBE(movement, adtMessage.getPV1(), adtMessage.getZBE());
        try {
            movement.setMovementDate(adtMessage.getEVN().getEventOccurred().getTime().getValueAsDate());
        } catch (NullPointerException e) {
            log.info("Movement effective date is missing");
        } catch (DataTypeException e) {
            log.info(CANNOT_RETRIEVE_MOVEMENT_EFFECTIVE_DATE);
        }
        movement.save(entityManager);

    }

    private Date setExpectedAdmitDate(net.ihe.gazelle.pam.hl7v2.model.v25.message.ADT_A05 adtMessage) {
        Date expectedAdmitDate;
        try {
            expectedAdmitDate = adtMessage.getPV2().getExpectedAdmitDateTime().getTime().getValueAsDate();
        } catch (NullPointerException | DataTypeException e) {
            expectedAdmitDate = null;
        }
        return expectedAdmitDate;
    }

    /**
     * transfer patient (ADT^A02^ADT_A02)
     *
     * @param receivedMessage
     * @return
     */
    private void processTransferEvent(Message receivedMessage) throws HL7v2ParsingException {
        ModelClassFactory mcf = new CustomModelClassFactory(PAM_HL7_PACKAGE_NAME);
        Parser parser = new PipeParser(mcf);
        ADT_A02 adtMessage;
        try {
            adtMessage = (ADT_A02) parser.parse(receivedMessage.encode());
        } catch (HL7Exception e) {
            // cannot encode the received message
            log.error("cannot encode received message");
            throw new UnexpectedMessageStructureException();
        }

        // check the PV1 segment is present and the visit number exists
        isPV1Present(receivedMessage, "A02");
        // extract the visit number and check that this visit exists and is open
        Encounter encounter;
        String visitNumber = getVisitNumberForADTMessage(adtMessage.getPID(), adtMessage.getPV1());
        encounter = EncounterDAO.getOpenEncounterByActorByNumberByStatus(visitNumber, simulatedActor, entityManager,
                null);
        if (encounter == null) {
            // create the patient from the PID segment
            Patient pidPatient = HL7MessageDecoder.createPatientFromPIDSegment(adtMessage.getPID());
            Patient selectedPatient = null;
            // retrieve the patient if it is known by the application
            for (PatientIdentifier pid : pidPatient.getPatientIdentifiers()) {
                if (pid.alreadyExists() != null) {
                    selectedPatient = PatientDAO.getActivePatientByIdentifierByActor(pid, entityManager,
                            simulatedActor);
                    break;
                }
            }
            // if the patient is unknown, create it using PID segment
            if (selectedPatient == null) {
                selectedPatient = createNewPatientFromPID(pidPatient);
            }
            encounter = createEncounterForPatientFromPV1Segment(adtMessage.getPV1(), selectedPatient);
            encounter.setOpen(true);
            encounter.setCreator(creator);
            encounter.setPatientStatus(PatientStatus.STATUS_ADMITTED);
            encounter.setSimulatedActor(simulatedActor);
            encounter = encounter.save(entityManager);

        }
        Movement movement = new Movement(encounter);
        movement = HL7MessageDecoder.fillMovementWithPV1AndZBE(movement, adtMessage.getPV1(), adtMessage.getZBE());
        movement.setTriggerEvent("A02");
        movement.setCreator(creator);
        movement.setLastChanged(new Date());
        movement.setActionType(ActionType.INSERT);
        movement.setPatientClassCode(encounter.getPatientClassCode());
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);
        movement.setSimulatedActor(simulatedActor);
        try {
            movement.setMovementDate(adtMessage.getEVN().getEventOccurred().getTime().getValueAsDate());
        } catch (NullPointerException e) {
            log.info("Movement effective date is missing");
        } catch (DataTypeException e) {
            log.info(CANNOT_RETRIEVE_MOVEMENT_EFFECTIVE_DATE);
        }
        closePreviousMovement(movement, true);
    }

    /**
     * <p>Getter for the field <code>newPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getNewPatient() {
        return newPatient;
    }

    /**
     * <p>Getter for the field <code>deactivatedPatient</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.Patient} object.
     */
    public Patient getDeactivatedPatient() {
        return deactivatedPatient;
    }

    private void closePreviousMovement(Movement insertedMovement, boolean copyLocation) {
        Movement previousMovement = insertedMovement.getPreviousValidMovement(entityManager, true);
        if (previousMovement != null) {
            previousMovement.setLastChanged(new Date());
            previousMovement.setCurrentMovement(false);
            previousMovement.save(entityManager);
            if (copyLocation && (insertedMovement.getPriorPatientLocation() == null) || insertedMovement.getPriorPatientLocation().isEmpty()) {
                insertedMovement.setPriorPatientLocation(previousMovement.getAssignedPatientLocation());
            }
        }
        insertedMovement.save(entityManager);
    }

    private Patient createNewPatientFromPID(Patient pidPatient) {
        return createNewPatientFromPID(pidPatient, false);
    }

    private Patient createNewPatientFromPID(Patient pidPatient, boolean isMessageBp6Compliant) {
        List<PatientIdentifier> mergedIdentifiers = PatientIdentifierDAO
                .savePatientIdentifierList(getPatientIdentifierListWithoutInsIds(pidPatient
                        .getPatientIdentifiers(), isMessageBp6Compliant));
        pidPatient.setSimulatedActor(simulatedActor);
        pidPatient.setCreator(creator);
        pidPatient.setStillActive(true);
        pidPatient.setPatientIdentifiers(mergedIdentifiers);
        return pidPatient.savePatient(entityManager);
    }

    private Movement newMovement(Encounter encounter, PV1 pv1, ZBE zbe, EVN evn, ActionType type, String trigger) {
        Movement movement = new Movement(encounter);
        movement = HL7MessageDecoder.fillMovementWithPV1AndZBE(movement, pv1, zbe);
        movement.setTriggerEvent(trigger);
        movement.setCreator(creator);
        movement.setLastChanged(new Date());
        movement.setActionType(type);
        movement.setPatientClassCode(encounter.getPatientClassCode());
        movement.setCurrentMovement(true);
        movement.setPendingMovement(false);
        movement.setSimulatedActor(simulatedActor);
        try {
            movement.setMovementDate(evn.getEventOccurred().getTime().getValueAsDate());
        } catch (NullPointerException e) {
            log.info("Movement effective date is missing");
        } catch (DataTypeException e) {
            log.info(CANNOT_RETRIEVE_MOVEMENT_EFFECTIVE_DATE);
        }
        return movement;
    }

    /**
     * @return PipeParser
     * @deprecated replace by ADTParser
     */
    private static PipeParser getParserForPAMMessage() {
        ModelClassFactory mcf = new CustomModelClassFactory(PAM_HL7_PACKAGE_NAME);
        PipeParser parser = new PipeParser(mcf);
        parser.getParserConfiguration().setValidating(false);
        parser.getParserConfiguration().setEncodeEmptyMandatoryFirstSegments(true);
        return parser;
    }

    private void logAction(String trigger, AcknowledgmentCode ackCode, String comment) {
        String sut = creator;
        ReceiverConsole.newLog(simulatedActor, transaction, domain, trigger, messageControlId, sut, ackCode.name(), comment, entityManager);
    }
}
