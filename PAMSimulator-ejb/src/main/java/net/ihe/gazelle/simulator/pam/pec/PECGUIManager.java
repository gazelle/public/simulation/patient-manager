package net.ihe.gazelle.simulator.pam.pec;

import net.ihe.gazelle.HL7Common.action.SimulatorResponderConfigurationDisplay;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * <p>PECGUIManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pecGUIManagerBean")
@Scope(ScopeType.PAGE)
public class PECGUIManager extends SimulatorResponderConfigurationDisplay {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private Actor actor;

	/** {@inheritDoc} */
	@Override
    public String getUrlForHL7Messages(SimulatorResponderConfiguration conf) {
        return "/messages/browser.seam?simulatedActor=" + conf.getSimulatedActor().getId()
                + "&transaction=" + conf.getTransaction().getId()
                + "&domain=" + conf.getDomain().getId();
    }

	/** {@inheritDoc} */
	@Override
	@Create
	public void getSimulatorResponderConfiguration() {
        actor = Actor.findActorWithKeyword("PEC");
		getSimulatorResponderConfigurations(actor, null, null);
	}

    /**
     * <p>linkToPatients.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String linkToPatients(){
        return "/patient/allPatients.seam?actor=" + actor.getId();
    }
}
