package net.ihe.gazelle.simulator.adapter.gui.pdqm;

import net.ihe.gazelle.simulator.adapter.gui.SimulationBean;
import net.ihe.gazelle.simulator.application.TransactionInstanceFinder;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pdqm.pdc.PDCMessagingClient;
import net.ihe.gazelle.simulator.pdqm.pdc.exception.ConnectionException;
import net.ihe.gazelle.simulator.pdqm.pdc.exception.SimulationException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Page scoped Bean to control the simulator page for PDQm PDC.
 */
@Name("pdqmSimulationBean")
@Scope(ScopeType.PAGE)
public class PDQmSimulationBean implements SimulationBean {

    private static final Logger LOG = LoggerFactory.getLogger(PDQmSimulationBean.class);

    @In(create = true, value = "pdqmMessageConfigurationBean")
    private PDQmMessageConfigurationBean pdqmMessageConfigurationBean;

    @In(create = true, value = "pdqmSUTSelectionBean")
    private PDQmSUTSelectionBean pdqmSUTSelectionBean;

    @In(create = true, value = "transactionInstanceFinder")
    private TransactionInstanceFinder transactionInstanceFinder;

    @In(create = true, value = "pdcMessagingClient")
    private PDCMessagingClient pdcMessagingClient;

    private List<TransactionInstance> messages;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TransactionInstance> getMessages() {
        return this.messages;
    }

    /**
     * Add a message to the messages property.
     *
     * @param message {@link TransactionInstance} to add to the list.
     */
    public void addMessage(TransactionInstance message) {
        if (this.messages == null) {
            messages = new ArrayList<>();
        }
        messages.add(message);
    }

    /**
     * Getter for the pdqmMessageConfigurationBean property.
     *
     * @return the value of the property.
     */
    public PDQmMessageConfigurationBean getPdqmMessageConfigurationBean() {
        return pdqmMessageConfigurationBean;
    }

    /**
     * Setter for the pdqmMessageConfigurationBean property.
     *
     * @param pdqmMessageConfigurationBean value to set to the property.
     */
    public void setPdqmMessageConfigurationBean(PDQmMessageConfigurationBean pdqmMessageConfigurationBean) {
        this.pdqmMessageConfigurationBean = pdqmMessageConfigurationBean;
    }

    /**
     * Getter for the pdqmSUTSelectionBean property.
     *
     * @return the value of the property.
     */
    public PDQmSUTSelectionBean getPdqmSUTSelectionBean() {
        return pdqmSUTSelectionBean;
    }

    /**
     * Setter for the pdqmSUTSelectionBean property.
     *
     * @param pdqmSUTSelectionBean value to set to the property.
     */
    public void setPdqmSUTSelectionBean(PDQmSUTSelectionBean pdqmSUTSelectionBean) {
        this.pdqmSUTSelectionBean = pdqmSUTSelectionBean;
    }

    /**
     * Getter for the transactionInstanceFinder property.
     *
     * @return the value of the property.
     */
    public TransactionInstanceFinder getTransactionInstanceFinder() {
        return transactionInstanceFinder;
    }

    /**
     * Setter for the transactionInstanceFinder property.
     *
     * @param transactionInstanceFinder value to set to the property.
     */
    public void setTransactionInstanceFinder(TransactionInstanceFinder transactionInstanceFinder) {
        this.transactionInstanceFinder = transactionInstanceFinder;
    }

    /**
     * Getter for the pdcMessagingClient property.
     *
     * @return the value of the property.
     */
    public PDCMessagingClient getPdcMessagingClient() {
        return pdcMessagingClient;
    }

    /**
     * Setter for the pdcMessagingClient property.
     *
     * @param pdcMessagingClient value to set to the property.
     */
    public void setPdcMessagingClient(PDCMessagingClient pdcMessagingClient) {
        this.pdcMessagingClient = pdcMessagingClient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send() {
        String patResourceId = pdqmMessageConfigurationBean.getPatientIdentifier();
        String sutEndpoint = pdqmSUTSelectionBean.getSelectedSUT().getEndpoint();

        try {
            String transactionInstanceID = pdcMessagingClient.query(patResourceId, sutEndpoint);

            TransactionInstance transactionInstance = transactionInstanceFinder.retreiveRecordById(transactionInstanceID);
            this.addMessage(transactionInstance);
        } catch (MalformedURLException e) {
            LOG.error("Error trying to send message :", e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
        } catch (SimulationException e) {
            LOG.error("Error trying to send message :", e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, String.format("Simulation Exception with message : %s", e.getMessage()));
        } catch (ConnectionException e) {
            LOG.error("Error trying to send message :", e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, String.format("Cannot connect with PDQm Connector : %s", e.getMessage()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void resetForm() {
        pdqmMessageConfigurationBean.setPatientIdentifier(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void performAnotherTest() {
        this.messages = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTitle() {
        return "PDQm Patient Demographics Consumer";
    }
}
