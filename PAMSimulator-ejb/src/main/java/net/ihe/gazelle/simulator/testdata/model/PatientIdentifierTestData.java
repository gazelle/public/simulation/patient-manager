package net.ihe.gazelle.simulator.testdata.model;

import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.jboss.seam.annotations.Name;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * Created by aberge on 08/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Entity
@Name("patientIdentifierTestData")
@DiscriminatorValue("patientIdentifier")
public class PatientIdentifierTestData extends TestData {

    @OneToOne(targetEntity = PatientIdentifier.class)
    @JoinColumn(name = "patient_identifier_id")
    @Cascade(CascadeType.MERGE)
    private PatientIdentifier patientIdentifier;

    /**
     * <p>Constructor for PatientIdentifierTestData.</p>
     */
    public PatientIdentifierTestData(){
        super();
    }

    /**
     * <p>Constructor for PatientIdentifierTestData.</p>
     *
     * @param identifier a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     */
    public PatientIdentifierTestData(PatientIdentifier identifier) {
        this.patientIdentifier = identifier;
    }

    /**
     * <p>Getter for the field <code>patientIdentifier</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * <p>Setter for the field <code>patientIdentifier</code>.</p>
     *
     * @param patientIdentifier a {@link net.ihe.gazelle.simulator.pam.model.PatientIdentifier} object.
     */
    public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
        this.patientIdentifier = patientIdentifier;
    }
}
