package net.ihe.gazelle.simulator.pam.hl7;

import net.ihe.gazelle.simulator.utils.PatientManagerConstants;

public class IHEHL7Domain implements HL7Domain<AbstractHL7MessageGenerator> {
    private AbstractHL7MessageGenerator generator;

    public IHEHL7Domain() {
        generator = new IHEHL7MessageGenerator();
    }

    @Override
    public AbstractHL7MessageGenerator getFrameGenerator() {
        return generator;
    }

    @Override
    public String getDomainKeyword() {
        return PatientManagerConstants.ITI;
    }
}
