package net.ihe.gazelle.simulator.cat.action;

import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.cat.dao.SystemUnderTestPreferencesDAO;
import net.ihe.gazelle.simulator.cat.model.EConnectathonMessageType;
import net.ihe.gazelle.simulator.cat.model.SharingResult;
import net.ihe.gazelle.simulator.cat.model.SystemUnderTestPreferences;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerAuthorizations;
import net.ihe.gazelle.simulator.pam.menu.PatientManagerPages;
import net.ihe.gazelle.simulator.pam.menu.SimulatorFeatureDAO;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.sut.model.SystemConfigurationQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <p>PatientSharingManager class.</p>
 *
 * @author aberge
 * @version 1.0: 29/11/17
 */

@Name("patientSharingManager")
@Scope(ScopeType.PAGE)
public class PatientSharingManager implements Serializable, QueryModifier<Patient> {


    private List<SystemUnderTestPreferences> selectedSystems;
    private List<SystemConfiguration> availableSystemsUnderTest;
    private SystemConfiguration systemToAdd;

    private FilterDataModel<Patient> selectedPatients;
    private SystemUnderTestPreferences selectedPreferences;
    private Integer singlePatientId;
    private boolean bp6Mode;

    @In(create = true)
    public CATMessageSenderLocal catMessageSender;

    private SharingResult sharingResult = new SharingResult();

    private boolean autoRefresh;


    @Create
    public void getPatientListFromUrl() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey("id")) {
            String idAsString = urlParams.get("id");
            singlePatientId = Integer.decode(idAsString);
        } else {
            singlePatientId = null;
        }
        Filter<Patient> filter = new Filter<Patient>(getCriterionsForFilter(), urlParams);
        selectedPatients = new FilterDataModel<Patient>(filter) {
            @Override
            protected Object getId(Patient patient) {
                return patient.getId();
            }
        };
        availableSystemsUnderTest = initializeListOfSystems();
        selectedSystems = new ArrayList<SystemUnderTestPreferences>();
        autoRefresh = true;
    }

    private List<SystemConfiguration> initializeListOfSystems() {
        SystemConfigurationQuery query = new SystemConfigurationQuery();
        query.isAvailable().eq(true);
        query.name().order(true);
        query.listUsages().transaction().keyword().in(EConnectathonMessageType.getListOfTransactions());
        return query.getList();
    }

    public void addSelectedSystemToList() {
        if (systemToAdd != null) {
            SystemUnderTestPreferences preferencesToAdd = SystemUnderTestPreferencesDAO.getPreferencesForSUT(systemToAdd);
            if (preferencesToAdd == null) {
                preferencesToAdd = SystemUnderTestPreferencesDAO.createPreferencesForSUT(systemToAdd);
            }
            selectedSystems.add(preferencesToAdd);
            availableSystemsUnderTest.remove(systemToAdd);
            systemToAdd = null;
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "First select a system in the list and retry");
        }
    }

    public String getUrlForSharing() {
        String baseUrl = PreferenceService.getString("application_url");
        StringBuilder url = new StringBuilder(baseUrl);
        url.append(PatientManagerPages.CAT_SHARE.getLink());
        url.append('?');
        if (singlePatientId != null) {
            url.append("id=");
            url.append(singlePatientId);
        } else {
            url.append(selectedPatients.getFilter().getUrlParameters());
        }
        return url.toString();
    }

    private HQLCriterionsForFilter<Patient> getCriterionsForFilter() {
        PatientQuery query = new PatientQuery();
        HQLCriterionsForFilter<Patient> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("firstname", query.firstName());
        criteria.addPath("lastname", query.lastName());
        criteria.addPath("owner", query.creator());
        criteria.addPath("gender", query.genderCode());
        criteria.addPath("country", query.countryCode());
        criteria.addPath("mothersmaidenname", query.motherMaidenName());
        criteria.addPath("testData", query.testData(), false, false);
        criteria.addPath("actor", query.simulatedActor());
        criteria.addPath("active", query.stillActive(), true, true);
        criteria.addQueryModifier(this);
        return criteria;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<Patient> queryBuilder, Map<String, Object> params) {
        PatientQuery query = new PatientQuery();
        if (params.containsKey("pidpart")) {
            String searchedPatientId = (String) params.get("pidpart");
            if (searchedPatientId != null && !searchedPatientId.isEmpty()) {
                queryBuilder.addRestriction(query.patientIdentifiers().fullPatientId().likeRestriction(searchedPatientId, HQLRestrictionLikeMatchMode
                        .ANYWHERE));
            }
        }
        if (singlePatientId != null) {
            queryBuilder.addRestriction(query.id().eqRestriction(singlePatientId));
        }
    }

    public void removeSUTFromList(SystemUnderTestPreferences preference) {
        selectedSystems.remove(preference);
        availableSystemsUnderTest.add(preference.getSystemUnderTest());
        Collections.sort(availableSystemsUnderTest);
    }

    public List<SystemUnderTestPreferences> getSelectedSystems() {
        return selectedSystems;
    }

    public List<SystemConfiguration> getAvailableSystemsUnderTest() {
        return availableSystemsUnderTest;
    }

    public FilterDataModel<Patient> getSelectedPatients() {
        return selectedPatients;
    }

    public SystemConfiguration getSystemToAdd() {
        return systemToAdd;
    }

    public void setSystemToAdd(SystemConfiguration systemToAdd) {
        this.systemToAdd = systemToAdd;
    }

    public String getEndpointToDisplay(SystemUnderTestPreferences sutPreferences) {
        String endpoint = null;
        if (sutPreferences != null && sutPreferences.getSystemUnderTest() != null) {
            SystemConfiguration sut = sutPreferences.getSystemUnderTest();
            if (sut instanceof HL7V2ResponderSUTConfiguration) {
                HL7V2ResponderSUTConfiguration hl7v2sut = (HL7V2ResponderSUTConfiguration) sut;
                endpoint = hl7v2sut.getIpAddress() + ":" + hl7v2sut.getPort();
            } else {
                endpoint = sut.getEndpoint();
            }
        }
        return endpoint;
    }

    public boolean isBp6Mode() {
        return bp6Mode;
    }

    public void setBp6Mode(boolean bp6Mode) {
        this.bp6Mode = bp6Mode;
    }

    public boolean isBp6Authorized() {
        return SimulatorFeatureDAO.isFeatureEnabled(PatientManagerAuthorizations.BP6);
    }

    public void updateSUTPref(SystemUnderTestPreferences systemUnderTestPreferences) {
        SystemUnderTestPreferencesDAO.save(systemUnderTestPreferences);
    }

    public void savePreferences() {
        updateSUTPref(selectedPreferences);
        selectedPreferences = null;
    }

    public SystemUnderTestPreferences getSelectedPreferences() {
        return selectedPreferences;
    }

    public void setSelectedPreferences(SystemUnderTestPreferences selectedPreferences) {
        this.selectedPreferences = selectedPreferences;
    }

    public List<HierarchicDesignator> getAvailableAssigningAuthorities() {
        return HierarchicDesignatorDAO.getToolAssigningAuthoritiesForPatientId();
    }

    public void emptySUTList() {
        this.selectedSystems = new ArrayList<SystemUnderTestPreferences>();
    }

    public void sendMessages() {
        sharingResult.setSelectedPatients(getPatientsAsList());
        sharingResult.setSelectedSystems(selectedSystems);
        catMessageSender.setBp6Mode(bp6Mode);
        catMessageSender.sendMessages(sharingResult);
    }

    public List<Patient> getPatientsAsList() {
        return selectedPatients.getAllItems(FacesContext.getCurrentInstance());
    }

    public GazelleListDataModel<TransactionInstance> getMessages() {
        return new GazelleListDataModel<TransactionInstance>(sharingResult.getTransactionInstances());
    }

    public boolean hasMessages() {
        return sharingResult.getTransactionInstances().size() > 0;
    }

    public void sendMessage(SystemUnderTestPreferences preferences) {
        sharingResult.setSelectedPatients(getPatientsAsList());
        sharingResult.setSingleSystem(preferences);
        catMessageSender.setBp6Mode(bp6Mode);
        catMessageSender.sendMessages(sharingResult);
    }

    public void clearMessages() {
        sharingResult.clearMessageList();
    }

    public List<String> getIssues() {
        return sharingResult.getIssues();
    }

    public boolean isAutoRefresh() {
        return autoRefresh;
    }

    public void setAutoRefresh(boolean autoRefresh) {
        this.autoRefresh = autoRefresh;
    }

    public boolean areAllMessagesSent() {
        return this.sharingResult.isProcessEnded();
    }
}
