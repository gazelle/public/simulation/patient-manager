package net.ihe.gazelle.simulator.pam.automaton.Manager;

import net.ihe.gazelle.simulator.pam.automaton.helper.GraphWalkerInitialisationException;
import org.graphwalker.core.machine.Context;
import org.graphwalker.core.model.Element;
import org.graphwalker.dsl.antlr.DslException;
import org.graphwalker.dsl.antlr.generator.GeneratorFactory;
import org.graphwalker.io.factory.ContextFactoryException;
import org.graphwalker.io.factory.yed.YEdContextFactory;
import org.graphwalker.java.test.ContextConfiguration;
import org.graphwalker.java.test.TestExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by jlabbe on 06/11/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ContextExecutor {

    private static final Logger LOG = LoggerFactory.getLogger(ContextExecutor.class);

    /**
     * <p>create.</p>
     *
     * @param clazz              a {@link java.lang.Class} object.
     * @param isFullEdgeCoverage a boolean.
     * @param graphmlLink        a {@link java.lang.String} object.
     *
     * @return a {@link org.graphwalker.core.machine.Context} object.
     *
     * @throws java.lang.IllegalAccessException if any.
     * @throws java.lang.InstantiationException if any.
     */
    public static Context create(Class clazz, boolean isFullEdgeCoverage, String graphmlLink) throws IllegalAccessException,
            GraphWalkerInitialisationException {

        ContextConfiguration contextConfiguration = new ContextConfiguration();
        contextConfiguration.setTestClass(clazz);
        Context context;
        try {
            context = (Context) (contextConfiguration.getTestClass().newInstance());
            configureContext(context, isFullEdgeCoverage, graphmlLink);
        } catch (InstantiationException e) {
            throw new GraphWalkerInitialisationException(e);
        }

        return context;
    }

    private static void configureContext(Context context, boolean isFullEdgeCoverage, String graphmlLink) throws GraphWalkerInitialisationException {

        Path path = Paths.get(graphmlLink);
        if (!path.toFile().exists()) {
            throw new GraphWalkerInitialisationException(graphmlLink + " does not exist");
        } else {
            YEdContextFactory f = new YEdContextFactory();
            try {
                f.create(path, context);
                if (isFullEdgeCoverage) {
                    context.setPathGenerator(GeneratorFactory.parse("random(edge_coverage(100))"));
                } else {
                    context.setPathGenerator(GeneratorFactory.parse("random(vertex_coverage(100))"));
                }
                context.setNextElement(getElement(context.getModel(), "ini"));
            } catch (ContextFactoryException e) {
                throw new GraphWalkerInitialisationException("The tool did not manage to initialize the automaton: " + e.getMessage(), e);
            } catch (TestExecutionException | DslException e) {
                throw new GraphWalkerInitialisationException("An error occurred during automaton execution, the graphml file might " +
                        "not be valid: " + e.getMessage(), e);
            }
        }
    }

    private static Element getElement(org.graphwalker.core.model.Model.RuntimeModel model, String name) {
        List<Element> elements = model.findElements(name);
        if (null == elements || 0 == elements.size()) {
            throw new TestExecutionException("Start element not found");
        }
        if (1 < elements.size()) {
            throw new TestExecutionException("Ambiguous start element defined");
        }
        return elements.get(0);
    }

}
