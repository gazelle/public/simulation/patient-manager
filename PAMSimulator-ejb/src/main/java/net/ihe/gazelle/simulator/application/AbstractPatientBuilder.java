package net.ihe.gazelle.simulator.application;

import net.ihe.gazelle.patient.PatientGenerator;
import net.ihe.gazelle.simulator.pam.dao.HierarchicDesignatorDAO;
import net.ihe.gazelle.simulator.pam.dao.PatientIdentifierDAO;
import net.ihe.gazelle.simulator.pam.hl7.BP6HL7Domain;
import net.ihe.gazelle.simulator.pam.iti31.action.NumberGenerator;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import org.jboss.seam.security.Identity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class AbstractPatientBuilder extends PatientGenerator implements PatientBuilder {

    protected Patient newPatient(PatientBuilder.Context c) {
        Patient newPatient;
        String username = null;
        if (Identity.instance().getCredentials() != null) {
            username = Identity.instance().getCredentials().getUsername();
        }
        if (c.aPatient != null) {
            newPatient = new Patient(c.aPatient);
            newPatient.setAccountNumber(NumberGenerator.generate(DesignatorType.ACCOUNT_NUMBER));
            newPatient.setSimulatedActor(c.sendingActor);
            newPatient.setStillActive(true);
            newPatient.setCreator(username);
            newPatient.setCreationDate(new Date());
            generatePatientIdentifiers(newPatient, c.selectedAuthorities, c.useIdsFromDDS, c.generateIdsForAuthorities, c.generateIdsForConnectathon);
        } else {
            newPatient = null;
        }
        return newPatient;
    }

    /**
     * <p>generatePatientIdentifiers.</p>
     *
     * @param newPatient a {@link Patient} object.
     */
    public void generatePatientIdentifiers(Patient newPatient,
                                           List<HierarchicDesignator> selectedAuthorities,
                                           boolean useIdsFromDDS,
                                           boolean generateIdsForAuthorities,
                                           boolean generateIdsForConnectathon
    ) {
        boolean generated = false;
        newPatient.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
        if (useIdsFromDDS) {
            List<PatientIdentifier> pidList = PatientIdentifierDAO.createListFromNationalAndDDSIdentifiers(
                    newPatient.getNationalPatientIdentifier(), newPatient.getDdsIdentifier());
            if (pidList != null) {
                newPatient.setPatientIdentifiers(pidList);
                generated = true;
            }
        }
        if (generateIdsForAuthorities && (selectedAuthorities != null && !selectedAuthorities.isEmpty())) {
            generateAdditionalIdentifiers(newPatient, selectedAuthorities);
            generated = true;
        }
        if (generateIdsForConnectathon) {
            generateAdditionalIdentifiersForConnectathon(newPatient);
        }
        if (!generated) {
            String newid = NumberGenerator.generate(DesignatorType.PATIENT_ID);
            HierarchicDesignator hd = HierarchicDesignatorDAO.getAssigningAuthority(DesignatorType.PATIENT_ID);
            PatientIdentifier pid = new PatientIdentifier(newid, hd.getType());
            pid.setDomain(hd);
            newPatient.getPatientIdentifiers().add(pid);
        }
    }

    private void generateAdditionalIdentifiersForConnectathon(Patient newPatient) {
        List<HierarchicDesignator> connectathonAuthorities = HierarchicDesignatorDAO.getAssigningAuthoritiesForConnectathon();
        if (connectathonAuthorities != null) {
            for (HierarchicDesignator catAA : connectathonAuthorities) {
                generateIdForAuthority(newPatient, catAA);
            }
        }
    }

    @Override
    public Patient buildIdentifiers(PatientBuilder.IdentifierContext context) {
        generatePatientIdentifiers((Patient)context.aPatient,
                context.selectedAuthorities,
                context.useIdsFromDDS,
                context.generateIdsForAuthorities,
                context.generateIdsForConnectathon);
        return (Patient)context.aPatient;
    }

    public Patient buildIdentifiers(Patient patient, List<HierarchicDesignator> authorities) {
        generateAdditionalIdentifiers(patient, authorities);
        return patient;
    }

    /**
     * <p>generateAdditionalIdentifiers.</p>
     *
     * @param inPatient a {@link Patient} object.
     */
    public void generateAdditionalIdentifiers(Patient inPatient, List<HierarchicDesignator> selectedAuthorities) {
        for (HierarchicDesignator hd : selectedAuthorities) {
            if (!BP6HL7Domain.INS.getAuthorities().contains(hd.getUniversalID())) {
                generateIdForAuthority(inPatient, hd);
            }
        }
    }

    private void generateIdForAuthority(Patient inPatient, HierarchicDesignator hd) {
        String newid = HierarchicDesignatorDAO.generatePatientIdentifier(hd);
        PatientIdentifier pid = new PatientIdentifier(newid, hd.getType());
        pid.setDomain(hd);
        inPatient.getPatientIdentifiers().add(pid);
    }
}
