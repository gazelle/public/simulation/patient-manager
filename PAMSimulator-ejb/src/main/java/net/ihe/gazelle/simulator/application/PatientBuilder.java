package net.ihe.gazelle.simulator.application;

import net.ihe.gazelle.demographic.ws.CountryCode;
import net.ihe.gazelle.demographic.ws.DemographicDataServerServiceStub;
import net.ihe.gazelle.patient.AbstractPatient;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.pam.action.PatientSelectorManager;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;

import java.util.List;

public interface PatientBuilder {
    class Context {
        Actor sendingActor;
        AbstractPatient aPatient;
        List<HierarchicDesignator> selectedAuthorities;
        boolean useIdsFromDDS;
        boolean generateIdsForAuthorities;
        boolean generateIdsForConnectathon;
        boolean isQualifiedIdentity;
        CountryCode selectedCountry;
        DemographicDataServerServiceStub ddsStub;

        protected Context(Actor sendingActor, AbstractPatient patient, List<HierarchicDesignator> selectedAuthorities, boolean useIdsFromDDS, boolean generateIdsForAuthorities, boolean generateIdsForConnectathon, boolean isQualifiedIdentity, CountryCode selectedCountry,DemographicDataServerServiceStub ddsStub) {
            this.sendingActor = sendingActor;
            this.aPatient = patient;
            this.selectedAuthorities = selectedAuthorities;
            this.useIdsFromDDS = useIdsFromDDS;
            this.generateIdsForAuthorities = generateIdsForAuthorities;
            this.generateIdsForConnectathon = generateIdsForConnectathon;
            this.isQualifiedIdentity = isQualifiedIdentity;
            this.selectedCountry = selectedCountry;
            this.ddsStub = ddsStub;
        }

        public Context(PatientSelectorManager manager) {
            this(   manager.getSendingActor(),
                    manager.generatePatient(),
                    manager.getSelectedAuthorities(),
                    manager.isUseIdsFromDDS(),
                    manager.isGenerateIdsForAuthorities(),
                    manager.isGenerateIdsForConnectathon(),
                    manager.isQualifiedIdentity(),
                    manager.getSelectedCountry(),
                    manager.getDdsStub()
            );
        }
    }

    class IdentifierContext extends Context {
        public IdentifierContext(Patient patient, List<HierarchicDesignator> selectedAuthorities, boolean useIdsFromDDS, boolean generateIdsForAuthorities, boolean generateIdsForConnectathon, boolean isQualifiedIdentity) {
            super(null, patient, selectedAuthorities, useIdsFromDDS, generateIdsForAuthorities, generateIdsForConnectathon, isQualifiedIdentity, null, null);
        }
    }

    class GenerationException extends RuntimeException {
        public GenerationException(String message) {
            super(message);
        }
        public GenerationException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    Patient build(PatientBuilder.Context context);

    Patient buildIdentifiers(Patient patient, List<HierarchicDesignator> selectedAuthorities);

    Patient buildIdentifiers(PatientBuilder.IdentifierContext context);
}