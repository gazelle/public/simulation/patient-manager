package net.ihe.gazelle.simulator.hl7v3.responder;

import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Device;
import net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Receiver;
import net.ihe.gazelle.hl7v3.mccimt000100UV01.MCCIMT000100UV01Sender;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type;
import net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02QUQIMT021001UV01ControlActProcess;
import net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02Type;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02Type;
import net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02DataSource;
import net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02PatientIdentifier;
import net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02QueryByParameter;
import net.ihe.gazelle.hl7v3transformer.HL7V3Transformer;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.hl7v3.initiator.HL7V3Utils;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3AcknowledgmentCode;
import net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException;
import net.ihe.gazelle.simulator.pam.model.DesignatorType;
import net.ihe.gazelle.simulator.pam.model.HierarchicDesignator;
import net.ihe.gazelle.simulator.pam.model.Patient;
import net.ihe.gazelle.simulator.pam.model.PatientIdentifier;
import net.ihe.gazelle.simulator.pam.model.PatientQuery;
import net.ihe.gazelle.simulator.pixv3.manager.PRPAIN201301UV02Parser;
import net.ihe.gazelle.simulator.pixv3.manager.PRPAIN201302UV02Parser;
import net.ihe.gazelle.simulator.pixv3.manager.PRPAIN201304UV02Parser;
import net.ihe.gazelle.simulator.utils.PatientManagerConstants;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import javax.xml.ws.handler.MessageContext;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by aberge on 12/03/15.
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PIXV3QueryHandler extends QueryHandler {

    /**
     * Constant <code>NULL_REQUEST_ERROR_MESSAGE="Simulator cannot handle null requests"</code>
     */
    public static final String NULL_REQUEST_ERROR_MESSAGE = "Simulator cannot handle null requests";
    /**
     * Constant <code>NO_SENDER_DEVICE_ID_ERROR_MESSAGE="No Sender/device/id element in request"</code>
     */
    public static final String NO_SENDER_DEVICE_ID_ERROR_MESSAGE = "No Sender/device/id element in request";
    /**
     * Constant <code>NO_RECEIVER_WARN_MESSAGE="Receiver is either not present or malfo"{trunked}</code>
     */
    public static final String NO_RECEIVER_WARN_MESSAGE = "Receiver is either not present or malformed";
    public static final String SPID_ROOT = "2.16.756.5.30.1.127.3.10.3";
    private static Logger log = Logger.getLogger(PIXV3QueryHandler.class);

    /**
     * <p>Constructor for PIXV3QueryHandler.</p>
     *
     * @param inDomain         a {@link Domain} object.
     * @param inSimulatedActor a {@link Actor} object.
     * @param inSutActor       a {@link Actor} object.
     * @param inTransaction    a {@link Transaction} object.
     * @param servletRequest   a {@link String} object.
     * @param messageContext   a {@link MessageContext} object.
     */
    public PIXV3QueryHandler(Domain inDomain, Actor inSimulatedActor, Actor inSutActor, Transaction inTransaction,
                             HttpServletRequest servletRequest, MessageContext messageContext) {
        super(inDomain, inSimulatedActor, inSutActor, inTransaction, servletRequest, messageContext);
    }

    /**
     * <p>Constructor for PIXV3QueryHandler.</p>
     *
     * @param inDomain         a {@link Domain} object.
     * @param inSimulatedActor a {@link Actor} object.
     * @param inSutActor       a {@link Actor} object.
     * @param inTransaction    a {@link Transaction} object.
     * @param servletRequest   a {@link String} object.
     */
    public PIXV3QueryHandler(Domain inDomain, Actor inSimulatedActor, Actor inSutActor, Transaction inTransaction,
                             HttpServletRequest servletRequest) {
        super(inDomain, inSimulatedActor, inSutActor, inTransaction, servletRequest);
    }

    /**
     * <p>handleGetCorrespondingIdentifiersQuery.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02Type} object.
     * @throws net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException if any.
     */
    public PRPAIN201310UV02Type handleGetCorrespondingIdentifiersQuery(PRPAIN201309UV02Type request) throws HL7V3ParserException {
        if (request == null) {
            log.error(NULL_REQUEST_ERROR_MESSAGE);
            throw new HL7V3ParserException("/", NULL_REQUEST_ERROR_MESSAGE);
        } else {
            PRPAIN201310UV02Type response = new PRPAIN201310UV02Type();
            instance.getResponse().setType(PatientManagerConstants.PRPA_IN_201310_UV_02);
            // store request in TransactionInstance object
            ByteArrayOutputStream requestStream = new ByteArrayOutputStream();
            try {
                HL7V3Transformer.marshallMessage(PRPAIN201309UV02Type.class, requestStream, request);
            } catch (JAXBException e) {
                throw new HL7V3ParserException("/", e.getMessage(), e);
            }
            instance.getRequest().setContent(requestStream.toByteArray());
            instance.getRequest().setType(PatientManagerConstants.PRPA_IN_201309_UV_02);
            String sutOid = null;
            try {
                sutOid = request.getSender().getDevice().getId().get(0).getRoot();
            } catch (Exception e) {
                log.warn("Sender/device/id is not present in the query");
            }
            PRPAIN201309UV02QUQIMT021001UV01ControlActProcess controlActProcess = request.getControlActProcess();
            if (controlActProcess == null) {
                response = buildAEResponse("/PRPA_IN201309UV02", request);
            } else {
                PRPAMT201307UV02QueryByParameter parameters = controlActProcess.getQueryByParameter();
                String extension = null;
                String root = null;
                if (parameters == null) {
                    response = PIXV3ResponseBuilder
                            .buildPRPAIN2010UV02Message(request.getId(), null, null, null, null,
                                    sutOid);
                } else {
                    if (parameters.getParameterList() != null) {
                        List<PRPAMT201307UV02PatientIdentifier> searchedIds = parameters.getParameterList()
                                .getPatientIdentifier();
                        if (searchedIds.size() != 1) {
                            response = buildAEResponse("/PRPA_IN201309UV02/controlActProcess/queryByParameter/parameterList",
                                    request);
                            return sendResponse(PRPAIN201310UV02Type.class, response);
                        } else {
                            PRPAMT201307UV02PatientIdentifier searchedId = searchedIds.get(0);
                            if (searchedId.getValue().size() == 1) {
                                extension = searchedId.getValue().get(0).getExtension();
                                root = searchedId.getValue().get(0).getRoot();
                            } else {
                                response = buildAEResponse("/PRPA_IN201309UV02/controlActProcess/queryByParameter/parameterList/patientIdentifier",
                                        request);
                                return sendResponse(PRPAIN201310UV02Type.class, response);
                            }
                        }
                        List<HierarchicDesignator> domainsToReturn = new ArrayList<HierarchicDesignator>();
                        List<Integer> unknownDomains = new ArrayList<Integer>();
                        int index = 1;
                        for (PRPAMT201307UV02DataSource source : parameters.getParameterList().getDataSource()) {
                            if (!source.getValue().isEmpty()) {
                                List<HierarchicDesignator> domains = HierarchicDesignator
                                        .getDomainFiltered(null, source.getValue().get(0).getRoot(), null,
                                                EntityManagerService.provideEntityManager(),
                                                DesignatorType.PATIENT_ID);
                                for (HierarchicDesignator domain : domains) {
                                    if (domain == null) {
                                        unknownDomains.add(index);
                                    } else {
                                        domainsToReturn.add(domain);
                                    }
                                }
                            }
                            index++;
                        }
                        if (!unknownDomains.isEmpty()) {
                            response = PIXV3ResponseBuilder
                                    .buildPRPAIN2010UV02Message(request.getId(), null, null, unknownDomains, parameters,
                                            sutOid);
                        } else {
                            // search for patients
                            List<II> identifiersToReturn = new ArrayList<II>();
                            Patient foundPatient = getPatientForIdentifier(root, extension);
                            if (foundPatient == null) {
                                response = PIXV3ResponseBuilder
                                        .buildPRPAIN2010UV02Message(request.getId(), null, null, null, parameters,
                                                sutOid);
                            } else if (foundPatient.getPixReference() == null) {
                                addIdentifierToList(foundPatient.getPatientIdentifiers(), identifiersToReturn,
                                        domainsToReturn, extension, root);
                                response = PIXV3ResponseBuilder
                                        .buildPRPAIN2010UV02Message(request.getId(), foundPatient, identifiersToReturn,
                                                null, parameters, sutOid);
                            } else {
                                for (Patient patient : foundPatient.getPixReference().getPatients()) {
                                    addIdentifierToList(patient.getPatientIdentifiers(), identifiersToReturn,
                                            domainsToReturn, extension, root);
                                }
                                response = PIXV3ResponseBuilder
                                        .buildPRPAIN2010UV02Message(request.getId(), foundPatient, identifiersToReturn,
                                                null, parameters, sutOid);
                            }
                        }
                    }
                }
            }
            sendResponse(PRPAIN201310UV02Type.class, response);
            HL7V3Utils.saveMessageId(request.getId(), instance.getRequest());
            HL7V3Utils.saveMessageId(response.getId(), instance.getResponse());
            return response;
        }

    }

    private PRPAIN201310UV02Type buildAEResponse(String errorMessage, PRPAIN201309UV02Type request) {
        String sutOid = null;
        try {
            sutOid = request.getSender().getDevice().getId().get(0).getRoot();
        } catch (Exception e) {
            log.warn("Sender/device/id is not present in the query");
        }
        PRPAMT201307UV02QueryByParameter queryByParameter = (request.getControlActProcess() != null) ?
                request.getControlActProcess().getQueryByParameter() :
                null;
        return PIXV3ResponseBuilder.buildPRPAIN2010UV02MessageErrorCase(request.getId(), sutOid, errorMessage, queryByParameter);
    }

    private Patient getPatientForIdentifier(String root, String extension) {
        PatientQuery query = new PatientQuery();
        query.patientIdentifiers().idNumber().eq(extension);
        query.patientIdentifiers().domain().universalID().eq(root);
        query.stillActive().eq(true);
        query.simulatedActor().keyword().in(getActorKeywords());
        return query.getUniqueResult();
    }

    /**
     * PAM-510
     *
     * @return
     */
    private List<String> getActorKeywords() {
        return Arrays.asList(PatientManagerConstants.PAT_IDENTITY_X_REF_MGR, PatientManagerConstants.CONNECTATHON);
    }


    private void addIdentifierToList(List<PatientIdentifier> ids, List<II> identifiersToReturn,
                                     List<HierarchicDesignator> domainsToReturn, String extension, String root) {
        for (PatientIdentifier id : ids) {
            if ((domainsToReturn.isEmpty() || domainsToReturn.contains(id.getDomain())) && !id.getIdNumber()
                    .equals(extension) && !id.getDomain().getUniversalID().equals(root)) {
                II ii = new II(id.getDomain().getUniversalID(), id.getIdNumber());
                ii.setAssigningAuthorityName(id.getDomain().getNamespaceID());
                identifiersToReturn.add(ii);
            }
        }
    }

    private MCCIIN000002UV01Type generateDefaultAcknowledgement(PRPAIN201301UV02Type request) throws HL7V3ParserException {
        if (request == null) {
            log.error(NULL_REQUEST_ERROR_MESSAGE);
            throw new HL7V3ParserException("", NULL_REQUEST_ERROR_MESSAGE);
        } else {
            // store request in TransactionInstance object
            ByteArrayOutputStream requestStream = new ByteArrayOutputStream();
            try {
                HL7V3Transformer.marshallMessage(PRPAIN201301UV02Type.class, requestStream, request);
            } catch (JAXBException e) {
                throw new HL7V3ParserException("/", e.getMessage(), e);
            }
            instance.getRequest().setContent(requestStream.toByteArray());
            instance.getRequest().setType(PatientManagerConstants.PRPA_IN_201301_UV_02);
            return getMcciin000002UV01Type(request.getSender(), request.getReceiver(), request.getId());
        }
    }

    private MCCIIN000002UV01Type getMcciin000002UV01Type(MCCIMT000100UV01Sender sender, List<MCCIMT000100UV01Receiver> receiver2, II id) {
        String sutOid = null;
        try {
            sutOid = sender.getDevice().getId().get(0).getRoot();
        } catch (NullPointerException e) {
            log.warn(NO_SENDER_DEVICE_ID_ERROR_MESSAGE);
        }
        MCCIMT000100UV01Device device = null;
        try {
            MCCIMT000100UV01Receiver receiver = receiver2.get(0);
            device = receiver.getDevice();
        } catch (Exception e) {
            log.warn(NO_RECEIVER_WARN_MESSAGE);
        }
        MCCIIN000002UV01Type response = MCCIIN000002UV01Builder
                .buildAcceptAcknowledgement(sutOid, id, device, HL7V3AcknowledgmentCode.CA.getValue());
        instance.getResponse().setType(PatientManagerConstants.ACCEPT_ACK_MESSAGE_TYPE);
        sendResponse(MCCIIN000002UV01Type.class, response);
        HL7V3Utils.saveMessageId(id, instance.getRequest());
        HL7V3Utils.saveMessageId(response.getId(), instance.getResponse());
        return response;
    }

    /**
     * <p>generateDefaultAcknowledgement.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     * @throws net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException if any.
     */
    public MCCIIN000002UV01Type generateDefaultAcknowledgement(PRPAIN201302UV02Type request) throws HL7V3ParserException {
        if (request == null) {
            log.error(NULL_REQUEST_ERROR_MESSAGE);
            throw new HL7V3ParserException("/", NULL_REQUEST_ERROR_MESSAGE);
        } else {
            // store request in TransactionInstance object
            ByteArrayOutputStream requestStream = new ByteArrayOutputStream();
            try {
                HL7V3Transformer.marshallMessage(PRPAIN201302UV02Type.class, requestStream, request);
            } catch (JAXBException e) {
                throw new HL7V3ParserException("/", e.getMessage(), e);
            }
            instance.getRequest().setContent(requestStream.toByteArray());
            instance.getRequest().setType(PatientManagerConstants.PRPA_IN_201302_UV_02);
            return getMcciin000002UV01Type(request.getSender(), request.getReceiver(), request.getId());
        }
    }

    /**
     * <p>generateDefaultAcknowledgement.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     * @throws net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException if any.
     */
    public MCCIIN000002UV01Type generateDefaultAcknowledgement(PRPAIN201304UV02Type request) throws HL7V3ParserException {
        if (request == null) {
            log.error(NULL_REQUEST_ERROR_MESSAGE);
            throw new HL7V3ParserException("/", NULL_REQUEST_ERROR_MESSAGE);
        } else {
            // store request in TransactionInstance object
            ByteArrayOutputStream requestStream = new ByteArrayOutputStream();
            try {
                HL7V3Transformer.marshallMessage(PRPAIN201304UV02Type.class, requestStream, request);
            } catch (JAXBException e) {
                throw new HL7V3ParserException("/", e.getMessage(), e);
            }
            instance.getRequest().setContent(requestStream.toByteArray());
            instance.getRequest().setType(PatientManagerConstants.PRPA_IN_201304_UV_02);
            return getMcciin000002UV01Type(request.getSender(), request.getReceiver(), request.getId());
        }
    }

    /**
     * <p>handleAddRecordMessage.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     * @throws net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException if any.
     */
    public MCCIIN000002UV01Type handleAddRecordMessage(PRPAIN201301UV02Type request) throws HL7V3ParserException {
        new PRPAIN201301UV02Parser().extractPatientFromAddRecord(request);
        return generateDefaultAcknowledgement(request);
    }

    /**
     * <p>handleReviseRecordMessage.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     * @throws net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException if any.
     */
    public MCCIIN000002UV01Type handleReviseRecordMessage(PRPAIN201302UV02Type request) throws HL7V3ParserException {
        new PRPAIN201302UV02Parser().extractPatientFromReviseRecord(request);
        return generateDefaultAcknowledgement(request);
    }

    /**
     * <p>handlePatientIdentityMergeMessage.</p>
     *
     * @param request a {@link net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type} object.
     * @return a {@link net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type} object.
     * @throws net.ihe.gazelle.simulator.hl7v3.messages.HL7V3ParserException if any.
     */
    public MCCIIN000002UV01Type handlePatientIdentityMergeMessage(PRPAIN201304UV02Type request) throws HL7V3ParserException {
        new PRPAIN201304UV02Parser().mergePatients(request);
        return generateDefaultAcknowledgement(request);
    }
}
