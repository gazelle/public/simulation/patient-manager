package net.ihe.gazelle.gitb;

import com.gitb.core.v1.ActorConfiguration;
import com.gitb.core.v1.AnyContent;
import com.gitb.ms.v1.*;
import com.gitb.tr.v1.TestResultType;
import net.ihe.gazelle.simulator.application.PreferenceServiceFinder;
import net.ihe.gazelle.simulator.pdqm.pdc.PDCMessagingClient;
import net.ihe.gazelle.simulator.pdqm.pdc.exception.ConnectionException;
import net.ihe.gazelle.simulator.pdqm.pdc.exception.SimulationException;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.ws.WebServiceException;
import java.net.MalformedURLException;
import java.net.URL;

import static net.ihe.gazelle.gitb.GITBConstant.*;

@Name("pdcMessagingClient")
public class PDCMessagingClientImplementation implements PDCMessagingClient {

    private static final Logger LOG = LoggerFactory.getLogger(PDCMessagingClientImplementation.class);

    @In(create = true, value = "preferenceServiceFinder")
    private PreferenceServiceFinder preferenceServiceFinder;

    private GITBMessagingClient gitbMessagingClient;

    private GITBMapper gitbMapper = new GITBMapper();


    /**
     * Empty constructor
     */
    public PDCMessagingClientImplementation() {
        //Empty
    }

    /**
     * Constructor with the url
     *
     * @param url the url of the service
     */
    public PDCMessagingClientImplementation(URL url) {
        this.gitbMessagingClient = new GITBMessagingClient(url, "MessagingServiceService", "MessagingServicePort");
    }

    /**
     * Initialize the Messaging Client.
     *
     * @throws MalformedURLException if the URL of the PDQm Connector defined by the application is not a valid URL.
     */
    public void init() throws MalformedURLException, ConnectionException {
        String urlService = preferenceServiceFinder.retrievePreferenceValue("pdqm_service_pdc_messaging_client");
        try {
            this.gitbMessagingClient = new GITBMessagingClient(new URL(urlService), "MessagingServiceService", "MessagingServicePort");
        } catch (MalformedURLException e) {
            LOG.error(String.format("Malformed URL in Application Preference [pdqm_service_pdc_messaging_client] : %s", urlService), e);
            throw new MalformedURLException(String.format("Malformed URL in Application Preference [pdqm_service_pdc_messaging_client] : %s",
                    urlService));
        } catch (WebServiceException e) {
            LOG.error("Error creating service for PDQm Messaging Client !", e);
            throw new ConnectionException("Error creating service for PDQm Messaging Client !", e);
        }

    }

    /**
     * Getter for the preferenceServiceFinder property.
     *
     * @return the value of the property.
     */
    public PreferenceServiceFinder getPreferenceServiceFinder() {
        return preferenceServiceFinder;
    }

    /**
     * Setter for the preferenceServiceFinder property.
     *
     * @param preferenceServiceFinder value to set to the property.
     */
    public void setPreferenceServiceFinder(PreferenceServiceFinder preferenceServiceFinder) {
        this.preferenceServiceFinder = preferenceServiceFinder;
    }

    /**
     * Query request
     *
     * @param id          id of the Patient to query.
     * @param sutEndpoint endpoint of the system under test.
     * @return The record id
     * @throws SimulationException if the simulation encountered an error.
     */
    @Override
    public String query(String id, String sutEndpoint) throws SimulationException, MalformedURLException, ConnectionException {
        if (this.gitbMessagingClient == null) {
            init();
        }
        AnyContent searchCriteria = gitbMapper.getSearchCriteriaAnyContent(id);
        AnyContent anyContentOperator = gitbMapper.createAnyContentSimpleString(OPERATION, QUERY);
        return getRecordIdPDCMessagingClient(id, sutEndpoint, anyContentOperator, searchCriteria);
    }

    /**
     * Retrieve request
     *
     * @param id          id of the Patient to retrieve.
     * @param sutEndpoint endpoint of the system under test.
     * @return The record id
     * @throws SimulationException if the simulation encountered an error.
     */
    @Override
    public String retrieve(String id, String sutEndpoint) throws SimulationException, MalformedURLException, ConnectionException {
        if (this.gitbMessagingClient == null) {
            init();
        }
        AnyContent searchCriteria = gitbMapper.getSearchCriteriaAnyContentRetrieve(id);
        AnyContent anyContentOperator = gitbMapper.createAnyContentSimpleString(OPERATION, RETRIEVE);
        return getRecordIdPDCMessagingClient(id, sutEndpoint, anyContentOperator, searchCriteria);
    }

    /**
     * Init GITBMessagingClient transactions, throw exception and return the record id of the transaction
     *
     * @param id                 The id of the Patient
     * @param sutEndpoint        The endPoint of the system under test
     * @param anyContentOperator The operation of the request
     * @return The record id of the transaction
     * @throws SimulationException if the simulation encountered an error.
     */
    public String getRecordIdPDCMessagingClient(String id, String sutEndpoint, AnyContent anyContentOperator, AnyContent anyContentCriteria) throws SimulationException {

        ActorConfiguration actorConfiguration = gitbMapper.getEndpointAnyContent(sutEndpoint);

        // 1- InitiateRequest
        InitiateRequest initiateRequest = new InitiateRequest();
        initiateRequest.getActorConfiguration().add(actorConfiguration);

        // 2- BeginTransactionRequest
        BeginTransactionRequest beginTransactionRequest = new BeginTransactionRequest();
        beginTransactionRequest.setSessionId(id);
        beginTransactionRequest.setFrom(PDQM_PDC);
        beginTransactionRequest.setTo(PDQM_PDS);

        // 3- SendRequest
        SendRequest sendRequest = new SendRequest();
        sendRequest.getInput().add(anyContentOperator);
        sendRequest.getInput().add(anyContentCriteria);
        sendRequest.setSessionId(id);
        sendRequest.setTo(PDQM_PDS);

        // 4- BasicRequest
        BasicRequest basicRequest = new BasicRequest();
        basicRequest.setSessionId(id);

        // 5- FinalizeRequest
        FinalizeRequest finalizeRequest = new FinalizeRequest();
        finalizeRequest.setSessionId(id);

        // Implement GITBMessagingClient
        gitbMessagingClient.initiate(initiateRequest);
        gitbMessagingClient.beginTransaction(beginTransactionRequest);
        gitbMessagingClient.send(sendRequest);
        gitbMessagingClient.endTransaction(basicRequest);
        gitbMessagingClient.finalize(finalizeRequest);

        if (gitbMapper.getSendResponseResultValue(gitbMessagingClient.send(sendRequest)).equals(TestResultType.FAILURE)) {
            gitbMapper.throwExceptionForErrorInBARFromSendResponse(gitbMessagingClient.send(sendRequest));
        }

        return gitbMapper.getRecordIdFromSendResponse(gitbMessagingClient.send(sendRequest));
    }


}
