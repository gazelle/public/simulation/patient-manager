package net.ihe.gazelle.gitb;

public class GITBConstant {

    public static final String OPERATION = "operation";
    public static final String QUERY = "QUERY";
    public static final String RETRIEVE = "RETRIEVE";
    public static final String PDQM_PDC = "PDQmPDC";
    public static final String PDQM_PDS = "PDQmPDS";

}
