package net.ihe.gazelle.simulators.pam.ws;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;


@Local
@Path("/")
@Api("/PAMRestService")
public interface PAMRestServiceLocal {

	@GET
	@Path("/GenerateRandomData")
	@Produces("application/xml")
	@ApiOperation(value = "generate a patient and an encounter")
	public Response generateRandomData(@QueryParam("getPatient") Boolean getPatient,
									   @QueryParam("patientsCountryCode") String patientsCountryCode, @Context HttpServletRequest req);

}
