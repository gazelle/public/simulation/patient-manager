package net.ihe.gazelle.gitb;

import com.gitb.core.v1.ActorConfiguration;
import com.gitb.core.v1.AnyContent;
import com.gitb.ms.v1.SendRequest;
import com.gitb.ms.v1.SendResponse;
import com.gitb.tr.v1.TestResultType;
import com.github.tomakehurst.wiremock.WireMockServer;
import net.ihe.gazelle.annotations.Covers;
import net.ihe.gazelle.simulator.pdqm.pdc.exception.SimulationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.ws.rs.core.MediaType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.Assert.*;

public class GITBMapperTest {

    /**
     * tested class
     */
    private GITBMapper gitbMapper;

    /**
     * GIT Messaging Client to retrieve SendResponse
     */
    private GITBMessagingClient gitbMessagingClient;

    /**
     * mock server
     */
    private WireMockServer server = new WireMockServer(wireMockConfig().port(8780));

    private static final String SERVICE_PATH = "MessagingServiceService";

    @Before
    public void init() throws IOException {
        gitbMapper = new GITBMapper();

        server.start();
        server.stubFor(get(urlEqualTo("/" + SERVICE_PATH + "?wsdl"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBody(readFileContent("src/main/resources/gitb/gitb_ms.wsdl"))));
        server.stubFor(get(urlPathEqualTo("/gitb_core.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile(readFileContent("src/main/resources/gitb/gitb_core.xsd"))));
        server.stubFor(get(urlPathEqualTo("/gitb_ms.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile(readFileContent("src/main/resources/gitb/gitb_ms.xsd"))));
        server.stubFor(get(urlPathEqualTo("/gitb_tr.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile(readFileContent("src/main/resources/gitb/gitb_tr.xsd"))));
        gitbMessagingClient = new GITBMessagingClient(new URL("http://localhost:8780/" + SERVICE_PATH + "?wsdl"),
                "MessagingServiceService", "MessagingServicePort");
    }

    /**
     * reset the server
     */
    @After
    public void stopServer() {
        server.resetAll();
        server.stop();
    }

    @Test
    public void getSearchCriteriaAnyContentTest() {
        String id = "123-abc";

        AnyContent result = gitbMapper.getSearchCriteriaAnyContent(id);

        assertNotNull("Result shall not be null", result);
        assertEquals("PatientSearchCriteria", result.getName());
        assertEquals(2, result.getItem().size());
        assertEquals("AND", result.getItem().get(0).getValue());
        assertEquals(1, result.getItem().get(1).getItem().size());
        assertTrue(result.getItem().get(1).getItem().get(0).getName().contains("StringSearchCriterion"));

        AnyContent searchCriterionAnyContent = result.getItem().get(1).getItem().get(0);

        assertEquals(4, searchCriterionAnyContent.getItem().size());

        assertEquals("options", searchCriterionAnyContent.getItem().get(0).getName());
        assertEquals(1, searchCriterionAnyContent.getItem().get(0).getItem().size());
        assertEquals("isCaseSensitive", searchCriterionAnyContent.getItem().get(0).getItem().get(0).getName());
        assertEquals("true", searchCriterionAnyContent.getItem().get(0).getItem().get(0).getValue());
        assertEquals("options", searchCriterionAnyContent.getItem().get(0).getName());
        assertEquals("operator", searchCriterionAnyContent.getItem().get(1).getName());
        assertEquals("EXACT", searchCriterionAnyContent.getItem().get(1).getValue());
        assertEquals("value", searchCriterionAnyContent.getItem().get(2).getName());
        assertEquals(id, searchCriterionAnyContent.getItem().get(2).getValue());
        assertEquals("key:net.ihe.gazelle.app.patientregistryapi.business.PatientSearchCriterionKey", searchCriterionAnyContent.getItem().get(3).getName());
        assertEquals("UUID", searchCriterionAnyContent.getItem().get(3).getValue());
    }

    @Test
    public void getEndpointAnyContentTest() {
        String endpoint = "https://gazelle.ihe.net/toto";
        ActorConfiguration result = gitbMapper.getEndpointAnyContent(endpoint);
        assertEquals(endpoint, result.getEndpoint());
    }

    @Test
    @Covers(requirements = {"CONNECT-116", "CONNECT-121", "CONNECT-115", "CONNECT-114"})
    public void getSendResponseResultValueSuccessTest() throws IOException {
        TestResultType successValue = TestResultType.SUCCESS;
        stubSoapFromFile("src/test/resources/gitb/sendResponseSuccess.xml");
        SendResponse sendResponse = gitbMessagingClient.send(new SendRequest());
        TestResultType result = gitbMapper.getSendResponseResultValue(sendResponse);
        XMLGregorianCalendar date = sendResponse.getReport().getDate();

        assertEquals("[CONNECT-121] SendResponse message shall contain the date of the send operation", "2020-05-19", date.toString());
        assertEquals("[CONNECT-116] SendResponse message shall contain SUCCESS result in the TAR report", successValue, result);
    }

    @Test
    @Covers(requirements = {"CONNECT-118", "CONNECT-115", "CONNECT-114"})
    public void getSendResponseResultValueFailureTest() throws IOException {
        TestResultType failure = TestResultType.FAILURE;
        stubSoapFromFile("src/test/resources/gitb/sendResponseFailureWithError.xml");
        SendResponse sendResponse = gitbMessagingClient.send(new SendRequest());
        TestResultType result = gitbMapper.getSendResponseResultValue(sendResponse);

        assertEquals("[CONNECT-118] SendResponse message shall contain FAILURE result in the TAR report", failure, result);
    }

    @Test
    @Covers(requirements = {"CONNECT-117", "CONNECT-115", "CONNECT-114"})
    public void getSendResponseResultValueUndefinedTest() throws IOException {
        TestResultType undefined = TestResultType.UNDEFINED;
        stubSoapFromFile("src/test/resources/gitb/sendResponseUndefined.xml");
        SendResponse sendResponse = gitbMessagingClient.send(new SendRequest());
        TestResultType result = gitbMapper.getSendResponseResultValue(sendResponse);

        assertEquals("[CONNECT-117] SendResponse message shall contain UNDEFINED result in the TAR report", undefined, result);
    }

    @Test
    @Covers(requirements = {"CONNECT-119","CONNECT-115", "CONNECT-114"})
    public void getRecordIdSendResponseWithSuccessTest() throws IOException {
        String expectedRecordId = "TOTO";
        stubSoapFromFile("src/test/resources/gitb/sendResponseSuccessRecordId.xml");
        SendResponse sendResponse = gitbMessagingClient.send(new SendRequest());
        String recordId = gitbMapper.getRecordIdFromSendResponse(sendResponse);

        assertEquals("[CONNECT-119] SendResponse message shall contain the ID of the record of the exchanged messages",expectedRecordId, recordId);
    }

    @Rule
    public ExpectedException expectedRule = ExpectedException.none();

    @Test
    public void createExceptionFromBARDescriptionErrorTest() throws IOException, SimulationException {
        stubSoapFromFile("src/test/resources/gitb/sendResponseFailureWithError.xml");
        SendResponse sendResponse = gitbMessagingClient.send(new SendRequest());
        expectedRule.expect(SimulationException.class);
        expectedRule.expectMessage("errorDescription");
        gitbMapper.throwExceptionForErrorInBARFromSendResponse(sendResponse);
    }


    /**
     * stub the server from file
     *
     * @param path the file path
     */
    private void stubSoapFromFile(String path) throws IOException {
        server.stubFor(post(urlEqualTo("/" + SERVICE_PATH))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.TEXT_XML)
                        .withBody(readFileContent(path))));
    }

    /**
     * Read file content to use as Wiremock return messages
     *
     * @param path path to the sample message
     * @return the litteral value of the sample
     * @throws IOException if the sample cannot be read properly.
     */
    private String readFileContent(String path) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File(path).getAbsolutePath()));
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
        try {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }
}
