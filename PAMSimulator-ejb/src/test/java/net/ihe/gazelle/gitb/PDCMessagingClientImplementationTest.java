package net.ihe.gazelle.gitb;

import com.github.tomakehurst.wiremock.WireMockServer;
import net.ihe.gazelle.annotations.Covers;
import net.ihe.gazelle.simulator.pdqm.pdc.exception.ConnectionException;
import net.ihe.gazelle.simulator.pdqm.pdc.exception.SimulationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PDCMessagingClientImplementationTest {


    /**
     * GIT Messaging Client to retrieve SendResponse
     */
    private GITBMessagingClient gitbMessagingClient;

    /**
     * mock server
     */
    private WireMockServer server = new WireMockServer(wireMockConfig().port(8780));

    private static final String SERVICE_PATH = "MessagingServiceService";

    private URL URL = new URL("http://localhost:8780/" + SERVICE_PATH + "?wsdl");


    /**
     * rule for the Exception
     */
    @Rule
    public ExpectedException expectedRule = ExpectedException.none();

    public PDCMessagingClientImplementationTest() throws MalformedURLException {
    }

    /**
     * init the server
     *
     * @throws IOException
     */
    @Before
    public void init() throws IOException {
        server.start();
        server.stubFor(get(urlEqualTo("/" + SERVICE_PATH + "?wsdl"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBody(readFileContent("src/main/resources/gitb/gitb_ms.wsdl"))));
        server.stubFor(get(urlPathEqualTo("/gitb_core.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile(readFileContent("src/main/resources/gitb/gitb_core.xsd"))));
        server.stubFor(get(urlPathEqualTo("/gitb_ms.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile(readFileContent("src/main/resources/gitb/gitb_ms.xsd"))));
        server.stubFor(get(urlPathEqualTo("/gitb_tr.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile(readFileContent("src/main/resources/gitb/gitb_tr.xsd"))));
        gitbMessagingClient = new GITBMessagingClient(URL, "MessagingServiceService", "MessagingServicePort");
    }

    /**
     * reset the server
     */
    @After
    public void stopServer() {
        server.resetAll();
        server.stop();
    }

    /**
     * Retrieve a sendResponse with Success
     *
     * @throws SimulationException
     * @throws IOException
     */
    @Test
    @Covers(requirements = {"PDQMCONN-22", "PDQMCONN-23", "PDQMCONN-42", "PDQMCONN-43", "PDQMCONN-44", "PDQMCONN-52",})
    public void querySuccessTest() throws SimulationException, IOException, ConnectionException {
        stubSoapFromFile("src/test/resources/gitb/sendResponseSuccessRecordId.xml");

        PDCMessagingClientImplementation pdcMessagingClientImplementation = new PDCMessagingClientImplementation(URL);
        pdcMessagingClientImplementation.query("TOTO", URL.toString());

        assertTrue("[PDQMCONN-22][PDQMCONN-42] SendRequest message must contain PDQmPDS in the element to", checkBodyContainsMatchingOperation("<to" +
                ">PDQmPDS</to>", getRequestBody("SendRequest")));
        assertTrue("[PDQMCONN-44] SendRequest message must contain string type in input element", checkBodyContainsMatchingOperation("type=\"string" +
                "\"", getRequestBody("SendRequest")));
        assertTrue("[PDQMCONN-44] SendRequest message must contain operation name in input element", checkBodyContainsMatchingOperation("name" +
                "=\"operation\"", getRequestBody("SendRequest")));
        assertTrue("[PDQMCONN-43] SendRequest message must contain STRING embeddingMethod in input element", checkBodyContainsMatchingOperation(
                "embeddingMethod=\"STRING\"", getRequestBody("SendRequest")));
        assertTrue("[PDQMCONN-23] SendRequest message must contain Query operation in input element", checkBodyContainsMatchingOperation(">QUERY<",
                getRequestBody("SendRequest")));
        assertTrue("[PDQMCONN-52] SendRequest message must contain SearchCriterion in the operationParameters", checkBodyContainsMatchingOperation(
                "name=\"searchCriterion\"", getRequestBody("SendRequest")));
    }

    /**
     * Query a sendResponse with failure and with error node
     *
     * @throws SimulationException
     * @throws IOException
     */
    @Test
    public void queryFailureWithErrorTest() throws SimulationException, IOException, ConnectionException {
        stubSoapFromFile("src/test/resources/gitb/sendResponseFailureWithError.xml");

        PDCMessagingClientImplementation pdcMessagingClientImplementation = new PDCMessagingClientImplementation(URL);
        expectedRule.expect(SimulationException.class);
        expectedRule.expectMessage("errorDescription");
        pdcMessagingClientImplementation.query("TOTO", URL.toString());
    }

    /**
     * Query a sendResponse with failure and without error node
     *
     * @throws SimulationException
     * @throws IOException
     */
    @Test
    public void queryFailureTest() throws SimulationException, IOException, ConnectionException {
        stubSoapFromFile("src/test/resources/gitb/sendResponseFailure.xml");

        PDCMessagingClientImplementation pdcMessagingClientImplementation = new PDCMessagingClientImplementation(URL);
        expectedRule.expect(SimulationException.class);
        expectedRule.expectMessage("No information reported");
        pdcMessagingClientImplementation.query("TOTO", URL.toString());
    }

    /**
     * Retrieve a sendResponse with Success
     *
     * @throws SimulationException
     * @throws IOException
     */
    @Test
    @Covers(requirements = {"PDQMCONN-22", "PDQMCONN-25", "PDQMCONN-42", "PDQMCONN-43", "PDQMCONN-45", "PDQMCONN-51",})
    public void retrieveSuccessTest() throws SimulationException, IOException, ConnectionException {
        stubSoapFromFile("src/test/resources/gitb/sendResponseSuccessRecordId.xml");

        PDCMessagingClientImplementation pdcMessagingClientImplementation = new PDCMessagingClientImplementation(URL);
        pdcMessagingClientImplementation.retrieve("TOTO", URL.toString());
        String test = getRequestBody("SendRequest");

        assertTrue("[PDQMCONN-22][PDQMCONN-42] SendRequest message must contain PDQmPDS in the element to", checkBodyContainsMatchingOperation("<to" +
                ">PDQmPDS</to>", getRequestBody("SendRequest")));
        assertTrue("[PDQMCONN-45] SendRequest message must contain string type in input element", checkBodyContainsMatchingOperation("type=\"string" +
                "\"", getRequestBody("SendRequest")));
        assertTrue("[PDQMCONN-45] SendRequest message must contain operation name in input element", checkBodyContainsMatchingOperation("name" +
                "=\"operation\"", getRequestBody("SendRequest")));
        assertTrue("[PDQMCONN-43] SendRequest message must contain STRING embeddingMethod in input element", checkBodyContainsMatchingOperation(
                "embeddingMethod=\"STRING\"", getRequestBody("SendRequest")));
        assertTrue("[PDQMCONN-25] SendRequest message must contain RETRIEVE operation in input element", checkBodyContainsMatchingOperation(
                ">RETRIEVE<", getRequestBody("SendRequest")));
        assertTrue("[PDQMCONN-51] SendRequest message must contain SearchCriterion UUID in the operationParameters",
                checkBodyContainsMatchingOperation("name=\"UUID\"", getRequestBody("SendRequest")));
    }


    /**
     * Retrieve a sendResponse with Failure and with error node
     *
     * @throws SimulationException
     * @throws IOException
     */
    @Test
    public void retrieveFailureWithErrorTest() throws SimulationException, IOException, ConnectionException {
        stubSoapFromFile("src/test/resources/gitb/sendResponseFailureWithError.xml");

        PDCMessagingClientImplementation pdcMessagingClientImplementation = new PDCMessagingClientImplementation(URL);
        expectedRule.expect(SimulationException.class);
        expectedRule.expectMessage("errorDescription");
        pdcMessagingClientImplementation.query("TOTO", URL.toString());
    }

    /**
     * Retrieve a sendResponse with Failure and without error node
     *
     * @throws SimulationException
     * @throws IOException
     */
    @Test
    public void retrieveFailureTest() throws SimulationException, IOException, ConnectionException {
        stubSoapFromFile("src/test/resources/gitb/sendResponseFailure.xml");

        PDCMessagingClientImplementation pdcMessagingClientImplementation = new PDCMessagingClientImplementation(URL);
        expectedRule.expect(SimulationException.class);
        expectedRule.expectMessage("No information reported");
        pdcMessagingClientImplementation.query("TOTO", URL.toString());
    }


    /**
     * Check PDQm PDC actor in initiateRequest
     *
     * @throws SimulationException
     * @throws IOException
     */
    @Test
    @Covers(requirements = {"PDQMCONN-36", "PDQMCONN-37"})
    public void initiateRequestTest() throws SimulationException, IOException, ConnectionException {
        stubSoapFromFile("src/test/resources/gitb/sendResponseSuccessRecordId.xml");

        PDCMessagingClientImplementation pdcMessagingClientImplementation = new PDCMessagingClientImplementation(URL);
        pdcMessagingClientImplementation.retrieve("TOTO", URL.toString());

        assertTrue("[PDQMCONN-36] InitiateRequest message must contain PDQmPDC actor", checkBodyContainsMatchingOperation("actor=\"PDQmPDC\"",
                getRequestBody("InitiateRequest")));
        assertTrue("[PDQMCONN-37] InitiateRequest message must contain endPoint", checkBodyContainsMatchingOperation("endpoint=\"http://localhost" +
                ":8780/MessagingServiceService?wsdl\"", getRequestBody("InitiateRequest")));

    }

    /**
     * Check beginTransactionRequest contains values for from and to
     *
     * @throws SimulationException
     * @throws IOException
     */
    @Test
    @Covers(requirements = {"PDQMCONN-39", "PDQMCONN-40", "PDQMCONN-41"})
    public void beginTransactionRequestTest() throws SimulationException, IOException, ConnectionException {
        stubSoapFromFile("src/test/resources/gitb/sendResponseSuccessRecordId.xml");

        PDCMessagingClientImplementation pdcMessagingClientImplementation = new PDCMessagingClientImplementation(URL);
        pdcMessagingClientImplementation.retrieve("TOTO", URL.toString());
        String test = getRequestBody("BeginTransactionRequest");

        assertTrue("[PDQMCONN-40] BeginTransactionRequest message must contain PDQmPDS in the node to", checkBodyContainsMatchingOperation("<to" +
                ">PDQmPDS</to>", getRequestBody("BeginTransactionRequest")));
        assertTrue("[PDQMCONN-39] BeginTransactionRequest message must contain PDQmPDC in the node from", checkBodyContainsMatchingOperation("<from" +
                ">PDQmPDC</from>", getRequestBody("BeginTransactionRequest")));
        assertFalse("[PDQMCONN-41] BeginTransactionRequest message must no contain config element", checkBodyContainsMatchingOperation("<config",
                getRequestBody("BeginTransactionRequest")));
    }

    /**
     * stub the server from file
     *
     * @param path the file path
     */
    private void stubSoapFromFile(String path) throws IOException {
        server.stubFor(post(urlEqualTo("/" + SERVICE_PATH))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.TEXT_XML)
                        .withBody(readFileContent(path))));
    }

    /**
     * Read file content to use as Wiremock return messages
     *
     * @param path path to the sample message
     * @return the litteral value of the sample
     * @throws IOException if the sample cannot be read properly.
     */
    private String readFileContent(String path) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File(path).getAbsolutePath()));
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
        try {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }

    /**
     * Check the request contains the operation
     *
     * @param attendedOperation String operation value
     * @return true if body request contains operation
     */
    private boolean checkBodyContainsMatchingOperation(String attendedOperation, String request) {
        if (request.contains(attendedOperation)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the request in the list
     *
     * @return String request
     */
    private String getRequestBody(String requestName) {
        int id = 0;
        while (!(new String(server.getServeEvents().getServeEvents().get(id).getRequest().getBody())).contains(requestName)) {
            id++;
        }
        return new String(server.getServeEvents().getServeEvents().get(id).getRequest().getBody());
    }


}


