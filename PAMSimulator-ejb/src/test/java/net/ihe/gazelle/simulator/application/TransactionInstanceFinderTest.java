package net.ihe.gazelle.simulator.application;

import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link TransactionInstanceFinder}.
 */
public class TransactionInstanceFinderTest {

    /**
     * Test for transactionInstanceDAO property getter and setter.
     */
    @Test
    public void getSetTransactionInstanceDAO() {
        TransactionInstanceFinder finder = new TransactionInstanceFinder();
        TransactionInstanceDAO dao = new TransactionInstanceDAO() {
            @Override
            public TransactionInstance retrieveTransactionInstanceById(String id) {
                return new TransactionInstance();
            }
        };

        finder.setTransactionInstanceDAO(dao);

        assertEquals(dao, finder.getTransactionInstanceDAO());
    }

    /**
     * Test for {@link TransactionInstanceFinder#retreiveRecordById(String)}.
     */
    @Test
    public void retreiveRecordById() {
        TransactionInstanceFinder finder = new TransactionInstanceFinder();
        final TransactionInstance transactionInstance = new TransactionInstance();
        transactionInstance.setCompanyKeyword("TEST");
        TransactionInstanceDAO dao = new TransactionInstanceDAO() {
            @Override
            public TransactionInstance retrieveTransactionInstanceById(String id) {
                return transactionInstance;
            }
        };
        finder.setTransactionInstanceDAO(dao);

        assertEquals(transactionInstance, finder.retreiveRecordById("ID"));
    }
}