package net.ihe.gazelle.simulator.application;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link PreferenceServiceFinder}
 */
public class PreferenceServiceFinderTest {

    private static final String TEST = "TEST";

    /**
     * Test for preferenceServiceDAO property getter and setter.
     */
    @Test
    public void getSetPreferenceServiceDAO() {
        PreferenceServiceFinder finder = new PreferenceServiceFinder();
        PreferenceServiceDAO dao = new PreferenceServiceDAO() {
            @Override
            public String retrievePreferenceServiceValue(String preferenceName) {
                return TEST;
            }
        };

        finder.setPreferenceServiceDAO(dao);

        assertEquals(dao, finder.getPreferenceServiceDAO());
    }

    /**
     * Test for {@link PreferenceServiceFinder#retrievePreferenceValue(String)}
     */
    @Test
    public void retrievePreferenceValue() {
        PreferenceServiceFinder finder = new PreferenceServiceFinder();
        PreferenceServiceDAO dao = new PreferenceServiceDAO() {
            @Override
            public String retrievePreferenceServiceValue(String preferenceName) {
                return TEST;
            }
        };
        finder.setPreferenceServiceDAO(dao);

        assertEquals(TEST, finder.retrievePreferenceValue("PrefName"));

    }
}