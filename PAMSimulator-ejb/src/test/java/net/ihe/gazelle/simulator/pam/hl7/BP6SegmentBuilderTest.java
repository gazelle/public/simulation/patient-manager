package net.ihe.gazelle.simulator.pam.hl7;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.message.ADT_A05;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.ZFD;
import net.ihe.gazelle.simulator.pam.model.AdditionalDemographics;
import net.ihe.gazelle.simulator.pam.model.LunarWeek;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class BP6SegmentBuilderTest {

    @Test
    public void fillZFD() throws HL7Exception {
        ADT_A05 bp6Message = new ADT_A05();

        AdditionalDemographics additionalDemographics = new AdditionalDemographics();
        additionalDemographics.setDateOfBirthCorrected(true);
        additionalDemographics.setWeekInMonth(LunarWeek.FIRST_WEEK);
        additionalDemographics.setMonth(12);
        additionalDemographics.setYearOfBirth(1993);
        additionalDemographics.setPregnant(true);
        additionalDemographics.setNumberOfWeeksOfGestation(5);
        additionalDemographics.setProofOfIdentity("CS");
        additionalDemographics.setSmsConsent(false);
        additionalDemographics.setDateOfBirthCorrected(true);
        Date dateRequest = new Date();
        additionalDemographics.setDateOfTheInsiWebserviceRequest(dateRequest);
        Date expirationDate = new Date();
        additionalDemographics.setDocumentExpirationDate(expirationDate);

        ZFD zfd = bp6Message.getZFD();
        BP6SegmentBuilder.fillZFDSegment(zfd, additionalDemographics);

        assertEquals("31", zfd.getLunarDate().getNa1_Value1().getValue());
        assertEquals("12", zfd.getLunarDate().getNa2_Value2().getValue());
        assertEquals("1993", zfd.getLunarDate().getNa3_Value3().getValue());

        assertEquals("Y", zfd.getIndicateurDeDateDeNaissanceCorrigée().getValue());
        assertEquals("N", zfd.getSMSConsent().getValue());

        assertEquals("5", zfd.getNumberOfWeeksOfGestation().getValue());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(expirationDate);
        assertEquals(calendar.get(Calendar.DAY_OF_MONTH), zfd.getDateDeFinDeValiditéDuDocument().getTime().getDay());

        calendar.setTime(dateRequest);
        assertEquals(calendar.get(Calendar.DAY_OF_MONTH), zfd.getDateDInterrogationDuTéléserviceINSi().getTime().getDay());
    }

}
