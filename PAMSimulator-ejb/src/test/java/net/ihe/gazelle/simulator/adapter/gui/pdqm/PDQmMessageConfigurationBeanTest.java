package net.ihe.gazelle.simulator.adapter.gui.pdqm;

import net.ihe.gazelle.annotations.Covers;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link PDQmMessageConfigurationBean}
 */
@Covers(requirements = {"MASTERSIMU-11"})
public class PDQmMessageConfigurationBeanTest {

    private static final String IDENTIFIER = "identifier";

    /**
     * Test for the idenfitier property getter and setter.
     */
    @Test
    public void getPatientIdentifier() {
        PDQmMessageConfigurationBean bean = new PDQmMessageConfigurationBean();

        bean.setPatientIdentifier(IDENTIFIER);

        assertEquals(IDENTIFIER, bean.getPatientIdentifier());
    }
}