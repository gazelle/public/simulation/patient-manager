@startuml
hide footbox
title PIXv3 Update Notification (ITI-46)
participant MGR as "PAT_IDENTITY_X_REF_MGR\n(Patient Manager)" #99FF99
participant CONSUMER as "PAT_IDENTITY_CONSUMER\n(SUT)"
MGR -> CONSUMER: PRPA_IN201302UV02
activate CONSUMER
CONSUMER --> MGR: MCCI_MT000200UV01
deactivate CONSUMER
@enduml
