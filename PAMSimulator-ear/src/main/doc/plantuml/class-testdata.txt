@startuml
abstract class TestData{
-String name
-String description
-boolean shared
-String lastModifier
-boolean deferredOption
}
enum FhirReturnType{
XML
JSON
}

class PicketlinkCredential{
-String label
-String username
-String password
}
note bottom of PicketlinkCredential
References the authentication
parameters if XUA is used
end note
class HierarchicDesignator
note bottom of HierarchicDesignator
References the domains
for which the supplier
is asked to return results
end note
note right of FhirReturnType
Selected return type for PDQm/PIXm
end note
TestData -- "0..1" PicketlinkCredential
TestData -right- "0..1" FhirReturnType
TestData -right- "1" Transaction
TestData -- "0..*" HierarchicDesignator
TestData --|> PatientTestData
TestData --|> PatientIdentifierTestData
PatientTestData -- "1" Patient
PatientIdentifierTestData -- "1" PatientIdentifier
@enduml