@startuml
hide footbox
title Patient Registration (RAD-1)
participant PES as "ADT\n(Patient Manager)" #99FF99
participant PEC as "MPI, DSS, OP\n(SUT)"
PES -> PEC: ADT^A11^ADT_A09
activate PEC
PEC --> PES: ACK
deactivate PEC
@enduml
