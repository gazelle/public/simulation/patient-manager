@startuml
hide footbox
title Patient Update (RAD-12)
participant PES as "ADT\n(Patient Manager)" #99FF99
participant PEC as "MPI, OF, OP, RM, IM\n(SUT)"
PES -> PEC: ADT^A12^ADT_A12
activate PEC
PEC --> PES: ACK
deactivate PEC
@enduml
