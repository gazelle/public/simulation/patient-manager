@startuml
hide footbox
title Patient Demographics Query for Mobile
participant PDC as "PDC\n(SUT)" #99FF99
participant PDS as "PDS\n(Patient Manager)"

group ITI-78 Patient Demographics Query for Mobile
PDC -> PDS: Query Patient Resource Request
activate PDS
PDS --> PDC: Query Patient Resource Response
deactivate PDS
end

@enduml