<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:f="http://hl7.org/fhir">
    <xsl:output method="text"/>
    <xsl:variable name="CATT">1.3.6.1.4.1.12559.11.20.5</xsl:variable>
    <xsl:variable name="CATT-GOLD">1.3.6.1.4.1.12559.11.20.6</xsl:variable>
    <xsl:variable name="CATT-SILVER">1.3.6.1.4.1.12559.11.20.7</xsl:variable>
    <xsl:variable name="UNKNOWN">1.3.6.1.4.1.12559.11.20.99</xsl:variable>
    <xsl:template match="/">
        <xsl:text>id in CATT, id in CATT-GOLD, id in CATT-SILVER, id in UNKNOWN, Last name, First name, gender, birthdate, street, city, state, postalcode, country</xsl:text>
        <xsl:text>&#xa;</xsl:text>
        <xsl:for-each select="*/f:entry">
            <xsl:for-each select="f:resource/f:Patient/f:identifier">
                <xsl:choose>
                    <xsl:when test="contains(f:system/@value, $CATT)">
                        <xsl:value-of select="f:value/@value"/>
                        <xsl:text>^^^CATT&amp;</xsl:text>
                        <xsl:value-of select="$CATT"/>
                    </xsl:when>
                    <xsl:when test="contains(f:system/@value, $UNKNOWN)">
                        <xsl:value-of select="f:value/@value"/>
                        <xsl:text>^^^UNKNOWN&amp;</xsl:text>
                        <xsl:value-of select="$UNKNOWN"/>
                    </xsl:when>
                    <xsl:when test="contains(f:system/@value, $CATT-GOLD)">
                        <xsl:value-of select="f:value/@value"/>
                        <xsl:text>^^^CATT-GOLD&amp;</xsl:text>
                        <xsl:value-of select="$CATT-GOLD"/>
                    </xsl:when>
                    <xsl:when test="contains(f:system/@value, $CATT-SILVER)">
                        <xsl:value-of select="f:value/@value"/>
                        <xsl:text>^^^CATT-SILVER&amp;</xsl:text>
                        <xsl:value-of select="$CATT-SILVER"/>
                    </xsl:when>
                </xsl:choose>
                <xsl:text>&amp;ISO,</xsl:text>
            </xsl:for-each>
            <xsl:choose>
                <xsl:when test="count(f:resource/f:Patient/f:identifier) = 1">
                    <xsl:text>,,,</xsl:text>
                </xsl:when>
                <xsl:when test="count(f:resource/f:Patient/f:identifier) = 2">
                    <xsl:text>,,</xsl:text>
                </xsl:when>
                <xsl:when test="count(f:resource/f:Patient/f:identifier) = 3">
                    <xsl:text>,</xsl:text>
                </xsl:when>
            </xsl:choose>
            <xsl:value-of select="f:resource/f:Patient/f:name/f:family/@value"/><xsl:text>,</xsl:text>
            <xsl:value-of select="f:resource/f:Patient/f:name/f:given/@value"/><xsl:text>,</xsl:text>
            <xsl:value-of select="f:resource/f:Patient/f:gender/@value"/><xsl:text>,</xsl:text>
            <xsl:value-of select="f:resource/f:Patient/f:birthDate/@value"/><xsl:text>,</xsl:text>
            <xsl:value-of select="f:resource/f:Patient/f:address/f:line/@value"/><xsl:text>,</xsl:text>
            <xsl:value-of select="f:resource/f:Patient/f:address/f:city/@value"/><xsl:text>,</xsl:text>
            <xsl:value-of select="f:resource/f:Patient/f:address/f:state/@value"/><xsl:text>,</xsl:text>
            <xsl:value-of select="f:resource/f:Patient/f:address/f:postalCode/@value"/><xsl:text>,</xsl:text>
            <xsl:value-of select="f:resource/f:Patient/f:address/f:country/@value"/>
            <xsl:text>&#xa;</xsl:text>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>