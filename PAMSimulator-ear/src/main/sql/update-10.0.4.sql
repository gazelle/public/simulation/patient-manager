ALTER TABLE system_configuration ALTER COLUMN url TYPE varchar(2083);

update pam_encounter_management_event set selection='ENCOUNTERS_PRE_ADMIT' where selection='0';
update pam_encounter_management_event set selection='ENCOUNTERS_OPEN' where selection='1';
update pam_encounter_management_event set selection='PATIENTS' where selection='2';
update pam_encounter_management_event set selection='MOVEMENTS' where selection='3';
update pam_encounter_management_event set selection='ENCOUNTERS_OR_PATIENTS' where selection='4';