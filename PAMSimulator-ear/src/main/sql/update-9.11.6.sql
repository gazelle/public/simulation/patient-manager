ALTER TABLE public.affinity_domain DROP COLUMN description;
ALTER TABLE public.pam_fr_encounter_additional_info ADD COLUMN care_during_transport character varying(255);

--
-- Name: pam_fr_legal_care_mode; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE IF NOT EXISTS public.pam_fr_legal_care_mode (
    id integer NOT NULL,
    action character varying(255) NOT NULL,
    care_end_date_time timestamp without time zone,
    care_start_date_time timestamp without time zone NOT NULL,
    comment text,
    identifier character varying(255) NOT NULL,
    mode character varying(255),
    rimp_code character varying(255),
    set_id integer NOT NULL,
    patient_id integer
);


ALTER TABLE public.pam_fr_legal_care_mode OWNER TO gazelle;

--
-- Name: pam_fr_legal_care_mode_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_fr_legal_care_mode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: pam_fr_legal_care_mode fk_1mrkxoyjlb2plabqbf92vks79; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_fr_legal_care_mode
    ADD CONSTRAINT fk_1mrkxoyjlb2plabqbf92vks79 FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


ALTER TABLE public.pam_fr_legal_care_mode_id_seq OWNER TO gazelle;
ALTER TABLE public.pam_hierarchic_designator ALTER COLUMN usage SET NOT NULL ;

DROP TABLE IF EXISTS public.pam_patient;
DROP SEQUENCE IF EXISTS public.pam_patient_id_seq;
DROP TABLE IF EXISTS public.user_account_role;
DROP TABLE IF EXISTS public.user_permission;
DROP SEQUENCE IF EXISTS public.user_permission_id_seq;
DROP TABLE IF EXISTS public.user_role_group;
DROP SEQUENCE IF EXISTS public.user_role_group_id_seq;
DROP TABLE IF EXISTS public.user_role;
DROP SEQUENCE IF EXISTS public.user_role_id_seq;
DROP TABLE IF EXISTS public.user_account;
DROP SEQUENCE IF EXISTS public.user_account_id_seq;

ALTER TABLE public.xua_picketlink_credentials ALTER COLUMN assertion_profile SET NOT NULL;

-- PAM-666
ALTER TABLE pam_phone_number ADD COLUMN type_string character varying(255);
UPDATE pam_phone_number SET type_string = 'HOME' where type = 0;
UPDATE pam_phone_number SET type_string = 'OTHER' where type = 1;
UPDATE pam_phone_number SET type_string = 'WORK' where type = 2;
UPDATE pam_phone_number SET type_string = 'VACATION' where type = 3;
UPDATE pam_phone_number SET type_string = 'MOBILE' where type = 4;
UPDATE pam_phone_number SET type_string = 'EMERGENCY' where type = 5;
UPDATE pam_phone_number SET type_string = 'BEEPER' where type = 6;
ALTER TABLE pam_phone_number DROP COLUMN type;
ALTER TABLE pam_phone_number RENAME type_string TO type;

-- PAM-667
ALTER TABLE pat_patient ADD COLUMN uuid character varying(255);
create extension "uuid-ossp";
UPDATE pat_patient set uuid =  uuid_generate_v4() ;

-- PAM-670
ALTER TABLE pat_patient ADD COLUMN simulated_actor_keyword character varying(255);
update pat_patient set simulated_actor_keyword = (select keyword from tf_actor a where a.id = actor_id);



DROP FUNCTION IF EXISTS link_messages_to_transaction();
DROP FUNCTION IF EXISTS  sut_usages();
DROP FUNCTION IF EXISTS  update_all();
DROP FUNCTION IF EXISTS  update_patient_demographics();
