-- PAM 695
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'pdqm_service_pdc_messaging_client', 'http://localhost:8088/mockMessagingServicePortBinding?wsdl');

-- PAM 682
ALTER TABLE cmn_message_instance ADD COLUMN payload bytea;