UPDATE pam_test_step SET execution_result_id = (SELECT pam_graph_execution_results_id FROM pam_graph_execution_results_pam_test_step WHERE messageslist_id = id);
UPDATE pam_graph_execution SET  bp6mode = TRUE ;
UPDATE pam_graph_description SET active = TRUE;
UPDATE pam_graph_description SET active = TRUE;
UPDATE pam_graph_execution SET validate_transaction_instance = TRUE;

DROP TABLE pam_graph_execution_results_pam_test_step;