update app_configuration set variable = 'application_universal_id_type' where variable = 'application_namespace_id_type';
update pam_patient_identifier set full_patient_id = (select regexp_replace(full_patient_id, 'null', 'ISO'));
update pam_hierarchic_designator set universal_id_type = 'ISO' where universal_id_type = 'null';

INSERT INTO tf_transaction(id, description, keyword, name) VALUES (14, 'PIXV3 Query', 'ITI-45', 'ITI-45');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (15, 'Patient Identity Feed HL7V3', 'ITI-44', 'ITI-44');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (16, 'PIXV3 Update notification', 'ITI-46', 'ITI-46');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (17, 'None', 'None', 'NONE');
SELECT pg_catalog.setval('tf_transaction_id_seq', 17, true);
UPDATE tf_transaction set name = 'ITI-10' where keyword = 'ITI-10';

INSERT INTO pam_message_id_generator (id, index, issuing_actor_id, oid) VALUES (nextval('pam_message_id_generator_id_seq'), 0, 5, '1.3.6.1.4.1.12559.11.20.1.5');
INSERT INTO pam_message_id_generator (id, index, issuing_actor_id, oid) VALUES (nextval('pam_message_id_generator_id_seq'), 0, 6, '1.3.6.1.4.1.12559.11.20.1.6');
INSERT INTO pam_message_id_generator (id, index, issuing_actor_id, oid) VALUES (nextval('pam_message_id_generator_id_seq'), 0, 7, '1.3.6.1.4.1.12559.11.20.1.7');

--for reminder, the actor id inserted with import.sql
--5 = 'PAT_IDENTITY_SRC'
--6 = 'PAT_IDENTITY_CONSUMER'
--7 = 'PAT_IDENTITY_X_REF_MGR'

INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (90, 'MCCI_IN000002UV01', 'PIXV3 - Patient Identity Feed HL7V3 - Acknowledgement', 7, 15);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (91, 'PRPA_IN201309UV02', 'PIXV3 - PIXV3 Query', 6, 14);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (97, 'PRPA_IN201310UV02', 'PIXV3 - PIXV3 Query Response', 7, 14);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (92, 'PRPA_IN201302UV02', 'PIXV3 - PIXV3 Update Notification', 7, 16);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (93, 'MCCI_IN000002UV01', 'PIXV3 - PIXV3 Update Notification acknowledgement', 6, 16);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (94, 'PRPA_IN201301UV02', 'PIXV3 - Patient Identity Feed HL7V3 - Add Patient Record', 5, 15);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (95, 'PRPA_IN201302UV02', 'PIXV3 - Patient Identity Feed HL7V3 - Revise Patient Record', 5, 15);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (96, 'PRPA_IN201304UV02', 'PIXV3 - Patient Identity Feed HL7V3 - Patient Identity Merge', 5, 15);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,90);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,91);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,92);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,93);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,94);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,95);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,96);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,97);
SELECT pg_catalog.setval('ort_validation_parameters_id_seq', 97, true);

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (nextval('usage_id_seq'), 14, 1, 'PIXV3 Manager', 'IHE_PIX_MGR_ITI45');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (nextval('usage_id_seq'), 15, 1, 'PIXV3 Manager', 'IHE_PIX_MGR_ITI44');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (nextval('usage_id_seq'), 16, 1, 'PIXV3 Consumer', 'IHE_PIX_CONS');


INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v3_pix_consumer_device_id', '1.3.6.1.4.1.12559.11.20.1.8');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v3_pix_organization_oid', '1.3.6.1.4.1.12559.11.20.1.9');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v3_pix_mgr_device_id', '1.3.6.1.4.1.12559.11.20.1.10');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'pixv3_mgr_url', 'http://localhost:8080/PAMSimulator-ejb/PIXManager_Service/PIXManager_PortType');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'pixv3_cons_url', 'http://localhost:8080/PAMSimulator-ejb/PIXConsumer_Service/PIXConsumer_PortType');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v3_pix_src_device_id', '1.3.6.1.4.1.12559.11.20.1.11');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'enable_worklist', 'false');
