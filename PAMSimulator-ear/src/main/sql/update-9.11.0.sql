DELETE FROM validation_parameters where message_type = 'ADT^Z80^ADT_A01';
DELETE FROM validation_parameters where message_type = 'ADT^Z81^ADT_A01';
DELETE FROM validation_parameters where message_type = 'ADT^Z82^ADT_A01';
DELETE FROM validation_parameters where message_type = 'ADT^Z83^ADT_A01';
DELETE FROM validation_parameters where message_type = 'ADT^Z84^ADT_A01';
DELETE FROM validation_parameters where message_type = 'ADT^Z85^ADT_A01';
DELETE FROM validation_parameters where message_type = 'ADT^Z86^ADT_A01';
DELETE FROM validation_parameters where message_type = 'ADT^Z87^ADT_A01';
DELETE FROM validation_parameters where message_type = 'ADT^Z88^ADT_A01';
DELETE FROM validation_parameters where message_type = 'ADT^Z89^ADT_A01';

DELETE FROM pam_encounter_management_event where insert_trigger_event = 'Z80';
DELETE FROM pam_encounter_management_event where insert_trigger_event = 'Z82';
DELETE FROM pam_encounter_management_event where insert_trigger_event = 'Z84';
DELETE FROM pam_encounter_management_event where insert_trigger_event = 'Z86';
DELETE FROM pam_encounter_management_event where insert_trigger_event = 'Z88';

INSERT INTO cmn_value_set(id, value_set_keyword, value_set_oid, usage, accessible, last_check, value_set_name) values (nextval('cmn_value_set_id_seq'), 'HL73306', '2.16.840.1.113883.2.8.3.3.3306', 'ZFV-11', true, 'now', 'Types de prise en charge durant le transport' );

ALTER TABLE pam_fr_encounter_additional_info ADD COLUMN "care_during_transport" varchar(255);

ALTER TABLE pam_encounter_management_event ADD COLUMN "selection_string" varchar(255);
UPDATE pam_encounter_management_event set selection_string = 'ENCOUNTERS_PRE_ADMIT' where selection = 0;
UPDATE pam_encounter_management_event set selection_string = 'ENCOUNTERS_OPEN' where selection = 1;
UPDATE pam_encounter_management_event set selection_string = 'PATIENTS' where selection = 2;
UPDATE pam_encounter_management_event set selection_string = 'MOVEMENTS' where selection = 3;
UPDATE pam_encounter_management_event set selection_string = 'ENCOUNTERS_OR_PATIENTS' where selection = 4;
ALTER TABLE pam_encounter_management_event DROP COLUMN selection;
ALTER TABLE pam_encounter_management_event RENAME COLUMN "selection_string" TO "selection";
UPDATE pam_encounter_management_event set selection = 'PATIENTS' where insert_trigger_event = 'A44';