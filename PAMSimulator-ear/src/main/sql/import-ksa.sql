-- application preferences
INSERT INTO app_configuration(id, variable, value) VALUES (1, 'application_issue_tracker_url', 'http://gazelle.ihe.net/jira/');
INSERT INTO app_configuration(id, variable, value) VALUES (2, 'application_name', 'Patient Manager');
INSERT INTO app_configuration(id, variable, value) VALUES (3, 'application_release_notes_url', 'http://gazelle.ihe.net/jira/browse/PAM#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel');
INSERT INTO app_configuration(id, variable, value) VALUES (4, 'application_time_zone', 'UTC+01');
INSERT INTO app_configuration(id, variable, value) VALUES (5, 'sending_application', 'PatientManager');
INSERT INTO app_configuration(id, variable, value) VALUES (6, 'sending_facility', 'IHE');
INSERT INTO app_configuration(id, variable, value) VALUES (7, 'svs_repository_url', 'http://k-project.ihe-europe.net');
INSERT INTO app_configuration(id, variable, value) VALUES (8, 'dds_ws_endpoint', 'http://jumbo.irisa.fr:8080/DemographicDataServer-DemographicDataServer-ejb/DemographicDataServerBean?wsdl');
INSERT INTO app_configuration(id, variable, value) VALUES (9, 'xsl_location', 'http://k-project.ihe-europe.net/xsl/hl7Validation/resultStylesheet.xsl');
INSERT INTO app_configuration(id, variable, value) VALUES (10, 'url_EVCS_ws', 'http://ovh4.ihe-europe.net:8180/GazelleHL7v2Validator-GazelleHL7v2Validator-ejb/GazelleHL7v2ValidationWS?wsdl');
INSERT INTO app_configuration(id, variable, value) VALUES (11, 'sex_oid', '1.3.6.1.4.1.21367.101.101');
INSERT INTO app_configuration(id, variable, value) VALUES (12, 'religion_oid', '1.3.6.1.4.1.21367.101.122');
INSERT INTO app_configuration(id, variable, value) VALUES (13, 'race_oid', '1.3.6.1.4.1.21367.101.102');
INSERT INTO app_configuration(id, variable, value) VALUES (14, 'timeout_for_receiving_messages', '30000');
INSERT INTO app_configuration(id, variable, value) VALUES (15, 'documentation_url', 'http://gazelle.ihe.net/content/pam-simulator-user-manual');
INSERT INTO app_configuration(id, variable, value) VALUES (16, 'application_url', 'http://k-project.ihe-europe.net/PatientManager');
INSERT INTO app_configuration(id, variable, value) VALUES (18, 'gazelle_hl7v2_validator_url', 'http://k-project.ihe-europe.net/GazelleHL7Validator');
INSERT INTO app_configuration(id, variable, value) VALUES (19, 'patient_class_oid', '1.3.6.1.4.1.21367.101.104');
INSERT INTO app_configuration(id, variable, value) VALUES (20, 'doctor_oid', '1.3.6.1.4.1.21367.101.110');
INSERT INTO app_configuration(id, variable, value) VALUES (21, 'point_of_care_oid', '1.3.6.1.4.1.21367.101.106');
INSERT INTO app_configuration(id, variable, value) VALUES (22, 'bed_oid', '1.3.6.1.4.1.21367.101.108');
INSERT INTO app_configuration(id, variable, value) VALUES (23, 'room_oid', '1.3.6.1.4.1.21367.101.107');
INSERT INTO app_configuration(id, variable, value) VALUES (24, 'facility_oid', '1.3.6.1.4.1.21367.101.109');
INSERT INTO app_configuration(id, variable, value) VALUES (25, 'number_of_segments_to_display', '40');
INSERT INTO app_configuration(id, variable, value) VALUES (26, 'create_worklist_url', 'http://localhost:8080/OrderManager/dispatcher.seam');
INSERT INTO app_configuration(id, variable, value) VALUES (27, 'time_zone', 'Europe/Paris');
INSERT INTO app_configuration(id, variable, value) VALUES (28, 'hospital_service_oid', '1.3.6.1.4.1.21367.101.111');
INSERT INTO app_configuration(id, variable, value) VALUES (29, 'application_universal_id', '2.16.840.1.113883.3.3731.1.1.100.1');
INSERT INTO app_configuration(id, variable, value) VALUES (30, 'application_namespace_id', 'HealthID');
INSERT INTO app_configuration(id, variable, value) VALUES (31, 'use_ids_from_dds', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (32, 'application_works_without_cas', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (33, 'application_namespace_id_type', 'ISO');
INSERT INTO app_configuration (id, variable, value) VALUES (34, 'dds_mode', 'full');
INSERT INTO app_configuration (id, variable, value) VALUES (35, 'cas_url', 'https://k-project.ihe-europe.net/cas');
INSERT INTO app_configuration (id, variable, value) VALUES (36, 'ip_login', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (37, 'ip_login_admin', '.*');
INSERT INTO app_configuration (id, variable, value) VALUES (38, 'message_permanent_link', 'http://k-project.ihe-europe.net/PatientManager/hl7v3/messages/messageDisplay.seam?id=');
INSERT INTO app_configuration (id, variable, value) VALUES (39, 'NUMBER_OF_ITEMS_PER_PAGE', '20');
INSERT INTO app_configuration (id, variable, value) VALUES (40, 'hl7v3_validator_url', 'http://ovh4.ihe-europe.net:8180/GazelleHL7v2Validator-GazelleHL7v2Validator-ejb/GazelleHL7v3ValidationWS?wsdl');
INSERT INTO app_configuration (id, variable, value) VALUES (41, 'default_pdq_domain', 'KSA');
SELECT pg_catalog.setval('app_configuration_id_seq', 41, true);



-- transaction definition
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (1, 'Patient Encounter Management', 'ITI-31', 'ITI-31');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (2, 'Patient Identification Management', 'ITI-30', 'ITI-30');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (3, 'Patient Demographics Query', 'ITI-21', 'ITI-21');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (4, 'Patient Demographics and Visit Query', 'ITI-22', 'ITI-22');
INSERT INTO tf_transaction (id, keyword, name, description) values (5, 'ITI-8', 'Patient Identity Feed', 'Patient Identity Feed');
INSERT INTO tf_transaction (id, keyword, name, description) values (6, 'ITI-9', 'PIX Query', 'PIX Query');
INSERT INTO tf_transaction (id, keyword, name, description) values (7, 'ITI-10', 'PIX Update notification', 'PIX Update notification');
INSERT INTO tf_transaction (id, keyword, name, description) values (8, 'RAD-1', 'Patient Registration', 'Patient Registration');
INSERT INTO tf_transaction (id, keyword, name, description) values (10, 'ITI-47', 'Patient Demographics Query HL7v3', 'Patient Demographics Query HL7v3');
INSERT INTO tf_transaction (id, keyword, name, description) values (9, 'RAD-12', 'Patient Update', 'Patient Update');
INSERT INTO tf_transaction (id, keyword, name, description) values (11, 'ITI-55', 'Cross Gateway Patient Discovery', 'Cross Gateway Patient Discovery');
INSERT INTO tf_transaction (id, keyword, name, description) values (12, 'ITI-56', 'Patient Location Query', 'Patient Location Query');
SELECT pg_catalog.setval('tf_transaction_id_seq', 12, true);

-- actor definition
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (1, 'Patient Demographic Consumer', 'PDC', 'Patient Demographic Consumer', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (2, 'Patient Demographic Supplier', 'PDS', 'Patient Demographic Supplier', false);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (3, 'Patient Encounter Consumer', 'PEC', 'Patient Encounter Consumer', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (4, 'Patient Encounter Supplier', 'PES', 'Patient Encounter Supplier', false);
INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) values (5, 'PAT_IDENTITY_SRC', 'Patient Identity Source', 'Patient Identity Source', false);
INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) values (6, 'PAT_IDENTITY_CONSUMER', 'Patient Identity Consumer', 'Patient Identity Consumer', true);
INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) values (7, 'PAT_IDENTITY_X_REF_MGR', 'Patient Identity Cross-Reference Manager', 'Patient Identity Cross-Reference Manager', true);
INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) values (8, 'ADT', 'ADT', 'ADT', false);
INSERT INTO tf_actor (id, keyword, name, description, can_act_as_responder) values (9, 'MPI', 'ADT Client', 'MPI, OF, OP, IM, RM', true);
SELECT pg_catalog.setval('tf_actor_id_seq', 9, true);


-- set there the charset used by your simulator
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (1,  'ASCII','ASCII', 'US-ASCII' );                                
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (2,  'ISO8859-1',	'8859/1'	,'ISO8859_1');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (3,  'ISO8859-2', 	'8859/2'	, 'ISO8859_2');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (4,  'ISO8859-3', 	'8859/3'	, 'ISO8859_3');    
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (5,  'ISO8859-4', 	'8859/4'	, 'ISO8859_4');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (6,  'ISO8859-5', 	'8859/5'	, 'ISO8859_5');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (7,  'ISO8859-6', 	'8859/6'	, 'ISO8859_6');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (8,  'ISO8859-7', 	'8859/7'	, 'ISO8859_7');   
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (9,  'ISO8859-8', 	'8859/8'	, 'ISO8859_8');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (10, 'ISO8859-9', 	'8859/9'	, 'ISO8859_9');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (11, 'Japanese (JIS X 0201)', 	'ISO IR14', 'JIS0201' );
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (12, 'Japanese (JIS X 0208)', 	'ISO IR87', 'JIS0208' );            
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (13, 'Japanese (JIS X 0212)', 	'ISO IR159',  'JIS0212');       
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (14, 'Chinese', 	'GB 18030-2000'	, 'EUC_CN');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (15, 'Korean', 	'KS X 1001'	, 'EUC_KR');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (16, 'Taiwanese', 	'CNS 11643-1992', 'EUC_TW');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (17, 'Taiwanese (BIG 5)', 'BIG-5', 'Big5');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (19, 'UTF-8', 		'UNICODE UTF-8' , 'UTF8'    ); 
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (20, 'UTF-16', 		'UNICODE UTF-16', 'UTF-16');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (21, 'UTF-32', 		'UNICODE UTF-32', 'UTF-32');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (22, 'ISO8859-15', '8859/15', 'ISO8859_15');

SELECT pg_catalog.setval('hl7_charset_id_seq', 22, true);


-- default config
INSERT INTO sys_config (id, name, application, facility, ip_address, port, actor_id, active, shared, charset_id) VALUES (1, 'PAM PDC simulator', 'PatientManager', 'IHE', '127.0.0.1', 10010, 1, true, true, 19);
INSERT INTO sys_config (id, name, application, facility, ip_address, port, actor_id, active, shared, charset_id) VALUES (2, 'PAM PEC Simulator', 'PatientManager', 'IHE', '127.0.0.1', 10050, 3, true, true, 19);
INSERT INTO sys_config (id, name, application, facility, ip_address, port, actor_id, active, shared, charset_id) VALUES (3, 'PDQ PDS Simulator', 'PatientManager', 'IHE', '127.0.0.1', 10055, 2, true, true, 19);
SELECT pg_catalog.setval('sys_config_id_seq', 3, true);

-- update with INSERT INTO affinity_domain (id, label_to_display, profile, keyword) VALUES (1, 'IHE', 'HPD', 'IHE');
INSERT INTO affinity_domain (id, keyword, label_to_display, profile) VALUES (1, 'IHE', 'IHE', 'All');
INSERT INTO affinity_domain (id, label_to_display, profile, keyword) VALUES (2, 'KSA', 'KPDQ', 'KSA');
SELECT pg_catalog.setval('affinity_domain_id_seq', 2, true);

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (1, 10, 1, 'IHE - PDQv3 Supplier', 'IHE_PDQ_PDS');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (2, 10, 2, 'KSA - PDQv3 Supplier', 'KSA_PDQ_PDS');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (3, 11, 1, 'XCPD - Responding Gateway', 'IHE_XCPD_RESP');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (4, 12, 1, 'XCPD - Responding Gateway - Health Data Locator option', 'IHE_XCPD_RESP_PLQ');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (5, 11, 1, 'XCPD - Initiating Gateway - Deferred response option', 'IHE_XCPD_INIT_DEFERRED');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (6, 11, 1, 'XCPD - Responding Gateway - Deferred response option', 'IHE_XCPD_RESP_DEFERRED');
SELECT pg_catalog.setval('usage_id_seq', 6, true);

-- set there the configuration of the simulated actors acting as responders
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, affinity_domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (1, 'PAM PDC', 10010, 1, 2, 1, '*', '*', 'pdcServer', '127.0.0.1', 'PatientManager', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, affinity_domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (2, 'PAM PEC', 10050, 3, 1, 1, '*', '*', 'pecServer', '127.0.0.1', 'PatientManager', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, affinity_domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (3, 'PDQ PDS', 10055, 2, null, 1, '*', '*', 'pdqpdsServer', '127.0.0.1', 'PatientManager', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, affinity_domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (4, 'PIX Manager', 10056, 7, null, 1, '*', '*', 'pixManager', '127.0.0.1', 'PIXManager', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, affinity_domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (5, 'PIX Consumer', 10054, 6, null, 1, '*', '*', 'pixConsumer', '127.0.0.1', 'PIXConsumer', 'IHE', false);
SELECT pg_catalog.setval('hl7_simulator_responder_configuration_id_seq', 4, true);

INSERT INTO pam_message_id_generator (id, index, issuing_actor_id, oid) VALUES (1, 0, 1, '1.3.6.1.4.1.12559.11.13.9.1');
INSERT INTO pam_message_id_generator (id, index, issuing_actor_id, oid) VALUES (2, 0, 2, '1.3.6.1.4.1.12559.11.13.9.2');
SELECT pg_catalog.setval('pam_message_id_generator_id_seq', 2, true);

INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (1, 'ADT^A28^ADT_A05', '1.3.6.1.4.12559.11.1.1.36', 2, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (2, 'ADT^A31^ADT_A05', '1.3.6.1.4.12559.11.1.1.39', 2, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (3, 'ADT^A47^ADT_A30', '1.3.6.1.4.12559.11.1.1.38', 2, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (4, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.40', 2, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (5, 'ADT^A24^ADT_A24', '1.3.6.1.4.12559.11.1.1.41', 2, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (6, 'ADT^A37^ADT_A37', '1.3.6.1.4.12559.11.1.1.37', 2, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (7, 'ADT^A01^ADT_A01', '1.3.6.1.4.12559.11.1.1.60', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (8, 'ADT^A04^ADT_A01', '1.3.6.1.4.12559.11.1.1.67', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (9, 'ADT^A03^ADT_A03', '1.3.6.1.4.12559.11.1.1.64', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (10, 'ADT^A08^ADT_A01', '1.3.6.1.4.12559.11.1.1.49', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (11, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.69', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (12, 'ADT^A11^ADT_A09', '1.3.6.1.4.12559.11.1.1.61', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (13, 'ADT^A13^ADT_A01', '1.3.6.1.4.12559.11.1.1.66', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (14, 'ADT^A05^ADT_A05', '1.3.6.1.4.12559.11.1.1.71', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (15, 'ADT^A06^ADT_A06', '1.3.6.1.4.12559.11.1.1.74', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (16, 'ADT^A07^ADT_A06', '1.3.6.1.4.12559.11.1.1.46', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (17, 'ADT^A02^ADT_A02', '1.3.6.1.4.12559.11.1.1.62', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (18, 'ADT^A38^ADT_A38', '1.3.6.1.4.12559.11.1.1.57', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (19, 'ADT^A12^ADT_A12', '1.3.6.1.4.12559.11.1.1.63', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (20, 'ADT^A14^ADT_A05', '1.3.6.1.4.12559.11.1.1.70', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (21, 'ADT^A15^ADT_A15', '1.3.6.1.4.12559.11.1.1.73', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (22, 'ADT^A16^ADT_A16', '1.3.6.1.4.12559.11.1.1.45', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (23, 'ADT^A27^ADT_A21', '1.3.6.1.4.12559.11.1.1.54', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (24, 'ADT^A26^ADT_A21', '1.3.6.1.4.12559.11.1.1.50', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (25, 'ADT^A25^ADT_A21', '1.3.6.1.4.12559.11.1.1.47', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (26, 'ADT^A21^ADT_A21', '1.3.6.1.4.12559.11.1.1.65', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (27, 'ADT^A54^ADT_A54', '1.3.6.1.4.12559.11.1.1.55', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (28, 'ADT^A22^ADT_A21', '1.3.6.1.4.12559.11.1.1.68', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (29, 'ADT^A44^ADT_A43', '1.3.6.1.4.12559.11.1.1.51', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (30, 'ADT^A55^ADT_A52', '1.3.6.1.4.12559.11.1.1.56', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (31, 'ADT^A52^ADT_A52', '1.3.6.1.4.12559.11.1.1.48', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (32, 'ADT^A53^ADT_A52', '1.3.6.1.4.12559.11.1.1.52', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (33, 'ADT^A09^ADT_A09', '1.3.6.1.4.12559.11.1.1.53', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (34, 'ADT^A10^ADT_A09', '1.3.6.1.4.12559.11.1.1.59', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (35, 'ADT^A33^ADT_A21', '1.3.6.1.4.12559.11.1.1.75', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (36, 'ADT^A32^ADT_A21', '1.3.6.1.4.12559.11.1.1.72', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (38, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.33', 1, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (37, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.44', 3, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (39, 'ADT^Z99^ADT_A01', '1.3.6.1.4.12559.11.1.1.58', 4, 1);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (40, 'QBP^Q22^QBP_Q21', '1.3.6.1.4.12559.11.1.1.31', 1, 3);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (41, 'RSP^K22^RSP_K21', '1.3.6.1.4.12559.11.1.1.35', 2, 3);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (42, 'QBP^ZV1^QBP_Q21', '1.3.6.1.4.12559.11.1.1.34', 1, 4);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (43, 'RSP^ZV2^RSP_ZV2', '1.3.6.1.4.12559.11.1.1.42', 2, 4);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (44, 'ADT^A28^ADT_A05', '1.3.6.1.4.12559.11.1.1.36', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (45, 'ADT^A31^ADT_A05', '1.3.6.1.4.12559.11.1.1.39', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (46, 'ADT^A47^ADT_A30', '1.3.6.1.4.12559.11.1.1.38', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (47, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.40', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (48, 'ADT^A24^ADT_A24', '1.3.6.1.4.12559.11.1.1.41', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (49, 'ADT^A37^ADT_A37', '1.3.6.1.4.12559.11.1.1.37', 5, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (50, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.33', 7, 2);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (51, 'ADT^A31^ADT_A05', '1.3.6.1.4.12559.11.1.1.28', 7, 7);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (52, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.33', 6, 7);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (53, 'ACK^A01^ACK', '1.3.6.1.4.12559.11.1.1.147', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (54, 'ACK^A04^ACK', '1.3.6.1.4.12559.11.1.1.148', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (55, 'ACK^A05^ACK', '1.3.6.1.4.12559.11.1.1.149', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (56, 'ACK^A08^ACK', '1.3.6.1.4.12559.11.1.1.150', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (57, 'ACK^A40^ACK', '1.3.6.1.4.12559.11.1.1.151', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (58, 'ADT^A08^ADT_A01', '1.3.6.1.4.12559.11.1.1.23', 5, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (59, 'ADT^A01^ADT_A01', '1.3.6.1.4.12559.11.1.1.24', 5, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (60, 'ADT^A04^ADT_A01', '1.3.6.1.4.12559.11.1.1.25', 5, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (61, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.26', 5, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (62, 'ADT^A05^ADT_A01', '1.3.6.1.4.12559.11.1.1.27', 5, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (63, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.29', 7, 5);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (64, 'QBP^Q23^QBP_Q21', '1.3.6.1.4.12559.11.1.1.92', 6, 6);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (65, 'RSP^K23^RSP_K23', '1.3.6.1.4.12559.11.1.1.30', 7, 6);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (66, 'QCN^J01^QCN_J01', '1.3.6.1.4.12559.11.1.1.32', 1, 3);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (67, 'QCN^J01^QCN_J01', '1.3.6.1.4.12559.11.1.1.43', 1, 4);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (68, 'ACK^J01^ACK', '1.3.6.1.4.12559.11.1.1.192', 2, 3);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (69, 'ACK^J01^ACK', '1.3.6.1.4.12559.11.1.1.193', 2, 4);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (70, 'ADT^A01^ADT_A01', '1.3.6.1.4.12559.11.1.1.77', 8, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (71, 'ADT^A04^ADT_A01', '1.3.6.1.4.12559.11.1.1.79', 8, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (72, 'ADT^A05^ADT_A01', '1.3.6.1.4.12559.11.1.1.80', 8, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (73, 'ADT^A11^ADT_A09', '1.3.6.1.4.12559.11.1.1.78', 8, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (74, 'ADT^A38^ADT_A38', '1.3.6.1.4.12559.11.1.1.76', 8, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (75, 'ADT^A08^ADT_A01', '1.3.6.1.4.12559.11.1.1.82', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (76, 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.87', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (77, 'ADT^A06^ADT_A06', '1.3.6.1.4.12559.11.1.1.88', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (78, 'ADT^A07^ADT_A06', '1.3.6.1.4.12559.11.1.1.81', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (79, 'ADT^A03^ADT_A03', '1.3.6.1.4.12559.11.1.1.84', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (80, 'ADT^A13^ADT_A01', '1.3.6.1.4.12559.11.1.1.86', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (81, 'ADT^A02^ADT_A02', '1.3.6.1.4.12559.11.1.1.83', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (82, 'ADT^A12^ADT_A12', '1.3.6.1.4.12559.11.1.1.85', 8, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (83, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.29', 9, 8);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (84, 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.29', 9, 9);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (85, 'PRPA_IN201305UV02', 'PDQv3 - Patient Demographics Query', 1, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (86, 'QUQI_IN000003UV01', 'PDQv3 - Patient Demographics Query HL7V3 Continuation', 1, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (87, 'QUQI_IN000003UV01_Cancel', 'PDQv3 - Patient Demographics Query HL7V3 Cancellation', 1, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (88, 'PRPA_IN201306UV02', 'PDQv3 - Patient Demographics Query Response', 2, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (89, 'MCCI_IN000002UV01', 'PDQv3 - Accept Acknowledgement', 2, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (90, 'PRPA_IN201305UV02', 'KPDQ - Patient Demographics Query', 1, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (91, 'PRPA_IN201306UV02', 'KPDQ - Patient Demographics Query Response', 2, 10);


SELECT pg_catalog.setval('ort_validation_parameters_id_seq', 91, true);



INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,1); 
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,2); 
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,3); 
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,4); 
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,5); 
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,6); 
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,37);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,38);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,39);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,7);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,10);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,11);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,8);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,9);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,12);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,13);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,14);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,15);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,16);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,17);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,18);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,19);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,40);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,41);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,42);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,43);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,44);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,45);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,46);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,47);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,48);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,49);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,50);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,51);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,52);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,53);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,54);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,55);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,56);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,57);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,58);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,59);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,60);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,61);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,62);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,63);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,64);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,65);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,66);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,67);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,68);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,69);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,70);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,71);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,72);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,73);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,74);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,75);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,76);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,77);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,78);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,79);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,80);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,81);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,82);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,83);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,84);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,85);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,86);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,87);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,88);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,89);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (2,90);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (2,91);

insert into tf_domain (id, description, keyword, name) values (1, 'ITI', 'ITI', 'IT-Infrasctructure');
insert into tf_domain (id, description, keyword, name) values (2, 'KSA', 'KSA', 'KSA MoH');
SELECT pg_catalog.setval('tf_domain_id_seq', 2, true);


--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (1, 'A01', 'A11', 'Z99', 'Admit inpatient', 1, true, true, null);
--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (2, 'A04', 'A11', 'Z99', 'Register outpatient', 1, true, true, null);
--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (3, 'A03', 'A13', 'Z99', 'Discharge patient', 1, true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (1, 'A01', 'A11', null, 'Admit inpatient', 'IHE', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (2, 'A04', 'A11', null, 'Register outpatient', 'IHE', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (3, 'A03', 'A13', null, 'Discharge patient', 'IHE', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (4, 'A08', null, null, 'Update patient information', 'IHE', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (5, 'A40', null, null, 'Merge patient identifier lists', 'IHE', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (6, 'A05', 'A38', null, 'Pre-admit patient', 1, true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (7, 'A06', null, null, 'Change patient class to inpatient', 'IHE', true, false, true);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (8, 'A07', null, null, 'Change patient class to outpatient', 'IHE', true, false, false);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (9, 'A02', 'A12', null, 'Transfer patient', 'IHE', true, false, false);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (10, 'A01', null, null, 'Admission of an in-patient', 'IHE_PIX', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (11, 'A04', null, null, 'Registration of an out-patient', 'IHE_PIX', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (12, 'A08', null, null, 'Update Patient Information', 'IHE_PIX', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (13, 'A40', null, null, 'Merge patient - Internal ID', 'IHE_PIX', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (14, 'A05', null, null, 'Pre-admission of an in-patient', 'IHE_PIX', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (15, 'A01', 'A11', null, 'Admission of an in-patient', 'IHE_RAD1', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (16, 'A04', 'A11', null, 'Registration of an out-patient', 'IHE_RAD1', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (17, 'A05', 'A38', null, 'Pre-admission of an in-patient', 'IHE_RAD1', true, true, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (18, 'A08', null, null, 'Update Patient Information', 'IHE_RAD12', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (19, 'A40', null, null, 'Merge patient - Internal ID', 'IHE_RAD12', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (20, 'A02', 'A12', null, 'Transfer patient', 'IHE_RAD12', true, false, false);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (21, 'A03', 'A13', null, 'Discharge patient', 'IHE_RAD12', true, false, null);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (22, 'A06', null, null, 'Change patient class to inpatient', 'IHE_RAD12', true, false, true);
INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (23, 'A07', null, null, 'Change patient class to outpatient', 'IHE_RAD12', true, false, false);
--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (10, 'A14', 'A27', 'Z99', 'Pending admit', 1, true, true, null);
--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (11, 'A15', 'A26', 'Z99', 'Pending transfer', 1, true, false, false);
--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (12, 'A16', 'A25', 'Z99', 'Pending discharge', 1, true, false, false);
--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (13, 'A54', 'A55', 'Z99', 'Change attending doctor', 1, true, false, null);
--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (14, 'A21', 'A52', 'Z99', 'Leave of absence', 1, true, false, null);
--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (15, 'A22', 'A53', 'Z99', 'Return from leave of absence', 1, true, false, null);
--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (16, 'A44', null, null, 'Move account information', 1, true, false, null);
--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (17, 'A09', 'A33', null, 'Patient departing - Tracking', 1, true, false, false);
--INSERT INTO pam_encounter_management_event (id, insert_trigger_event, cancel_trigger_event, update_trigger_event, event_name, page_id, available, insert_requires_new_encounter, insert_requires_outpatient) VALUES (18, 'A10', 'A32', null, 'Patient arriving - Tracking', 1, true, false, false);

SELECT pg_catalog.setval('pam_encounter_management_event_id_seq', 23, true);

--
-- Data for Name: cmn_home; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO cmn_home VALUES (1, 'Welcome to PAM Simulator', 'eng', 'PAM simulator');
INSERT INTO cmn_home VALUES (2, 'Bienvenue dans le Simulateur PAM', 'fra', 'PAM simulator');
SELECT pg_catalog.setval('cmn_home_id_seq', 2, true);




