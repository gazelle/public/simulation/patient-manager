--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.22
-- Dumped by pg_dump version 9.6.22

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cmn_value_set; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_value_set (
    id integer NOT NULL,
    usage character varying(255),
    value_set_keyword character varying(255),
    value_set_name character varying(255),
    value_set_oid character varying(255),
    accessible boolean,
    last_check timestamp without time zone
);


ALTER TABLE public.cmn_value_set OWNER TO gazelle;

--
-- Data for Name: cmn_value_set; Type: TABLE DATA; Schema: public; Owner: gazelle
--

COPY public.cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid, accessible, last_check) FROM stdin;
22	ACC-2	HL70050	Accident code	2.16.840.1.113883.2.8.3.3.22	t	2018-12-18 18:12:25.839
17	PV1-41	HL70117	Account status	2.16.840.1.113883.2.8.3.3.17	t	2018-12-18 18:12:25.941
28	ZFV-6-7	HL79105	address type	2.16.840.1.113883.2.8.3.3.28	t	2018-12-18 18:12:26.055
1	PID-8	HL70001	Administrative Sex	2.16.840.1.113883.2.8.3.3.1	t	2018-12-18 18:12:26.107
11	PV1-4	HL70007	Admission type	2.16.840.1.113883.2.8.3.3.11	t	2018-12-18 18:12:26.159
18	PV2-3	HL79110	Admit reason (Psychiatry)	2.16.840.1.113883.2.8.3.3.18	t	2018-12-18 18:12:26.208
12	PV1-14	HL70023	Admit source	2.16.840.1.113883.2.8.3.3.12	t	2018-12-18 18:12:26.257
40	PV1	BED	Bed	2.16.840.1.113883.2.8.3.3.49	t	2018-12-18 18:12:26.315
10	PV1-3.5	HL70116	Bed status	2.16.840.1.113883.2.8.3.3.10	t	2018-12-18 18:12:26.365
41	OBX	BLOOD_GROUP	Blood Group (KSA)	1.3.6.1.4.1.12559.11.10.1.3.1.42.20	t	2018-12-18 18:12:26.422
20	PV2-30	HL70218	Charge adjustment	2.16.840.1.113883.2.8.3.3.20	t	2018-12-18 18:12:26.474
14	PV1-21	HL70032	Charge Price indicator	2.16.840.1.113883.2.8.3.3.14	t	2018-12-18 18:12:26.547
8	NK1-7	HL70131	Contact role	2.16.840.1.113883.2.8.3.3.8	t	2018-12-18 18:12:26.622
52	\N	CONTACT_ROLE	Contact role	2.16.840.1.113883.2.8.3.3.8	t	2018-12-18 18:12:26.674
15	PV1-22	HL70045	Courtesy code	2.16.840.1.113883.2.8.3.3.15	t	2018-12-18 18:12:26.724
16	PV1-36	HL70112	Discharge disposition	2.16.840.1.113883.2.8.3.3.16	t	2018-12-18 18:12:26.774
42	PV1	DOCTOR	Doctors	2.16.840.1.113883.2.8.3.3.43	t	2018-12-18 18:12:26.824
30	ZFM-1	HL73303	DRG admit mode	2.16.840.1.113883.2.8.3.3.30	t	2018-12-18 18:12:26.874
31	ZFM-2	HL73304	DRG discharge mode	2.16.840.1.113883.2.8.3.3.31	t	2018-12-18 18:12:26.93
32	ZFM-3 + ZFM-4 ?	HL73305	DRG origin and destination mode	2.16.840.1.113883.2.8.3.3.32	t	2018-12-18 18:12:26.989
43	PV1	FACILITY	Facilities	2.16.840.1.113883.2.8.3.3.41	t	2018-12-18 18:12:27.041
51		FACILITY_ADDR	Facilities addresses	2.16.840.1.113883.2.8.3.3.42	t	2018-12-18 18:12:27.099
33	IN1-2	HL70068	Guarantor Type	2.16.840.1.113883.2.8.3.3.33	t	2018-12-18 18:12:27.149
44	PV1	HOSPITAL_SERVICE	Hospital services	2.16.840.1.113883.2.8.3.3.40	t	2018-12-18 18:12:27.208
3	PID-32	HL70445	Identity Reliability Code	2.16.840.1.113883.2.8.3.3.3	t	2018-12-18 18:12:27.273
4	PD1-2	HL70220	Living arrangement	2.16.840.1.113883.2.8.3.3.4	t	2018-12-18 18:12:27.341
2	PID-16	HL70002	Marital status	2.16.840.1.113883.2.8.3.3.2	t	2018-12-18 18:12:27.399
49	ZBE-7	MEDICAL_WARD	Medical ward	2.16.840.1.113883.2.8.3.3.45	t	2018-12-18 18:12:27.463
21	PV2-38	HL70430	Mode of arrival code	2.16.840.1.113883.2.8.3.3.21	t	2018-12-18 18:12:27.515
36	PID-5-7	HL70200	Name Type Code	2.16.840.1.113883.2.8.3.3.36	t	2018-12-18 18:12:27.566
23	ZBE-9	HL79106	Nature of movement	2.16.840.1.113883.2.8.3.3.23	t	2018-12-18 18:12:27.616
50	ZBE-8	NURSING_WARD	Nursing ward	2.16.840.1.113883.2.8.3.3.46	t	2018-12-18 18:12:27.665
35	OBX-11	HL70085	Observation status	2.16.840.1.113883.2.8.3.3.35	t	2018-12-18 18:12:27.715
26	ZFA-6	HL79103	Opposition of the patient to the bris de glace mode access	2.16.840.1.113883.2.8.3.3.26	t	2018-12-18 18:12:27.807
27	ZFA-7	HL79104	Opposition of the patient to the centre 15 mode access	2.16.840.1.113883.2.8.3.3.27	t	2018-12-18 18:12:27.857
9	PV1-2	HL70004	Patient class	2.16.840.1.113883.2.8.3.3.9	t	2018-12-18 18:12:27.907
24	ZFA-1	HL79101	Patient PHR s status	2.16.840.1.113883.2.8.3.3.24	t	2018-12-18 18:12:27.957
45	PV1	POINT_OF_CARE	Point of care	2.16.840.1.113883.2.8.3.3.44	t	2018-12-18 18:12:28.007
5	ROL-2	HL70287	Problem/Goal action code	2.16.840.1.113883.2.8.3.3.5	t	2018-12-18 18:12:28.082
6	ROL-3	HL70443	Provider role	2.16.840.1.113883.2.8.3.3.6	t	2018-12-18 18:12:28.132
46	PID	RACE	Race	1.3.6.1.4.1.21367.101.102	t	2018-12-18 18:12:28.207
7	NK1-3	HL70063	Relationship	2.16.840.1.113883.2.8.3.3.7	t	2018-12-18 18:12:28.39
53	\N	RELATIONSHIP	Relationship	2.16.840.1.113883.2.8.3.3.7	t	2018-12-18 18:12:28.449
47	PID	RELIGION	Religion	1.3.6.1.4.1.21367.101.122	t	2018-12-18 18:12:28.538
29	ZFV-10	HL73302	RIMP Code	2.16.840.1.113883.2.8.3.3.29	t	2018-12-18 18:12:28.624
48	PV1	ROOM	Room	2.16.840.1.113883.2.8.3.3.48	t	2018-12-18 18:12:28.674
39	ZFD-3	HL79109	SMS Consent	2.16.840.1.113883.2.8.3.3.39	t	2018-12-18 18:12:28.722
54	BP6	HL79108	Socio-professional	2.16.840.1.113883.2.8.3.3.4	t	2018-12-18 18:12:28.774
38	ZFP-2	HL79108	Socio-Professional group	2.16.840.1.113883.2.8.3.3.38	t	2018-12-18 18:12:28.824
37	ZFP-1	HL79107	Socio-Professional occupation	2.16.840.1.113883.2.8.3.3.37	t	2018-12-18 18:12:28.874
34	IN3-5.1	HL70146	User amount type	2.16.840.1.113883.2.8.3.3.34	t	2018-12-18 18:12:28.941
25	ZFA-4	HL79102	Valid access authorization to the patient s PHR	2.16.840.1.113883.2.8.3.3.25	t	2018-12-18 18:12:29.048
13	PV1-16	HL70099	VIP indicator	2.16.840.1.113883.2.8.3.3.13	t	2018-12-18 18:12:29.199
19	PV2-7	HL70130	Visit User Code	2.16.840.1.113883.2.8.3.3.19	t	2018-12-18 18:12:29.266
55	ZFV-11	HL73306	Types de prise en charge durant le transport	2.16.840.1.113883.2.8.3.3.3306	t	2019-06-05 08:43:38.636039
\.


--
-- Name: cmn_value_set cmn_value_set_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_value_set
    ADD CONSTRAINT cmn_value_set_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

