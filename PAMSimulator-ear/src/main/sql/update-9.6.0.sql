INSERT INTO cmn_value_set(id, value_set_oid, value_set_name, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'), '2.16.840.1.113883.12.131', 'Contact role', 'CONTACT_ROLE');
INSERT INTO cmn_value_set(id, value_set_oid, value_set_name, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'), '2.16.840.1.113883.12.63', 'Relationship', 'RELATIONSHIP');
ALTER TABLE pat_patient_address ADD COLUMN dtype varchar(31);
ALTER TABLE pam_phone_number ADD COLUMN dtype varchar(31);
UPDATE pat_patient_address SET dtype = 'patient_address';
UPDATE pam_phone_number SET dtype = 'patient_phone_number';

INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 3, 'EPD - Electronische Patient Dossier (eHealthSuisse)', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 4, 'Radiology ADT', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 5, 'PAM - Patient Administration Management', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 6, 'PAM Automaton', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 7, 'PIX - 	Patient Identifier Cross-referencing for MPI', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 8, 'PIXV3 - 	Patient Identifier Cross-referencing HL7V3', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 9, 'PIXm - 	Patient Identifier Cross-referencing for Mobile', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 10, 'PDQ - 	Patient Demographic Query', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 11, 'PDQV3 - 	Patient Demographic Query HL7V3', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 12, 'PDQm - 	Patient Demographic Query for Mobile', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 13, 'XCPD - 	Cross-Community Patient Discovery', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 14, 'XUA - 	Cross-Enterprise User Access', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 16, 'ITI-FR - French version of the ITI profiles', false);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 17, 'Sequoia - Extensions for The Sequoia project', false);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 18, 'Modality Worklist - Users can share patients with Order Manager', true);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 19, 'SeHe - Saudi eHealth Project', false);
INSERT INTO pam_simulator_feature (id, keyword, label, enabled) VALUES (nextval('pam_simulator_feature_id_seq'), 15, 'IHE - Default version of the profiles', false);

delete from app_configuration where variable = 'bp6_mode_enabled';
delete from app_configuration where variable = 'is_sequoia';
DELETE FROM app_configuration WHERE variable = 'enable_worklist';

INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'BP6', 'HL79108', 'Socio-professional', '2.16.840.1.113883.2.8.3.3.4');

update app_configuration set variable = 'fhir_server_url' WHERE variable = 'pdqm_pds_url';

ALTER TABLE pam_hierarchic_designator DROP CONSTRAINT uk_hg2191v23q6fig8yfvb6e31fq;