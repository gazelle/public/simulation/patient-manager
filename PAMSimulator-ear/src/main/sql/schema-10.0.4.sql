--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Ubuntu 11.7-0ubuntu0.19.10.1)
-- Dumped by pg_dump version 11.7 (Ubuntu 11.7-0ubuntu0.19.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.affinity_domain (
    id integer NOT NULL,
    keyword character varying(255),
    label_to_display character varying(255),
    profile character varying(255)
);


ALTER TABLE public.affinity_domain OWNER TO gazelle;

--
-- Name: affinity_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.affinity_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affinity_domain_id_seq OWNER TO gazelle;

--
-- Name: affinity_domain_transactions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.affinity_domain_transactions (
    affinity_domain_id integer NOT NULL,
    transaction_id integer NOT NULL
);


ALTER TABLE public.affinity_domain_transactions OWNER TO gazelle;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE public.app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_configuration (
    id integer NOT NULL,
    comment character varying(255),
    approved boolean NOT NULL,
    is_secured boolean,
    host_id integer
);


ALTER TABLE public.cfg_configuration OWNER TO gazelle;

--
-- Name: cfg_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_dicom_scp_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE public.cfg_dicom_scp_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_dicom_scp_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_dicom_scp_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_dicom_scu_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE public.cfg_dicom_scu_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_dicom_scu_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_dicom_scu_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_out integer,
    port_secure integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_v3_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_v3_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_v3_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_v3_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_v3_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_v3_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_v3_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_v3_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_host; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_host (
    id integer NOT NULL,
    alias character varying(255),
    comment character varying(255),
    hostname character varying(255) NOT NULL,
    ip character varying(255)
);


ALTER TABLE public.cfg_host OWNER TO gazelle;

--
-- Name: cfg_host_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_host_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_host_id_seq OWNER TO gazelle;

--
-- Name: cfg_sop_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_sop_class (
    id integer NOT NULL,
    keyword character varying(255) NOT NULL,
    name character varying(255)
);


ALTER TABLE public.cfg_sop_class OWNER TO gazelle;

--
-- Name: cfg_sop_class_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_sop_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_sop_class_id_seq OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_syslog_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_syslog_configuration OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_syslog_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_syslog_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_web_service_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    assigning_authority character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_web_service_configuration OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_web_service_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_web_service_configuration_id_seq OWNER TO gazelle;

--
-- Name: cmn_company_details; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_company_details (
    id integer NOT NULL,
    company_keyword character varying(255)
);


ALTER TABLE public.cmn_company_details OWNER TO gazelle;

--
-- Name: cmn_company_details_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_company_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_company_details_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE public.cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_home_id_seq OWNER TO gazelle;

--
-- Name: cmn_ip_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_ip_address (
    id integer NOT NULL,
    added_by character varying(255),
    added_on timestamp without time zone,
    value character varying(255),
    company_details_id integer
);


ALTER TABLE public.cmn_ip_address OWNER TO gazelle;

--
-- Name: cmn_ip_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_ip_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_ip_address_id_seq OWNER TO gazelle;

--
-- Name: cmn_message_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_message_instance (
    id integer NOT NULL,
    content bytea,
    issuer character varying(255),
    type character varying(255),
    validation_detailed_result bytea,
    validation_status character varying(255),
    issuing_actor integer,
    issuer_ip_address character varying(255),
    payload bytea
);


ALTER TABLE public.cmn_message_instance OWNER TO gazelle;

--
-- Name: cmn_message_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_message_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_message_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_message_instance_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_message_instance_metadata (
    id integer NOT NULL,
    label character varying(255),
    value character varying(255),
    message_instance_id integer
);


ALTER TABLE public.cmn_message_instance_metadata OWNER TO gazelle;

--
-- Name: cmn_message_instance_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_message_instance_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_message_instance_metadata_id_seq OWNER TO gazelle;

--
-- Name: cmn_receiver_console; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_receiver_console (
    id integer NOT NULL,
    code character varying(255),
    comment text,
    message_identifier character varying(255),
    message_type character varying(255),
    sut character varying(255),
    "timestamp" timestamp without time zone,
    domain_id integer,
    simulated_actor_id integer,
    transaction_id integer
);


ALTER TABLE public.cmn_receiver_console OWNER TO gazelle;

--
-- Name: cmn_receiver_console_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_receiver_console_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_receiver_console_id_seq OWNER TO gazelle;

--
-- Name: cmn_transaction_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_transaction_instance (
    id integer NOT NULL,
    "timestamp" timestamp without time zone,
    company_keyword character varying(255),
    is_visible boolean,
    domain_id integer,
    request_id integer,
    response_id integer,
    simulated_actor_id integer,
    transaction_id integer,
    standard character varying(64)
);


ALTER TABLE public.cmn_transaction_instance OWNER TO gazelle;

--
-- Name: cmn_transaction_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_transaction_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_transaction_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_validator_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_validator_usage (
    id integer NOT NULL,
    caller character varying(255),
    date date,
    status character varying(255),
    type character varying(255)
);


ALTER TABLE public.cmn_validator_usage OWNER TO gazelle;

--
-- Name: cmn_validator_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_validator_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_validator_usage_id_seq OWNER TO gazelle;

--
-- Name: cmn_value_set; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_value_set (
    id integer NOT NULL,
    usage character varying(255),
    value_set_keyword character varying(255),
    value_set_name character varying(255),
    value_set_oid character varying(255),
    accessible boolean,
    last_check timestamp without time zone
);


ALTER TABLE public.cmn_value_set OWNER TO gazelle;

--
-- Name: cmn_value_set_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_value_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_value_set_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_contextual_information (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    value character varying(255),
    path integer NOT NULL
);


ALTER TABLE public.gs_contextual_information OWNER TO gazelle;

--
-- Name: gs_contextual_information_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_contextual_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_contextual_information_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_contextual_information_instance (
    id integer NOT NULL,
    form character varying(255),
    value character varying(255),
    contextual_information_id integer NOT NULL,
    test_steps_instance_id integer NOT NULL
);


ALTER TABLE public.gs_contextual_information_instance OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_contextual_information_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_contextual_information_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_message (
    id integer NOT NULL,
    message_content bytea,
    message_type_id character varying(255),
    time_stamp timestamp without time zone,
    test_instance_participants_receiver_id integer,
    test_instance_participants_sender_id integer,
    transaction_id integer
);


ALTER TABLE public.gs_message OWNER TO gazelle;

--
-- Name: gs_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_message_id_seq OWNER TO gazelle;

--
-- Name: gs_system; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_system (
    id integer NOT NULL,
    institution_keyword character varying(255),
    keyword character varying(255),
    system_owner character varying(255)
);


ALTER TABLE public.gs_system OWNER TO gazelle;

--
-- Name: gs_system_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_system_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance (
    id integer NOT NULL,
    server_test_instance_id character varying(255) NOT NULL,
    test_instance_status_id integer
);


ALTER TABLE public.gs_test_instance OWNER TO gazelle;

--
-- Name: gs_test_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_message (
    test_instance_id integer NOT NULL,
    message_id integer NOT NULL
);


ALTER TABLE public.gs_test_instance_message OWNER TO gazelle;

--
-- Name: gs_test_instance_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_oid (
    oid_configuration_id integer NOT NULL,
    test_instance_id integer NOT NULL
);


ALTER TABLE public.gs_test_instance_oid OWNER TO gazelle;

--
-- Name: gs_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_participants (
    id integer NOT NULL,
    server_aipo_id character varying(255) NOT NULL,
    server_test_instance_participants_id character varying(255) NOT NULL,
    aipo_id integer,
    system_id integer
);


ALTER TABLE public.gs_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_instance_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_participants_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_status (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE public.gs_test_instance_status OWNER TO gazelle;

--
-- Name: gs_test_instance_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_status_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_test_instance_participants (
    test_instance_id integer NOT NULL,
    test_instance_participants_id integer NOT NULL
);


ALTER TABLE public.gs_test_instance_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_steps_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_steps_instance (
    id integer NOT NULL,
    dicom_scp_config_id integer,
    dicom_scu_config_id integer,
    hl7v2_initiator_config_id integer,
    hl7v2_responder_config_id integer,
    hl7v3_initiator_config_id integer,
    hl7v3_responder_config_id integer,
    syslog_config_id integer,
    testinstance_id integer,
    web_service_config_id integer
);


ALTER TABLE public.gs_test_steps_instance OWNER TO gazelle;

--
-- Name: gs_test_steps_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_steps_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_steps_instance_id_seq OWNER TO gazelle;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO gazelle;

--
-- Name: hl7_charset; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.hl7_charset (
    id integer NOT NULL,
    display_name character varying(255) NOT NULL,
    hl7_code character varying(255),
    java_code character varying(255)
);


ALTER TABLE public.hl7_charset OWNER TO gazelle;

--
-- Name: hl7_charset_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hl7_charset_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hl7_charset_id_seq OWNER TO gazelle;

--
-- Name: hl7_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hl7_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hl7_message_id_seq OWNER TO gazelle;

--
-- Name: hl7_simulator_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.hl7_simulator_responder_configuration (
    id integer NOT NULL,
    accepted_message_type character varying(255),
    accepted_trigger_event character varying(255),
    ip_address character varying(255),
    listening_port integer,
    name character varying(255) NOT NULL,
    receiving_application character varying(255) NOT NULL,
    receiving_facility character varying(255) NOT NULL,
    server_bean_name character varying(255),
    charset_id integer,
    simulated_actor_id integer NOT NULL,
    transaction_id integer,
    is_running boolean,
    hl7_protocol integer,
    message_encoding integer,
    url character varying(255),
    domain_id integer
);


ALTER TABLE public.hl7_simulator_responder_configuration OWNER TO gazelle;

--
-- Name: hl7_simulator_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hl7_simulator_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hl7_simulator_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: ort_evscvalidationresults_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.ort_evscvalidationresults_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ort_evscvalidationresults_id_seq OWNER TO gazelle;

--
-- Name: ort_validation_parameters_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.ort_validation_parameters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ort_validation_parameters_id_seq OWNER TO gazelle;

--
-- Name: package_name_for_profile_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.package_name_for_profile_oid (
    id integer NOT NULL,
    package_name character varying(255),
    profile_oid character varying(255)
);


ALTER TABLE public.package_name_for_profile_oid OWNER TO gazelle;

--
-- Name: package_name_for_profile_oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.package_name_for_profile_oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.package_name_for_profile_oid_id_seq OWNER TO gazelle;

--
-- Name: pam_administrative_account; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_administrative_account (
    id integer NOT NULL,
    account_number character varying(255),
    creation_date timestamp without time zone,
    creator character varying(255),
    patient_id integer,
    simulated_actor_id integer
);


ALTER TABLE public.pam_administrative_account OWNER TO gazelle;

--
-- Name: pam_administrative_account_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_administrative_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_administrative_account_id_seq OWNER TO gazelle;

--
-- Name: pam_encounter; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_encounter (
    id integer NOT NULL,
    admit_date timestamp without time zone,
    attending_doctor_code character varying(255),
    creation_date timestamp without time zone,
    creator character varying(255),
    discharge_date timestamp without time zone,
    expected_admit_date timestamp without time zone,
    expected_discharge_date timestamp without time zone,
    expected_loa_date date,
    leave_of_absence boolean,
    leave_of_absence_date timestamp without time zone,
    patient_class_code character varying(255),
    prior_attending_doctor_code character varying(255),
    prior_patient_class_code character varying(255),
    referring_doctor_code character varying(255),
    transfer_planned_date timestamp without time zone,
    visit_number character varying(255),
    patient_id integer,
    simulated_actor_id integer,
    administrative_account_id integer,
    expected_loa_return timestamp without time zone,
    loa_return_date timestamp without time zone,
    is_open boolean,
    admitting_doctor_code character varying(255),
    hospital_service_code character varying(255),
    visit_number_id character varying(255),
    visit_designator_id integer,
    accident_code character varying(255),
    accident_date timestamp without time zone,
    admission_type character varying(255),
    patient_account_number character varying(255),
    status integer,
    prior_status integer,
    drg_movement_id integer,
    encounter_additional_info_id integer,
    patients_phr_info_id integer
);


ALTER TABLE public.pam_encounter OWNER TO gazelle;

--
-- Name: pam_encounter_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_encounter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_encounter_id_seq OWNER TO gazelle;

--
-- Name: pam_encounter_management_event; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_encounter_management_event (
    id integer NOT NULL,
    available boolean,
    cancel_trigger_event character varying(255),
    event_name character varying(255),
    insert_trigger_event character varying(255),
    page_id character varying(255),
    update_trigger_event character varying(255),
    selection character varying(255)
);


ALTER TABLE public.pam_encounter_management_event OWNER TO gazelle;

--
-- Name: pam_encounter_management_event_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_encounter_management_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_encounter_management_event_id_seq OWNER TO gazelle;

--
-- Name: pam_fr_additional_demographics; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_fr_additional_demographics (
    id integer NOT NULL,
    month integer,
    number_of_week_of_gestation integer,
    sms_consent boolean,
    socio_professional_group character varying(255),
    socio_professional_occupation character varying(255),
    week_in_month integer,
    year_of_birth integer,
    date_of_birth_on_carte_vitale timestamp without time zone,
    date_of_birth_corrected boolean,
    identity_acquisition_mode character varying(255),
    proof_of_identity character varying(255),
    document_expiration_date timestamp without time zone,
    date_of_the_insi_webservice_request timestamp without time zone
);


ALTER TABLE public.pam_fr_additional_demographics OWNER TO gazelle;

--
-- Name: pam_fr_additional_demographics_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_fr_additional_demographics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_fr_additional_demographics_id_seq OWNER TO gazelle;

--
-- Name: pam_fr_drg_movement; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_fr_drg_movement (
    id integer NOT NULL,
    pmsi_admission_mode character varying(255),
    pmsi_destination_mode character varying(255),
    pmsi_discharge_mode character varying(255),
    pmsi_establishment_of_origin_mode character varying(255)
);


ALTER TABLE public.pam_fr_drg_movement OWNER TO gazelle;

--
-- Name: pam_fr_drg_movement_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_fr_drg_movement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_fr_drg_movement_id_seq OWNER TO gazelle;

--
-- Name: pam_fr_encounter_additional_info; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_fr_encounter_additional_info (
    id integer NOT NULL,
    discharge_mode character varying(255),
    establishment_of_destination_address character varying(255),
    establishment_of_origin character varying(255),
    establishment_of_origin_address character varying(255),
    last_visit_date timestamp without time zone,
    legal_care_mode character varying(255),
    origin_account_number character varying(255),
    placement_end_date timestamp without time zone,
    placement_start_date timestamp without time zone,
    transport_mode character varying(255),
    care_during_transport character varying(255)
);


ALTER TABLE public.pam_fr_encounter_additional_info OWNER TO gazelle;

--
-- Name: pam_fr_encounter_additional_info_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_fr_encounter_additional_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_fr_encounter_additional_info_id_seq OWNER TO gazelle;

--
-- Name: pam_fr_legal_care_mode; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_fr_legal_care_mode (
    id integer NOT NULL,
    action character varying(255) NOT NULL,
    care_end_date_time timestamp without time zone,
    care_start_date_time timestamp without time zone NOT NULL,
    comment text,
    identifier character varying(255) NOT NULL,
    mode character varying(255),
    rimp_code character varying(255),
    set_id integer NOT NULL,
    patient_id integer
);


ALTER TABLE public.pam_fr_legal_care_mode OWNER TO gazelle;

--
-- Name: pam_fr_legal_care_mode_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_fr_legal_care_mode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_fr_legal_care_mode_id_seq OWNER TO gazelle;

--
-- Name: pam_fr_patients_phr_info; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_fr_patients_phr_info (
    id integer NOT NULL,
    bris_de_glace boolean,
    centre_15 boolean,
    closing_date timestamp without time zone,
    facility_access boolean,
    facility_access_collection_date timestamp without time zone,
    opposition_collection_date timestamp without time zone,
    status character varying(255),
    status_collection_date timestamp without time zone
);


ALTER TABLE public.pam_fr_patients_phr_info OWNER TO gazelle;

--
-- Name: pam_fr_patients_phr_info_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_fr_patients_phr_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_fr_patients_phr_info_id_seq OWNER TO gazelle;

--
-- Name: pam_graph_description; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_graph_description (
    id integer NOT NULL,
    description character varying(255),
    edges integer NOT NULL,
    graphmllink character varying(255),
    imagelink character varying(255),
    name character varying(255),
    vertices integer NOT NULL,
    active boolean
);


ALTER TABLE public.pam_graph_description OWNER TO gazelle;

--
-- Name: pam_graph_description_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_graph_description_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_graph_description_id_seq OWNER TO gazelle;

--
-- Name: pam_graph_execution; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_graph_execution (
    id integer NOT NULL,
    fulledgecoverage boolean NOT NULL,
    graph_id integer,
    sut_id integer,
    bp6mode boolean,
    validate_transaction_instance boolean
);


ALTER TABLE public.pam_graph_execution OWNER TO gazelle;

--
-- Name: pam_graph_execution_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_graph_execution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_graph_execution_id_seq OWNER TO gazelle;

--
-- Name: pam_graph_execution_results; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_graph_execution_results (
    id integer NOT NULL,
    executionstatus integer,
    "timestamp" timestamp without time zone,
    graphexecution_id integer
);


ALTER TABLE public.pam_graph_execution_results OWNER TO gazelle;

--
-- Name: pam_graph_execution_results_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_graph_execution_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_graph_execution_results_id_seq OWNER TO gazelle;

--
-- Name: pam_graph_execution_results_pam_test_step; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_graph_execution_results_pam_test_step (
    pam_graph_execution_results_id integer NOT NULL,
    messageslist_id integer NOT NULL
);


ALTER TABLE public.pam_graph_execution_results_pam_test_step OWNER TO gazelle;

--
-- Name: pam_hierarchic_designator; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_hierarchic_designator (
    id integer NOT NULL,
    namespace_id character varying(255),
    type character varying(255),
    universal_id character varying(255),
    universal_id_type character varying(255),
    is_default boolean,
    index integer,
    prefix character varying(255),
    is_tool_assigning_authority boolean,
    usage integer NOT NULL,
    cat_usage boolean
);


ALTER TABLE public.pam_hierarchic_designator OWNER TO gazelle;

--
-- Name: pam_hierarchic_designator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_hierarchic_designator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_hierarchic_designator_id_seq OWNER TO gazelle;

--
-- Name: pam_message_id_generator; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_message_id_generator (
    id integer NOT NULL,
    index integer,
    oid character varying(255),
    issuing_actor_id integer
);


ALTER TABLE public.pam_message_id_generator OWNER TO gazelle;

--
-- Name: pam_message_id_generator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_message_id_generator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_message_id_generator_id_seq OWNER TO gazelle;

--
-- Name: pam_movement; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_movement (
    id integer NOT NULL,
    assigned_patient_location character varying(255),
    creator character varying(255),
    is_current_movement boolean,
    is_patient_deceased boolean,
    last_changed timestamp without time zone,
    movement_date timestamp without time zone,
    movement_unique_id character varying(255),
    patient_class_code character varying(255),
    is_pending_movement boolean,
    pending_patient_location character varying(255),
    prior_patient_location character varying(255),
    prior_temporary_location character varying(255),
    temporary_patient_location character varying(255),
    transfer_planned_date date,
    trigger_event character varying(255),
    ward_code character varying(255),
    simulated_actor_id integer,
    encounter_id integer,
    action_type integer,
    movement_nature character varying(255),
    nursing_care_ward character varying(255)
);


ALTER TABLE public.pam_movement OWNER TO gazelle;

--
-- Name: pam_movement_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_movement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_movement_id_seq OWNER TO gazelle;

--
-- Name: pam_number_generator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_number_generator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_number_generator_id_seq OWNER TO gazelle;

--
-- Name: pam_patient_history; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_patient_history (
    id integer NOT NULL,
    action character varying(255) NOT NULL,
    application character varying(255),
    facility character varying(255),
    message_type character varying(255),
    "timestamp" timestamp without time zone,
    username character varying(255),
    current_patient_id integer,
    other_patient_id integer
);


ALTER TABLE public.pam_patient_history OWNER TO gazelle;

--
-- Name: pam_patient_history_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_patient_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_patient_history_id_seq OWNER TO gazelle;

--
-- Name: pam_patient_identifier; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_patient_identifier (
    id integer NOT NULL,
    full_patient_id character varying(255) NOT NULL,
    identifier_type_code character varying(255),
    id_number character varying(255),
    domain_id integer
);


ALTER TABLE public.pam_patient_identifier OWNER TO gazelle;

--
-- Name: pam_patient_identifier_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_patient_identifier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_patient_identifier_id_seq OWNER TO gazelle;

--
-- Name: pam_patient_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_patient_link (
    id integer NOT NULL,
    creation_date timestamp without time zone,
    patient_identifier_one_id integer,
    patient_identifier_two_id integer,
    simulated_actor_id integer
);


ALTER TABLE public.pam_patient_link OWNER TO gazelle;

--
-- Name: pam_patient_link_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_patient_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_patient_link_id_seq OWNER TO gazelle;

--
-- Name: pam_patient_patient_identifier; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_patient_patient_identifier (
    patient_id integer NOT NULL,
    patient_identifier_id integer NOT NULL
);


ALTER TABLE public.pam_patient_patient_identifier OWNER TO gazelle;

--
-- Name: pam_person; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_person (
    id integer NOT NULL,
    contact_role_code character varying(255),
    first_name character varying(255),
    identifier character varying(255),
    last_name character varying(255),
    relationship_code character varying(255),
    second_name character varying(255),
    patient_id integer
);


ALTER TABLE public.pam_person OWNER TO gazelle;

--
-- Name: pam_person_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_person_id_seq OWNER TO gazelle;

--
-- Name: pam_phone_number; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_phone_number (
    id integer NOT NULL,
    is_principal boolean,
    value character varying(255),
    patient_id integer,
    person_id integer,
    dtype character varying(31),
    type character varying(255)
);


ALTER TABLE public.pam_phone_number OWNER TO gazelle;

--
-- Name: pam_phone_number_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_phone_number_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_phone_number_id_seq OWNER TO gazelle;

--
-- Name: pam_simulator_feature; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_simulator_feature (
    id integer NOT NULL,
    enabled boolean,
    label character varying(255),
    keyword character varying(64)
);


ALTER TABLE public.pam_simulator_feature OWNER TO gazelle;

--
-- Name: pam_simulator_feature_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_simulator_feature_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_simulator_feature_id_seq OWNER TO gazelle;

--
-- Name: pam_sut_assigning_authorities; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_sut_assigning_authorities (
    sut_preference_id integer NOT NULL,
    hierarchic_designator_id integer NOT NULL
);


ALTER TABLE public.pam_sut_assigning_authorities OWNER TO gazelle;

--
-- Name: pam_systemundertest_preferences; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_systemundertest_preferences (
    id integer NOT NULL,
    last_modifier character varying(255),
    last_update timestamp without time zone,
    preferred_message integer,
    system_under_test_id integer
);


ALTER TABLE public.pam_systemundertest_preferences OWNER TO gazelle;

--
-- Name: pam_systemundertest_preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_systemundertest_preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_systemundertest_preferences_id_seq OWNER TO gazelle;

--
-- Name: pam_test_data; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_test_data (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    deferred_option boolean,
    description character varying(255),
    fhir_return_type integer,
    last_modified timestamp without time zone,
    last_modifier character varying(255),
    limit_value integer,
    name character varying(255) NOT NULL,
    is_shared boolean,
    transaction_id integer,
    credentials_id integer,
    patient_id integer,
    patient_identifier_id integer
);


ALTER TABLE public.pam_test_data OWNER TO gazelle;

--
-- Name: pam_test_data_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_test_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_test_data_id_seq OWNER TO gazelle;

--
-- Name: pam_test_data_restricted_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_test_data_restricted_domain (
    restricted_domain_id integer NOT NULL,
    test_data_id integer NOT NULL
);


ALTER TABLE public.pam_test_data_restricted_domain OWNER TO gazelle;

--
-- Name: pam_test_step; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_test_step (
    id integer NOT NULL,
    ackcode character varying(255),
    errormessage character varying(255),
    event character varying(255),
    finalstatus integer,
    initialstatus integer,
    requestandresponse_id integer,
    execution_result_id integer
);


ALTER TABLE public.pam_test_step OWNER TO gazelle;

--
-- Name: pam_test_step_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_test_step_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_test_step_id_seq OWNER TO gazelle;

--
-- Name: pam_xcpd_deferred_transaction; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pam_xcpd_deferred_transaction (
    id integer NOT NULL,
    discarded_reason character varying(255),
    init_gw_endpoint character varying(255),
    message_id_extension character varying(255),
    message_id_root character varying(255),
    status integer,
    query_id integer,
    response_id integer,
    simulated_actor_id integer
);


ALTER TABLE public.pam_xcpd_deferred_transaction OWNER TO gazelle;

--
-- Name: pam_xcpd_deferred_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pam_xcpd_deferred_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pam_xcpd_deferred_transaction_id_seq OWNER TO gazelle;

--
-- Name: pat_patient; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pat_patient (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    alternate_first_name character varying(255),
    alternate_last_name character varying(255),
    alternate_mothers_maiden_name character varying(255),
    alternate_second_name character varying(255),
    alternate_third_name character varying(255),
    character_set character varying(255),
    country_code character varying(255),
    creation_date timestamp without time zone,
    date_of_birth timestamp without time zone,
    first_name character varying(255),
    gender_code character varying(255),
    last_name character varying(255),
    mother_maiden_name character varying(255),
    race_code character varying(255),
    religion_code character varying(255),
    second_name character varying(255),
    size integer,
    third_name character varying(255),
    weight integer,
    account_number character varying(255),
    birth_order integer,
    blood_group character varying(255),
    creator character varying(255),
    dmetaphone_first_name character varying(255),
    dmetaphone_last_name character varying(255),
    dmetaphone_mother_maiden_name character varying(255),
    email character varying(255),
    identity_reliability_code character varying(255),
    last_update_facility character varying(255),
    marital_status character varying(255),
    multiple_birth_indicator boolean,
    is_patient_dead boolean,
    patient_death_time timestamp without time zone,
    still_active boolean,
    is_test_data boolean,
    vip_indicator character varying(255),
    additional_demographics_id integer,
    cross_reference_id integer,
    actor_id integer,
    birth_place_name character varying(255),
    uuid character varying(255),
    simulated_actor_keyword character varying(255)
);


ALTER TABLE public.pat_patient OWNER TO gazelle;

--
-- Name: pat_patient_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pat_patient_address (
    id integer NOT NULL,
    address_line character varying(255),
    address_type integer,
    city character varying(255),
    country_code character varying(255),
    is_main_address boolean,
    state character varying(255),
    street character varying(255),
    street_number character varying(255),
    zip_code character varying(255),
    patient_id integer,
    person_id integer,
    dtype character varying(31),
    insee_code character varying(255)
);


ALTER TABLE public.pat_patient_address OWNER TO gazelle;

--
-- Name: pat_patient_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pat_patient_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pat_patient_address_id_seq OWNER TO gazelle;

--
-- Name: pat_patient_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pat_patient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pat_patient_id_seq OWNER TO gazelle;

--
-- Name: pix_cross_reference; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pix_cross_reference (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier character varying(255)
);


ALTER TABLE public.pix_cross_reference OWNER TO gazelle;

--
-- Name: pix_cross_reference_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pix_cross_reference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pix_cross_reference_id_seq OWNER TO gazelle;

--
-- Name: sys_conf_type_usages; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.sys_conf_type_usages (
    system_configuration_id integer NOT NULL,
    listusages_id integer NOT NULL
);


ALTER TABLE public.sys_conf_type_usages OWNER TO gazelle;

--
-- Name: sys_config_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.sys_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sys_config_id_seq OWNER TO gazelle;

--
-- Name: system_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.system_configuration (
    id integer NOT NULL,
    is_available boolean,
    is_public boolean,
    name character varying(255),
    owner character varying(255),
    system_name character varying(255),
    url character varying(2083),
    dtype character varying(31),
    device_oid character varying(255),
    organization_oid character varying(255),
    application character varying(255),
    facility character varying(255),
    hl7_protocol integer,
    ip_address character varying(255),
    message_encoding integer,
    port integer,
    private_ip boolean,
    charset_id integer,
    owner_company character varying(255)
);


ALTER TABLE public.system_configuration OWNER TO gazelle;

--
-- Name: system_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.system_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_configuration_id_seq OWNER TO gazelle;

--
-- Name: tf_actor; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128),
    can_act_as_responder boolean
);


ALTER TABLE public.tf_actor OWNER TO gazelle;

--
-- Name: tf_actor_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile (
    id integer NOT NULL,
    actor_id integer,
    integration_profile_id integer
);


ALTER TABLE public.tf_actor_integration_profile OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_option (
    id integer NOT NULL,
    actor_integration_profile_id integer NOT NULL,
    integration_profile_option_id integer
);


ALTER TABLE public.tf_actor_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_domain OWNER TO gazelle;

--
-- Name: tf_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_domain_integration_profiles; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_integration_profiles (
    domain_id integer NOT NULL,
    integration_profile_id integer NOT NULL
);


ALTER TABLE public.tf_domain_integration_profiles OWNER TO gazelle;

--
-- Name: tf_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile OWNER TO gazelle;

--
-- Name: tf_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_option (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_transaction OWNER TO gazelle;

--
-- Name: tf_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_id_seq OWNER TO gazelle;

--
-- Name: tm_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_oid (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    oid character varying(255) NOT NULL,
    system_id integer
);


ALTER TABLE public.tm_oid OWNER TO gazelle;

--
-- Name: tm_oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_oid_id_seq OWNER TO gazelle;

--
-- Name: tm_path; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_path (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    keyword character varying(255) NOT NULL,
    type character varying(255)
);


ALTER TABLE public.tm_path OWNER TO gazelle;

--
-- Name: tm_path_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_path_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_path_id_seq OWNER TO gazelle;

--
-- Name: usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usage_id_seq OWNER TO gazelle;

--
-- Name: usage_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usage_metadata (
    id integer NOT NULL,
    action character varying(255),
    affinity_id integer,
    transaction_id integer,
    keyword character varying(255)
);


ALTER TABLE public.usage_metadata OWNER TO gazelle;

--
-- Name: validation_parameters; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.validation_parameters (
    id integer NOT NULL,
    message_type character varying(255) NOT NULL,
    profile_oid character varying(255) NOT NULL,
    actor_id integer NOT NULL,
    transaction_id integer NOT NULL,
    java_package character varying(255),
    domain_id integer,
    validator_type character varying(64)
);


ALTER TABLE public.validation_parameters OWNER TO gazelle;

--
-- Name: xua_log; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xua_log (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    assertion bytea,
    reason text,
    "timestamp" timestamp without time zone,
    called_service character varying(255),
    caller_ip character varying(255),
    message_id character varying(255),
    status_code integer,
    is_accepted boolean,
    credentials character varying(255),
    endpoint character varying(255)
);


ALTER TABLE public.xua_log OWNER TO gazelle;

--
-- Name: xua_log_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xua_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_log_id_seq OWNER TO gazelle;

--
-- Name: xua_picketlink_credentials; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.xua_picketlink_credentials (
    id integer NOT NULL,
    description character varying(255),
    is_default boolean,
    label character varying(255) NOT NULL,
    assertion_profile character varying(255) NOT NULL
);


ALTER TABLE public.xua_picketlink_credentials OWNER TO gazelle;

--
-- Name: xua_picketlink_credentials_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.xua_picketlink_credentials_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_picketlink_credentials_id_seq OWNER TO gazelle;

--
-- Name: affinity_domain affinity_domain_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain
    ADD CONSTRAINT affinity_domain_keyword_key UNIQUE (keyword);


--
-- Name: affinity_domain affinity_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain
    ADD CONSTRAINT affinity_domain_pkey PRIMARY KEY (id);


--
-- Name: affinity_domain_transactions affinity_domain_transactions_affinity_domain_id_transaction_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT affinity_domain_transactions_affinity_domain_id_transaction_key UNIQUE (affinity_domain_id, transaction_id);


--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_configuration cfg_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_configuration
    ADD CONSTRAINT cfg_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scp_configuration cfg_dicom_scp_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT cfg_dicom_scp_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scu_configuration cfg_dicom_scu_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT cfg_dicom_scu_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_initiator_configuration cfg_hl7_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT cfg_hl7_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_responder_configuration cfg_hl7_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT cfg_hl7_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_initiator_configuration cfg_hl7_v3_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT cfg_hl7_v3_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_responder_configuration cfg_hl7_v3_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT cfg_hl7_v3_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_host cfg_host_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_host
    ADD CONSTRAINT cfg_host_pkey PRIMARY KEY (id);


--
-- Name: cfg_sop_class cfg_sop_class_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_sop_class
    ADD CONSTRAINT cfg_sop_class_pkey PRIMARY KEY (id);


--
-- Name: cfg_syslog_configuration cfg_syslog_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT cfg_syslog_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_web_service_configuration cfg_web_service_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT cfg_web_service_configuration_pkey PRIMARY KEY (id);


--
-- Name: cmn_company_details cmn_company_details_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_company_details
    ADD CONSTRAINT cmn_company_details_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_iso3_language_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_iso3_language_key UNIQUE (iso3_language);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: cmn_ip_address cmn_ip_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_ip_address
    ADD CONSTRAINT cmn_ip_address_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance_metadata cmn_message_instance_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance_metadata
    ADD CONSTRAINT cmn_message_instance_metadata_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance cmn_message_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance
    ADD CONSTRAINT cmn_message_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_receiver_console cmn_receiver_console_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT cmn_receiver_console_pkey PRIMARY KEY (id);


--
-- Name: cmn_transaction_instance cmn_transaction_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT cmn_transaction_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_validator_usage cmn_validator_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_validator_usage
    ADD CONSTRAINT cmn_validator_usage_pkey PRIMARY KEY (id);


--
-- Name: cmn_value_set cmn_value_set_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_value_set
    ADD CONSTRAINT cmn_value_set_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information_instance gs_contextual_information_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT gs_contextual_information_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information gs_contextual_information_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information
    ADD CONSTRAINT gs_contextual_information_pkey PRIMARY KEY (id);


--
-- Name: gs_message gs_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT gs_message_pkey PRIMARY KEY (id);


--
-- Name: gs_system gs_system_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_system
    ADD CONSTRAINT gs_system_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_message gs_test_instance_message_message_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_message
    ADD CONSTRAINT gs_test_instance_message_message_id_key UNIQUE (message_id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_server_aipo_id_server_test_in_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_server_aipo_id_server_test_in_key UNIQUE (server_aipo_id, server_test_instance_participants_id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_server_test_instance_particip_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_server_test_instance_particip_key UNIQUE (server_test_instance_participants_id);


--
-- Name: gs_test_instance gs_test_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT gs_test_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance gs_test_instance_server_test_instance_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT gs_test_instance_server_test_instance_id_key UNIQUE (server_test_instance_id);


--
-- Name: gs_test_instance_status gs_test_instance_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_status
    ADD CONSTRAINT gs_test_instance_status_pkey PRIMARY KEY (id);


--
-- Name: gs_test_steps_instance gs_test_steps_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT gs_test_steps_instance_pkey PRIMARY KEY (id);


--
-- Name: hl7_charset hl7_charset_display_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_charset
    ADD CONSTRAINT hl7_charset_display_name_key UNIQUE (display_name);


--
-- Name: hl7_charset hl7_charset_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_charset
    ADD CONSTRAINT hl7_charset_pkey PRIMARY KEY (id);


--
-- Name: hl7_simulator_responder_configuration hl7_simulator_responder_configuration_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT hl7_simulator_responder_configuration_name_key UNIQUE (name);


--
-- Name: hl7_simulator_responder_configuration hl7_simulator_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT hl7_simulator_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: package_name_for_profile_oid package_name_for_profile_oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.package_name_for_profile_oid
    ADD CONSTRAINT package_name_for_profile_oid_pkey PRIMARY KEY (id);


--
-- Name: pam_administrative_account pam_administrative_account_account_number_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_administrative_account
    ADD CONSTRAINT pam_administrative_account_account_number_key UNIQUE (account_number);


--
-- Name: pam_administrative_account pam_administrative_account_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_administrative_account
    ADD CONSTRAINT pam_administrative_account_pkey PRIMARY KEY (id);


--
-- Name: pam_encounter_management_event pam_encounter_management_event_event_name_page_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_encounter_management_event
    ADD CONSTRAINT pam_encounter_management_event_event_name_page_id_key UNIQUE (event_name, page_id);


--
-- Name: pam_encounter_management_event pam_encounter_management_event_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_encounter_management_event
    ADD CONSTRAINT pam_encounter_management_event_pkey PRIMARY KEY (id);


--
-- Name: pam_encounter pam_encounter_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_encounter
    ADD CONSTRAINT pam_encounter_pkey PRIMARY KEY (id);


--
-- Name: pam_fr_additional_demographics pam_fr_additional_demographics_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_fr_additional_demographics
    ADD CONSTRAINT pam_fr_additional_demographics_pkey PRIMARY KEY (id);


--
-- Name: pam_fr_drg_movement pam_fr_drg_movement_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_fr_drg_movement
    ADD CONSTRAINT pam_fr_drg_movement_pkey PRIMARY KEY (id);


--
-- Name: pam_fr_encounter_additional_info pam_fr_encounter_additional_info_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_fr_encounter_additional_info
    ADD CONSTRAINT pam_fr_encounter_additional_info_pkey PRIMARY KEY (id);


--
-- Name: pam_fr_legal_care_mode pam_fr_legal_care_mode_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_fr_legal_care_mode
    ADD CONSTRAINT pam_fr_legal_care_mode_pkey PRIMARY KEY (id);


--
-- Name: pam_fr_patients_phr_info pam_fr_patients_phr_info_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_fr_patients_phr_info
    ADD CONSTRAINT pam_fr_patients_phr_info_pkey PRIMARY KEY (id);


--
-- Name: pam_graph_description pam_graph_description_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_graph_description
    ADD CONSTRAINT pam_graph_description_pkey PRIMARY KEY (id);


--
-- Name: pam_graph_execution pam_graph_execution_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_graph_execution
    ADD CONSTRAINT pam_graph_execution_pkey PRIMARY KEY (id);


--
-- Name: pam_graph_execution_results pam_graph_execution_results_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_graph_execution_results
    ADD CONSTRAINT pam_graph_execution_results_pkey PRIMARY KEY (id);


--
-- Name: pam_hierarchic_designator pam_hierarchic_designator_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_hierarchic_designator
    ADD CONSTRAINT pam_hierarchic_designator_pkey PRIMARY KEY (id);


--
-- Name: pam_message_id_generator pam_message_id_generator_issuing_actor_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_message_id_generator
    ADD CONSTRAINT pam_message_id_generator_issuing_actor_id_key UNIQUE (issuing_actor_id);


--
-- Name: pam_message_id_generator pam_message_id_generator_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_message_id_generator
    ADD CONSTRAINT pam_message_id_generator_pkey PRIMARY KEY (id);


--
-- Name: pam_movement pam_movement_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_movement
    ADD CONSTRAINT pam_movement_pkey PRIMARY KEY (id);


--
-- Name: pam_patient_history pam_patient_history_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_history
    ADD CONSTRAINT pam_patient_history_pkey PRIMARY KEY (id);


--
-- Name: pam_patient_identifier pam_patient_identifier_full_patient_id_identifier_type_code_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_identifier
    ADD CONSTRAINT pam_patient_identifier_full_patient_id_identifier_type_code_key UNIQUE (full_patient_id, identifier_type_code);


--
-- Name: pam_patient_identifier pam_patient_identifier_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_identifier
    ADD CONSTRAINT pam_patient_identifier_pkey PRIMARY KEY (id);


--
-- Name: pam_patient_link pam_patient_link_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_link
    ADD CONSTRAINT pam_patient_link_pkey PRIMARY KEY (id);


--
-- Name: pam_patient_patient_identifier pam_patient_patient_identifie_patient_id_patient_identifier_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_patient_identifier
    ADD CONSTRAINT pam_patient_patient_identifie_patient_id_patient_identifier_key UNIQUE (patient_id, patient_identifier_id);


--
-- Name: pam_person pam_person_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_person
    ADD CONSTRAINT pam_person_pkey PRIMARY KEY (id);


--
-- Name: pam_phone_number pam_phone_number_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_phone_number
    ADD CONSTRAINT pam_phone_number_pkey PRIMARY KEY (id);


--
-- Name: pam_simulator_feature pam_simulator_feature_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_simulator_feature
    ADD CONSTRAINT pam_simulator_feature_pkey PRIMARY KEY (id);


--
-- Name: pam_systemundertest_preferences pam_systemundertest_preferences_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_systemundertest_preferences
    ADD CONSTRAINT pam_systemundertest_preferences_pkey PRIMARY KEY (id);


--
-- Name: pam_test_data pam_test_data_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_data
    ADD CONSTRAINT pam_test_data_pkey PRIMARY KEY (id);


--
-- Name: pam_test_step pam_test_step_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_step
    ADD CONSTRAINT pam_test_step_pkey PRIMARY KEY (id);


--
-- Name: pam_xcpd_deferred_transaction pam_xcpd_deferred_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_xcpd_deferred_transaction
    ADD CONSTRAINT pam_xcpd_deferred_transaction_pkey PRIMARY KEY (id);


--
-- Name: pat_patient_address pat_patient_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient_address
    ADD CONSTRAINT pat_patient_address_pkey PRIMARY KEY (id);


--
-- Name: pat_patient pat_patient_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient
    ADD CONSTRAINT pat_patient_pkey PRIMARY KEY (id);


--
-- Name: pix_cross_reference pix_cross_reference_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pix_cross_reference
    ADD CONSTRAINT pix_cross_reference_pkey PRIMARY KEY (id);


--
-- Name: system_configuration system_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.system_configuration
    ADD CONSTRAINT system_configuration_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile__actor_integration_profile_id__key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile__actor_integration_profile_id__key UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_actor_id_integration_profile_i_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_actor_id_integration_profile_i_key UNIQUE (actor_id, integration_profile_id);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_actor tf_actor_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT tf_actor_keyword_key UNIQUE (keyword);


--
-- Name: tf_actor tf_actor_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT tf_actor_pkey PRIMARY KEY (id);


--
-- Name: tf_domain_integration_profiles tf_domain_integration_profile_integration_profile_id_domain_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT tf_domain_integration_profile_integration_profile_id_domain_key UNIQUE (integration_profile_id, domain_id);


--
-- Name: tf_domain tf_domain_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT tf_domain_keyword_key UNIQUE (keyword);


--
-- Name: tf_domain tf_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT tf_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_keyword_key UNIQUE (keyword);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_keyword_key UNIQUE (keyword);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction tf_transaction_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT tf_transaction_keyword_key UNIQUE (keyword);


--
-- Name: tf_transaction tf_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT tf_transaction_pkey PRIMARY KEY (id);


--
-- Name: tm_oid tm_oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid
    ADD CONSTRAINT tm_oid_pkey PRIMARY KEY (id);


--
-- Name: tm_path tm_path_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path
    ADD CONSTRAINT tm_path_keyword_key UNIQUE (keyword);


--
-- Name: tm_path tm_path_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path
    ADD CONSTRAINT tm_path_pkey PRIMARY KEY (id);


--
-- Name: system_configuration uk_2iwxlu65fuwpbhmeg96ebawhw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.system_configuration
    ADD CONSTRAINT uk_2iwxlu65fuwpbhmeg96ebawhw UNIQUE (name);


--
-- Name: pam_patient_identifier uk_3xtcsyky3tf2htxn89q019mgw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_identifier
    ADD CONSTRAINT uk_3xtcsyky3tf2htxn89q019mgw UNIQUE (full_patient_id, identifier_type_code);


--
-- Name: tf_domain uk_436tct1jl8811q2xgd8bjth9q; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT uk_436tct1jl8811q2xgd8bjth9q UNIQUE (keyword);


--
-- Name: pam_graph_description uk_4mqk4emya8msu1laexa5tg1mm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_graph_description
    ADD CONSTRAINT uk_4mqk4emya8msu1laexa5tg1mm UNIQUE (name);


--
-- Name: pam_message_id_generator uk_53itymo393ll2qpcw55w7uup6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_message_id_generator
    ADD CONSTRAINT uk_53itymo393ll2qpcw55w7uup6 UNIQUE (issuing_actor_id);


--
-- Name: pam_test_data_restricted_domain uk_5m70p11xkspgf7se3lwrpfx27; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_data_restricted_domain
    ADD CONSTRAINT uk_5m70p11xkspgf7se3lwrpfx27 UNIQUE (restricted_domain_id, test_data_id);


--
-- Name: tf_transaction uk_6nt35dm69gbrrj0aegkkqalr2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT uk_6nt35dm69gbrrj0aegkkqalr2 UNIQUE (keyword);


--
-- Name: gs_test_instance_participants uk_7rrch4mgjwqsbug2p4fgu9nwm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT uk_7rrch4mgjwqsbug2p4fgu9nwm UNIQUE (server_aipo_id, server_test_instance_participants_id);


--
-- Name: tf_actor_integration_profile_option uk_8b4tb6bcp3eh7mtsucokgh7fx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT uk_8b4tb6bcp3eh7mtsucokgh7fx UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: pam_encounter_management_event uk_8eois775kgqpr6eyf6kn8hmd8; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_encounter_management_event
    ADD CONSTRAINT uk_8eois775kgqpr6eyf6kn8hmd8 UNIQUE (event_name, page_id);


--
-- Name: tf_actor_integration_profile uk_b6ejd87o8v27xinqw27nn1hss; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT uk_b6ejd87o8v27xinqw27nn1hss UNIQUE (actor_id, integration_profile_id);


--
-- Name: pam_sut_assigning_authorities uk_c8t2vy1y139qdktg3n2r1w2o; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_sut_assigning_authorities
    ADD CONSTRAINT uk_c8t2vy1y139qdktg3n2r1w2o UNIQUE (sut_preference_id, hierarchic_designator_id);


--
-- Name: hl7_charset uk_cr8csf9v305hy0glwmdrbagpr; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_charset
    ADD CONSTRAINT uk_cr8csf9v305hy0glwmdrbagpr UNIQUE (display_name);


--
-- Name: pam_patient_patient_identifier uk_elwqa9mo5gr8aaff9n5rb1td2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_patient_identifier
    ADD CONSTRAINT uk_elwqa9mo5gr8aaff9n5rb1td2 UNIQUE (patient_id, patient_identifier_id);


--
-- Name: tf_actor uk_fpb6vpa9bnw6cjsb1pucuuivw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT uk_fpb6vpa9bnw6cjsb1pucuuivw UNIQUE (keyword);


--
-- Name: pam_systemundertest_preferences uk_fucrqddhxhnlpyp9qa0cwmb2m; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_systemundertest_preferences
    ADD CONSTRAINT uk_fucrqddhxhnlpyp9qa0cwmb2m UNIQUE (system_under_test_id);


--
-- Name: tf_domain_integration_profiles uk_iw9qgddyj9xsshmek3gr6u588; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT uk_iw9qgddyj9xsshmek3gr6u588 UNIQUE (integration_profile_id, domain_id);


--
-- Name: pam_test_data uk_klqxd6rwessjv7st3gsylxrsm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_data
    ADD CONSTRAINT uk_klqxd6rwessjv7st3gsylxrsm UNIQUE (name);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: tf_integration_profile uk_ny4glgtyovt2w045nwct19arx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT uk_ny4glgtyovt2w045nwct19arx UNIQUE (keyword);


--
-- Name: hl7_simulator_responder_configuration uk_ov92e0w7cimojbr4oaco0x7dh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT uk_ov92e0w7cimojbr4oaco0x7dh UNIQUE (name);


--
-- Name: pam_graph_execution_results_pam_test_step uk_pvdr5kstmwc5u37hn30dic3cn; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_graph_execution_results_pam_test_step
    ADD CONSTRAINT uk_pvdr5kstmwc5u37hn30dic3cn UNIQUE (messageslist_id);


--
-- Name: pam_simulator_feature uk_qk2aavghb93m16hv1om256gdt; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_simulator_feature
    ADD CONSTRAINT uk_qk2aavghb93m16hv1om256gdt UNIQUE (keyword);


--
-- Name: affinity_domain_transactions uk_r4g9ud7n9b2gx7tia9engpipe; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT uk_r4g9ud7n9b2gx7tia9engpipe UNIQUE (affinity_domain_id, transaction_id);


--
-- Name: cmn_company_details uk_r6osh086b3nqbuxpswcp1hcc2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_company_details
    ADD CONSTRAINT uk_r6osh086b3nqbuxpswcp1hcc2 UNIQUE (company_keyword);


--
-- Name: xua_picketlink_credentials uk_sf1nu9amjbkmugpev20c3lwrm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xua_picketlink_credentials
    ADD CONSTRAINT uk_sf1nu9amjbkmugpev20c3lwrm UNIQUE (label);


--
-- Name: tf_integration_profile_option uk_thqc1vykug4qhn8jdij04d1bh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT uk_thqc1vykug4qhn8jdij04d1bh UNIQUE (keyword);


--
-- Name: usage_metadata usage_metadata_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT usage_metadata_keyword_key UNIQUE (keyword);


--
-- Name: usage_metadata usage_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT usage_metadata_pkey PRIMARY KEY (id);


--
-- Name: validation_parameters validation_parameters_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_parameters
    ADD CONSTRAINT validation_parameters_pkey PRIMARY KEY (id);


--
-- Name: xua_log xua_log_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xua_log
    ADD CONSTRAINT xua_log_pkey PRIMARY KEY (id);


--
-- Name: xua_picketlink_credentials xua_picketlink_credentials_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.xua_picketlink_credentials
    ADD CONSTRAINT xua_picketlink_credentials_pkey PRIMARY KEY (id);


--
-- Name: pam_administrative_account fk13e9e31f72ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_administrative_account
    ADD CONSTRAINT fk13e9e31f72ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: gs_test_instance_oid fk200bd51aadaef596; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_oid
    ADD CONSTRAINT fk200bd51aadaef596 FOREIGN KEY (test_instance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gs_test_instance_oid fk200bd51afb5a2c1c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_oid
    ADD CONSTRAINT fk200bd51afb5a2c1c FOREIGN KEY (oid_configuration_id) REFERENCES public.tm_oid(id);


--
-- Name: cfg_web_service_configuration fk23f4a6263927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT fk23f4a6263927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_web_service_configuration fk23f4a626511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT fk23f4a626511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: cfg_hl7_initiator_configuration fk2891f8ff3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk2891f8ff3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_initiator_configuration fk2891f8ff511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk2891f8ff511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: pam_patient_identifier fk2993aaa62a77cdf0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_identifier
    ADD CONSTRAINT fk2993aaa62a77cdf0 FOREIGN KEY (domain_id) REFERENCES public.pam_hierarchic_designator(id);


--
-- Name: tf_domain_integration_profiles fk2c03ea431b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT fk2c03ea431b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tf_domain_integration_profiles fk2c03ea43866df480; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT fk2c03ea43866df480 FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: pam_patient_link fk35df2fb73e2b8fd6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_link
    ADD CONSTRAINT fk35df2fb73e2b8fd6 FOREIGN KEY (patient_identifier_one_id) REFERENCES public.pam_patient_identifier(id);


--
-- Name: pam_patient_link fk35df2fb747372a30; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_link
    ADD CONSTRAINT fk35df2fb747372a30 FOREIGN KEY (patient_identifier_two_id) REFERENCES public.pam_patient_identifier(id);


--
-- Name: pam_patient_link fk35df2fb772ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_link
    ADD CONSTRAINT fk35df2fb772ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk484bb2fe3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk484bb2fe3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk484bb2fe511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk484bb2fe511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gs_contextual_information fk4a7365f13b128c1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information
    ADD CONSTRAINT fk4a7365f13b128c1 FOREIGN KEY (path) REFERENCES public.tm_path(id);


--
-- Name: gs_test_instance_participants fk5c650a50611bae11; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT fk5c650a50611bae11 FOREIGN KEY (system_id) REFERENCES public.gs_system(id);


--
-- Name: gs_test_instance_participants fk5c650a5083369963; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT fk5c650a5083369963 FOREIGN KEY (aipo_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: validation_parameters fk61e8f9701b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_parameters
    ADD CONSTRAINT fk61e8f9701b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: validation_parameters fk61e8f9709d1084ab; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_parameters
    ADD CONSTRAINT fk61e8f9709d1084ab FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: validation_parameters fk61e8f970bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_parameters
    ADD CONSTRAINT fk61e8f970bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde4f3bdb32; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde4f3bdb32 FOREIGN KEY (sop_class_id) REFERENCES public.cfg_sop_class(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gs_test_instance_message fk6df0ff57adaef596; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_message
    ADD CONSTRAINT fk6df0ff57adaef596 FOREIGN KEY (test_instance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gs_test_instance_message fk6df0ff57d7b65883; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_message
    ADD CONSTRAINT fk6df0ff57d7b65883 FOREIGN KEY (message_id) REFERENCES public.gs_message(id);


--
-- Name: tf_actor_integration_profile_option fk78115a4d698ea7bd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk78115a4d698ea7bd FOREIGN KEY (integration_profile_option_id) REFERENCES public.tf_integration_profile_option(id);


--
-- Name: tf_actor_integration_profile_option fk78115a4d72619921; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk78115a4d72619921 FOREIGN KEY (actor_integration_profile_id) REFERENCES public.tf_actor_integration_profile(id);


--
-- Name: usage_metadata fk7d18f40d7007d3ad; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT fk7d18f40d7007d3ad FOREIGN KEY (affinity_id) REFERENCES public.affinity_domain(id);


--
-- Name: usage_metadata fk7d18f40dbd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT fk7d18f40dbd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: cfg_configuration fk7d98485b9a8e05a9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_configuration
    ADD CONSTRAINT fk7d98485b9a8e05a9 FOREIGN KEY (host_id) REFERENCES public.cfg_host(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk94b8666b3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk94b8666b3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk94b8666b511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk94b8666b511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: pam_encounter fk_1j1ilo0ysc6qg1ia72hr9ypmq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_encounter
    ADD CONSTRAINT fk_1j1ilo0ysc6qg1ia72hr9ypmq FOREIGN KEY (encounter_additional_info_id) REFERENCES public.pam_fr_encounter_additional_info(id);


--
-- Name: pam_fr_legal_care_mode fk_1mrkxoyjlb2plabqbf92vks79; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_fr_legal_care_mode
    ADD CONSTRAINT fk_1mrkxoyjlb2plabqbf92vks79 FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


--
-- Name: pam_test_step fk_1povbuqcepbbixa10rhmxxsj4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_step
    ADD CONSTRAINT fk_1povbuqcepbbixa10rhmxxsj4 FOREIGN KEY (execution_result_id) REFERENCES public.pam_graph_execution_results(id);


--
-- Name: pam_patient_history fk_35i41lltclsrjgwfp15wfhrop; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_history
    ADD CONSTRAINT fk_35i41lltclsrjgwfp15wfhrop FOREIGN KEY (other_patient_id) REFERENCES public.pat_patient(id);


--
-- Name: pam_graph_execution_results_pam_test_step fk_3c7ou3r269lc9587w1hctf982; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_graph_execution_results_pam_test_step
    ADD CONSTRAINT fk_3c7ou3r269lc9587w1hctf982 FOREIGN KEY (pam_graph_execution_results_id) REFERENCES public.pam_graph_execution_results(id);


--
-- Name: pat_patient fk_4rdweqd3ocg9vviyvubmw0813; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient
    ADD CONSTRAINT fk_4rdweqd3ocg9vviyvubmw0813 FOREIGN KEY (cross_reference_id) REFERENCES public.pix_cross_reference(id);


--
-- Name: pam_sut_assigning_authorities fk_4t73qvea7htvflxrqr9oy4yff; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_sut_assigning_authorities
    ADD CONSTRAINT fk_4t73qvea7htvflxrqr9oy4yff FOREIGN KEY (hierarchic_designator_id) REFERENCES public.pam_hierarchic_designator(id);


--
-- Name: pam_test_data fk_5dx3kxatclk4j9960n6kojxl9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_data
    ADD CONSTRAINT fk_5dx3kxatclk4j9960n6kojxl9 FOREIGN KEY (patient_identifier_id) REFERENCES public.pam_patient_identifier(id);


--
-- Name: pam_phone_number fk_5l3sj7n9sdx5up3v6fhhubqmt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_phone_number
    ADD CONSTRAINT fk_5l3sj7n9sdx5up3v6fhhubqmt FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


--
-- Name: pam_test_data_restricted_domain fk_5mjlu3v7tbim5iysaeveee24a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_data_restricted_domain
    ADD CONSTRAINT fk_5mjlu3v7tbim5iysaeveee24a FOREIGN KEY (restricted_domain_id) REFERENCES public.pam_test_data(id);


--
-- Name: hl7_simulator_responder_configuration fk_5s19daeg7pjo57niothsvnglc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT fk_5s19daeg7pjo57niothsvnglc FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: pam_encounter fk_6bqta09hlsu5p1t0nuoftv41u; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_encounter
    ADD CONSTRAINT fk_6bqta09hlsu5p1t0nuoftv41u FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


--
-- Name: cmn_message_instance_metadata fk_6pxpnca029j7ewvr7dus8q3sf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance_metadata
    ADD CONSTRAINT fk_6pxpnca029j7ewvr7dus8q3sf FOREIGN KEY (message_instance_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: pam_encounter fk_6rs3o6r8u55okr7hli04lxynk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_encounter
    ADD CONSTRAINT fk_6rs3o6r8u55okr7hli04lxynk FOREIGN KEY (drg_movement_id) REFERENCES public.pam_fr_drg_movement(id);


--
-- Name: cmn_receiver_console fk_6s2mr59uvd93xmp2dpkepfdvp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT fk_6s2mr59uvd93xmp2dpkepfdvp FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: pam_test_data fk_7opo2d3f2bylysy0h55nmc6nq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_data
    ADD CONSTRAINT fk_7opo2d3f2bylysy0h55nmc6nq FOREIGN KEY (credentials_id) REFERENCES public.xua_picketlink_credentials(id);


--
-- Name: pat_patient fk_7p3rjy46wkm1rdyb5as8816d4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient
    ADD CONSTRAINT fk_7p3rjy46wkm1rdyb5as8816d4 FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cmn_ip_address fk_8b3pqht8rw1vyiu1o0dm1ewtt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_ip_address
    ADD CONSTRAINT fk_8b3pqht8rw1vyiu1o0dm1ewtt FOREIGN KEY (company_details_id) REFERENCES public.cmn_company_details(id);


--
-- Name: pam_test_data fk_8lh8p3v0nawu4pcr8wjnwsvin; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_data
    ADD CONSTRAINT fk_8lh8p3v0nawu4pcr8wjnwsvin FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


--
-- Name: pam_phone_number fk_9kxiix708hk28cn6shqjiaf0t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_phone_number
    ADD CONSTRAINT fk_9kxiix708hk28cn6shqjiaf0t FOREIGN KEY (person_id) REFERENCES public.pam_person(id);


--
-- Name: pam_graph_execution_results fk_9n4j8lrk9r0bwshc26qodsc3b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_graph_execution_results
    ADD CONSTRAINT fk_9n4j8lrk9r0bwshc26qodsc3b FOREIGN KEY (graphexecution_id) REFERENCES public.pam_graph_execution(id);


--
-- Name: pam_encounter fk_9odt4we3gh50gjfoc6ddkooso; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_encounter
    ADD CONSTRAINT fk_9odt4we3gh50gjfoc6ddkooso FOREIGN KEY (patients_phr_info_id) REFERENCES public.pam_fr_patients_phr_info(id);


--
-- Name: pat_patient_address fk_9unnhbo9qfbfu2c7hfexts89g; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient_address
    ADD CONSTRAINT fk_9unnhbo9qfbfu2c7hfexts89g FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


--
-- Name: pam_graph_execution fk_bee12d5bsmphvi61wkdcm0kho; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_graph_execution
    ADD CONSTRAINT fk_bee12d5bsmphvi61wkdcm0kho FOREIGN KEY (graph_id) REFERENCES public.pam_graph_description(id);


--
-- Name: pam_xcpd_deferred_transaction fk_bggbh7rk2fa5y4ha3mo8ryr22; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_xcpd_deferred_transaction
    ADD CONSTRAINT fk_bggbh7rk2fa5y4ha3mo8ryr22 FOREIGN KEY (response_id) REFERENCES public.cmn_transaction_instance(id);


--
-- Name: pam_sut_assigning_authorities fk_bms3hvo96444l1hh4vajd4cm2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_sut_assigning_authorities
    ADD CONSTRAINT fk_bms3hvo96444l1hh4vajd4cm2 FOREIGN KEY (sut_preference_id) REFERENCES public.pam_systemundertest_preferences(id);


--
-- Name: system_configuration fk_d64t2lopq2p50cq65m2g4p4bw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.system_configuration
    ADD CONSTRAINT fk_d64t2lopq2p50cq65m2g4p4bw FOREIGN KEY (charset_id) REFERENCES public.hl7_charset(id);


--
-- Name: cmn_receiver_console fk_dvjns4jx634sntdq34baohlny; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT fk_dvjns4jx634sntdq34baohlny FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: pam_test_data fk_fhbm50adx8996kiiv9k02ev7m; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_data
    ADD CONSTRAINT fk_fhbm50adx8996kiiv9k02ev7m FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: pam_systemundertest_preferences fk_fucrqddhxhnlpyp9qa0cwmb2m; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_systemundertest_preferences
    ADD CONSTRAINT fk_fucrqddhxhnlpyp9qa0cwmb2m FOREIGN KEY (system_under_test_id) REFERENCES public.system_configuration(id);


--
-- Name: pam_test_data_restricted_domain fk_gq65whvsnm5u82b8l202j8ll9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_data_restricted_domain
    ADD CONSTRAINT fk_gq65whvsnm5u82b8l202j8ll9 FOREIGN KEY (test_data_id) REFERENCES public.pam_hierarchic_designator(id);


--
-- Name: pam_person fk_k9mmeamn9uxi4p42gck1koc7g; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_person
    ADD CONSTRAINT fk_k9mmeamn9uxi4p42gck1koc7g FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


--
-- Name: pam_test_step fk_mv8ng6fduxdmmj8nti39n6np0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_test_step
    ADD CONSTRAINT fk_mv8ng6fduxdmmj8nti39n6np0 FOREIGN KEY (requestandresponse_id) REFERENCES public.cmn_transaction_instance(id);


--
-- Name: pat_patient fk_p39fmyu2q2nymx0fvn5fvhtd4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient
    ADD CONSTRAINT fk_p39fmyu2q2nymx0fvn5fvhtd4 FOREIGN KEY (additional_demographics_id) REFERENCES public.pam_fr_additional_demographics(id);


--
-- Name: pam_graph_execution_results_pam_test_step fk_pvdr5kstmwc5u37hn30dic3cn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_graph_execution_results_pam_test_step
    ADD CONSTRAINT fk_pvdr5kstmwc5u37hn30dic3cn FOREIGN KEY (messageslist_id) REFERENCES public.pam_test_step(id);


--
-- Name: pam_xcpd_deferred_transaction fk_r2esk7wtodcumwg4v1vk4sheu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_xcpd_deferred_transaction
    ADD CONSTRAINT fk_r2esk7wtodcumwg4v1vk4sheu FOREIGN KEY (query_id) REFERENCES public.cmn_transaction_instance(id);


--
-- Name: pam_xcpd_deferred_transaction fk_rf5lgc62scovq94yougcvtof4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_xcpd_deferred_transaction
    ADD CONSTRAINT fk_rf5lgc62scovq94yougcvtof4 FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: pam_graph_execution fk_sb1jos1cpg0834iokksuy5t7o; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_graph_execution
    ADD CONSTRAINT fk_sb1jos1cpg0834iokksuy5t7o FOREIGN KEY (sut_id) REFERENCES public.system_configuration(id);


--
-- Name: pam_patient_history fk_sof0llsdqly10kivtxbo7orls; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_history
    ADD CONSTRAINT fk_sof0llsdqly10kivtxbo7orls FOREIGN KEY (current_patient_id) REFERENCES public.pat_patient(id);


--
-- Name: pam_patient_patient_identifier fk_sumftoqfo0iufsxfyyh2w8ph5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_patient_identifier
    ADD CONSTRAINT fk_sumftoqfo0iufsxfyyh2w8ph5 FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


--
-- Name: cmn_receiver_console fk_tnagd28ej30pkampt9gjs28d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT fk_tnagd28ej30pkampt9gjs28d FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: pat_patient_address fk_tob3wkgm2cuvahvph4tayjkym; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient_address
    ADD CONSTRAINT fk_tob3wkgm2cuvahvph4tayjkym FOREIGN KEY (person_id) REFERENCES public.pam_person(id);


--
-- Name: pam_patient_patient_identifier fka05a23206633695d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_patient_patient_identifier
    ADD CONSTRAINT fka05a23206633695d FOREIGN KEY (patient_identifier_id) REFERENCES public.pam_patient_identifier(id);


--
-- Name: cfg_dicom_scp_configuration fka165b9993927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b9993927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_dicom_scp_configuration fka165b9994f3bdb32; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b9994f3bdb32 FOREIGN KEY (sop_class_id) REFERENCES public.cfg_sop_class(id);


--
-- Name: cfg_dicom_scp_configuration fka165b999511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b999511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: cfg_syslog_configuration fka36479093927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT fka36479093927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_syslog_configuration fka3647909511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT fka3647909511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gs_message fka9c507b465385ec7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fka9c507b465385ec7 FOREIGN KEY (test_instance_participants_receiver_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: gs_message fka9c507b4bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fka9c507b4bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_message fka9c507b4e0c3e041; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fka9c507b4e0c3e041 FOREIGN KEY (test_instance_participants_sender_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: pam_encounter fkab1c015024b794ac; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_encounter
    ADD CONSTRAINT fkab1c015024b794ac FOREIGN KEY (visit_designator_id) REFERENCES public.pam_hierarchic_designator(id);


--
-- Name: pam_encounter fkab1c015072ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_encounter
    ADD CONSTRAINT fkab1c015072ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: pam_encounter fkab1c015086706b23; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_encounter
    ADD CONSTRAINT fkab1c015086706b23 FOREIGN KEY (administrative_account_id) REFERENCES public.pam_administrative_account(id);


--
-- Name: gs_test_instance_test_instance_participants fkbc3b31cd63e3d0fb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_test_instance_participants
    ADD CONSTRAINT fkbc3b31cd63e3d0fb FOREIGN KEY (test_instance_participants_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: gs_test_instance_test_instance_participants fkbc3b31cdadaef596; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_test_instance_participants
    ADD CONSTRAINT fkbc3b31cdadaef596 FOREIGN KEY (test_instance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: hl7_simulator_responder_configuration fkc5ef82b26c33fa9e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT fkc5ef82b26c33fa9e FOREIGN KEY (charset_id) REFERENCES public.hl7_charset(id);


--
-- Name: hl7_simulator_responder_configuration fkc5ef82b272ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT fkc5ef82b272ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: hl7_simulator_responder_configuration fkc5ef82b2bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT fkc5ef82b2bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_test_instance fkc783578f43e9dc7b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT fkc783578f43e9dc7b FOREIGN KEY (test_instance_status_id) REFERENCES public.gs_test_instance_status(id);


--
-- Name: pam_movement fkcadc70f272ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_movement
    ADD CONSTRAINT fkcadc70f272ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: pam_movement fkcadc70f29707fbc0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_movement
    ADD CONSTRAINT fkcadc70f29707fbc0 FOREIGN KEY (encounter_id) REFERENCES public.pam_encounter(id);


--
-- Name: tm_oid fkcc1f0704611bae11; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid
    ADD CONSTRAINT fkcc1f0704611bae11 FOREIGN KEY (system_id) REFERENCES public.gs_system(id);


--
-- Name: tf_actor_integration_profile fkd5a41967866df480; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fkd5a41967866df480 FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tf_actor_integration_profile fkd5a419679d1084ab; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fkd5a419679d1084ab FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cmn_transaction_instance fkd74918f11b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f11b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: cmn_transaction_instance fkd74918f11bc07e58; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f11bc07e58 FOREIGN KEY (response_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: cmn_transaction_instance fkd74918f172ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f172ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cmn_transaction_instance fkd74918f1afd7554a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f1afd7554a FOREIGN KEY (request_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: cmn_transaction_instance fkd74918f1bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f1bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: sys_conf_type_usages fkd97ede2e5e9a5b69; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_conf_type_usages
    ADD CONSTRAINT fkd97ede2e5e9a5b69 FOREIGN KEY (system_configuration_id) REFERENCES public.system_configuration(id);


--
-- Name: sys_conf_type_usages fkd97ede2edd54bc19; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_conf_type_usages
    ADD CONSTRAINT fkd97ede2edd54bc19 FOREIGN KEY (listusages_id) REFERENCES public.usage_metadata(id);


--
-- Name: cfg_hl7_responder_configuration fkdc2545923927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT fkdc2545923927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_responder_configuration fkdc254592511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT fkdc254592511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gs_contextual_information_instance fke02d7f235a577540; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT fke02d7f235a577540 FOREIGN KEY (contextual_information_id) REFERENCES public.gs_contextual_information(id);


--
-- Name: gs_contextual_information_instance fke02d7f2383812bd3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT fke02d7f2383812bd3 FOREIGN KEY (test_steps_instance_id) REFERENCES public.gs_test_steps_instance(id);


--
-- Name: affinity_domain_transactions fke0a98f5972a3043a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT fke0a98f5972a3043a FOREIGN KEY (affinity_domain_id) REFERENCES public.affinity_domain(id);


--
-- Name: affinity_domain_transactions fke0a98f59bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT fke0a98f59bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_test_steps_instance fke1c7ae27146029f1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27146029f1 FOREIGN KEY (testinstance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gs_test_steps_instance fke1c7ae2724c9d386; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2724c9d386 FOREIGN KEY (hl7v3_responder_config_id) REFERENCES public.cfg_hl7_v3_responder_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae273202260; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae273202260 FOREIGN KEY (syslog_config_id) REFERENCES public.cfg_syslog_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae2739f74269; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2739f74269 FOREIGN KEY (web_service_config_id) REFERENCES public.cfg_web_service_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27462961a4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27462961a4 FOREIGN KEY (hl7v2_initiator_config_id) REFERENCES public.cfg_hl7_initiator_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae2753d7b3a7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2753d7b3a7 FOREIGN KEY (dicom_scp_config_id) REFERENCES public.cfg_dicom_scp_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae279cf3f066; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae279cf3f066 FOREIGN KEY (hl7v3_initiator_config_id) REFERENCES public.cfg_hl7_v3_initiator_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27a5a42187; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27a5a42187 FOREIGN KEY (dicom_scu_config_id) REFERENCES public.cfg_dicom_scu_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27cdff44c4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27cdff44c4 FOREIGN KEY (hl7v2_responder_config_id) REFERENCES public.cfg_hl7_responder_configuration(id);


--
-- Name: pam_message_id_generator fke7604daa690c2374; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pam_message_id_generator
    ADD CONSTRAINT fke7604daa690c2374 FOREIGN KEY (issuing_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cmn_message_instance fkfeccef683360a4d2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance
    ADD CONSTRAINT fkfeccef683360a4d2 FOREIGN KEY (issuing_actor) REFERENCES public.tf_actor(id);


--
-- PostgreSQL database dump complete
--
