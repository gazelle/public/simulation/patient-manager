update app_configuration set variable = 'application_universal_id_type' where variable = 'application_namespace_id_type';

INSERT INTO tf_transaction (id, keyword, name, description) values (10, 'ITI-47', 'Patient Demographics Query HL7v3', 'Patient Demographics Query HL7v3');
INSERT INTO tf_transaction (id, keyword, name, description) values (11, 'ITI-55', 'Cross Gateway Patient Discovery', 'Cross Gateway Patient Discovery');
INSERT INTO tf_transaction (id, keyword, name, description) values (12, 'ITI-56', 'Patient Location Query', 'Patient Location Query');
SELECT pg_catalog.setval('tf_transaction_id_seq', 12, true);

UPDATE affinity_domain SET profile = 'All' where keyword = 'IHE';
INSERT INTO affinity_domain (id, label_to_display, profile, keyword) VALUES (3, 'KSA', 'KPDQ', 'KSA');
SELECT pg_catalog.setval('affinity_domain_id_seq', 3, true);

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (1, 10, 1, 'IHE - PDQv3 Supplier', 'IHE_PDQ_PDS');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (2, 10, 3, 'KSA - PDQv3 Supplier', 'KSA_PDQ_PDS');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (3, 11, 1, 'XCPD - Responding Gateway', 'IHE_XCPD_RESP');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (4, 12, 1, 'XCPD - Responding Gateway - Health Data Locator option', 'IHE_XCPD_RESP_PLQ');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (5, 11, 1, 'XCPD - Initiating Gateway - Deferred response option', 'IHE_XCPD_INIT_DEFERRED');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword) VALUES (6, 11, 1, 'XCPD - Responding Gateway - Deferred response option', 'IHE_XCPD_RESP_DEFERRED');
SELECT pg_catalog.setval('usage_id_seq', 6, true);

INSERT INTO pam_message_id_generator (id, index, issuing_actor_id, oid) VALUES (1, 0, 1, '1.2.3.1.2.3');
INSERT INTO pam_message_id_generator (id, index, issuing_actor_id, oid) VALUES (2, 0, 2, '1.2.3.1.2.4');
SELECT pg_catalog.setval('pam_message_id_generator_id_seq', 2, true);

INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'message_permanent_link', 'http://localhost:8180/PatientManager/hl7v3/messages/messageDisplay.seam?id=');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'NUMBER_OF_ITEMS_PER_PAGE', '20');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v3_validator_url', 'http://sake.irisa.fr:8080/GazelleHL7v2Validator-GazelleHL7v2Validator-ejb/GazelleHL7v3ValidationWS?wsdl');


INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (85, 'PRPA_IN201305UV02', 'PDQv3 - Patient Demographics Query', 1, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (86, 'QUQI_IN000003UV01', 'PDQv3 - Patient Demographics Query HL7V3 Continuation', 1, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (87, 'QUQI_IN000003UV01_Cancel', 'PDQv3 - Patient Demographics Query HL7V3 Cancellation', 1, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (88, 'PRPA_IN201306UV02', 'PDQv3 - Patient Demographics Query Response', 2, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (89, 'MCCI_IN000002UV01', 'PDQv3 - Accept Acknowledgement', 2, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (90, 'PRPA_IN201305UV02', 'KPDQ - Patient Demographics Query', 1, 10);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (91, 'PRPA_IN201306UV02', 'KPDQ - Patient Demographics Query Response', 2, 10);
SELECT pg_catalog.setval('ort_validation_parameters_id_seq', 91, true);

INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,85);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,86);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,87);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,88);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,89);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (3,90);
INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (3,91);

insert into tf_domain (id, description, keyword, name) values (1, 'ITI', 'ITI', 'IT-Infrasctructure');
insert into tf_domain (id, description, keyword, name) values (2, 'KSA', 'KSA', 'KSA MoH');
SELECT pg_catalog.setval('tf_domain_id_seq', 2, true);

INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'default_pdq_domain', 'ITI');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v3_organization_oid', '1.2.3');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v3_pdq_pdc_device_id', '1.2.3');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v3_pdq_pds_device_id', '1.2.3');